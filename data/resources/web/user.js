class ___AppDreyEnvelopeMailPageObserver {
    lastHeight = 0;
    isLoaded = false;
    heightObserver = new ResizeObserver((entries) => {
        this.heightChanged();
    });

    constructor() {
        window.document.addEventListener("DOMContentLoaded", (e) => {
            // Observe all elements that might be relevant for the height calculation
            this.isLoaded = true;
            this.heightChanged();

            this.heightObserver.observe(window.document.documentElement);
            this.heightObserver.observe(window.document.body);
            for (const node of window.document.body.children) {
                this.heightObserver.observe(node);
            }

            for (const image of window.document.images) {
                // Images often change size, observe them
                image.addEventListener("load", (e) => {
                    // Recheck the page after the image is loaded
                    this.heightChanged();
                });
            }
        });

        window.addEventListener("resize", (e) => {
            // The document height has changed
            this.heightChanged();
        })

        window.addEventListener("load", (e) => {
            // Recheck the page after the entire page is loaded
            this.isLoaded = true;
            this.heightChanged();
        });

        document.addEventListener("securitypolicyviolation", (e) => {
            this.securityPolicyViolation(e.blockedURI);
        });
    }

    parseStyleHeight(style, property) {
        return parseInt(style.getPropertyValue(property))
    }

    findBiggestMarginBottom(node) {
        var margin = this.parseStyleHeight(window.getComputedStyle(node), 'margin-bottom');
        for (const child of node.children) {
            let childMargin = this.parseStyleHeight(window.getComputedStyle(child), 'margin-bottom');
            margin = Math.max(margin, childMargin);
        }

        return margin;
    }

    /**
     * Find the bottom most pixel of the bottom most node in the list
     *
     * @param {Iterable} nodesList
     * @returns bottom most pixel position and its calculated margin
     */
    findBottomMostNode(nodesList) {
        var bottom = 0.0;
        var margin = 0.0;

        for (var node of nodesList) {
            // This is the position of the node relative to the viewport
            const rect = node.getBoundingClientRect();

            var node_margin = this.parseStyleHeight(window.getComputedStyle(node), 'margin-bottom');

            // We need to recurse the entire DOM here as margin + bottom might be bigger on a child node
            const child_node = this.findBottomMostNode(node.children);
            const node_bottom = window.scrollY + rect.bottom;
            node_margin = Math.max(node_margin, child_node.margin_bottom);

            if (node_bottom + node_margin > bottom + margin) {
                bottom = Math.ceil(node_bottom);
                margin = node_margin;
            }
        }

        return { "bottom": bottom, "margin_bottom": margin };
    }

    /**
     * Returns the remaining height of the HTMLElement from position of the bottom-most child element
     *
     * @param {Element} element
     * @returns bottom part of the style height
     */
    elementStyleHeightFromBottom(element) {
        const style = window.getComputedStyle(element);
        return this.parseStyleHeight(style, 'margin-bottom')
            + this.parseStyleHeight(style, 'border-bottom')
            + this.parseStyleHeight(style, 'padding-bottom');
    }

    /**
     * Calculate the minimum height of the document.
     *
     * We try to be very clever here, because webkit doesn't give us this
     * value directly in some way or form. To allow for the document to ever
     * shrink after growing, we can't just take the scrollHeight of the
     * documentElement or of body as it will always grow with the window size.
     *
     * Those values will always be at least as big as the window itself -
     * usually web browsers don't have the need to resize the entire viewport
     * to tightly fit the content of the document.
     *
     * What we need to do is iterate over all children of body. Those
     * will not grow beyond what they need, unless they have a percentage
     * height associated to them (at which point we're at a loss here, but
     * this is rather rare with Email).
     *
     * From this list we will recursively find the child that ends at the
     * bottom-most position.
     *
     * Then we just add the bottom part of the margin/padding of
     * documentElement and body to get the final minimum height of the
     * document.
     *
     * This algorithm should be relatively stable and not lead to situations
     * where the change of height will trigger a new (bigger) height
     * calculation which in turn will cause the height to run away.
     *
     */
    get height() {
        const node = this.findBottomMostNode(window.document.body.children);
        const calculatedHeight = node.bottom + node.margin_bottom +
            + this.elementStyleHeightFromBottom(window.document.body)
            + this.elementStyleHeightFromBottom(window.document.documentElement);
        const scrollHeight = window.document.documentElement.scrollHeight;

        // If scrollHeight is smaller then we did our calculations too conservatively.
        // In that case we can just take scrollHeight.
        return Math.min(scrollHeight, calculatedHeight);
    }

    heightChanged() {
        const height = this.height;
        if (this.isLoaded && height > 0 && height != this.lastHeight) {
            this.lastHeight = height;
            window.webkit.messageHandlers.height_changed.postMessage({ height: height });
        }
    }

    securityPolicyViolation(blockedURI) {
        window.webkit.messageHandlers.security_policy_violation.postMessage({ blocked_uri: blockedURI });
    }
}

const ___app_drey_envelope_mail_page_observer = new ___AppDreyEnvelopeMailPageObserver();
