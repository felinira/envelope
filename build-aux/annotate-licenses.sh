#!/usr/bin/env sh

reuse annotate --exclude-year --copyright "The Envelope Developers" --license "MPL-2.0" envelib --recursive
reuse annotate --exclude-year --copyright "The Envelope Developers" --license "GPL-3.0-or-later" src --recursive
reuse lint
