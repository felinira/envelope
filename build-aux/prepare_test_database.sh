#!/usr/bin/bash

if [[ "$1" == "" ]]
then
    ROOT="$(dirname ${BASH_SOURCE[0]})/../"
    DATABASE="${ROOT}/test.sqlite"
else
    ROOT="$1"
    shift
    DATABASE="$1"
    shift
    if [[ $1 == "--" ]]
    then
        shift
        CARGO_OPTIONS="$@"
    else
        CARGO_OPTIONS=""
    fi
fi

echo "Using source root $ROOT and database file $DATABASE" >&2
echo "Cargo options: $CARGO_OPTIONS"

cd ${ROOT}
DATABASE_URL="sqlite://${DATABASE}"

OUTPUT=$(cargo sqlx database create --database-url ${DATABASE_URL} 2>&1)
ret=$?
if test $ret == 101; then
  echo "⛔ cargo sqlx not installed. Install via `cargo install sqlx-cli`"
  rm -f ${ROOT}/.env || true
  exit 1
elif test $ret != 0; then
  echo "⛔ Create test database ${DATABASE}"
  echo $OUTPUT
else
  echo "✅ Create test database ${DATABASE}"

  OUTPUT=$(cargo sqlx migrate run --database-url ${DATABASE_URL} --source envelib/migrations 2>&1)
  if test $? != 0; then
    echo "⛔ Apply database migrations"
    echo $OUTPUT
    echo "Recreating ${DATABASE}"
    rm ${DATABASE}
    cargo sqlx database create --database-url ${DATABASE_URL} 2>&1
  else
    echo "✅ Apply database migrations"
  fi

  echo "DATABASE_URL=${DATABASE_URL}" > ${ROOT}/.env

  cargo sqlx prepare --workspace --database-url ${DATABASE_URL} -- ${CARGO_OPTIONS}
  if test $? != 0; then
    echo "⛔ Prepare queries"
    exit 1
  fi
  echo "✅ Prepare queries"
fi
