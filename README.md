# Envelope

Envelope is a mobile and desktop email client for the GNOME ecosystem.

🚧 Envelope is pre-alpha software. This has the following implications:

- It doesn't work
- It might delete all your messages
- It might crash your computer
- It is not suitable for end users, not even for “testing” purposes
- Do not try to use it regardless
- In no circumstance *ever* try to package it
- Only develop with test accounts

## Developing

See [CONTRIBUTING.md](CONTRIBUTING.md) for details on contributing and testing.

## LICENSE

Envelope is available under the GPL-3.0-or-later license. The mail backend is available under the MPL-2.0. See the LICENSES folder for the complete license text, and the [REUSE.toml](./REUSE.toml) file for details.
