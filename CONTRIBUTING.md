# Contributing to Envelope

Whenever you get stuck or have any questions, feel free to join us in [#email:gnome.org](https://matrix.to/#/#email:gnome.org) on matrix.

Before contributing any new major features or large changes, make sure to discuss the scope, design and future maintenance with us.

## Compiling and Running

### Dependencies

Make sure you have flatpak [GNOME nightlies](https://nightly.gnome.org/) set up.

First, you need to install the SDK:

```shell
flatpak install --user org.gnome.Sdk//master
flatpak install --user org.freedesktop.Sdk.Extension.rust-nightly//24.08
```

To automatically use the development [test server](#test-server), a docker or podman daemon must be running and its socket must be exposed at `/run/docker.sock`.

### GNOME Builder

We recommend using GNOME Builder to get started:

- Install GNOME Builder from Flathub
- Open Builder and select "Clone Repository..."
- Clone https://gitlab.gnome.org/felinira/envelope.git (or your fork)
- Press the Run ▶ button

Make sure that you're building the development target `app.drey.Envelope.Devel`.

### Command line

If you're not using GNOME Builder, the setup is more involved.

The commands below should give you a shell in a flatpak environment all the build dependencies set up. Building without
flatpak is possible, although a bit more difficult.

```shell
flatpak-builder --user workdir build-aux/app.drey.Envelope.Devel.json
flatpak-builder --run workdir build-aux/app.drey.Envelope.Devel.json bash
```

To build the application then:

```shell
mkdir build && cd build
meson setup --prefix=/app -D profile=development ..
ninja
```

To install inside the development environment, run:

```shell
ninja install
```

Then you will simply be able to run the application from the `PATH` by simply running:

```shell
envelope
```

## Test Server

Envelope comes with a test server (greenmail) which is used automatically in development builds.

### Prerequisites

To use the test server for the flatpak build, a [docker](https://docs.docker.com/engine/install/) or [podman](https://podman.io/docs/installation#installing-on-linux) daemon must be running and its socket must be exposed at `/run/docker.sock`.

The server will be automatically launched on startup, and terminated when the app is quit gracefully via the close button.

It will however keep running in case the app crashes or is terminated via SIGKILL (like pressing the stop button in Builder). In those cases, stop the container by restarting the app and subsequently quitting it, or [using the command line](#stop).

### Command line

#### Start

You can run the test server from the command line:

```shell
cargo run -p envelib --example test_server --features="devel"
```

#### Stop

The server should terminate gracefully when pressing Ctrl+C. If it doesn't, or to stop lingering processes left behind, it can be stopped manually.

```shell
cargo run -p envelib --example test_server --features="devel" -- --stop
```
