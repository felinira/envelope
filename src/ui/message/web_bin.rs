// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use const_format::concatc;
use envelib::gettext::*;
use glib::{SignalHandlerId, closure_local};

/// The shared parts of the content security policy.
const CONTENT_SECURITY_POLICY_SHARED: &str = concat!(
    // Disallow anything by default. Specifically, we don't want iframes, AJAX, javascript, media tags
    "default-src 'none';",
    // Inline styles are allowed, as this is the only way to style email messages
    "style-src 'unsafe-inline';",
);

/// Restrict the policy to not allow any external resource loads
const CONTENT_SECURITY_POLICY_RESTRICTIVE: &str = concatc!(
    CONTENT_SECURITY_POLICY_SHARED,
    // Allow images from cid URLs (which resolve local to the MIME message) and in data: URLs
    "img-src mid: cid: data:;",
    // Allow embedding video and audio content from cid and data URLs
    "media-src mid: cid: data: blob:;"
);

/// The permissive content security policy.
///
/// This allows fetching remote style, images and media, but still no javascript
const CONTENT_SECURITY_POLICY_PERMISSIVE: &str = concatc!(
    CONTENT_SECURITY_POLICY_SHARED,
    // Allow images from cid URLs (which resolve local to the MIME message) and in data: URLs, but also http and https
    "img-src mid: cid: data: http: https:;",
    // Allow embedding video and audio content from cid and data URLs, but also http and https
    "media-src mid: cid: data: blob: http: https:;"
);

const URI_SCHEME_INTERNAL: &str = "envelope";
const URI_INTERNAL_BODY: &str = concatc!(URI_SCHEME_INTERNAL, ":///body.html");

mod imp {
    use envelib::model::{self, MessageExt};
    use glib::subclass::Signal;
    use webkit::{prelude::*, soup::MessageHeaders};

    use crate::{
        error::{AppError, HandleError},
        ui::message::MessageAction,
    };

    use super::*;
    use std::{
        cell::{Cell, OnceCell, RefCell},
        sync::LazyLock,
    };

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::WebBin)]
    pub struct WebBin {
        #[property(get, set)]
        prop: Cell<bool>,

        webkit_user_content_manager: OnceCell<webkit::UserContentManager>,
        webkit_settings: OnceCell<webkit::Settings>,
        webkit_web_context: OnceCell<webkit::WebContext>,
        web_view: OnceCell<webkit::WebView>,
        pref_height: Cell<i32>,

        #[property(get, set = Self::set_allow_remote_content)]
        allow_remote_content: Cell<bool>,
        /// The view has blocked at least one URI request to a remote location
        #[property(get)]
        has_blocked_remote_content: Cell<bool>,
        /// This property holds the currently-displayed message.
        ///
        /// It is only set once the message is actually ready to be displayed.
        #[property(get, set = Self::set_message, nullable)]
        message: RefCell<Option<model::Message>>,
        #[property(get)]
        load_progress: Cell<f64>,
        #[property(get)]
        message_loaded: Cell<bool>,
        #[property(get)]
        context_menu_actions: gio::SimpleActionGroup,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for WebBin {
        const NAME: &'static str = "EnvelopeWebBin";
        type Type = super::WebBin;
        type ParentType = adw::Bin;
    }

    #[glib::derived_properties]
    impl ObjectImpl for WebBin {
        fn constructed(&self) {
            self.obj().add_css_class("message-web-view");
            self.parent_constructed();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("search-found")
                        .param_types([str::static_type(), u32::static_type()])
                        .build(),
                    Signal::builder("search-not-found")
                        .param_types([str::static_type()])
                        .build(),
                    Signal::builder("error")
                        .param_types([str::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }
    impl WidgetImpl for WebBin {}
    impl BinImpl for WebBin {}

    #[gtk::template_callbacks]
    impl WebBin {
        fn set_message(&self, message: Option<model::Message>) {
            if message != self.message.replace(message.clone()) {
                self.load_progress.set(0.);
                self.obj().notify_load_progress();

                //self.pref_height.set(-1);

                if message.is_some() {
                    let uri = URI_INTERNAL_BODY;
                    log::debug!("Loading URI {}", uri);
                    self.web_view().load_uri(uri);
                } else {
                    self.web_view().load_uri("about:blank");
                    self.message_loaded.set(false);
                    self.obj().notify_message_loaded();
                }

                self.obj().notify_message();
            }
        }

        fn set_allow_remote_content(&self, allow: bool) {
            let changed = allow != self.allow_remote_content.get();

            if allow {
                self.has_blocked_remote_content.set(false);
                self.obj().notify_has_blocked_remote_content();
            }

            if changed {
                self.allow_remote_content.set(allow);
                self.web_view().reload();
            }
        }

        fn display_context_menu(
            &self,
            _view: &webkit::WebView,
            context_menu: &webkit::ContextMenu,
            target: &webkit::HitTestResult,
        ) -> bool {
            context_menu.remove_all();

            // Top Message actions
            for action in MessageAction::SECTIONS[0] {
                if let Some(gaction) = self.context_menu_actions.lookup_action(action.as_ref()) {
                    context_menu.append(&webkit::ContextMenuItem::from_gaction(
                        &gaction,
                        &action.localized_name(),
                        None,
                    ));
                }
            }

            context_menu.append(&webkit::ContextMenuItem::new_separator());

            // Default actions
            context_menu.append(&webkit::ContextMenuItem::from_stock_action(
                webkit::ContextMenuAction::Reload,
            ));

            // Text actions
            if target.context_is_selection() {
                context_menu.append(&webkit::ContextMenuItem::from_stock_action(
                    webkit::ContextMenuAction::Copy,
                ));
            }

            context_menu.append(&webkit::ContextMenuItem::from_stock_action(
                webkit::ContextMenuAction::SelectAll,
            ));

            // Image actions
            if target.context_is_image() {
                context_menu.append(&webkit::ContextMenuItem::from_stock_action(
                    webkit::ContextMenuAction::CopyImageToClipboard,
                ));
            }

            // Separator
            context_menu.append(&webkit::ContextMenuItem::new_separator());

            // Other Message actions
            for action in MessageAction::SECTIONS[1] {
                if let Some(gaction) = self.context_menu_actions.lookup_action(action.as_ref()) {
                    context_menu.append(&webkit::ContextMenuItem::from_gaction(
                        &gaction,
                        &action.localized_name(),
                        None,
                    ));
                }
            }

            // Inspector
            if crate::globals::DEBUG_BUILD {
                context_menu.append(&webkit::ContextMenuItem::from_stock_action(
                    webkit::ContextMenuAction::InspectElement,
                ));
            }

            false
        }

        /// Return the currently valid content security policy
        fn content_security_policy(&self) -> &'static str {
            if self.allow_remote_content.get() {
                CONTENT_SECURITY_POLICY_PERMISSIVE
            } else {
                CONTENT_SECURITY_POLICY_RESTRICTIVE
            }
        }

        fn gresource_load_string(path: impl AsRef<str>) -> String {
            String::from_utf8_lossy(
                &gio::resources_lookup_data(
                    &format!("/app/drey/Envelope/{}", path.as_ref()),
                    gio::ResourceLookupFlags::NONE,
                )
                .unwrap(),
            )
            .into_owned()
        }

        fn user_script(path: &str) -> webkit::UserScript {
            webkit::UserScript::new(
                &Self::gresource_load_string(path),
                webkit::UserContentInjectedFrames::TopFrame,
                webkit::UserScriptInjectionTime::Start,
                &[],
                &[],
            )
        }

        pub fn update_height(&self) {
            if self.web_view().height() != self.pref_height.get() {
                // Set a minimum height.
                //
                // Also set a maximum height, to prevent height run-away in
                // case of yet-unknown bugs that will increase the height of
                // the view until the inevitable heat-death of the universe
                // consumes us all.
                self.web_view()
                    .set_height_request(self.pref_height.get().clamp(0, 1000000));
            }
        }

        fn webkit_user_content_manager(&self) -> &webkit::UserContentManager {
            self.webkit_user_content_manager.get_or_init(|| {
                let content_manager = webkit::UserContentManager::new();
                let script1 = Self::user_script("web/user.js");

                let user_style_sheet = webkit::UserStyleSheet::new(
                    &Self::gresource_load_string("web/user.css"),
                    webkit::UserContentInjectedFrames::TopFrame,
                    webkit::UserStyleLevel::User,
                    &[],
                    &[],
                );

                let default_style_sheet = webkit::UserStyleSheet::new(
                    &Self::gresource_load_string("web/default.css"),
                    webkit::UserContentInjectedFrames::AllFrames,
                    webkit::UserStyleLevel::User,
                    &[],
                    &[],
                );

                content_manager.add_script(&script1);
                content_manager.add_style_sheet(&user_style_sheet);
                content_manager.add_style_sheet(&default_style_sheet);

                content_manager.register_script_message_handler("height_changed", None);
                content_manager.connect_script_message_received(
                    Some("height_changed"),
                    glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |_manager, value| {
                            let height: i32 = if let Some(height) = value
                                .object_get_property("height")
                                .map(|height| height.to_double())
                            {
                                height as i32
                            } else {
                                240
                            };

                            if height != imp.pref_height.get() {
                                imp.pref_height.set(height);

                                if imp.message_loaded.get() {
                                    imp.update_height();
                                }
                            }
                        }
                    ),
                );

                content_manager.register_script_message_handler("security_policy_violation", None);
                content_manager.connect_script_message_received(
                    Some("security_policy_violation"),
                    glib::clone!(
                        #[weak(rename_to = obj)]
                        self.obj(),
                        move |_manager, value| {
                            let Some(blocked_uri) = value
                                .object_get_property("blocked_uri")
                                .map(|uri| uri.to_string())
                            else {
                                return;
                            };

                            log::trace!("Security Policy Violation: Blocked URI '{}'", blocked_uri);

                            if !obj.imp().allow_remote_content.get() {
                                obj.imp().has_blocked_remote_content.set(true);
                                obj.notify_has_blocked_remote_content();
                            }
                        }
                    ),
                );

                content_manager
            })
        }

        fn webkit_settings(&self) -> &webkit::Settings {
            self.webkit_settings.get_or_init(|| {
                let settings: webkit::Settings = webkit::Settings::new();
                settings.set_default_charset("UTF-8");
                settings.set_enable_developer_extras(crate::globals::DEBUG_BUILD);
                settings.set_enable_html5_database(false);
                settings.set_enable_html5_local_storage(false);
                settings.set_enable_javascript(true);
                settings.set_enable_javascript_markup(false);
                settings.set_enable_media(false);
                settings.set_enable_media_stream(false);
                settings.set_enable_offline_web_application_cache(false);
                settings.set_enable_page_cache(true);
                settings.set_enable_write_console_messages_to_stdout(crate::globals::DEBUG_BUILD);
                settings.set_enable_back_forward_navigation_gestures(false);
                settings.set_javascript_can_open_windows_automatically(false);

                settings
            })
        }

        fn webkit_web_context(&self) -> &webkit::WebContext {
            self.webkit_web_context.get_or_init(|| {
                let ctx = webkit::WebContext::new();
                ctx.set_cache_model(webkit::CacheModel::DocumentBrowser);

                let handler = glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |request: &webkit::URISchemeRequest| {
                        imp.handle_uri(request);
                    }
                );

                let security = ctx
                    .security_manager()
                    .expect("Webkit does not provide a security manager. This is fatal.");

                for scheme in ["mid", "cid", URI_SCHEME_INTERNAL] {
                    ctx.register_uri_scheme(scheme, handler.clone());
                    security.register_uri_scheme_as_local(scheme);
                    security.register_uri_scheme_as_secure(scheme);
                }

                ctx
            })
        }

        fn web_view(&self) -> &webkit::WebView {
            self.web_view.get_or_init(|| {
                let content_manager = self.webkit_user_content_manager();
                let settings = self.webkit_settings();
                let network_session = webkit::NetworkSession::new_ephemeral();
                let context = self.webkit_web_context();

                let view = glib::Object::builder::<webkit::WebView>()
                    .property("user-content-manager", content_manager)
                    .property("settings", settings)
                    .property("network-session", network_session)
                    .property("web-context", context)
                    .property("hexpand", true)
                    .build();

                view.connect_decide_policy(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    #[upgrade_or]
                    false,
                    move |view: &webkit::WebView,
                          policy_decision: &webkit::PolicyDecision,
                          decision_type: webkit::PolicyDecisionType| imp
                        .web_view_decide_policy(view, policy_decision, decision_type)
                ));

                view.connect_load_changed(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_view, event| {
                        log::trace!("Load event: {:?}", event);
                        imp.message_loaded.set(
                            imp.message.borrow().is_some()
                                && matches!(event, webkit::LoadEvent::Finished),
                        );
                        imp.obj().notify_message_loaded();

                        if imp.message_loaded.get() {
                            imp.update_height();
                        }
                    }
                ));

                view.connect_load_failed(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    #[upgrade_or]
                    false,
                    move |_view, _event, _uri, error| {
                        imp.emit_error(&format!("{}", error));
                        false
                    }
                ));

                view.connect_authenticate(|_view, request| {
                    request.cancel();
                    true
                });

                view.connect_run_color_chooser(|_view, request| {
                    request.cancel();
                    true
                });

                view.connect_run_file_chooser(|_view, request| {
                    // Uploads are disabled
                    request.cancel();
                    true
                });

                view.connect_context_menu(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    #[upgrade_or]
                    false,
                    move |view, context_menu, target| imp.display_context_menu(
                        view,
                        context_menu,
                        target
                    )
                ));

                view.connect_script_dialog(|_view, _dialog| true);

                view.connect_show_notification(|_view, _notification| true);

                let update_color = glib::clone!(
                    #[weak]
                    view,
                    move |manager: &adw::StyleManager| {
                        if manager.is_dark() {
                            // This is the effective card-bg-color with default window background.
                            // This also needs to be updated in default.css
                            view.set_background_color(&gtk::gdk::RGBA::parse("#333337").unwrap());
                        } else {
                            view.set_background_color(&gtk::gdk::RGBA::parse("#ffffff").unwrap());
                        }
                    }
                );

                let style_manager = adw::StyleManager::default();
                update_color(&style_manager);
                style_manager.connect_dark_notify(update_color);

                if let Some(find_controller) = view.find_controller() {
                    find_controller.connect_found_text(glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |find, count| imp.found_text(find, count)
                    ));
                    find_controller.connect_failed_to_find_text(glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |find| imp.text_not_found(find)
                    ));
                } else {
                    log::error!("WebView has no FindController. Search will not work");
                }

                self.obj().set_child(Some(&view));

                view
            })
        }

        fn web_view_decide_policy(
            &self,
            _view: &webkit::WebView,
            policy_decision: &webkit::PolicyDecision,
            decision_type: webkit::PolicyDecisionType,
        ) -> bool {
            match decision_type {
                webkit::PolicyDecisionType::NavigationAction => {
                    let Some(mut action) = policy_decision
                        .downcast_ref::<webkit::NavigationPolicyDecision>()
                        .and_then(|decision| decision.navigation_action())
                    else {
                        log::error!("Disallowing navigation policy decision without action");
                        policy_decision.ignore();
                        return true;
                    };

                    let Some(uri) = action.request().and_then(|r| r.uri()) else {
                        log::error!("Disallowing navigation policy decision without request");
                        policy_decision.ignore();
                        return true;
                    };

                    let uri = match glib::Uri::parse(&uri, glib::UriFlags::PARSE_RELAXED) {
                        Ok(uri) => uri,
                        Err(err) => {
                            AppError::from(err).handle();
                            policy_decision.ignore();
                            return true;
                        }
                    };

                    if action.is_user_gesture() {
                        // Any links will be opened in an external web browser
                        log::debug!("handle link {}", uri.to_str());
                        policy_decision.ignore();

                        // Make sure we only navigate to known URI schemes
                        match uri.scheme().as_ref() {
                            "http" | "https" | "ftp" => {
                                log::debug!("Navigating to URI {}", uri);
                                gtk::UriLauncher::builder()
                                    .uri(uri.to_str())
                                    .build()
                                    .launch(gtk::Window::NONE, gio::Cancellable::NONE, |_| {});
                            }
                            "mailto" => {
                                // TODO
                                AppError::from(anyhow::Error::msg(format!(
                                    "mailto links are not implemented yet: {}",
                                    uri.to_str()
                                )))
                                .handle();
                            }
                            _ => {
                                let dialog = adw::AlertDialog::builder()
                                    .title(gettext("Open Link?"))
                                    .body(gettextf("This link uses an unrecognized protocol that could cause harm on your computer. Only open links you trust.\n\n{}", &[&uri.to_str()]))
                                    .build();
                                dialog.add_responses(&[
                                    ("cancel", &gettext("Cancel")),
                                    ("open", &gettext("Open Link")),
                                ]);
                                dialog.connect_response(None, move |_, response| {
                                    if response == "open" {
                                        gtk::UriLauncher::builder()
                                            .uri(uri.to_str())
                                            .build()
                                            .launch(
                                                gtk::Window::NONE,
                                                gio::Cancellable::NONE,
                                                |_| {},
                                            );
                                    }
                                });
                                dialog.present(Some(&*self.obj()));
                            }
                        }

                        true
                    } else {
                        // Only navigate to submessages
                        if uri.scheme() != URI_SCHEME_INTERNAL {
                            policy_decision.ignore();
                            true
                        } else {
                            false
                        }
                    }
                }
                webkit::PolicyDecisionType::NewWindowAction => {
                    // Disallow new windows
                    policy_decision.ignore();
                    true
                }
                webkit::PolicyDecisionType::Response => {
                    // Resource fetches etc.
                    // Only allow resource fetches from mid and cid URLs
                    let Some(response_decision) =
                        policy_decision.downcast_ref::<webkit::ResponsePolicyDecision>()
                    else {
                        policy_decision.ignore();
                        return true;
                    };

                    let Some(response) = response_decision.response() else {
                        policy_decision.ignore();
                        return true;
                    };

                    let Some(uri) = response_decision.request().and_then(|r| r.uri()) else {
                        policy_decision.ignore();
                        return true;
                    };

                    // Disallow message/rfc822 responses
                    if response.mime_type().as_deref() == Some("message/rfc822") {
                        log::error!("Ignoring mid request to full message");
                        policy_decision.ignore();
                        return true;
                    }

                    let uri = match glib::Uri::parse(&uri, glib::UriFlags::PARSE_RELAXED) {
                        Ok(uri) => uri,
                        Err(err) => {
                            AppError::from(err).handle();
                            policy_decision.ignore();
                            return true;
                        }
                    };

                    match uri.scheme().as_ref() {
                        "mid" | "cid" | URI_SCHEME_INTERNAL => policy_decision.use_(),
                        _ => {
                            log::debug!("Ignoring request to {}", uri.to_str());
                            policy_decision.ignore();
                        }
                    };

                    // Allow further handlers to disallow this request
                    false
                }
                ty => {
                    log::error!("Unknown policy decision type '{ty:?}'. Ignoring.");
                    policy_decision.ignore();
                    false
                }
            }
        }

        /// Handle `mid:` and `cid:` URIs [RFC 2392](https://www.rfc-editor.org/rfc/rfc2392)
        fn handle_uri(&self, request: &webkit::URISchemeRequest) {
            let finish = |request: &webkit::URISchemeRequest, data: &[u8], content_type: &str| {
                let response = webkit::URISchemeResponse::new(
                    &gio::MemoryInputStream::from_bytes(&glib::Bytes::from(data)),
                    data.len() as i64,
                );

                let headers = MessageHeaders::new(webkit::soup::MessageHeadersType::Response);

                response.set_content_type(content_type);
                headers.append("Content-Security-Policy", self.content_security_policy());
                headers.append("Cache-Control", "no-cache");
                headers.append("Server", &format!("Envelope/{}", env!("CARGO_PKG_VERSION")));
                headers.append("Referrer-Policy", "no-referrer");
                headers.append("Content-Type", content_type);

                response.set_http_headers(headers);
                response.set_status(200, None);
                request.finish_with_response(&response);
            };

            if let (Some(msg), Some(uri)) = (self.message.borrow().clone(), request.uri()) {
                log::trace!("Handle uri {}", uri);

                if uri == URI_INTERNAL_BODY {
                    if let Some(text) = msg.html_body_owned() {
                        let text = text.to_html_string();
                        let data = text.as_bytes();
                        finish(request, data, "text/html");
                        return;
                    }
                } else if let Some(part) = msg.message_part_by_uri(&uri) {
                    if let Some(data) = part.bytes() {
                        finish(request, data, &part.content_type());
                        return;
                    }
                }
            }

            // Fail
            let msg = format!("body part for URI {:?} not found", request.uri());
            log::error!("{}", msg);
            request.finish_error(&mut glib::Error::new(gio::IOErrorEnum::NotFound, &msg));
        }
    }

    /// Search
    impl WebBin {
        fn found_text(&self, find_controller: &webkit::FindController, count: u32) {
            let Some(search_text) = find_controller.search_text() else {
                self.search_abort();
                return;
            };

            self.emit_search_found(&search_text, count);
        }

        fn text_not_found(&self, find_controller: &webkit::FindController) {
            if find_controller.web_view().is_none() {
                self.search_abort();
                return;
            };

            let Some(search_text) = find_controller.search_text() else {
                self.search_abort();
                return;
            };

            self.emit_search_not_found(&search_text);
        }

        fn emit_search_not_found(&self, text: &str) {
            self.obj().emit_by_name::<()>("search-not-found", &[&text]);
        }

        fn emit_search_found(&self, text: &str, count: u32) {
            self.obj()
                .emit_by_name::<()>("search-found", &[&text, &count]);
        }

        fn emit_error(&self, message: &str) {
            self.obj().emit_by_name::<()>("error", &[&message]);
        }

        pub(super) fn search_abort(&self) {
            log::debug!("Aborting search");

            self.search_finish();
            //self.emit_search_not_found("");
        }

        fn do_search(&self, search_text: &str, reverse: bool, wrap: bool) {
            if let Some(find_controller) = self.web_view().find_controller() {
                let mut options = webkit::FindOptions::CASE_INSENSITIVE;
                if reverse {
                    options |= webkit::FindOptions::BACKWARDS;
                }

                if wrap {
                    options |= webkit::FindOptions::WRAP_AROUND;
                }

                let search_text = search_text.to_string();
                find_controller.search(&search_text, options.bits(), u32::MAX);
            }
        }

        pub fn search(&self, search_text: &str, reverse: bool) {
            // The first call to search will set wrap around to make sure we search the entire view
            self.do_search(search_text, reverse, true);
        }

        fn search_next_previous(&self, reverse: bool) {
            let Some(find_controller) = self.web_view().find_controller() else {
                self.search_abort();
                return;
            };

            let Some(search_text) = find_controller.search_text() else {
                self.search_abort();
                return;
            };

            let options = webkit::FindOptions::from_bits(find_controller.options());
            if options.is_some_and(|o| o.contains(webkit::FindOptions::WRAP_AROUND)) {
                self.do_search(&search_text, reverse, false);
            } else if reverse {
                find_controller.search_previous();
            } else {
                find_controller.search_next();
            }
        }

        pub fn search_previous(&self) {
            self.search_next_previous(true);
        }

        pub fn search_next(&self) {
            self.search_next_previous(false);
        }

        pub fn search_finish(&self) {
            if let Some(find_controller) = self.web_view().find_controller() {
                find_controller.search_finish();
            }
        }
    }
}

glib::wrapper! {
    pub struct WebBin(ObjectSubclass<imp::WebBin>)
    @extends adw::Bin, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl WebBin {
    fn new() -> Self {
        glib::Object::new()
    }

    pub fn connect_error<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(&Self, &str) + 'static,
    {
        self.connect_closure(
            "error",
            false,
            closure_local!(|view: &Self, message| callback(view, message)),
        )
    }
}

/// Search
impl WebBin {
    pub fn search(&self, search_text: &str, reverse: bool) {
        self.imp().search(search_text, reverse);
    }

    pub fn search_previous(&self) {
        self.imp().search_previous();
    }

    pub fn search_next(&self) {
        self.imp().search_next();
    }

    pub fn search_finish(&self) {
        self.imp().search_finish();
    }

    pub fn connect_search_found<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(&Self, &str, u32) + 'static,
    {
        self.connect_closure(
            "search-found",
            false,
            closure_local!(|view: &Self, text, count| callback(view, text, count)),
        )
    }

    pub fn connect_search_not_found<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(&Self, &str) + 'static,
    {
        self.connect_closure(
            "search-not-found",
            false,
            closure_local!(|view: &Self, text| callback(view, text)),
        )
    }
}
