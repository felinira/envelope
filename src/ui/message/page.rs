// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use envelib::{gettext::*, model};

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        sync::LazyLock,
    };

    use envelib::types::MailboxKind;
    use glib::{SignalHandlerId, subclass::Signal};
    use model::{
        IndexStoreExt, MailboxExt, MailboxExtProps, MessageExt, MessageExtProps, ThreadStore,
    };

    use crate::{
        controller::Controller,
        error::{AppError, HandleError},
        ui::{list::sorter::DynamicSorter, message::view::MessageView},
    };

    use super::*;

    #[derive(Default, glib::Properties, gtk::CompositeTemplate)]
    #[template(file = "page.ui")]
    #[properties(wrapper_type = super::MessagePage)]
    pub struct MessagePage {
        #[template_child]
        pub message_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub message_page_empty: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub message_page_multiple: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub message_page_list: TemplateChild<gtk::ScrolledWindow>,

        #[template_child]
        reply_btn: TemplateChild<gtk::Button>,
        #[template_child]
        reply_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        reply_all_btn: TemplateChild<gtk::Button>,
        #[template_child]
        reply_all_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        forward_btn: TemplateChild<gtk::Button>,
        #[template_child]
        forward_btn2: TemplateChild<gtk::Button>,

        #[template_child]
        trash_btn: TemplateChild<gtk::Button>,
        #[template_child]
        trash_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        delete_btn: TemplateChild<gtk::Button>,
        #[template_child]
        delete_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        junk_btn: TemplateChild<gtk::Button>,
        #[template_child]
        junk_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        not_junk_btn: TemplateChild<gtk::Button>,
        #[template_child]
        not_junk_btn2: TemplateChild<gtk::Button>,
        #[template_child]
        search_btn: TemplateChild<gtk::Button>,

        #[template_child]
        message_viewport: TemplateChild<gtk::Viewport>,
        #[template_child]
        pub message_list: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub message_search_bar: TemplateChild<gtk::SearchBar>,
        #[template_child]
        pub message_search_entry: TemplateChild<gtk::SearchEntry>,
        #[template_child]
        pub message_subject: TemplateChild<gtk::Label>,

        #[property(get, set = Self::set_controller, nullable)]
        controller: RefCell<Option<Controller>>,
        #[property(get, set = Self::set_mailbox, nullable)]
        mailbox: RefCell<Option<model::Mailbox>>,
        #[property(get, construct_only)]
        threads: OnceCell<ThreadStore>,
        #[property(get, set = Self::set_compact)]
        compact: Cell<bool>,
        #[property(get = Self::message_store)]
        message_store: RefCell<Option<model::MessageStore>>,
        message_sort_list_model: OnceCell<gtk::SortListModel>,
        displayed_thread_signal_group: OnceCell<glib::SignalGroup>,

        thread_changed_signal_handler_id: RefCell<Option<SignalHandlerId>>,
        search_active_message_view: RefCell<Option<MessageView>>,
        message_list_scrolled_to: RefCell<Option<MessageView>>,
        search_term_found: Cell<bool>,
        search_reverse: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessagePage {
        const NAME: &'static str = "EnvelopeMessagePage";
        type Type = super::MessagePage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("message-page");
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessagePage {
        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(Vec::new);
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.parent_constructed();

            // Search entries
            self.message_search_bar
                .connect_entry(&*self.message_search_entry);

            // Message list
            self.message_list.bind_model(
                Some(self.sort_model()),
                glib::clone!(
                    #[weak_allow_none(rename_to = imp)]
                    self.ref_counted(),
                    move |item| {
                        if let (Some(imp), Some(controller), Some(message), Some(mailbox)) = (
                            imp.clone(),
                            imp.and_then(|imp| imp.controller.borrow().clone()),
                            item.downcast_ref::<model::Message>(),
                            item.downcast_ref::<model::Message>()
                                .and_then(|m| m.mailbox()),
                        ) {
                            let message_view = MessageView::new(controller, mailbox);
                            message_view.set_compact(imp.compact.get());
                            message_view.set_message(message.clone());

                            message_view.web_bin().connect_search_found(glib::clone!(
                                #[weak]
                                imp,
                                #[weak]
                                message_view,
                                move |_web_view, search_term, count| imp.message_search_found(
                                    &message_view,
                                    search_term,
                                    count
                                )
                            ));

                            message_view
                                .web_bin()
                                .connect_search_not_found(glib::clone!(
                                    #[weak]
                                    imp,
                                    #[weak]
                                    message_view,
                                    move |_web_view, search_term| imp
                                        .message_search_not_found(&message_view, search_term)
                                ));
                            message_view.upcast()
                        } else {
                            adw::Bin::new().upcast()
                        }
                    }
                ),
            );

            let thread_store = self.threads.get_or_init(ThreadStore::new);
            thread_store.connect_items_changed(glib::clone!(
                #[weak(rename_to = imp)]
                self.ref_counted(),
                move |thread_store, _, _, _| {
                    if thread_store.len() != 1 {
                        imp.thread_signal_group().set_target(None::<&model::Thread>);
                        imp.set_message_store(None);
                        imp.update_thread(None);
                    } else if let Some(thread) = thread_store.first() {
                        let message_store = thread.messages();
                        imp.set_message_store(Some(message_store.clone()));
                        imp.thread_signal_group().set_target(Some(&thread));
                        imp.update_thread(Some(&thread));

                        if !message_store.is_empty() {
                            imp.scroll_to_first_unread();
                        }
                    }

                    imp.controller
                        .borrow()
                        .as_ref()
                        .inspect(|c| c.set_open_thread(imp.thread()));
                }
            ));
        }
    }

    impl WidgetImpl for MessagePage {}
    impl NavigationPageImpl for MessagePage {}

    #[gtk::template_callbacks]
    impl MessagePage {
        fn set_controller(&self, controller: Option<Controller>) {
            if self.controller.replace(controller.clone()) != controller {
                // TODO
            }
        }

        fn set_mailbox(&self, mailbox: Option<model::Mailbox>) {
            if self.mailbox.replace(mailbox.clone()) != mailbox {
                self.clear_message_view();
            }
        }

        fn set_compact(&self, compact: bool) {
            self.compact.set(compact);
            if compact {
                self.obj().add_css_class("compact");
            } else {
                self.obj().remove_css_class("compact");
            }

            for i in 0.. {
                if let Some(view) = self
                    .message_list
                    .row_at_index(i)
                    .and_downcast_ref::<MessageView>()
                {
                    view.set_compact(compact);
                } else {
                    break;
                }
            }

            self.update_message_pane();
        }

        fn message_store(&self) -> Option<model::MessageStore> {
            self.message_store.borrow().clone()
        }

        fn set_message_store(&self, store: Option<model::MessageStore>) {
            if self.message_store.replace(store.clone()) != store {
                self.sort_model().set_model(store.as_ref());
                self.obj().notify_message_store();
            }
        }

        fn sort_model(&self) -> &gtk::SortListModel {
            self.message_sort_list_model.get_or_init(|| {
                gtk::SortListModel::new(
                    None::<gio::ListModel>,
                    Some(DynamicSorter::with_sorter(model::Message::cmp_date)),
                )
            })
        }

        fn messages(&self) -> Vec<model::Message> {
            self.message_store()
                .map(|s| s.values().collect())
                .unwrap_or_default()
        }

        fn thread(&self) -> Option<model::Thread> {
            if let Some(threads) = self.threads.get() {
                if threads.len() == 1 {
                    threads.first()
                } else {
                    None
                }
            } else {
                None
            }
        }

        #[template_callback]
        fn message_search_mode_enabled(&self) {
            let search_bar = &*self.message_search_bar;
            if !search_bar.is_search_mode() {
                self.message_search_finish();
            }
        }

        #[template_callback]
        fn message_search_changed(&self, entry: &gtk::SearchEntry) {
            let search_text = entry.text();
            if !search_text.is_empty() {
                self.message_search_new(&search_text, false);
            }
        }

        #[template_callback]
        fn message_search_previous(&self, _entry: &gtk::SearchEntry) {
            self.search_reverse.set(true);
            let view = self.search_active_message_view.borrow();
            if let Some(row) = view.and_downcast_ref::<MessageView>() {
                row.web_bin().search_previous();
            }
        }

        #[template_callback]
        fn message_search_next(&self, _entry: &gtk::SearchEntry) {
            self.search_reverse.set(false);
            let view = self.search_active_message_view.borrow();
            if let Some(row) = view.and_downcast_ref::<MessageView>() {
                row.web_bin().search_next();
            }
        }

        fn message_search_new(&self, search_text: &str, wrap_around: bool) {
            if !wrap_around {
                self.search_reverse.set(false);
                self.message_search_finish();
            }

            let Some(message_store) = self.message_store() else {
                return;
            };

            let idx = if self.search_reverse.get() {
                message_store.n_items() as i32 - 1
            } else {
                0
            };

            if idx < 0 {
                return;
            }

            if let Some(view) = self
                .message_list
                .row_at_index(idx)
                .and_downcast_ref::<MessageView>()
            {
                self.search_active_message_view.replace(Some(view.clone()));
                view.web_bin()
                    .search(search_text, self.search_reverse.get());
            }
        }

        fn message_search_found(
            &self,
            message_view: &MessageView,
            _search_text: &str,
            _count: u32,
        ) {
            if self.message_list_scrolled_to.borrow().as_ref() != Some(message_view) {
                self.message_viewport.scroll_to(message_view, None);
                self.message_list_scrolled_to
                    .replace(Some(message_view.clone()));
            }

            self.search_active_message_view
                .replace(Some(message_view.clone()));
            self.search_term_found.set(true);
        }

        fn message_search_not_found(&self, message_view: &MessageView, search_text: &str) {
            // Bail out if search is finished as a protective measure
            if self.search_active_message_view.borrow().is_none() {
                return;
            }

            let Some(message_store) = self.message_store() else {
                return;
            };

            // Check next view if it has the text
            let reverse = self.search_reverse.get();
            let mut idx = if self.search_reverse.get() {
                message_store.n_items() as i32 - 1
            } else {
                0
            };

            if idx < 0 {
                return;
            }

            let mut use_next = false;

            while let Some(view) = self
                .message_list
                .row_at_index(idx)
                .and_downcast_ref::<MessageView>()
            {
                if use_next {
                    self.search_active_message_view.replace(Some(view.clone()));
                    view.web_bin().search(search_text, reverse);
                    return;
                }

                if view == message_view {
                    use_next = true;
                }

                if reverse {
                    idx -= 1
                } else {
                    idx += 1;
                }
            }

            if self.search_term_found.get() {
                log::trace!("Wrap around search: {}", search_text);
                self.message_search_new(search_text, true);
            } else {
                log::trace!("Search term '{search_text}' not found, done");
            }
        }

        fn message_search_finish(&self) {
            self.search_active_message_view.take();
            self.message_list_scrolled_to.take();
            self.search_term_found.set(false);

            let mut idx = 0;
            while let Some(view) = self
                .message_list
                .row_at_index(idx)
                .and_downcast_ref::<MessageView>()
            {
                view.web_bin().search_finish();
                idx += 1;
            }
        }

        fn clear_message_view(&self) {
            if let Some(id) = self.thread_changed_signal_handler_id.take() {
                if let Some(thread) = self.thread() {
                    thread.disconnect(id);
                }
            }

            self.obj().set_title(&gettext("Messages"));
        }

        fn update_thread(&self, thread: Option<&model::Thread>) {
            let subject = thread.and_then(|t| t.thread_name());
            if subject.is_some() {
                self.message_subject.remove_css_class("dim-label");
            } else {
                self.message_subject.add_css_class("dim-label");
            }

            self.obj()
                .set_title(&subject.unwrap_or(gettext("No Subject")));

            let Some(message_store) = self.message_store() else {
                self.clear_message_view();
                self.update_message_pane();
                return;
            };

            if !message_store.is_empty() && !self.obj().is_mapped() {
                self.scroll_to_first_unread();
            }

            self.update_message_pane();
        }

        fn thread_signal_group(&self) -> &glib::SignalGroup {
            self.displayed_thread_signal_group.get_or_init(|| {
                let group = glib::SignalGroup::new::<model::Thread>();

                group.connect_closure(
                    "notify::thread-name",
                    true,
                    glib::closure_local!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |thread: model::Thread, _: glib::Value| {
                            imp.update_thread(Some(&thread));
                        }
                    ),
                );

                group.connect_closure(
                    "messages-changed",
                    true,
                    glib::closure_local!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |thread: model::Thread| {
                            imp.update_thread(Some(&thread));
                        }
                    ),
                );

                group
            })
        }

        fn scroll_to_first_unread(&self) {
            log::debug!("scroll to first unread");
            if let Some(store) = self.message_store() {
                if !store.is_empty() {
                    // We have loaded new messages, and previously none were displayed
                    if let Some(view) = self
                        .sort_model()
                        .iter::<glib::Object>()
                        .enumerate()
                        .find(|(_, msg)| {
                            if let Ok(msg) = msg {
                                msg.downcast_ref()
                                    .is_some_and(|m: &model::Message| m.is_unread())
                            } else {
                                false
                            }
                        })
                        .and_then(|(i, _)| self.message_list.row_at_index(i as i32))
                        .and_downcast_ref::<MessageView>()
                    {
                        // We want to scroll so that the first unread message is at the very top of our view
                        glib::idle_add_local_once(glib::clone!(
                            #[weak(rename_to = imp)]
                            self.ref_counted(),
                            #[strong]
                            view,
                            move || {
                                let bounds = view.compute_bounds(&*imp.message_viewport);
                                let adj = imp.message_viewport.vadjustment();

                                if let (Some(bounds), Some(adj)) = (bounds, adj) {
                                    adj.set_value(
                                        adj.lower().max(adj.value() + bounds.y() as f64 - 10.),
                                    );
                                }
                            }
                        ));
                    }
                }
            }
        }

        fn update_message_pane(&self) {
            let multiple = self.threads.get().is_some_and(|s| s.len() > 1);
            if self.thread().is_none() && !multiple {
                for button in [
                    &*self.reply_btn,
                    &*self.reply_btn2,
                    &*self.reply_all_btn,
                    &*self.reply_all_btn2,
                    &*self.forward_btn,
                    &*self.forward_btn2,
                    &*self.delete_btn,
                    &*self.delete_btn2,
                    &*self.trash_btn,
                    &*self.trash_btn2,
                    &*self.junk_btn,
                    &*self.junk_btn2,
                    &*self.not_junk_btn,
                    &*self.not_junk_btn2,
                    &*self.search_btn,
                ] {
                    button.set_visible(false);
                }

                self.message_stack
                    .set_visible_child(&*self.message_page_empty);
            } else if let Some(mailbox) = &*self.mailbox.borrow() {
                let delete = mailbox.is_trash() || mailbox.is_junk();
                let junk = mailbox.is_junk();
                let hide_bottom_bar = !self.compact.get();

                self.reply_btn.set_visible(hide_bottom_bar && !multiple);
                self.reply_btn2.set_visible(!multiple);
                self.reply_all_btn.set_visible(hide_bottom_bar && !multiple);
                self.reply_all_btn2.set_visible(!multiple);
                self.forward_btn.set_visible(hide_bottom_bar && !multiple);
                self.forward_btn2.set_visible(!multiple);

                self.delete_btn.set_visible(delete && hide_bottom_bar);
                self.delete_btn2.set_visible(delete);
                self.trash_btn
                    .set_visible(!multiple && !delete && hide_bottom_bar);
                self.trash_btn2.set_visible(!multiple && !delete);
                self.junk_btn
                    .set_visible(!multiple && !junk && hide_bottom_bar);
                self.junk_btn2.set_visible(!multiple && !junk);
                self.not_junk_btn
                    .set_visible(!multiple && junk && hide_bottom_bar);
                self.not_junk_btn2.set_visible(!multiple && junk);
                self.search_btn.set_visible(!multiple);

                if multiple {
                    self.message_stack
                        .set_visible_child(&*self.message_page_multiple);
                } else {
                    self.message_stack
                        .set_visible_child(&*self.message_page_list);
                }
            }
        }

        fn move_thread_to(&self, mailbox_kind: MailboxKind) {
            let Some(controller) = &*self.controller.borrow() else {
                return;
            };

            // TODO: Is this the behaviour we want?
            // Only move the messages in *this* mailbox
            let Some(mailbox) = self.mailbox.borrow().clone() else {
                return;
            };

            let messages = self.thread().map(|t| t.messages()).unwrap_or_default();

            let messages: Vec<model::Message> = messages
                .into_iter()
                .filter_map(|(_, m)| (m.mailbox_id() == mailbox.id()).then_some(m))
                .collect();

            if messages.is_empty() {
                return;
            }

            controller.move_to(messages, mailbox_kind.into());
        }

        #[template_callback]
        fn trash_thread_clicked(&self) {
            self.move_thread_to(MailboxKind::Trash);
        }

        #[template_callback]
        fn junk_thread_clicked(&self) {
            self.move_thread_to(MailboxKind::Junk);
        }

        #[template_callback]
        fn not_junk_thread_clicked(&self) {
            self.move_thread_to(MailboxKind::Inbox);
        }

        #[template_callback]
        fn delete_thread_permanently_clicked(&self) {
            let Some(messages) = self.thread().map(|t| t.messages()) else {
                return;
            };

            if messages.is_empty() {
                return;
            }

            let Some(controller) = self
                .controller
                .borrow()
                .clone()
                .ok_or(AppError::NoAccount)
                .handle()
            else {
                return;
            };

            // TODO: Is this the behaviour we want?
            // Or do we want to only delete the messages in *this* mailbox?
            controller.delete_messages_permanently_confirm(
                messages.values().collect(),
                self.obj().as_ref(),
            );
        }
    }
}

glib::wrapper! {
    pub struct MessagePage(ObjectSubclass<imp::MessagePage>)
    @extends adw::NavigationPage, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl MessagePage {
    fn new(threads: gio::ListStore) -> Self {
        glib::Object::builder().property("threads", threads).build()
    }
}
