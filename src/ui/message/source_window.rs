// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::gio;

use envelib::model;
use envelib::model::MessageExt;

mod imp {
    use super::*;
    use std::cell::RefCell;

    #[derive(Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::MessageSourceWindow)]
    #[template(file = "source_window.ui")]
    pub struct MessageSourceWindow {
        #[template_child]
        text_view: TemplateChild<gtk::TextView>,

        #[property(get, set = Self::set_message)]
        message: RefCell<Option<model::Message>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageSourceWindow {
        const NAME: &'static str = "EnvelopeMessageSourceWindow";
        type Type = super::MessageSourceWindow;
        type ParentType = adw::Window;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessageSourceWindow {}
    impl WidgetImpl for MessageSourceWindow {}
    impl WindowImpl for MessageSourceWindow {}
    impl AdwWindowImpl for MessageSourceWindow {}

    #[gtk::template_callbacks]
    impl MessageSourceWindow {
        fn set_message(&self, message: Option<model::Message>) {
            self.text_view.buffer().set_text("");

            if let Some(msg) = message {
                if let Some(body) = msg.message_content() {
                    self.text_view.buffer().set_text(&body.to_string());
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct MessageSourceWindow(ObjectSubclass<imp::MessageSourceWindow>)
        @extends gtk::Widget, gtk::Window, adw::Window,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl MessageSourceWindow {
    pub fn new(message: model::Message) -> Self {
        glib::Object::builder().property("message", message).build()
    }
}
