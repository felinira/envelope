// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::gdk;

use envelib::gettext::*;
use envelib::model;
use envelib::model::{MessageExt, MessageExtProps};
use envelib::types::*;

mod imp {
    use model::MessageStore;

    use crate::ui::{
        components::{inscription::Inscription, label::Label},
        message::{MessageAction, drag::MessageDragView},
    };

    use super::*;
    use std::cell::{Cell, RefCell};

    #[derive(Debug, Default, PartialEq, Eq, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::MessageHeader)]
    #[template(file = "header.ui")]
    pub struct MessageHeader {
        #[template_child]
        pub avatar: TemplateChild<adw::Avatar>,
        #[template_child]
        pub from_short: TemplateChild<Inscription>,
        #[template_child]
        pub from_full: TemplateChild<Label>,
        #[template_child]
        pub attachment_icon: TemplateChild<gtk::Image>,
        #[template_child]
        pub date: TemplateChild<gtk::Label>,
        #[template_child]
        pub subject: TemplateChild<Label>,
        #[template_child]
        pub to: TemplateChild<gtk::Label>,
        #[template_child]
        pub cc: TemplateChild<gtk::Label>,
        #[template_child]
        pub bcc: TemplateChild<gtk::Label>,
        #[template_child]
        pub body_short: TemplateChild<Inscription>,

        #[property(get, set = Self::set_compact)]
        compact: Cell<bool>,
        #[property(get, set = Self::set_collapsed)]
        collapsed: Cell<bool>,
        #[property(get, set, nullable)]
        message: RefCell<Option<model::Message>>,
        #[property(get)]
        menu: gio::Menu,

        drag_source: gtk::DragSource,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageHeader {
        const NAME: &'static str = "EnvelopeMessageHeader";
        type Type = super::MessageHeader;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessageHeader {
        fn constructed(&self) {
            self.parent_constructed();

            // Create menu model from top level message actions
            for section in MessageAction::SECTIONS {
                let section_menu = gio::Menu::new();
                for action in *section {
                    let item = gio::MenuItem::new(
                        Some(&action.localized_name()),
                        Some(&action.detailed_name()),
                    );
                    item.set_attribute_value("hidden-when", Some(&"action-disabled".to_variant()));
                    section_menu.append_item(&item);
                }

                self.menu.append_section(None, &section_menu);
            }

            self.obj().notify_menu();

            let obj = self.obj().clone();
            self.drag_source.connect_prepare(glib::clone!(
                #[weak]
                obj,
                #[upgrade_or_default]
                move |source, x, y| obj.imp().drag_prepare(source, x, y)
            ));
            self.drag_source
                .connect_drag_begin(MessageDragView::drag_begin);
            self.drag_source
                .set_actions(gdk::DragAction::COPY | gdk::DragAction::MOVE);

            self.obj().add_controller(self.drag_source.clone());
        }
    }

    impl WidgetImpl for MessageHeader {}
    impl ListBoxRowImpl for MessageHeader {}
    impl PreferencesRowImpl for MessageHeader {}
    impl ActionRowImpl for MessageHeader {}

    impl MessageHeader {
        fn set_compact(&self, compact: bool) {
            self.compact.set(compact);
            if compact {
                self.avatar.set_size(36);
            } else {
                self.avatar.set_size(48);
            }

            self.update_visible();
        }

        pub fn update_visible(&self) {
            let show = !self.collapsed.get();
            self.to.set_visible(show && !self.to.label().is_empty());
            self.cc.set_visible(show && !self.cc.label().is_empty());
            self.bcc.set_visible(show && !self.bcc.label().is_empty());
        }

        fn set_collapsed(&self, collapsed: bool) {
            self.collapsed.set(collapsed);
            self.update_visible();
        }

        fn drag_prepare(
            &self,
            _source: &gtk::DragSource,
            _x: f64,
            _y: f64,
        ) -> Option<gdk::ContentProvider> {
            let messages: MessageStore = self.message.borrow().clone().into_iter().collect();
            Some(gdk::ContentProvider::for_value(&messages.to_value()))
        }
    }
}

glib::wrapper! {
    pub struct MessageHeader(ObjectSubclass<imp::MessageHeader>)
    @extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl MessageHeader {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn address_markup(address: &Address) -> String {
        let dim_opacity = if adw::StyleManager::default().is_high_contrast() {
            9
        } else {
            55
        };

        if let Some(name) = address.name() {
            format!(
                "{} <span alpha=\"{dim_opacity}%\">{}</span>",
                glib::markup_escape_text(name),
                glib::markup_escape_text(address.address())
            )
        } else {
            format!("{}", glib::markup_escape_text(address.address()))
        }
    }

    pub fn load_message(&self, message: &model::Message) {
        let imp = self.imp();
        self.set_message(Some(message.clone()));

        let first_from = message.addr_from();
        imp.avatar
            .set_text(first_from.as_ref().map(|a| a.name().unwrap_or(a.address())));
        imp.avatar
            .set_show_initials(first_from.is_some_and(|a| a.name().is_some()));

        // TODO Avatar image if available

        let from_short = if message.participants().from.is_empty() {
            imp.from_short.add_css_class("dim-label");
            "Unknown Sender".to_string()
        } else {
            message
                .participants()
                .from
                .iter()
                .map(Address::format_short)
                .collect::<Vec<_>>()
                .join(", ")
        };
        imp.from_short.set_text(Some(from_short));

        let from_full = if message.participants().from.is_empty() {
            imp.from_full.add_css_class("dim-label");
            "Unknown Sender".to_string()
        } else {
            message
                .participants()
                .from
                .iter()
                .map(Self::address_markup)
                .collect::<Vec<_>>()
                .join(", ")
        };

        imp.from_full.set_label(from_full);

        let dim_opacity = if adw::StyleManager::default().is_high_contrast() {
            9
        } else {
            55
        };

        if !message.participants().to.is_empty() {
            imp.to.set_label(&format!(
                "<span alpha=\"{dim_opacity}%\">To:</span> {}",
                &message
                    .participants()
                    .to
                    .iter()
                    .map(Self::address_markup)
                    .collect::<Vec<_>>()
                    .join(", ")
            ));
        }

        if !message.participants().cc.is_empty() {
            imp.cc.set_label(&format!(
                "<span alpha=\"{dim_opacity}%\">Cc:</span> {}",
                &message
                    .participants()
                    .cc
                    .iter()
                    .map(Self::address_markup)
                    .collect::<Vec<_>>()
                    .join(", ")
            ));
        }

        if !message.participants().bcc.is_empty() {
            imp.bcc.set_label(&format!(
                "<span alpha=\"{dim_opacity}%\">Bcc:</span> {}",
                &message
                    .participants()
                    .bcc
                    .iter()
                    .map(Self::address_markup)
                    .collect::<Vec<_>>()
                    .join(", ")
            ));
        }

        imp.attachment_icon
            .set_visible(message.attachment_count() > 0);

        imp.date.set_label(&message.short_date_str());

        imp.subject
            .set_label(message.subject().unwrap_or(gettext("No Subject")));

        if message.subject().is_none() {
            imp.subject.add_css_class("dim-label");
        }

        imp.body_short.set_text(message.preview_text().as_deref());

        self.imp().update_visible();
    }
}
