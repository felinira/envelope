// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::rc::Rc;

use crate::controller::Controller;
use crate::error::*;
use adw::prelude::*;
use adw::subclass::prelude::*;
use envelib::gettext::*;
use envelib::types::*;

use envelib::model;
use envelib::model::{MailboxExt, MessageExt, MessageExtProps};

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        time::Duration,
    };

    use envelib::model::MailboxExtProps;
    use strum::IntoEnumIterator;

    use super::*;
    use crate::{
        controller::Controller,
        ui::{
            components::attachment_row::AttachmentRow,
            message::{
                MessageAction, header::MessageHeader, source_window::MessageSourceWindow,
                web_bin::WebBin,
            },
        },
    };

    #[derive(Debug, Default, PartialEq, Eq, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::MessageView)]
    #[template(file = "view.ui")]
    pub struct MessageView {
        //#[template_child]
        //pub message_header_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub message_header: TemplateChild<MessageHeader>,
        #[template_child]
        pub attachment_row: TemplateChild<gtk::ListBoxRow>,
        #[template_child]
        pub attachment_list: TemplateChild<gtk::ListBox>,
        #[property(get)]
        #[template_child]
        web_bin: TemplateChild<WebBin>,

        action_group: OnceCell<gio::SimpleActionGroup>,

        pub loading: Cell<bool>,

        #[template_child]
        pub message_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        message_stack_error: TemplateChild<gtk::Box>,
        #[template_child]
        message_stack_spinner: TemplateChild<gtk::Box>,
        #[template_child]
        pub message_stack_message: TemplateChild<gtk::Box>,
        #[template_child]
        mailbox_banner: TemplateChild<adw::Banner>,

        #[template_child]
        error_label: TemplateChild<gtk::Label>,

        #[property(get, set)]
        compact: Cell<bool>,
        #[property(get, set = Self::set_collapsed, explicit_notify)]
        pub collapsed: Cell<bool>,
        #[property(get)]
        pub remote_content_banner_visible: Cell<bool>,

        pub message_signal_handlers: RefCell<Vec<glib::SignalHandlerId>>,

        #[property(get, construct_only)]
        pub controller: OnceCell<Controller>,
        #[property(get, construct_only)]
        mailbox: OnceCell<model::Mailbox>,
        #[property(get, set = Self::set_message)]
        message: RefCell<Option<model::Message>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageView {
        const NAME: &'static str = "EnvelopeMessageView";
        type Type = super::MessageView;
        type ParentType = adw::PreferencesRow;

        fn class_init(klass: &mut Self::Class) {
            klass.set_css_name("message-view");
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessageView {
        fn constructed(&self) {
            self.install_actions();
            self.update_actions();
            self.parent_constructed();

            let obj = self.obj().clone();
            self.controller()
                .connect_connection_active_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |_controller| {
                        obj.imp().update_connection_active();
                    }
                ));

            self.update_connection_active();
            self.update_mailbox_banner();

            // Uncollapse the view when search results are visible
            self.web_bin.connect_search_found(glib::clone!(
                #[weak(rename_to = obj)]
                self.obj(),
                move |_view, _text, count| {
                    if count > 0 && obj.imp().collapsed.get() {
                        obj.set_collapsed(false);
                    }
                }
            ));

            // Display an error message if the web view has thrown an error
            self.web_bin.connect_error(glib::clone!(
                #[weak(rename_to = imp)]
                self.ref_counted(),
                move |_view, message| {
                    imp.show_error(message);
                }
            ));

            // Display the message once it's loaded
            self.web_bin.connect_message_loaded_notify(glib::clone!(
                #[weak(rename_to = imp)]
                self.ref_counted(),
                move |bin| {
                    if bin.message_loaded() {
                        imp.show_web_view();
                    }
                }
            ));
        }
    }

    impl WidgetImpl for MessageView {}
    impl ListBoxRowImpl for MessageView {}
    impl PreferencesRowImpl for MessageView {}

    #[gtk::template_callbacks]
    impl MessageView {
        fn controller(&self) -> &Controller {
            self.controller.get().expect("controller must be set")
        }

        fn mailbox(&self) -> &model::Mailbox {
            self.mailbox.get().expect("mailbox must be set")
        }

        fn shows_error(&self) -> bool {
            self.message_stack.visible_child().as_ref()
                == Some(self.message_stack_error.upcast_ref())
        }

        fn show_error(&self, message: &str) {
            log::error!("Error loading page: {}", message);
            self.message_stack
                .set_visible_child(&*self.message_stack_error);
            self.error_label.set_label(message);
        }

        fn show_spinner(&self) {
            self.message_stack
                .set_visible_child(&*self.message_stack_spinner);
        }

        fn show_web_view(&self) {
            // Delay this by 10 ms to remove a visible flicker
            glib::timeout_add_local_once(
                Duration::from_millis(10),
                glib::clone!(
                    #[strong(rename_to = imp)]
                    self.ref_counted(),
                    move || {
                        imp.message_stack
                            .set_visible_child(&*imp.message_stack_message);
                    }
                ),
            );
        }

        pub(super) fn update_mailbox_banner(&self) {
            let mut reveal = false;
            let mailbox = self.mailbox();

            if let Some(message) = &*self.message.borrow() {
                if let Some(message_mailbox) = message.mailbox() {
                    if message_mailbox != *mailbox {
                        self.mailbox_banner.set_title(&gettextf(
                            "Message from {}",
                            &[&message_mailbox.localized_name()],
                        ));
                        reveal = true;
                    }
                }
            }

            self.mailbox_banner.set_revealed(reveal);
        }

        #[template_callback]
        fn row_activate(&self) {
            self.obj().set_collapsed(!self.obj().collapsed());
        }

        fn update_connection_active(&self) {
            if !self.web_bin.message_loaded() {
                if self.controller().connection_active() {
                    self.show_spinner();
                } else {
                    self.show_error(&gettext("Connection Error"));
                }
            }
        }

        fn message_action_group(&self) -> &gio::SimpleActionGroup {
            self.action_group.get_or_init(|| {
                let group = gio::SimpleActionGroup::new();
                let activate = glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_group: &gio::SimpleActionGroup,
                          action: &gio::SimpleAction,
                          params: Option<&glib::Variant>| {
                        imp.activate_action(&format!("message.{}", action.name()), params)
                    }
                );

                group.add_action_entries(MessageAction::iter().map(|name| {
                    gio::ActionEntry::builder(name.as_ref())
                        .activate(activate.clone())
                        .build()
                }));

                group
            })
        }

        fn install_actions(&self) {
            let group = self.message_action_group();
            self.obj().insert_action_group("message", Some(group));
        }

        fn mark_read(&self, set_read: bool) {
            let Some(message) = self.message.borrow().clone() else {
                return;
            };

            if message.is_unread() != set_read {
                // No change necessary
                return;
            }

            let change = if set_read {
                // Remove the seen flag
                MessageFlagsChange::Add
            } else {
                // Add the seen flag
                MessageFlagsChange::Remove
            };

            let flags = MessageFlags::seen();

            self.controller().store_flags(vec![message], flags, change)
        }

        fn view_source(&self) {
            if let Some(message) = self.message.borrow().clone() {
                let source_window = MessageSourceWindow::new(message);
                source_window.present();
            }
        }

        fn move_to(&self, mailbox_kind: MailboxKind) {
            let Some(message) = self.message.borrow().clone() else {
                return;
            };

            self.controller()
                .move_to(vec![message], mailbox_kind.into());
        }

        fn delete_message_permanently(&self) {
            let Some(message) = self.message.borrow().clone() else {
                return;
            };

            self.controller()
                .delete_messages_permanently_confirm(vec![message], &*self.obj());
        }

        async fn save_dialog(&self, part: &model::MessagePart) -> AppResult<()> {
            let Some(bytes) = part.bytes() else {
                return Err(AppError::msg(gettext("Message Part has no data")));
            };

            let filename = part.filename();

            let dialog = gtk::FileDialog::builder()
                .initial_name(filename)
                .modal(true)
                .build();

            let Some(window) = self
                .obj()
                .root()
                .and_then(|r| r.downcast::<gtk::Window>().ok())
            else {
                return Err(AppError::msg("window not found"));
            };

            let gfile = dialog.save_future(Some(&window)).await?;
            let Some(path) = gfile.path() else {
                return Err(AppError::msg(gettext("File has no path")));
            };

            smol::fs::write(path, bytes).await?;
            Ok(())
        }

        fn save_to_file(&self, uri: Option<&str>) -> AppResult<()> {
            log::debug!("Save to file");
            let Some(message) = self.message.borrow().clone() else {
                return Ok(());
            };

            let part = if let Some(uri) = uri {
                message.message_part_by_uri(uri)
            } else {
                message.message_content().map(Into::into)
            }
            .ok_or_else(|| AppError::msg(gettext("Message part not found")))?;

            glib::spawn_future_local(glib::clone!(
                #[strong(rename_to = imp)]
                self.ref_counted(),
                async move {
                    imp.save_dialog(&part).await.handle();
                }
            ));

            Ok(())
        }

        fn lookup_action(&self, action_name: &str) -> Option<gio::SimpleAction> {
            match action_name.split_once('.') {
                Some(("message", name)) => self
                    .message_action_group()
                    .lookup_action(name)
                    .and_downcast::<gio::SimpleAction>(),
                _ => None,
            }
        }

        fn action_set_enabled(&self, action: MessageAction, enabled: bool) {
            if let Some(action) = self.lookup_action(&action.detailed_name()) {
                action.set_enabled(enabled);
                let context_menu_group = self.web_bin.context_menu_actions();
                if enabled {
                    context_menu_group.add_action(&action);
                } else {
                    context_menu_group.remove_action(&action.name());
                }
            }
        }

        fn activate_action(&self, action_name: &str, params: Option<&glib::Variant>) {
            log::debug!("activate action {}", action_name);
            match action_name
                .split_once(".")
                .filter(|(group, _name)| *group == "message")
                .and_then(|(_group, name)| name.parse().ok())
            {
                Some(MessageAction::MarkRead) => self.mark_read(true),
                Some(MessageAction::MarkUnread) => self.mark_read(false),
                Some(MessageAction::MoveToTrash) => self.move_to(MailboxKind::Trash),
                Some(MessageAction::MoveToJunk) => self.move_to(MailboxKind::Junk),
                Some(MessageAction::MoveToInbox) => self.move_to(MailboxKind::Inbox),
                Some(MessageAction::DeletePermanently) => self.delete_message_permanently(),
                Some(MessageAction::SaveToFile) => {
                    self.save_to_file(params.and_then(|v| v.str())).handle();
                }
                Some(MessageAction::ViewSource) => self.view_source(),
                None => {}
            }
        }

        pub fn update_actions(&self) {
            let message = &*self.message.borrow();
            for action in MessageAction::iter() {
                let enabled = if let Some(message) = message {
                    match action {
                        MessageAction::MarkRead => message.is_unread(),
                        MessageAction::MarkUnread => message.is_read(),
                        MessageAction::MoveToTrash => message
                            .mailbox()
                            .is_some_and(|m| !m.is_trash() && !m.is_junk()),

                        MessageAction::MoveToJunk => {
                            message.mailbox().is_some_and(|m| !m.is_junk())
                        }
                        MessageAction::MoveToInbox => {
                            message.mailbox().is_some_and(|m| m.is_junk())
                        }
                        MessageAction::DeletePermanently => message
                            .mailbox()
                            .is_some_and(|m| m.is_trash() || m.is_junk()),
                        MessageAction::SaveToFile | MessageAction::ViewSource => true,
                    }
                } else {
                    false
                };

                self.action_set_enabled(action, enabled);
            }
        }

        #[template_callback]
        fn show_remote_content(&self) {
            self.web_bin.set_allow_remote_content(true);
        }

        fn set_collapsed(&self, collapsed: bool) {
            log::debug!("collapsed: {}", collapsed);
            self.collapsed.set(collapsed);
            self.obj().notify_collapsed();

            if collapsed {
                self.obj().add_css_class("collapsed");
            } else {
                self.obj().remove_css_class("collapsed");
            }
        }

        fn message_loaded(&self) {
            self.web_bin.set_message(self.message.borrow().as_ref());
            let Some(message) = &*self.message.borrow() else {
                return;
            };

            let attachments = message.attachments();
            if attachments.is_empty() {
                self.attachment_row.set_visible(false);
            }

            if self.attachment_list.row_at_index(0).is_none() {
                // Only load attachments once
                for attachment in attachments {
                    self.attachment_row.set_visible(true);
                    let view = AttachmentRow::new(attachment);
                    self.attachment_list.append(&view);
                }
            }
        }

        fn set_message(&self, message: model::Message) {
            if let Some(message) = &*self.message.borrow() {
                for handler in self.message_signal_handlers.borrow_mut().drain(..) {
                    message.disconnect(handler)
                }
            }

            self.set_collapsed(message.is_read() && !message.is_newest_in_thread());

            self.message.replace(Some(message.clone()));
            self.update_mailbox_banner();
            self.message_signal_handlers
                .borrow_mut()
                .push(message.connect_is_unread_notify(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_| {
                        imp.update_actions();
                    }
                )));

            self.update_actions();
            self.message_header.load_message(&message);

            if message.detail_level() == model::MessageDetailLevel::Full {
                self.message_loaded();
            } else {
                // We monitor the message for any changes
                let handler_id = Rc::new(Cell::new(None));
                handler_id
                    .clone()
                    .set(Some(message.connect_detail_level_notify(glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |msg| {
                            if msg.detail_level() == model::MessageDetailLevel::Full
                                && !imp.loading.get()
                            {
                                if let Some(id) = handler_id.take() {
                                    msg.disconnect(id);
                                };

                                imp.loading.set(true);
                                imp.message_loaded();
                            }
                        }
                    ))));
            }
        }
    }
}

glib::wrapper! {
    pub struct MessageView(ObjectSubclass<imp::MessageView>)
    @extends adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl MessageView {
    pub fn new(controller: Controller, mailbox: model::Mailbox) -> Self {
        glib::Object::builder()
            .property("controller", controller)
            .property("mailbox", mailbox)
            .build()
    }
}
