// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::gdk;

use envelib::gettext::*;
use envelib::model::MessageStore;

mod imp {
    use envelib::model::{IndexStoreExt, MessageStore};

    use super::*;
    use std::cell::{Cell, RefCell};

    #[derive(Default, glib::Properties, gtk::CompositeTemplate)]
    #[template(file = "drag.ui")]
    #[properties(wrapper_type = super::MessageDragView)]
    pub struct MessageDragView {
        #[property(get, set = Self::set_messages)]
        messages: RefCell<MessageStore>,
        action: Cell<Option<gdk::DragAction>>,

        #[template_child]
        label: TemplateChild<gtk::Label>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageDragView {
        const NAME: &'static str = "EnvelopeMessageDragView";
        type Type = super::MessageDragView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessageDragView {}
    impl WidgetImpl for MessageDragView {}
    impl BoxImpl for MessageDragView {}

    #[gtk::template_callbacks]
    impl MessageDragView {
        fn set_messages(&self, messages: MessageStore) {
            self.messages.replace(messages);
            self.update_text();
        }

        pub(super) fn set_action(&self, action: gdk::DragAction) {
            self.action.set(Some(action));
            self.update_text();
        }

        fn update_text(&self) {
            let n = self.messages.borrow().len() as u32;

            match self.action.get() {
                Some(action) => {
                    if action.contains(gdk::DragAction::COPY) {
                        self.label.set_text(&ngettextf(
                            "Copy One Message",
                            "Copy {} Messages",
                            n,
                            &[&n],
                        ));
                    } else {
                        self.label.set_text(&ngettextf(
                            "Move One Message",
                            "Move {} Messages",
                            n,
                            &[&n],
                        ));
                    }
                }
                None => self.label.set_text(""),
            }
        }
    }
}

glib::wrapper! {
    pub struct MessageDragView(ObjectSubclass<imp::MessageDragView>)
    @extends gtk::Box, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl MessageDragView {
    pub fn drag_begin(_source: &gtk::DragSource, drag: &gdk::Drag) {
        let Some(messages) = drag
            .content()
            .value(MessageStore::static_type())
            .ok()
            .and_then(|v| v.get::<MessageStore>().ok())
        else {
            return;
        };

        let widget: Self = glib::Object::builder()
            .property("messages", messages)
            .build();

        let drag_icon = gtk::DragIcon::for_drag(drag)
            .downcast::<gtk::DragIcon>()
            .unwrap();
        drag_icon.set_child(Some(&widget));

        widget.imp().set_action(drag.selected_action());
        drag.connect_selected_action_notify(glib::clone!(
            #[weak]
            widget,
            move |drag| {
                widget.imp().set_action(drag.selected_action());
            }
        ));
    }
}
