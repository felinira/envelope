// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::error::*;

pub struct Async {}

impl Async {
    pub fn spawn_task<
        R: Send + 'static,
        F: std::future::Future<Output = Result<R, crate::error::AppError>> + 'static + Send,
    >(
        f: F,
    ) -> smol::Task<Option<R>> {
        smol::spawn(async move {
            let res = f.await;
            if let Err(err) = res {
                glib::MainContext::default().invoke(move || {
                    err.handle();
                });
                None
            } else {
                res.ok()
            }
        })
    }

    pub fn spawn_local<
        R: 'static,
        F: 'static + std::future::Future<Output = Result<R, crate::error::AppError>>,
    >(
        f: F,
    ) -> glib::JoinHandle<Option<R>> {
        glib::MainContext::default().spawn_local(async move {
            let res = f.await;
            if let Err(err) = res {
                err.handle();
                None
            } else {
                res.ok()
            }
        })
    }

    pub fn spawn_local_with_priority<
        R: 'static,
        F: 'static + std::future::Future<Output = Result<R, crate::error::AppError>>,
    >(
        priority: glib::Priority,
        f: F,
    ) -> glib::JoinHandle<Option<R>> {
        glib::MainContext::default().spawn_local_with_priority(priority, async move {
            let res = f.await;
            if let Err(err) = res {
                err.handle();
                None
            } else {
                res.ok()
            }
        })
    }
}
