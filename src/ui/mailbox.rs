// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod page;
mod row;

pub use page::MailboxPage;
