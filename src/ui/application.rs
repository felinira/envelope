// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::future::Future;
use std::future::IntoFuture;
use std::pin::Pin;
use std::sync::Arc;

use envelib::account;
use gettextrs::gettext;
use tracing::{debug, info};

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::{gdk, gio};

use crate::config::PersistentConfig;
use crate::config::credentials::SecretService;
use crate::config::error::ConfigResult;
use crate::error::*;
use crate::globals;
use envelib::gettext::*;

pub struct ConfigSaveHandle<R>(Pin<Box<dyn Future<Output = R>>>);

impl<R> IntoFuture for ConfigSaveHandle<R> {
    type Output = R;
    type IntoFuture = Pin<Box<dyn Future<Output = R>>>;

    fn into_future(self) -> Self::IntoFuture {
        self.0
    }
}

mod imp {
    use super::*;
    use crate::config::PersistentConfig;

    use arc_swap::ArcSwap;
    use glib::WeakRef;
    use std::sync::Arc;

    #[derive(Debug, Default)]
    pub struct EnvelopeApplication {
        pub window: WeakRef<crate::ui::window::EnvelopeApplicationWindow>,
        pub config: ArcSwap<PersistentConfig>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnvelopeApplication {
        const NAME: &'static str = "EnvelopeApplication";
        type Type = super::EnvelopeApplication;
        type ParentType = adw::Application;
    }

    impl AdwApplicationImpl for EnvelopeApplication {}

    impl ObjectImpl for EnvelopeApplication {}

    impl ApplicationImpl for EnvelopeApplication {
        fn activate(&self) {
            debug!("activate");
            self.parent_activate();
            let app = self.obj();

            if let Some(window) = self.window.upgrade() {
                window.present();
                return;
            }

            let window = crate::ui::window::EnvelopeApplicationWindow::new(&app);
            self.window.set(Some(&window));

            // TODO remove blocking IO
            smol::block_on(glib::clone!(
                #[strong(rename_to = imp)]
                self.ref_counted(),
                async move {
                    if let Err(err) = imp.load_config().await {
                        if let Some(window) = imp.window.upgrade() {
                            window.connect_visible_notify(move |window| {if window.is_visible() {
                            anyhow::Error::msg(gettextf(
                                "Error loading config file “{0}”, using default config.\nError: {1}",
                                &[&PersistentConfig::path().display(),
                                &err]
                            )).handle();
                        }});
                        }
                    }
                }
            ));

            self.obj().main_window().present();
        }

        fn startup(&self) {
            debug!("startup");
            self.parent_startup();
            let app = self.obj();

            // Set icons for shell
            gtk::Window::set_default_icon_name(&globals::APP_ID);

            app.setup_css();
            app.setup_gactions();
            app.setup_accels();
        }

        fn shutdown(&self) {
            debug!("shutdown");
            self.parent_shutdown();

            smol::block_on(async move {});
        }
    }

    impl GtkApplicationImpl for EnvelopeApplication {}

    impl EnvelopeApplication {
        pub(super) async fn load_config(&self) -> AppResult<()> {
            self.config
                .store(Arc::new(PersistentConfig::from_file().await?));
            Ok(())
        }

        pub(super) async fn save_config(&self) -> AppResult<()> {
            let mut cur = self.config.load();

            // async rcu
            loop {
                let mut new = (**cur).clone();
                new.save().await.map_err(|err| {
                    anyhow::Error::msg(gettextf("Error saving configuration file: {}", &[&err]))
                })?;

                let prev = self.config.compare_and_swap(&cur, Arc::new(new));
                if Arc::ptr_eq(&cur, &*prev) {
                    return Ok(());
                } else {
                    cur = prev;
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct EnvelopeApplication(ObjectSubclass<imp::EnvelopeApplication>)
        @extends gio::Application, gtk::Application, adw::Application,
        @implements gio::ActionMap, gio::ActionGroup;
}

impl EnvelopeApplication {
    pub fn main_window(&self) -> super::window::EnvelopeApplicationWindow {
        self.imp().window.upgrade().unwrap()
    }

    pub fn config(&self) -> arc_swap::Guard<Arc<PersistentConfig>> {
        self.imp().config.load()
    }

    /// Update the config with the provided closure and save it to disk
    ///
    /// Returns a future that can be awaited to ensure the file has been persisted to disk.
    /// Dropping the future is allowed, and the file will be saved in the background.
    pub fn config_update(
        &self,
        mut update: impl FnMut(&mut PersistentConfig),
    ) -> ConfigSaveHandle<AppResult<()>> {
        self.imp().config.rcu(|swap| {
            let mut new = (**swap).clone();
            update(&mut new);
            new
        });

        let handle = glib::spawn_future_local(glib::clone!(
            #[strong(rename_to= obj)]
            self,
            async move {
                let res = obj.imp().save_config().await;
                if let Err(err) = &res {
                    log::error!("Error saving config file: {}", err);
                }

                res
            }
        ));

        ConfigSaveHandle(Box::pin(async move { handle.await? }))
    }

    fn setup_gactions(&self) {
        // Quit
        let action_quit = gio::ActionEntry::builder("quit")
            .activate(move |app: &Self, _, _| {
                // This is needed to trigger the delete event and saving the window state
                app.main_window().close();
                app.quit();
            })
            .build();

        // About
        let action_about = gio::ActionEntry::builder("about")
            .activate(|app: &Self, _, _| {
                app.show_about_dialog();
            })
            .build();
        self.add_action_entries([action_quit, action_about]);
    }

    // Sets up keyboard shortcuts
    fn setup_accels(&self) {
        self.set_accels_for_action("app.quit", &["<Control>q"]);
        self.set_accels_for_action("window.close", &["<Control>w"]);
    }

    fn setup_css(&self) {
        let provider = gtk::CssProvider::new();
        provider.load_from_resource("/app/drey/Envelope/style.css");
        if let Some(display) = gdk::Display::default() {
            gtk::style_context_add_provider_for_display(
                &display,
                &provider,
                gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
            );
        }
    }

    fn show_about_dialog(&self) {
        let dialog = adw::AboutDialog::builder()
            .application_name(gettext("Envelope Mail"))
            .application_icon(&*globals::APP_ID)
            .version(globals::VERSION)
            .license_type(gtk::License::Gpl30)
            .website("https://gitlab.gnome.org/felinira/envelope/")
            .version(globals::VERSION)
            .translator_credits(gettext("translator-credits"))
            .artists(["Fina Wilke"])
            .developer_name("Fina Wilke")
            .developers(["Fina Wilke"])
            .build();

        dialog.present(Some(&self.main_window()));
    }

    pub fn run(&self) -> glib::ExitCode {
        info!("Envelope Mail ({})", &*globals::APP_ID);
        info!("Version: {} ({})", globals::VERSION, globals::PROFILE);
        info!("Datadir: {}", &*globals::PKGDATADIR);

        ApplicationExtManual::run(self)
    }

    pub async fn add_account(&self, account: account::Account) -> ConfigResult<()> {
        // Save the secrets
        for (kind, service) in &account.services {
            service.store_secret(&account, *kind).await?;
        }

        let save_result = self
            .config_update(|config| {
                config.accounts_mut().push(account.clone());
            })
            .await;

        if save_result.handle_app_result().is_some() {
            self.main_window().setup_account(account);
        }

        Ok(())
    }
}

impl Default for EnvelopeApplication {
    fn default() -> Self {
        assert!(
            gtk::is_initialized_main_thread(),
            "Calling gio::Application::default from non-main thread"
        );

        if let Some(app) = gio::Application::default() {
            app.downcast().expect("Application is wrong subclass")
        } else {
            glib::Object::builder()
                .property("application-id", &*globals::APP_ID)
                .property("flags", gio::ApplicationFlags::empty())
                .property("resource-base-path", "/app/drey/Envelope/")
                .build()
        }
    }
}
