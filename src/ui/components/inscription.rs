// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use std::{
        cell::{Cell, RefCell},
        marker::PhantomData,
    };

    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Inscription)]
    pub struct Inscription {
        #[property(set = Self::set_text, nullable)]
        text: PhantomData<Option<String>>,
        #[property(get, set, nullable, construct)]
        fallback_text: RefCell<Option<String>>,
        #[property(get = Self::min_lines, set = Self::set_min_lines)]
        min_lines: PhantomData<u32>,
        #[property(get, set = Self::set_bold)]
        bold: Cell<bool>,
        #[property(get = Self::xalign, set = Self::set_xalign)]
        xalign: PhantomData<f32>,
        #[property(get = Self::yalign, set = Self::set_yalign)]
        yalign: PhantomData<f32>,

        inscription: gtk::Inscription,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Inscription {
        const NAME: &'static str = "EnvelopeInscription";
        type Type = super::Inscription;
        type ParentType = adw::Bin;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Inscription {
        fn constructed(&self) {
            self.parent_constructed();
            self.obj().set_child(Some(&self.inscription));
        }
    }

    impl WidgetImpl for Inscription {}
    impl BinImpl for Inscription {}

    impl Inscription {
        fn set_text(&self, value: Option<String>) {
            let text = if value.as_ref().is_some_and(|value| !value.is_empty()) {
                self.inscription.remove_css_class("dim-label");
                value
            } else {
                self.inscription.add_css_class("dim-label");
                self.fallback_text.borrow().clone()
            };

            self.inscription.set_text(text.as_deref());
            self.inscription.set_xalign(0.0);
            self.inscription.set_yalign(0.0);
            self.inscription
                .set_text_overflow(gtk::InscriptionOverflow::EllipsizeEnd);
            self.inscription.set_nat_chars(999);
        }

        fn min_lines(&self) -> u32 {
            self.inscription.min_lines()
        }

        fn set_min_lines(&self, lines: u32) {
            self.inscription.set_min_lines(lines);
        }

        fn set_bold(&self, bold: bool) {
            if bold {
                self.inscription.set_attributes(Some(
                    &gtk::pango::AttrList::from_string("0 -1 weight bold").unwrap(),
                ));
            } else {
                self.inscription.set_attributes(None);
            }
        }

        fn xalign(&self) -> f32 {
            self.inscription.xalign()
        }

        fn set_xalign(&self, xalign: f32) {
            self.inscription.set_xalign(xalign);
        }

        fn yalign(&self) -> f32 {
            self.inscription.yalign()
        }

        fn set_yalign(&self, yalign: f32) {
            self.inscription.set_yalign(yalign);
        }
    }
}

glib::wrapper! {
    pub struct Inscription(ObjectSubclass<imp::Inscription>)
    @extends adw::Bin, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
