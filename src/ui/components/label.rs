// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use std::{
        cell::{Cell, RefCell},
        marker::PhantomData,
    };

    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::Label)]
    pub struct Label {
        #[property(set = Self::set_label)]
        label: PhantomData<Option<String>>,
        #[property(get, set, nullable, construct)]
        fallback_text: RefCell<Option<String>>,
        #[property(get, set = Self::set_ellipsize)]
        ellipsize: Cell<bool>,
        #[property(get, set = Self::set_bold)]
        bold: Cell<bool>,
        #[property(get = Self::use_markup, set = Self::set_use_markup)]
        use_markup: Cell<bool>,
        #[property(get = Self::wrap, set = Self::set_wrap)]
        wrap: PhantomData<bool>,
        #[property(get = Self::wrap_mode, set = Self::set_wrap_mode, builder(gtk::pango::WrapMode::WordChar))]
        wrap_mode: PhantomData<gtk::pango::WrapMode>,
        #[property(get = Self::xalign, set = Self::set_xalign)]
        xalign: PhantomData<f32>,
        #[property(get = Self::yalign, set = Self::set_yalign)]
        yalign: PhantomData<f32>,

        inner: gtk::Label,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Label {
        const NAME: &'static str = "EnvelopeLabel";
        type Type = super::Label;
        type ParentType = adw::Bin;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Label {
        fn constructed(&self) {
            self.parent_constructed();
            self.obj().set_wrap_mode(gtk::pango::WrapMode::WordChar);
            self.obj().set_child(Some(&self.inner));
        }
    }

    impl WidgetImpl for Label {}
    impl BinImpl for Label {}

    impl Label {
        fn set_label(&self, value: Option<String>) {
            if value.is_some() {
                self.inner.remove_css_class("dim-label");
            } else {
                self.inner.add_css_class("dim-label");
            }

            self.inner.set_label(
                &value.unwrap_or_else(|| self.fallback_text.borrow().clone().unwrap_or_default()),
            );
        }

        fn set_ellipsize(&self, ellipsize: bool) {
            if ellipsize {
                self.inner.set_ellipsize(gtk::pango::EllipsizeMode::End);
            } else {
                self.inner.set_ellipsize(gtk::pango::EllipsizeMode::None);
            }
        }

        fn set_bold(&self, bold: bool) {
            if bold {
                self.inner.set_attributes(Some(
                    &gtk::pango::AttrList::from_string("0 -1 weight bold").unwrap(),
                ));
            } else {
                // TODO: Workaround gtk issue with use_markup and attributes
                let markup = self.inner.uses_markup();
                if markup {
                    self.inner.set_use_markup(false);
                }

                self.inner.set_attributes(None);
                self.inner.set_use_markup(markup);
            }
        }

        fn use_markup(&self) -> bool {
            self.inner.uses_markup()
        }

        fn set_use_markup(&self, value: bool) {
            self.inner.set_use_markup(value);
        }

        fn xalign(&self) -> f32 {
            self.inner.xalign()
        }

        fn set_xalign(&self, xalign: f32) {
            self.inner.set_xalign(xalign);
        }

        fn yalign(&self) -> f32 {
            self.inner.yalign()
        }

        fn set_yalign(&self, yalign: f32) {
            self.inner.set_yalign(yalign);
        }

        fn wrap(&self) -> bool {
            self.inner.wraps()
        }

        fn set_wrap(&self, wrap: bool) {
            self.inner.set_wrap(wrap);
        }

        fn wrap_mode(&self) -> gtk::pango::WrapMode {
            self.inner.wrap_mode()
        }

        fn set_wrap_mode(&self, wrap_mode: gtk::pango::WrapMode) {
            self.inner.set_wrap_mode(wrap_mode);
        }
    }
}

glib::wrapper! {
    pub struct Label(ObjectSubclass<imp::Label>)
    @extends adw::Bin, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
