// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use envelib::gettext::*;
use envelib::model;

mod imp {
    use std::{cell::RefCell, path::Path};

    use futures::AsyncWriteExt;

    use crate::error::*;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::AttachmentRow)]
    #[template(file = "attachment_row.ui")]
    pub struct AttachmentRow {
        #[property(get, set)]
        attachment: RefCell<Option<model::Attachment>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AttachmentRow {
        const NAME: &'static str = "EnvelopeAttachmentRow";
        type Type = super::AttachmentRow;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for AttachmentRow {
        fn constructed(&self) {
            self.parent_constructed();
        }

        fn dispose(&self) {
            self.attachment.replace(None);
        }
    }

    impl WidgetImpl for AttachmentRow {}
    impl ListBoxRowImpl for AttachmentRow {}
    impl PreferencesRowImpl for AttachmentRow {}
    impl ActionRowImpl for AttachmentRow {}

    #[gtk::template_callbacks]
    impl AttachmentRow {
        fn attachment(&self) -> AppResult<model::Attachment> {
            self.attachment
                .borrow()
                .clone()
                .ok_or_else(|| AppError::msg("Attachment View without attachment"))
        }

        async fn save_attachment(&self, path: &Path) -> AppResult<()> {
            let attachment = self.attachment()?.clone();
            let data = attachment
                .data()
                .ok_or_else(|| AppError::msg(gettext("The attachment doesn't have any data")))?;

            let mut file = smol::fs::File::create(path).await?;
            file.write_all(data).await?;
            file.flush().await?;

            Ok(())
        }

        #[template_callback]
        async fn open(&self) {
            let Some(attachment) = self.attachment().handle_app_result() else {
                return;
            };

            let Some(filename) = attachment.filename() else {
                // Open file chooser instead as we need a filename to open files
                self.download_clicked().await;
                return;
            };

            let dir = glib::user_cache_dir()
                .join(crate::globals::APP_NAME)
                .join("attachments");
            smol::fs::create_dir_all(&dir).await.handle_app_result();

            let path = dir.join(filename);
            if self
                .save_attachment(&path)
                .await
                .handle_app_result()
                .is_some()
            {
                let file = gio::File::for_path(&path);
                gtk::FileLauncher::new(Some(&file))
                    .launch_future(
                        self.obj()
                            .ancestor(gtk::Window::static_type())
                            .and_downcast_ref::<gtk::Window>(),
                    )
                    .await
                    .handle_app_result();
            }

            // TODO: Cleanup cache
        }

        #[template_callback]
        async fn download_clicked(&self) {
            let Some(attachment) = self.attachment.borrow().clone() else {
                log::error!("Attachment Row without attachment");
                return;
            };

            let file_dialog = gtk::FileDialog::builder()
                .modal(true)
                .initial_name(attachment.filename().unwrap_or_default())
                .build();
            file_dialog.set_initial_folder(
                glib::user_special_dir(glib::UserDirectory::Downloads)
                    .map(gio::File::for_path)
                    .as_ref(),
            );
            let Ok(file) = file_dialog
                .save_future(
                    self.obj()
                        .ancestor(gtk::Window::static_type())
                        .and_downcast_ref::<gtk::Window>(),
                )
                .await
            else {
                return;
            };

            if let Some(path) = file.path() {
                self.save_attachment(&path).await.handle();
            } else {
                log::error!("gio::File without path: {}", file.uri());
                AppError::Message(gettext("Unable to save file. Location invalid.")).handle();
            }
        }
    }
}

glib::wrapper! {
    pub struct AttachmentRow(ObjectSubclass<imp::AttachmentRow>)
    @extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl AttachmentRow {
    pub fn new(attachment: model::Attachment) -> Self {
        glib::Object::builder()
            .property("attachment", attachment)
            .build()
    }
}
