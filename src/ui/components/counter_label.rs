// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

mod imp {
    use std::{cell::Cell, marker::PhantomData};

    use super::*;

    #[derive(Debug, Default, glib::Properties)]
    #[properties(wrapper_type = super::CounterLabel)]
    pub struct CounterLabel {
        #[property(set = Self::set_value)]
        value: PhantomData<u32>,
        #[property(get, set = Self::set_important)]
        important: Cell<bool>,

        label: gtk::Label,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for CounterLabel {
        const NAME: &'static str = "EnvelopeCounterLabel";
        type Type = super::CounterLabel;
        type ParentType = adw::Bin;
    }

    impl ObjectImpl for CounterLabel {
        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }

        fn constructed(&self) {
            self.parent_constructed();
            self.obj().set_child(Some(&self.label));
            self.label.add_css_class("counter");
            self.label.add_css_class("numeric");
        }
    }

    impl WidgetImpl for CounterLabel {}
    impl BinImpl for CounterLabel {}

    impl CounterLabel {
        fn set_value(&self, value: u32) {
            if value >= 10 {
                self.label.add_css_class("long");
            } else {
                self.label.remove_css_class("long");
            }

            self.label.set_label(&value.to_string());
        }

        fn set_important(&self, important: bool) {
            if important {
                self.label.add_css_class("needs-attention");
            } else {
                self.label.remove_css_class("needs-attention");
            }

            self.important.set(important);
        }
    }
}

glib::wrapper! {
    pub struct CounterLabel(ObjectSubclass<imp::CounterLabel>)
    @extends adw::Bin, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}
