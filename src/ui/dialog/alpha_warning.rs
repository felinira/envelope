// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::glib;

mod imp {
    use gtk::CompositeTemplate;

    use crate::ui::{application::EnvelopeApplication, dialog::page::DialogPage};

    use super::*;

    #[derive(CompositeTemplate, Default)]
    #[template(file = "alpha_warning.ui")]
    pub struct AlphaWarningDialog {
        // Navigation
        #[template_child]
        navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        alpha_warning_page: TemplateChild<DialogPage>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for AlphaWarningDialog {
        const NAME: &'static str = "EnvelopeAlphaWarningDialog";
        type Type = super::AlphaWarningDialog;
        type ParentType = adw::Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for AlphaWarningDialog {}

    impl WidgetImpl for AlphaWarningDialog {}
    impl WindowImpl for AlphaWarningDialog {}
    impl AdwDialogImpl for AlphaWarningDialog {
        fn close_attempt(&self) {
            // Close the app unless we already have an account configured
            EnvelopeApplication::default().quit();
        }
    }

    #[gtk::template_callbacks]
    impl AlphaWarningDialog {
        #[template_callback]
        fn on_continue(&self) {
            self.obj().force_close();
        }
    }
}

glib::wrapper! {
    pub struct AlphaWarningDialog(ObjectSubclass<imp::AlphaWarningDialog>)
        @extends gtk::Widget, adw::Dialog,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl AlphaWarningDialog {
    pub fn new() -> Self {
        glib::Object::builder().property("can-close", false).build()
    }
}
