// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use enumset::EnumSet;
use gtk::prelude::*;

use crate::ui::{application::EnvelopeApplication, dialog::credentials::password::PasswordDialog};
use envelib::{account, gettext::*};

pub mod password;

/// Ask for credentials using the specified mechanism(s).
///
/// If multiple mechanisms are provided, the user will be given a choice.
pub async fn ask_for_credentials(
    previous: Option<account::Secret>,
    methods: EnumSet<account::AuthMethod>,
    message: &str,
) -> Option<account::Secret> {
    // TODO: Retrieve the window some other way
    let window: Option<gtk::Window> = match gio::Application::default() {
        Some(app) => match app.downcast::<gtk::Application>() {
            Ok(app) => {
                let windows = app.windows();
                windows
                    .first()
                    .cloned()
                    .and_then(|window| window.downcast().ok())
            }
            Err(_) => None,
        },
        _ => None,
    };

    if methods.is_subset(account::AuthMethod::PASSWORD) {
        // Use the password dialog if we only support password based methods
        let username = previous.and_then(|s| {
            if let account::Secret::Basic(basic) = s {
                Some(basic.user)
            } else {
                None
            }
        });

        let dialog = PasswordDialog::new(message, username.as_deref());

        if let Some(window) = window {
            dialog.secret_future(&window).await
        } else {
            // TODO: Ask credentials in notification that will open in the main window once clicked
            let notification = gio::Notification::new(&gettext("Credentials Required"));
            EnvelopeApplication::default().send_notification(None, &notification);
            None
        }
    } else {
        // TODO: Support more complex authentication methods like OAUTH2
        todo!()
    }
}
