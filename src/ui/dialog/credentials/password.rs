// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use envelib::account;

mod imp {
    use std::cell::RefCell;

    use gtk::subclass::widget::WidgetImpl;

    use super::*;

    #[derive(Default, gtk::CompositeTemplate)]
    #[template(file = "password.ui")]
    pub struct PasswordDialog {
        #[template_child]
        pub(super) username_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub(super) password_entry: TemplateChild<gtk::PasswordEntry>,
        pub(super) credentials: RefCell<Option<account::Secret>>,
        pub(super) secret_notify: gio::Cancellable,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PasswordDialog {
        const NAME: &'static str = "EnvelopePasswordDialog";
        type Type = super::PasswordDialog;
        type ParentType = adw::AlertDialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PasswordDialog {}
    impl WidgetImpl for PasswordDialog {}
    impl AdwDialogImpl for PasswordDialog {}
    impl AdwAlertDialogImpl for PasswordDialog {}

    impl PasswordDialog {}
}

glib::wrapper! {
    pub struct PasswordDialog(ObjectSubclass<imp::PasswordDialog>)
    @extends gtk::Widget, adw::Dialog, adw::AlertDialog,
    @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl PasswordDialog {
    pub fn new(message: &str, username: Option<&str>) -> Self {
        let dialog: PasswordDialog = glib::Object::new();
        dialog.set_body(message);
        dialog
            .imp()
            .username_entry
            .set_text(username.unwrap_or_default());

        if username.is_some() {
            dialog.imp().password_entry.grab_focus();
        }

        dialog
    }

    pub async fn secret_future(
        &self,
        transient_for: &impl IsA<gtk::Widget>,
    ) -> Option<account::Secret> {
        let response = self.clone().choose_future(transient_for).await;

        match &*response {
            "continue" => {
                let res = self.imp().credentials.borrow().clone();
                let secret = if let Some(credentials) = res {
                    self.imp().secret_notify.future().await;
                    credentials
                } else {
                    let secret = account::Secret::new_basic(
                        self.imp().username_entry.text().to_string(),
                        zeroize::Zeroizing::new(self.imp().password_entry.text().to_string()),
                    );

                    self.imp().credentials.replace(Some(secret.clone()));
                    self.imp().secret_notify.cancel();
                    secret
                };

                Some(secret)
            }
            _ => None,
        }
    }
}
