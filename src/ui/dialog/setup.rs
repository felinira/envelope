// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use envelib::gettext::*;
use gtk::glib;

mod imp {
    use std::cell::RefCell;

    use envelib::account::{self, TlsStrategy};
    use gtk::CompositeTemplate;
    use zeroize::Zeroizing;

    use crate::{
        controller::Controller,
        error::HandleAppResult,
        ui::{
            application::EnvelopeApplication,
            dialog::{page::DialogPage, spinner::SpinnerPage},
        },
    };

    use super::*;

    #[derive(CompositeTemplate, Default)]
    #[template(file = "setup.ui")]
    pub struct SetupDialog {
        // Navigation
        #[template_child]
        navigation_view: TemplateChild<adw::NavigationView>,
        #[template_child]
        devel_page: TemplateChild<DialogPage>,
        #[template_child]
        alias_page: TemplateChild<DialogPage>,
        #[template_child]
        imap_page: TemplateChild<DialogPage>,
        #[template_child]
        smtp_page: TemplateChild<DialogPage>,
        #[template_child]
        spinner_page: TemplateChild<SpinnerPage>,

        // Alias
        #[template_child]
        email_address_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        full_name_entry: TemplateChild<adw::EntryRow>,

        // IMAP
        #[template_child]
        imap_server_hostname_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        imap_username_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        imap_password_entry: TemplateChild<adw::PasswordEntryRow>,
        #[template_child]
        imap_encryption_settings: TemplateChild<adw::ComboRow>,

        // SMTP
        #[template_child]
        smtp_server_hostname_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        smtp_username_entry: TemplateChild<adw::EntryRow>,
        #[template_child]
        smtp_password_entry: TemplateChild<adw::PasswordEntryRow>,
        #[template_child]
        smtp_encryption_settings: TemplateChild<adw::ComboRow>,

        // Misc
        alias: RefCell<Option<account::Alias>>,
        imap_service: RefCell<Option<account::Service>>,
        smtp_service: RefCell<Option<account::Service>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SetupDialog {
        const NAME: &'static str = "EnvelopeSetupDialog";
        type Type = super::SetupDialog;
        type ParentType = adw::Dialog;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for SetupDialog {
        fn constructed(&self) {
            self.parent_constructed();

            if !cfg!(feature = "devel") {
                self.navigation_view.remove(&*self.devel_page);
            }
        }
    }

    impl WidgetImpl for SetupDialog {}
    impl WindowImpl for SetupDialog {}
    impl AdwDialogImpl for SetupDialog {}

    #[gtk::template_callbacks]
    impl SetupDialog {
        #[template_callback]
        fn on_test_server(&self) {
            let account = envelib::account::Account::new_test_server();
            // TODO save config
            EnvelopeApplication::default()
                .main_window()
                .setup_account(account);
            self.obj().force_close();
        }

        #[template_callback]
        fn on_regular_setup(&self) {
            self.navigation_view.push(&*self.alias_page);
        }

        #[template_callback]
        fn on_alias_next(&self) {
            let addr = self.email_address_entry.text().parse();
            let name = self.full_name_entry.text().parse();

            let alias = match (addr, name) {
                (Ok(addr), Ok(name)) => account::Alias::new(addr, Some(name)),
                (_, Err(err)) => {
                    self.show_error(&gettext("Name Invalid"), Some(&err.to_string()));
                    return;
                }
                (Err(err), _) => {
                    self.show_error(&gettext("Email Address Invalid"), Some(&err.to_string()));

                    return;
                }
            };

            if self.imap_username_entry.text().is_empty() {
                self.imap_username_entry
                    .set_text(&alias.address.to_string())
            }

            self.alias.replace(Some(alias));

            self.navigation_view.get().push(&*self.imap_page);
        }

        fn new_service(
            &self,
            kind: account::ServiceType,
            hostname: &str,
            tls: u32,
            username: &str,
            password: &str,
        ) -> Option<account::Service> {
            let host = match url::Host::parse(hostname) {
                Ok(host) => host,
                Err(err) => {
                    self.show_error(&gettext("Invalid Host"), Some(&err.to_string()));
                    return None;
                }
            };

            if username.is_empty() {
                self.show_error(
                    &gettext("Invalid Username"),
                    Some(&gettext("Username must not be empty")),
                );
                return None;
            };

            if password.is_empty() {
                self.show_error(
                    &gettext("Invalid Password"),
                    Some(&gettext("Password must not be empty")),
                );
                return None;
            };

            let tls_strategy = match tls {
                0 => TlsStrategy::Tls,
                1 => TlsStrategy::StartTls,
                _ => {
                    self.show_error(&gettext("Invalid Encryption Settings"), None);
                    return None;
                }
            };

            let secret = account::Secret::new_basic(
                username.to_string(),
                Zeroizing::new(password.to_string()),
            );

            Some(account::Service::new_basic(
                kind,
                host,
                tls_strategy,
                secret,
            ))
        }

        #[template_callback]
        fn on_imap_next(&self) {
            let hostname = self.imap_server_hostname_entry.text();
            let tls = self.imap_encryption_settings.selected();
            let username = self.imap_username_entry.text();
            let password = self.imap_password_entry.text();

            if let Some(service) = self.new_service(
                account::ServiceType::Imap,
                &hostname,
                tls,
                &username,
                &password,
            ) {
                self.imap_service.replace(Some(service));

                // Try to guess SMTP settings
                if self.smtp_server_hostname_entry.text().is_empty() {
                    if let Some(name) = hostname.strip_prefix("imap.") {
                        self.smtp_server_hostname_entry
                            .set_text(&format!("smtp.{}", name));
                    } else {
                        self.smtp_server_hostname_entry.set_text(&hostname);
                    }
                }

                if self.smtp_username_entry.text().is_empty() {
                    self.smtp_username_entry.set_text(&username);
                }

                if self.smtp_password_entry.text().is_empty() {
                    self.smtp_password_entry.set_text(&password);
                }

                self.navigation_view.get().push(&*self.smtp_page);
            }
        }

        #[template_callback]
        fn on_smtp_next(&self) {
            let hostname = self.smtp_server_hostname_entry.text();
            let tls = self.smtp_encryption_settings.selected();
            let username = self.smtp_username_entry.text();
            let password = self.smtp_password_entry.text();

            if let Some(service) = self.new_service(
                account::ServiceType::Smtp,
                &hostname,
                tls,
                &username,
                &password,
            ) {
                self.smtp_service.replace(Some(service));

                let imp = self.ref_counted();
                glib::spawn_future_local(async move {
                    imp.connection_test().await;
                });
            }
        }

        fn show_error(&self, title: &str, error: Option<&str>) {
            let dialog = adw::AlertDialog::new(Some(title), error);
            dialog.add_response("close", &gettext("Close"));
            dialog.set_response_appearance("close", adw::ResponseAppearance::Default);
            dialog.present(Some(&*self.obj()));
        }

        pub async fn connection_test(&self) {
            let Some(alias) = self.alias.borrow().clone() else {
                self.navigation_view.pop_to_page(&*self.alias_page);
                return;
            };

            let Some(imap_service) = self.imap_service.borrow().clone() else {
                self.navigation_view.pop_to_page(&*self.imap_page);
                return;
            };

            let Some(smtp_service) = self.smtp_service.borrow().clone() else {
                return;
            };

            self.navigation_view.get().push(&*self.spinner_page);

            let account = account::Account::new(
                alias,
                [
                    (account::ServiceType::Imap, imap_service),
                    (account::ServiceType::Smtp, smtp_service),
                ],
            );

            // TODO implement better isolated connection test
            let controller = Controller::new(EnvelopeApplication::default(), account.clone());
            match controller
                .connection_test(std::time::Duration::from_secs(20))
                .await
            {
                Ok(()) => {
                    log::info!("Created account for {}", &account.identity.address);
                    log::debug!("{:?}", account);
                    match EnvelopeApplication::default().add_account(account).await {
                        Ok(()) => {
                            controller.start().await.handle_app_result();
                            self.obj().force_close();
                        }
                        Err(err) => {
                            log::error!("{}", err.to_string());
                            self.show_error(
                                &gettext("Error Saving Secret"),
                                Some(&err.to_string()),
                            );
                            self.navigation_view.pop_to_page(&*self.alias_page);
                        }
                    }
                }
                Err(err) => {
                    self.show_error(&gettext("Connection Error"), Some(&err.to_string()));
                    self.navigation_view.pop_to_page(&*self.alias_page);
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct SetupDialog(ObjectSubclass<imp::SetupDialog>)
        @extends gtk::Widget, adw::Dialog,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl SetupDialog {
    pub fn new(can_close: bool) -> Self {
        glib::Object::builder()
            .property("can-close", can_close)
            .build()
    }
}
