// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use glib::prelude::*;
use glib::subclass::prelude::*;

mod imp {
    use gtk::subclass::sorter::SorterImpl;

    use super::*;
    use std::cell::{Cell, OnceCell};

    type SortFn = dyn Fn(&glib::Object, &glib::Object) -> std::cmp::Ordering;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::DynamicSorter)]
    pub struct DynamicSorter {
        pub(super) sort_fn: OnceCell<Box<SortFn>>,

        #[property(get, set)]
        is_reverse: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DynamicSorter {
        const NAME: &'static str = "EnvelopeDynamicSorter";
        type Type = super::DynamicSorter;
        type ParentType = gtk::Sorter;
    }

    #[glib::derived_properties]
    impl ObjectImpl for DynamicSorter {}

    impl SorterImpl for DynamicSorter {
        fn compare(&self, item1: &glib::Object, item2: &glib::Object) -> gtk::Ordering {
            let result = (self.sort_fn.get().expect("sort_fn must be set"))(item1, item2);
            if self.is_reverse.get() {
                result.reverse().into()
            } else {
                result.into()
            }
        }

        fn order(&self) -> gtk::SorterOrder {
            gtk::SorterOrder::Total
        }
    }

    impl DynamicSorter {}
}

glib::wrapper! {
    pub struct DynamicSorter(ObjectSubclass<imp::DynamicSorter>)
    @extends gtk::Sorter;
}

impl DynamicSorter {
    pub fn with_obj_sorter(
        sort_fn: impl Fn(&glib::Object, &glib::Object) -> std::cmp::Ordering + 'static,
    ) -> Self {
        let sorter: Self = glib::Object::new();
        let _ = sorter.imp().sort_fn.set(Box::new(sort_fn));
        sorter
    }

    pub fn with_sorter<T: ObjectType>(sort_fn: fn(&T, &T) -> std::cmp::Ordering) -> Self {
        Self::with_obj_sorter(move |obj1: &glib::Object, obj2: &glib::Object| {
            if let (Some(obj1), Some(obj2)) =
                (obj1.dynamic_cast_ref::<T>(), obj2.dynamic_cast_ref::<T>())
            {
                sort_fn(obj1, obj2)
            } else {
                std::cmp::Ordering::Equal
            }
        })
    }

    pub fn reverse(self) -> Self {
        self.set_is_reverse(true);
        self
    }
}
