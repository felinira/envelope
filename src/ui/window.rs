// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use gtk::gio;

use super::application::EnvelopeApplication;

mod imp {
    use std::cell::RefCell;

    use super::*;

    use crate::controller::Controller;
    use crate::error::{AppError, AppResult, HandleError};
    use crate::ui::dialog::{alpha_warning, setup};
    use crate::ui::mailbox::MailboxPage;
    use crate::ui::message::MessagePage;
    use crate::ui::thread::ThreadPage;
    use crate::ui::utils::Async;
    use envelib::account::AccountId;
    use envelib::model::{self, IndexStoreExt};

    #[derive(Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::EnvelopeApplicationWindow)]
    #[template(file = "window.ui")]
    pub struct EnvelopeApplicationWindow {
        // Split views
        #[template_child]
        pub outer_view: TemplateChild<adw::NavigationSplitView>,
        #[template_child]
        pub inner_view: TemplateChild<adw::NavigationSplitView>,

        // Column 1: Mailbox List
        #[template_child]
        mailbox_page: TemplateChild<MailboxPage>,

        // Column 2: Thread List
        #[template_child]
        thread_page: TemplateChild<ThreadPage>,

        // Column 3: Message Display
        #[template_child]
        message_page: TemplateChild<MessagePage>,

        // Misc
        #[property(get)]
        pub account_id: RefCell<Option<AccountId>>,
        #[property(get, set)]
        controller: RefCell<Option<Controller>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for EnvelopeApplicationWindow {
        const NAME: &'static str = "EnvelopeApplicationWindow";
        type Type = super::EnvelopeApplicationWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for EnvelopeApplicationWindow {
        fn dispose(&self) {
            self.dispose_template();
        }

        fn properties() -> &'static [glib::ParamSpec] {
            Self::derived_properties()
        }

        fn set_property(&self, id: usize, value: &glib::Value, pspec: &glib::ParamSpec) {
            self.derived_set_property(id, value, pspec)
        }
        fn property(&self, id: usize, pspec: &glib::ParamSpec) -> glib::Value {
            self.derived_property(id, pspec)
        }
    }

    impl WidgetImpl for EnvelopeApplicationWindow {
        fn map(&self) {
            // Load latest window state
            self.load_window_size();

            self.parent_map();

            let config = self.app().config();

            // Show the dialog every 120 days
            if config.alpha_warning_seen_date.is_none_or(|date| {
                date + std::time::Duration::from_secs(60 * 60 * 24 * 120)
                    < std::time::SystemTime::now()
            }) {
                let dialog = alpha_warning::AlphaWarningDialog::new();
                dialog.present(Some(&*self.obj()));

                dialog.connect_closed(glib::clone!(
                    #[strong(rename_to = imp)]
                    self.ref_counted(),
                    move |_| {
                        imp.app().config_update(|config| {
                            config.alpha_warning_seen_date = Some(std::time::SystemTime::now());
                        });
                        imp.maybe_run_setup();
                    }
                ));
            } else {
                self.maybe_run_setup();
            }
        }
    }

    impl WindowImpl for EnvelopeApplicationWindow {
        // Save window state on close
        fn close_request(&self) -> glib::Propagation {
            if let Err(err) = self.save_window_size() {
                tracing::warn!("Failed to save window state, {}", &err);
            }

            if let Some(controller) = &*self.controller.borrow() {
                controller.shutdown();
            }

            // Pass close request on to the parent
            self.parent_close_request()
        }
    }

    impl ApplicationWindowImpl for EnvelopeApplicationWindow {}
    impl AdwApplicationWindowImpl for EnvelopeApplicationWindow {}

    #[gtk::template_callbacks]
    impl EnvelopeApplicationWindow {
        pub fn app(&self) -> EnvelopeApplication {
            self.obj()
                .application()
                .expect("Window should have an application")
                .downcast()
                .expect("Application must be of type EnvelopeApplicationWindow")
        }

        fn controller(&self) -> AppResult<Controller> {
            self.controller.borrow().clone().ok_or(AppError::NoAccount)
        }

        fn maybe_run_setup(&self) {
            let config = self.app().config();

            if let Some(account) = config.accounts().first().cloned() {
                self.setup_account(account);
            } else {
                let setup = setup::SetupDialog::new(false);
                setup.present(Some(&*self.obj()));
            }
        }

        pub(super) fn setup_account(&self, account: envelib::account::Account) {
            self.obj().notify_account_id();

            // TODO: Don't block in async init
            let controller = Controller::new(self.app(), account);

            Async::spawn_local(glib::clone!(
                #[strong(rename_to = imp)]
                self.ref_counted(),
                async move {
                    controller.start().await?;

                    imp.controller.replace(Some(controller.clone()));
                    imp.obj().notify_controller();

                    Ok(())
                }
            ));
        }

        #[template_callback]
        fn on_mailbox_activated(&self, mailbox: model::Mailbox) {
            self.inner_view.set_show_content(true);
            self.set_selected_mailbox(Some(mailbox));
        }

        fn set_selected_mailbox(&self, mailbox: Option<model::Mailbox>) {
            let Some(controller) = self.controller().handle() else {
                return;
            };

            if controller.open_mailbox() == mailbox {
                return;
            }

            controller.set_open_mailbox(mailbox.as_ref());
        }

        #[template_callback]
        fn on_thread_activated(&self, _thread: model::Thread) {
            self.outer_view.set_show_content(true);
        }

        fn save_window_size(&self) -> Result<(), glib::BoolError> {
            let (width, height) = self.obj().default_size();

            let app = self.app();

            app.config_update(|config| {
                config.window.width = width;
                config.window.height = height;
                config.window.is_maximized = self.obj().is_maximized();
            });

            Ok(())
        }

        fn load_window_size(&self) {
            let app = self.app();
            let config = app.config();

            self.obj()
                .set_default_size(config.window.width, config.window.height);
            if config.window.is_maximized {
                self.obj().maximize();
            }
        }

        fn selected_mailbox(&self) -> Option<model::Mailbox> {
            self.controller
                .borrow()
                .as_ref()
                .and_then(|c| c.open_mailbox())
        }

        fn selected_thread(&self) -> Option<model::Thread> {
            self.thread_page.selected_threads().first()
        }

        #[template_callback]
        fn two_pane_apply_cb(&self) {
            if self.outer_view.shows_content() {
                self.inner_view.set_show_content(true);
            }
        }

        #[template_callback]
        fn on_notify_outer_show_content(&self) {
            if !self.outer_view.shows_content() {
                // We have changed from outer shows content to outer does not show content
                self.thread_page.navigated_back();
            }
        }

        #[template_callback]
        fn on_notify_inner_show_content(&self) {
            if !self.outer_view.shows_content() {
                // We have changed from inner shows content to inner does not show content
                self.mailbox_page.navigated_back();
            }
        }

        #[template_callback]
        fn on_notify_outer_collapsed(&self) {
            if self.outer_view.is_collapsed() {
                // We collapsed and now the outer sidebar is showing. The inner view collapses *after*
                // the outer view collapses. Therefore the inner view will never be collapsed here.

                if !self.outer_view.shows_content() {
                    // We navigated back to the outer view. It is not collapsed, therefore the thread page is showing.
                    self.thread_page.navigated_back();
                }
            }
        }

        #[template_callback]
        fn on_notify_inner_collapsed(&self) {
            if self.inner_view.is_collapsed() && !self.inner_view.shows_content() {
                // Navigated back to mailbox page
                self.mailbox_page.navigated_back();
            }
        }
    }
}

glib::wrapper! {
    pub struct EnvelopeApplicationWindow(ObjectSubclass<imp::EnvelopeApplicationWindow>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, adw::ApplicationWindow,
        @implements gio::ActionMap, gio::ActionGroup, gtk::Root;
}

impl EnvelopeApplicationWindow {
    pub fn new(app: &EnvelopeApplication) -> Self {
        glib::Object::builder().property("application", app).build()
    }

    pub fn app(&self) -> EnvelopeApplication {
        self.imp().app()
    }

    pub fn setup_account(&self, account: envelib::account::Account) {
        self.imp().setup_account(account);
    }
}
