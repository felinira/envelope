// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use envelib::gettext::*;
use envelib::model::MessageStore;

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        collections::BTreeMap,
        sync::LazyLock,
    };

    use envelib::{
        model::{self, IndexStoreExt, MailboxExtProps, MessageStore, ThreadStore},
        service::incoming::IncomingMailServiceExt,
    };
    use glib::subclass::Signal;

    use crate::{
        controller::Controller,
        ui::{list::sorter::DynamicSorter, thread::ThreadRow},
    };

    use super::*;

    #[derive(Default, glib::Properties, gtk::CompositeTemplate)]
    #[template(file = "page.ui")]
    #[properties(wrapper_type = super::ThreadPage)]
    pub struct ThreadPage {
        #[template_child]
        header_bar: TemplateChild<adw::HeaderBar>,
        #[template_child]
        pub thread_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub thread_page_loading: TemplateChild<adw::StatusPage>,
        #[template_child]
        thread_page_disconnected: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub thread_page_empty: TemplateChild<adw::StatusPage>,
        #[template_child]
        pub thread_page_threads: TemplateChild<gtk::ScrolledWindow>,

        #[template_child]
        pub thread_list: TemplateChild<gtk::ListView>,
        #[template_child]
        pub thread_search_bar: TemplateChild<gtk::SearchBar>,
        #[template_child]
        pub thread_search_entry: TemplateChild<gtk::SearchEntry>,
        #[template_child]
        pub thread_sort_list_model: TemplateChild<gtk::SortListModel>,
        #[template_child]
        pub thread_list_multi_selection: TemplateChild<gtk::MultiSelection>,

        #[property(get, set = Self::set_controller, nullable)]
        controller: RefCell<Option<Controller>>,
        #[property(get, set = Self::set_mailbox, nullable)]
        mailbox: RefCell<Option<model::Mailbox>>,
        #[property(get, set = Self::set_single_pane_layout)]
        single_pane_layout: Cell<bool>,
        #[property(get, set = Self::set_collapsed)]
        collapsed: Cell<bool>,
        allow_empty_selection: Cell<bool>,

        pub thread_store: RefCell<Option<ThreadStore>>,

        #[property(get = Self::selected_threads, construct_only)]
        selected_threads: OnceCell<model::ThreadStore>,
        selected_thread_indexes: RefCell<Option<gtk::Bitset>>,

        #[property(get)]
        active_thread: RefCell<Option<model::Thread>>,

        thread_selection_model_last_insertion: Cell<u32>,
        thread_list_hiding_selected_position: Cell<Option<u32>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThreadPage {
        const NAME: &'static str = "EnvelopeThreadPage";
        type Type = super::ThreadPage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ThreadPage {
        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("thread-activated")
                        .param_types([model::Thread::static_type()])
                        .build(),
                    Signal::builder("thread-selection-changed")
                        .param_types([model::ThreadStore::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.parent_constructed();
            self.update_title();

            self.thread_search_bar
                .connect_entry(&*self.thread_search_entry);

            self.thread_sort_list_model.set_sorter(Some(
                &DynamicSorter::with_sorter(model::Thread::cmp_date).reverse(),
            ));
            self.thread_sort_list_model
                .connect_items_changed(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_model, _pos, _remove, _mod| {
                        imp.update_thread_list(false);
                    }
                ));
            self.thread_selection_model_last_insertion
                .set(gtk::INVALID_LIST_POSITION);
            self.thread_list_multi_selection
                .connect_items_changed(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_model, position, added, removed| {
                        if added > 0 {
                            imp.thread_selection_model_last_insertion.set(position);
                        } else if removed > 0 {
                            imp.thread_selection_model_last_insertion
                                .set(gtk::INVALID_LIST_POSITION);
                        }
                    }
                ));
        }
    }

    impl WidgetImpl for ThreadPage {}
    impl NavigationPageImpl for ThreadPage {}

    #[gtk::template_callbacks]
    impl ThreadPage {
        fn emit_thread_activated(&self, thread: &model::Thread) {
            self.obj().emit_by_name::<()>("thread-activated", &[thread]);
        }

        fn emit_thread_selection_changed(&self, thread_store: &model::ThreadStore) {
            self.obj()
                .emit_by_name::<()>("thread-selection-changed", &[thread_store]);
        }

        fn set_controller(&self, controller: Option<Controller>) {
            if self.controller.replace(controller.clone()) != controller {
                self.obj()
                    .set_mailbox(controller.as_ref().and_then(|c| c.open_mailbox()).as_ref());

                if let Some(service) = &controller.map(|c| c.incoming_service()) {
                    // TODO move these signals to controller
                    service.connect_message_fetch_complete(glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |_, mailbox| imp.event_message_fetch_complete(mailbox)
                    ));
                }
            }
        }

        fn set_single_pane_layout(&self, collapsed: bool) {
            self.single_pane_layout.set(collapsed);
        }

        fn set_collapsed(&self, collapsed: bool) {
            self.collapsed.set(collapsed);

            if !collapsed {
                // The outer view was uncollapsed. We need to apply the selection again to the now visible message pane
                if self.thread_list_multi_selection.selection().size() == 0 {
                    let set = self.selected_thread_indexes.borrow().clone();
                    if let Some(set) = set {
                        self.thread_list_multi_selection.set_selection(&set, &set);
                    }
                }
            }
        }

        fn event_message_fetch_complete(&self, mailbox: model::Mailbox) {
            log::debug!("Message fetch complete");

            if self.mailbox.borrow().as_ref().map(|m| m.id()) == Some(mailbox.id()) {
                self.update_thread_list(true);
            }
        }

        fn selected_threads(&self) -> model::ThreadStore {
            self.selected_threads
                .get_or_init(model::ThreadStore::new)
                .clone()
        }

        fn set_mailbox(&self, mailbox: Option<model::Mailbox>) {
            if self.mailbox.replace(mailbox.clone()) != mailbox {
                let store = mailbox.map(|m| m.threads());
                self.thread_sort_list_model.set_model(store.as_ref());
                self.thread_store.replace(store);
                self.allow_empty_selection.set(true);
                self.update_title();
                self.selected_threads().remove_all();
            }
        }

        fn update_title(&self) {
            if let Some(mailbox) = &*self.mailbox.borrow() {
                self.obj().set_title(&mailbox.localized_name());
                self.header_bar.set_show_title(true);
            } else {
                self.obj().set_title(&gettext("No Folder"));
                self.header_bar.set_show_title(false);
            }
        }

        /// Update the visibility of the thread list based on whether any threads are showing
        fn update_thread_list(&self, fetch_complete: bool) {
            if let Some(store) = &*self.thread_store.borrow() {
                if store.n_items() == 0 {
                    if fetch_complete && store.finished_loading() {
                        // This mailbox is empty
                        self.thread_stack
                            .set_visible_child(&*self.thread_page_empty);
                    } else if self
                        .controller
                        .borrow()
                        .as_ref()
                        .is_some_and(|c| c.connection_active())
                    {
                        // We are still waiting for server data, and we have nothing cached
                        self.thread_stack
                            .set_visible_child(&*self.thread_page_loading);
                    } else {
                        // We have nothing cached, but we are disconnected
                        self.thread_stack
                            .set_visible_child(&*self.thread_page_disconnected);
                    }
                } else {
                    // There is at least one thread in the store
                    self.thread_stack
                        .set_visible_child(&*self.thread_page_threads);
                }
            } else {
                // We don't have a mailbox selected
                self.thread_stack
                    .set_visible_child(&*self.thread_page_loading);
            }
        }

        pub(super) fn all_selected_messages(&self) -> MessageStore {
            let store = MessageStore::new();
            store.extend(
                self.selected_threads()
                    .values()
                    .flat_map(|t| t.messages().into_iter().into_owned()),
            );
            store
        }

        /// Tries to scroll to a message.
        ///
        /// First we check whether the last inserted message position contains the message.
        ///
        /// If this is not found, we iterate through the selection model.
        /// This method will give up after 500 messages from each end point to not stall the UI
        fn scroll_to(&self, thread: &model::Thread) {
            let last_insertion = self.thread_selection_model_last_insertion.get();
            let pos = if last_insertion != gtk::INVALID_LIST_POSITION
                && self
                    .thread_list_multi_selection
                    .item(last_insertion)
                    .and_downcast_ref::<model::Thread>()
                    .is_some_and(|item| item.thread_id() == thread.thread_id())
            {
                last_insertion
            } else {
                let mut pos = gtk::INVALID_LIST_POSITION;

                for (idx, item) in self
                    .thread_list_multi_selection
                    .iter::<glib::Object>()
                    .enumerate()
                    .take(500)
                {
                    if item
                        .ok()
                        .and_downcast_ref::<model::Thread>()
                        .is_some_and(|item| item.thread_id() == thread.thread_id())
                    {
                        pos = idx as u32;
                        break;
                    }
                }

                if pos == gtk::INVALID_LIST_POSITION {
                    for (idx, item) in self
                        .thread_list_multi_selection
                        .iter::<glib::Object>()
                        .rev()
                        .enumerate()
                        .take(500)
                    {
                        if item
                            .ok()
                            .and_downcast_ref::<model::Thread>()
                            .is_some_and(|item| item.thread_id() == thread.thread_id())
                        {
                            pos = idx as u32;
                            break;
                        }
                    }
                }

                pos
            };

            if pos != gtk::INVALID_LIST_POSITION {
                self.thread_list
                    .scroll_to(pos, gtk::ListScrollFlags::NONE, None);
            }
        }

        #[template_callback]
        fn on_thread_row_activated(&self, position: u32) {
            let thread = self
                .thread_list_multi_selection
                .item(position)
                .and_then(|item| item.downcast::<model::Thread>().ok());

            if let Some(thread) = thread {
                log::debug!("Activated thread: {}", thread.thread_id());
                self.emit_thread_activated(&thread);
            }
        }

        #[template_callback]
        fn thread_selection_changed(&self, _first_position: u32, _n_items: u32) {
            self.active_thread.replace(None);

            let mut selected_threads = BTreeMap::new();
            let selection = self.thread_list_multi_selection.selection();

            for idx in 0..selection.size() {
                if let Some(thread) = self
                    .thread_list_multi_selection
                    .item(selection.nth(idx as u32))
                    .and_then(|item| item.downcast::<model::Thread>().ok())
                {
                    selected_threads.insert(thread.thread_id(), thread);
                }
            }

            // We only update the externally exposed selection if it was
            // actually issued by the user. This means we never allow to
            // unselect the last thread. The only way to get an unselected
            // model is to change the mailbox or have an empty list view.
            if self
                .thread_store
                .borrow()
                .as_ref()
                .is_some_and(|s| s.is_empty())
                || self.allow_empty_selection.get()
                || !selected_threads.is_empty()
            {
                self.allow_empty_selection.set(false);

                self.selected_thread_indexes.replace(Some(selection.copy()));

                let model = self.selected_threads();
                for thread_id in model.keys() {
                    if !selected_threads.contains_key(&thread_id) {
                        // Remove from the model
                        model.remove(thread_id);
                    }
                }

                log::debug!(
                    "Selected threads: {:?}",
                    selected_threads.keys().collect::<Vec<_>>()
                );

                model.extend(selected_threads);

                self.emit_thread_selection_changed(&model);

                if model.len() == 1 {
                    if let Some(thread) = model.first() {
                        self.active_thread.replace(Some(thread.clone()));
                        self.emit_thread_activated(&thread);
                    }
                }
            }
        }

        #[template_callback]
        fn thread_list_setup(&self, item: gtk::ListItem, _factory: gtk::SignalListItemFactory) {
            let row = ThreadRow::new();
            row.connect_drag_find_adjacent_selected_threads(glib::clone!(
                #[weak(rename_to = imp)]
                self,
                #[upgrade_or_default]
                move |row| {
                    let their_thread = row.thread();
                    let messages: MessageStore = imp
                        .selected_threads()
                        .values()
                        .filter(|t| Some(t) != their_thread.as_ref())
                        .flat_map(|t| t.messages().into_iter().into_owned())
                        .collect();

                    messages
                }
            ));

            item.set_child(Some(&row));
        }

        #[template_callback]
        fn thread_list_bind(&self, item: gtk::ListItem, _factory: gtk::SignalListItemFactory) {
            let Some(Ok(thread)) = item.item().map(|item| item.downcast::<model::Thread>()) else {
                return;
            };

            let Some(Ok(thread_row)) = item.child().map(|item| item.downcast::<ThreadRow>()) else {
                return;
            };

            let Some(controller) = &*self.controller.borrow() else {
                return;
            };

            controller.mark_thread_visible(&thread, true);
            thread_row.set_thread(Some(thread.clone()));
        }

        #[template_callback]
        fn thread_list_unbind(&self, item: gtk::ListItem, _factory: gtk::SignalListItemFactory) {
            let Some(Ok(thread_row)) = item.child().map(|item| item.downcast::<ThreadRow>()) else {
                return;
            };

            thread_row.set_thread(None::<model::Thread>);

            let Some(Ok(thread)) = item.item().map(|item| item.downcast::<model::Thread>()) else {
                return;
            };

            if let Some(controller) = &*self.controller.borrow() {
                // Don't show an error, this can happen when closing the window
                controller.mark_thread_visible(&thread, false)
            }
        }

        /// Scrolls to the top of the message list
        pub(super) fn scroll_to_top(&self) {
            let flags = if self.selected_threads().n_items() == 0 {
                gtk::ListScrollFlags::NONE
            } else {
                gtk::ListScrollFlags::SELECT
            };

            self.thread_list.scroll_to(0, flags, None);
        }

        pub(super) fn navigated_back(&self) {
            if !self.selected_threads().is_empty() {
                self.thread_list_multi_selection.unselect_all();
            }
        }
    }
}

glib::wrapper! {
    pub struct ThreadPage(ObjectSubclass<imp::ThreadPage>)
    @extends adw::NavigationPage, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl ThreadPage {
    fn new() -> Self {
        glib::Object::new()
    }

    pub fn all_selected_messages(&self) -> MessageStore {
        self.imp().all_selected_messages()
    }

    pub fn scroll_to_top(&self) {
        self.imp().scroll_to_top();
    }

    pub fn navigated_back(&self) {
        self.imp().navigated_back();
    }
}
