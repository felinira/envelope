// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::SignalHandlerId;

use envelib::model::{MessageStore, Thread};

mod imp {

    use std::cell::{OnceCell, RefCell};

    use envelib::model::{IndexStoreExt, MessageStore};
    use glib::subclass::Signal;
    use gtk::gdk;
    use std::sync::LazyLock;

    use crate::ui::{
        components::{counter_label::CounterLabel, inscription::Inscription},
        message::MessageDragView,
    };

    use super::*;

    #[derive(Default, gtk::CompositeTemplate, glib::Properties)]
    #[template(file = "row.ui")]
    #[properties(wrapper_type = super::ThreadRow)]
    pub struct ThreadRow {
        #[property(get, set = Self::set_thread, nullable)]
        thread: RefCell<Option<Thread>>,
        thread_signal_group: OnceCell<glib::SignalGroup>,

        #[template_child]
        avatar: TemplateChild<adw::Avatar>,
        #[template_child]
        participants: TemplateChild<Inscription>,
        #[template_child]
        attachment_icon: TemplateChild<gtk::Image>,
        #[template_child]
        date: TemplateChild<gtk::Label>,
        #[template_child]
        subject: TemplateChild<Inscription>,
        #[template_child]
        body: TemplateChild<Inscription>,
        #[template_child]
        msg_count: TemplateChild<CounterLabel>,

        drag_source: gtk::DragSource,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThreadRow {
        const NAME: &'static str = "EnvelopeThreadRow";
        type Type = super::ThreadRow;
        type ParentType = gtk::Grid;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ThreadRow {
        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("drag-find-adjacent-selected-threads")
                        .return_type_from(MessageStore::static_type())
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj().clone();
            self.drag_source.connect_prepare(glib::clone!(
                #[weak]
                obj,
                #[upgrade_or_default]
                move |source, x, y| obj.imp().drag_prepare(source, x, y)
            ));
            self.drag_source
                .connect_drag_begin(MessageDragView::drag_begin);
            self.drag_source
                .set_actions(gdk::DragAction::COPY | gdk::DragAction::MOVE);

            self.obj().add_controller(self.drag_source.clone());
        }
    }
    impl WidgetImpl for ThreadRow {}
    impl GridImpl for ThreadRow {}

    impl ThreadRow {
        fn update_thread(&self, thread: Option<&Thread>) {
            if let Some(thread) = thread {
                let unread = thread.is_unread();
                self.participants.set_bold(thread.is_unread());
                self.subject.set_bold(thread.is_unread());
                self.msg_count.set_important(unread);

                let count = thread.message_count();
                self.msg_count.set_value(count);
                self.msg_count.set_visible(count > 1);

                self.avatar.set_text(thread.avatar_text().as_deref());
                self.participants.set_text(thread.participant_names());
                self.attachment_icon
                    .set_visible(thread.attachment_count() > 0);
                self.date
                    .set_label(thread.short_date_str().unwrap_or_default().as_str());
                self.subject.set_text(thread.thread_name());
                self.body.set_text(thread.preview_text());
            } else {
                self.participants.set_text(Some(" "));
                self.attachment_icon.set_visible(false);
                self.date.set_text("");
                self.subject.set_text(Some(" "));
                self.body.set_text(None::<String>);
                self.msg_count.set_visible(false);
            }
        }

        fn thread_signal_group(&self) -> &glib::SignalGroup {
            let obj = self.obj();
            self.thread_signal_group.get_or_init(glib::clone!(
                #[strong]
                obj,
                move || {
                    let group = glib::SignalGroup::with_type(Thread::static_type());
                    group.connect_closure(
                        "messages-changed",
                        false,
                        glib::closure_local!(
                            #[watch]
                            obj,
                            move |thread: Thread| {
                                obj.imp().update_thread(Some(&thread));
                            }
                        ),
                    );

                    group
                }
            ))
        }

        fn drag_prepare(
            &self,
            _source: &gtk::DragSource,
            _x: f64,
            _y: f64,
        ) -> Option<gdk::ContentProvider> {
            self.thread.borrow().as_ref().map(|thread| {
                let messages = self
                    .emit_drag_find_adjacent_selected_threads()
                    .unwrap_or_default();

                messages.extend(IndexStoreExt::iter(&thread.messages()));

                log::debug!(
                    "Dragging {} messages: {:?}",
                    messages.len(),
                    IndexStoreExt::iter(&messages)
                        .map(|(k, _m)| k)
                        .collect::<Vec<_>>(),
                );

                gdk::ContentProvider::for_value(&messages.to_value())
            })
        }

        pub(super) fn emit_drag_find_adjacent_selected_threads(&self) -> Option<MessageStore> {
            self.obj()
                .emit_by_name_with_values("drag-find-adjacent-selected-threads", &[])
                .and_then(|v| v.get::<MessageStore>().ok())
        }

        fn set_thread(&self, thread: Option<Thread>) {
            if thread != *self.thread.borrow() {
                self.update_thread(thread.as_ref());
                self.thread_signal_group().set_target(thread.as_ref());
                self.thread.replace(thread);
                self.obj().notify_thread();
            }
        }
    }
}

glib::wrapper! {
    pub struct ThreadRow(ObjectSubclass<imp::ThreadRow>)
    @extends gtk::Grid, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl ThreadRow {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn connect_drag_find_adjacent_selected_threads<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(Self) -> MessageStore + 'static,
    {
        self.connect_closure(
            "drag-find-adjacent-selected-threads",
            false,
            glib::closure_local!(|obj: Self| callback(obj)),
        )
    }
}
