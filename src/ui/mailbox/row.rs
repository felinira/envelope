// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use crate::controller::Controller;
use envelib::model::Mailbox;

mod imp {

    use std::cell::RefCell;

    use gtk::gdk;

    use envelib::model::{IndexStoreExt, Mailbox, MessageStore};

    use super::*;

    #[derive(Default, gtk::CompositeTemplate, glib::Properties)]
    #[template(file = "row.ui")]
    #[properties(wrapper_type = super::MailboxRow)]
    pub struct MailboxRow {
        #[template_child]
        content_box: TemplateChild<gtk::Box>,
        #[property(get, set)]
        controller: RefCell<Option<Controller>>,
        #[property(get, set, construct_only)]
        mailbox: RefCell<Option<Mailbox>>,
        drop_target: gtk::DropTarget,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MailboxRow {
        const NAME: &'static str = "EnvelopeMailboxRow";
        type Type = super::MailboxRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MailboxRow {
        fn constructed(&self) {
            self.parent_constructed();

            self.drop_target.set_types(&[MessageStore::static_type()]);
            self.drop_target
                .set_actions(gdk::DragAction::COPY | gdk::DragAction::MOVE);
            let obj = self.obj().clone();

            self.drop_target.connect_motion(move |_target, _x, _y| {
                // Select move as default action
                gdk::DragAction::MOVE
            });

            self.drop_target.connect_drop(glib::clone!(
                #[weak]
                obj,
                #[upgrade_or]
                false,
                move |target, value, x, y| obj.imp().drop(target, value, x, y)
            ));
            self.obj().add_controller(self.drop_target.clone());
        }
    }

    impl WidgetImpl for MailboxRow {}
    impl ListBoxRowImpl for MailboxRow {}

    impl MailboxRow {
        fn drop(&self, target: &gtk::DropTarget, value: &glib::Value, _x: f64, _y: f64) -> bool {
            if let Ok(messages) = value.get::<MessageStore>() {
                let Some(drag) = target.current_drop().and_then(|drop| drop.drag()) else {
                    return false;
                };

                if messages.is_empty() {
                    log::warn!("Empty drop");
                    return false;
                };

                let Some(mailbox) = self.obj().mailbox() else {
                    log::error!("Tried to drop on mailbox row with no mailbox");
                    return false;
                };

                let Some(controller) = &*self.controller.borrow() else {
                    log::error!("Controller unset");
                    return false;
                };

                let action = drag.selected_action();
                if action == gdk::DragAction::MOVE {
                    controller.move_to(messages.values().collect(), mailbox.into());
                    true
                } else if action == gdk::DragAction::COPY {
                    controller.copy_to(messages.values().collect(), mailbox.into());
                    true
                } else {
                    false
                }
            } else {
                false
            }
        }
    }
}

glib::wrapper! {
    pub struct MailboxRow(ObjectSubclass<imp::MailboxRow>)
    @extends gtk::ListBoxRow, gtk::Widget,
    @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl MailboxRow {
    pub fn new(controller: Controller, mailbox: &Mailbox) -> Self {
        glib::Object::builder()
            .property("controller", controller)
            .property("mailbox", mailbox)
            .build()
    }
}
