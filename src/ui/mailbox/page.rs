// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use super::row::MailboxRow;

mod imp {
    use std::{
        cell::{Cell, RefCell},
        sync::LazyLock,
    };

    use envelib::{
        gettext::*,
        model::{self, MailboxExt, MailboxExtProps},
        service::incoming::IncomingMailServiceExt,
        types::{ConnectionStatus, MailboxKind},
    };
    use glib::subclass::Signal;

    use crate::{
        controller::Controller,
        globals,
        ui::{list::sorter::DynamicSorter, task::TaskList},
    };

    use super::*;

    #[derive(Default, glib::Properties, gtk::CompositeTemplate)]
    #[template(file = "page.ui")]
    #[properties(wrapper_type = super::MailboxPage)]
    pub struct MailboxPage {
        #[template_child]
        mailbox_list: TemplateChild<gtk::ListBox>,
        mailbox_sort_list_model: gtk::SortListModel,

        // Connection banner
        #[template_child]
        connection_disconnected_banner: TemplateChild<adw::Banner>,
        #[template_child]
        connection_lost_banner: TemplateChild<adw::Banner>,

        // Task view
        #[template_child]
        task_list: TemplateChild<TaskList>,
        #[template_child]
        task_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        task_spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        task_done: TemplateChild<gtk::Image>,
        #[template_child]
        debug_label: TemplateChild<gtk::Label>,

        #[property(get, set = Self::set_controller)]
        controller: RefCell<Option<Controller>>,

        #[property(get, set, nullable)]
        selected_mailbox: RefCell<Option<model::Mailbox>>,
        #[property(get, set)]
        selected_mailbox_name: RefCell<String>,
        #[property(get, set)]
        mailbox_selected: Cell<bool>,
        #[property(get, set = Self::set_single_pane_layout)]
        single_pane_layout: Cell<bool>,

        selected_mailbox_list_row: RefCell<Option<gtk::ListBoxRow>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MailboxPage {
        const NAME: &'static str = "EnvelopeMailboxPage";
        type Type = super::MailboxPage;
        type ParentType = adw::NavigationPage;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MailboxPage {
        fn dispose(&self) {
            self.task_list.shutdown();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("mailbox-activated")
                        .param_types([model::Mailbox::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn constructed(&self) {
            self.parent_constructed();

            // Devel Profile
            if globals::DEBUG_BUILD {
                self.obj().add_css_class("devel");
                self.debug_label.set_visible(true);
            }

            // Mailbox list
            self.mailbox_sort_list_model
                .set_sorter(Some(&DynamicSorter::with_sorter(
                    model::Mailbox::cmp_kind_name,
                )));

            self.mailbox_list.bind_model(
                Some(&self.mailbox_sort_list_model),
                glib::clone!(
                    #[weak_allow_none(rename_to = imp)]
                    self.ref_counted(),
                    move |item| {
                        if let (Some(controller), Some(mailbox)) = (
                            imp.and_then(|imp| imp.controller.borrow().clone()),
                            item.downcast_ref::<model::Mailbox>(),
                        ) {
                            let mailbox_view = MailboxRow::new(controller, mailbox);
                            mailbox_view.upcast()
                        } else {
                            log::error!("Error creating MailboxRow");
                            adw::Bin::new().upcast()
                        }
                    }
                ),
            );
            self.mailbox_sort_list_model
                .connect_items_changed(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_, _, _, _| imp.mailbox_default_select_inbox()
                ));

            self.task_list.connect_task_running_notify(glib::clone!(
                #[weak(rename_to = imp)]
                self.ref_counted(),
                move |list| {
                    if list.task_running() {
                        imp.task_stack.set_visible_child(&*imp.task_spinner);
                    } else {
                        imp.task_stack.set_visible_child(&*imp.task_done);
                    }
                }
            ));
        }
    }

    impl WidgetImpl for MailboxPage {}
    impl NavigationPageImpl for MailboxPage {}

    #[gtk::template_callbacks]
    impl MailboxPage {
        pub(super) fn emit_mailbox_activated(&self, mailbox: &model::Mailbox) {
            self.obj()
                .emit_by_name::<()>("mailbox-activated", &[mailbox]);
        }

        fn set_selected_mailbox(&self, mailbox: Option<model::Mailbox>) {
            if let Some(mailbox) = &mailbox {
                self.obj()
                    .set_selected_mailbox_name(mailbox.localized_name());
            } else {
                self.obj()
                    .set_selected_mailbox_name(gettext("No Selection"));
            }

            self.selected_mailbox.replace(mailbox.clone());

            log::debug!("selected mailbox: {:?}", mailbox);
            if self.mailbox_list.selected_row().is_some() && mailbox.is_none() {
                self.mailbox_list.unselect_all()
            }

            self.obj().set_mailbox_selected(mailbox.is_some());
        }

        fn set_single_pane_layout(&self, single_pane_layout: bool) {
            if single_pane_layout {
                self.mailbox_list
                    .set_selection_mode(gtk::SelectionMode::None);
            } else {
                self.mailbox_list
                    .set_selection_mode(gtk::SelectionMode::Single);
                self.mailbox_list
                    .select_row(self.selected_mailbox_list_row.borrow().as_ref())
            }
        }

        fn set_controller(&self, controller: Option<Controller>) {
            if let Some(controller) = &controller {
                // TODO signal group and disconnect signals
                controller.connect_connection_status_notify(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |controller| {
                        match controller.connection_status() {
                            ConnectionStatus::Unreachable => {
                                imp.connection_lost_banner.set_revealed(true)
                            }
                            _ => imp.connection_lost_banner.set_revealed(false),
                        }
                    }
                ));
            }

            let service = controller.as_ref().map(|c| c.incoming_service());

            let _old_controller = self.controller.replace(controller);
            self.mailbox_sort_list_model
                .set_model(service.as_ref().map(|s| s.mailboxes()).as_ref());

            if let Some(service) = &service {
                // TODO move these signals to controller
                if globals::DEBUG_BUILD {
                    service.connect_connection_debug_info_notify(glib::clone!(
                        #[weak(rename_to = imp)]
                        self.ref_counted(),
                        move |backend| {
                            let info = backend.connection_debug_info();
                            imp.debug_label.set_text(&info);
                        }
                    ));
                }

                service.connect_new_task(glib::clone!(
                    #[weak(rename_to = imp)]
                    self.ref_counted(),
                    move |_, task| {
                        imp.task_list.add_task(task);
                    }
                ));
            }
        }

        fn mailbox_default_select_inbox(&self) {
            if let Some(row) = self.mailbox_list.row_at_index(0) {
                if let Some(mailbox) = row.downcast_ref::<MailboxRow>().and_then(|r| r.mailbox()) {
                    if self.mailbox_list.selected_row().is_none()
                        && mailbox.kind() == MailboxKind::Inbox
                    // TODO && !self.inner_view.is_single_pane_layout()
                    {
                        row.activate();
                    }
                }
            }
        }

        #[template_callback]
        fn mailbox_row_activated(&self, row: &gtk::ListBoxRow) {
            let mailbox = row
                .downcast_ref::<MailboxRow>()
                .and_then(|row| row.mailbox());

            log::debug!(
                "Mailbox row activated: {:?}",
                mailbox.as_ref().map(|m| m.localized_name())
            );

            self.selected_mailbox_list_row.replace(Some(row.clone()));
            self.obj().set_selected_mailbox(mailbox.as_ref());

            if let Some(mailbox) = &mailbox {
                self.emit_mailbox_activated(mailbox);
            }
        }

        pub(super) fn navigated_back(&self) {
            if self.single_pane_layout.get() && self.selected_mailbox_list_row.borrow().is_some() {
                self.mailbox_list.unselect_all();
            }
        }
    }
}

glib::wrapper! {
    pub struct MailboxPage(ObjectSubclass<imp::MailboxPage>)
    @extends adw::NavigationPage, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl MailboxPage {
    fn new() -> Self {
        glib::Object::new()
    }

    pub fn navigated_back(&self) {
        self.imp().navigated_back();
    }
}
