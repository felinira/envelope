// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod drag;
mod header;
mod page;
mod source_window;
mod view;
mod web_bin;

pub use drag::MessageDragView;
pub use page::MessagePage;

use envelib::gettext::gettext;

/// These actions are shared by the context menu of the webview, and the dropdown menu in message header
#[derive(strum::Display, strum::AsRefStr, strum::EnumIter, strum::EnumString)]
#[strum(serialize_all = "kebab-case")]
pub enum MessageAction {
    MarkRead,
    MarkUnread,
    MoveToTrash,
    MoveToJunk,
    MoveToInbox,
    DeletePermanently,
    SaveToFile,
    ViewSource,
}

impl MessageAction {
    const SECTIONS: &'static [&'static [Self]] = &[
        &[
            Self::MarkRead,
            Self::MarkUnread,
            Self::MoveToTrash,
            Self::MoveToJunk,
            Self::MoveToInbox,
            Self::DeletePermanently,
        ],
        &[Self::SaveToFile, Self::ViewSource],
    ];

    pub fn detailed_name(&self) -> String {
        format!("message.{}", self)
    }

    pub fn localized_name(&self) -> String {
        match self {
            MessageAction::MarkRead => gettext("Mark _Read"),
            MessageAction::MarkUnread => gettext("Mark _Unread"),
            MessageAction::MoveToTrash => gettext("_Trash"),
            MessageAction::MoveToJunk => gettext("_Junk"),
            MessageAction::MoveToInbox => gettext("_Not Junk"),
            MessageAction::DeletePermanently => gettext("_Delete"),
            MessageAction::SaveToFile => gettext("Save To File"),
            MessageAction::ViewSource => gettext("_View Source"),
        }
    }
}
