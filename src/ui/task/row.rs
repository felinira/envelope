// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use envelib::model::Task;

mod imp {
    use std::cell::RefCell;

    use envelib::model::TaskStatus;
    use glib::WeakRef;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::TaskRow)]
    #[template(file = "row.ui")]
    pub struct TaskRow {
        #[template_child]
        status_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        spinner: TemplateChild<adw::Spinner>,
        #[template_child]
        idle_image: TemplateChild<gtk::Image>,
        #[template_child]
        error_image: TemplateChild<gtk::Image>,
        #[template_child]
        warning_image: TemplateChild<gtk::Image>,
        #[template_child]
        success_image: TemplateChild<gtk::Image>,
        #[template_child]
        cancel_button: TemplateChild<gtk::Button>,

        #[property(get, set = Self::set_task, construct_only)]
        task: WeakRef<Task>,
        task_signal_handler_id: RefCell<Option<glib::SignalHandlerId>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TaskRow {
        const NAME: &'static str = "EnvelopeTaskRow";
        type Type = super::TaskRow;
        type ParentType = adw::ActionRow;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TaskRow {}
    impl WidgetImpl for TaskRow {}
    impl ListBoxRowImpl for TaskRow {}
    impl PreferencesRowImpl for TaskRow {}
    impl ActionRowImpl for TaskRow {}

    #[gtk::template_callbacks]
    impl TaskRow {
        #[template_callback]
        fn stop_clicked(&self) {
            if let Some(task) = self.task.upgrade() {
                task.cancel();
            }
        }

        fn update_icon(&self) {
            let Some(task) = self.task.upgrade() else {
                self.status_stack.set_visible_child(&*self.spinner);
                return;
            };

            let status = task.status();

            if matches!(status, TaskStatus::Error(_) | TaskStatus::Aborted) {
                self.status_stack.set_visible_child(&*self.error_image);
            } else if task.warnings().is_some() {
                self.status_stack.set_visible_child(&*self.warning_image);
            } else if matches!(status, TaskStatus::Completed) {
                self.status_stack.set_visible_child(&*self.success_image);
            } else if matches!(status, TaskStatus::Idle) {
                self.status_stack.set_visible_child(&*self.idle_image);
            } else if matches!(status, TaskStatus::Running) {
                self.status_stack.set_visible_child(&*self.spinner);
            }
        }

        fn update_task(&self, task: &Task) {
            self.spinner.set_visible(task.running());
            self.cancel_button.set_visible(task.running());
            self.obj().set_subtitle(&task.status().to_string());
            self.obj().set_title(&task.name());
            self.update_icon();
        }

        fn set_task(&self, task: Task) {
            let obj = self.obj();
            if let Some(id) = self.task_signal_handler_id.take() {
                if let Some(task) = self.task.upgrade() {
                    task.disconnect(id);
                }
            }

            let task_sourceid = task.connect_status_notify(glib::clone!(
                #[weak]
                obj,
                move |task| {
                    obj.imp().update_task(task);
                }
            ));
            self.task_signal_handler_id.replace(Some(task_sourceid));

            self.task.set(Some(&task));
            self.update_task(&task);
        }
    }
}

glib::wrapper! {
    pub struct TaskRow(ObjectSubclass<imp::TaskRow>)
    @extends adw::ActionRow, adw::PreferencesRow, gtk::ListBoxRow, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl TaskRow {
    pub fn new(task: Task) -> Self {
        glib::Object::builder().property("task", task).build()
    }
}
