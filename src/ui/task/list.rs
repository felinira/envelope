// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use adw::prelude::*;
use adw::subclass::prelude::*;

use super::row::TaskRow;
use envelib::model::Task;

mod imp {
    use std::cell::{Cell, OnceCell};

    use envelib::model::{Task, TaskStatus};

    use crate::ui::list::sorter::DynamicSorter;

    use super::*;

    #[derive(Debug, Default, gtk::CompositeTemplate, glib::Properties)]
    #[properties(wrapper_type = super::TaskList)]
    #[template(file = "list.ui")]
    pub struct TaskList {
        #[template_child]
        list: TemplateChild<gtk::ListBox>,
        #[property(get)]
        task_running: Cell<bool>,
        shutdown: Cell<bool>,

        task_list: OnceCell<gio::ListStore>,
        sort_model: OnceCell<gtk::SortListModel>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for TaskList {
        const NAME: &'static str = "EnvelopeTaskList";
        type Type = super::TaskList;
        type ParentType = adw::Bin;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
            klass.bind_template_callbacks();
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for TaskList {
        fn constructed(&self) {
            self.parent_constructed();

            self.list.bind_model(Some(self.sort_model()), |task| {
                if let Some(task) = task.downcast_ref::<Task>() {
                    log::debug!("New task row for task {}", task.name());
                    TaskRow::new(task.clone()).upcast()
                } else {
                    adw::Bin::new().upcast()
                }
            });

            self.update_running();
        }
    }
    impl WidgetImpl for TaskList {}
    impl BinImpl for TaskList {}

    #[gtk::template_callbacks]
    impl TaskList {
        fn task_list(&self) -> gio::ListStore {
            self.task_list
                .get_or_init(|| gio::ListStore::with_type(Task::static_type()))
                .clone()
        }

        fn sort_model(&self) -> &gtk::SortListModel {
            self.sort_model.get_or_init(|| {
                gtk::SortListModel::new(
                    Some(self.task_list().clone()),
                    Some(DynamicSorter::with_sorter(Task::cmp_prio).reverse()),
                )
            })
        }

        pub(super) fn cleanup(&self) {
            // Keep all running tasks and the last 3 that are finished
            let mut not_running_count = 0;
            let mut error_count = 0;

            if let Some(s) = self.sort_model().sorter() {
                s.changed(gtk::SorterChange::Different)
            }

            self.task_list().retain(|task| {
                let Some(task) = task.downcast_ref::<Task>() else {
                    return false;
                };

                if task.running() {
                    true
                } else if matches!(task.status(), TaskStatus::Error(_)) || task.warnings().is_some()
                {
                    if error_count < 3 {
                        error_count += 1;
                        not_running_count += 1;
                        true
                    } else {
                        false
                    }
                } else if not_running_count < 3 {
                    not_running_count += 1;
                    true
                } else {
                    false
                }
            });

            self.update_running();
        }

        pub(crate) fn update_running(&self) {
            let running = self.task_list().iter::<Task>().any(|task| {
                task.as_ref()
                    .map(|t| matches!(t.status(), TaskStatus::Running))
                    .unwrap_or(false)
            });

            if running != self.task_running.get() {
                self.task_running.set(running);
                self.obj().notify_task_running();
            }
        }

        pub fn shutdown(&self) {
            self.shutdown.set(true);
            for task in self.task_list().into_iter() {
                if let Some(task) = task.ok().and_downcast_ref::<Task>() {
                    task.cancel();
                }
            }
        }

        pub(super) fn add_task(&self, task: Task) {
            self.task_list().insert(0, &task);

            let obj = self.obj();
            task.connect_status_notify(glib::clone!(
                #[weak]
                obj,
                move |_| {
                    obj.imp().update_running();
                }
            ));

            self.cleanup();
        }
    }
}

glib::wrapper! {
    pub struct TaskList(ObjectSubclass<imp::TaskList>)
    @extends adw::Bin, gtk::Widget,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget;
}

impl TaskList {
    pub fn new() -> Self {
        glib::Object::new()
    }

    pub fn add_task(&self, task: Task) {
        self.imp().add_task(task);
    }

    pub fn shutdown(&self) {
        self.imp().shutdown();
    }
}
