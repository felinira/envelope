// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

#![allow(dead_code, irrefutable_let_patterns, clippy::new_without_default)]

mod config;
mod controller;
mod error;
mod globals;
mod ui;

use std::{panic::PanicHookInfo, process::Stdio};

use adw::prelude::*;
use gettextrs::{LocaleCategory, gettext};
use gtk::{gio, glib};

fn setup_gresources() {
    let data = {
        log::debug!("Loading precompiled GResource data");
        glib::Bytes::from(globals::GRESOURCE_DATA)
    };

    let resource = gio::Resource::from_data(&data).expect("Error loading resource bundle");
    gio::resources_register(&resource);
}

fn ensure_types() {
    ui::components::counter_label::CounterLabel::ensure_type();
    ui::components::inscription::Inscription::ensure_type();
    ui::components::label::Label::ensure_type();
    ui::task::TaskList::ensure_type();
}

fn panic_hook(info: &PanicHookInfo<'_>) {
    let msg = info.to_string();
    log::error!("Fatal Error: {msg}");
    std::process::Command::new("notify-send")
        .arg("Fatal Error")
        .arg(msg)
        .stderr(Stdio::null())
        .spawn()
        .unwrap()
        .wait()
        .unwrap();
}

fn main() -> glib::ExitCode {
    // Initialize panic handler
    let default_hook = std::panic::take_hook();
    std::panic::set_hook(Box::new(move |info| {
        default_hook(info);
        panic_hook(info);
        std::process::abort();
    }));

    // Initialize logger
    tracing_subscriber::fmt::init();

    // Prepare i18n
    gettextrs::setlocale(LocaleCategory::LcAll, "");
    gettextrs::bindtextdomain(globals::GETTEXT_PACKAGE, *globals::LOCALEDIR)
        .expect("Unable to bind the text domain");
    gettextrs::textdomain(globals::GETTEXT_PACKAGE).expect("Unable to switch to the text domain");

    glib::set_application_name(&gettext("Envelope"));

    // Setup libraries
    adw::init().unwrap();
    envelib::init();

    setup_gresources();
    ensure_types();

    let app = ui::application::EnvelopeApplication::default();
    app.run()
}
