// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

pub mod credentials;
pub mod error;
pub mod ui;

use crate::globals;
use credentials::SecretService;
use error::ConfigError;
use serde::{Deserialize, Serialize};
use smol::io::{AsyncReadExt, AsyncWriteExt};
use std::ops::{Deref, DerefMut};
use std::path::PathBuf;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum SmtpConnectionType {
    StartTls,
    Tls,
}

#[derive(Clone, Default, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub struct Config {
    pub window: ui::WindowConfig,
    pub alpha_warning_seen_date: Option<std::time::SystemTime>,
    pub accounts: Vec<envelib::account::Account>,
}

#[derive(Clone, Default, Debug)]
pub struct PersistentConfig {
    pub config: Config,
    pub persisted_config: Config,
}

impl Deref for PersistentConfig {
    type Target = Config;

    fn deref(&self) -> &Self::Target {
        &self.config
    }
}

impl DerefMut for PersistentConfig {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.config
    }
}

impl PersistentConfig {
    pub async fn from_file() -> Result<Self, ConfigError> {
        let path = Self::path();
        log::info!("Loading config file: '{}'", path.display());

        let file = smol::fs::File::open(path).await;
        if let Err(err) = &file {
            if matches!(err.kind(), std::io::ErrorKind::NotFound) {
                log::info!("Config file not found. Using default values");
                return Ok(PersistentConfig::default());
            }

            log::error!("Unable to load config file: {:?}", err.kind());
        }

        let mut buf = Vec::new();
        file?.read_to_end(&mut buf).await?;
        let mut cfg: Config = serde_json::de::from_slice(&buf)?;

        // Credentials are serialized as None, we need to load them from libsecret
        for account in &mut cfg.accounts {
            for service in account.services.values_mut() {
                service.load_secret().await?;
            }
        }

        Ok(Self {
            config: cfg.clone(),
            persisted_config: cfg,
        })
    }

    pub async fn save(&mut self) -> Result<(), std::io::Error> {
        if self.config == self.persisted_config {
            log::info!("Not saving config, no values have changed");
            Ok(())
        } else {
            let dir = Self::dir();
            std::fs::create_dir_all(&dir)?;

            let temp = tempfile::NamedTempFile::new_in(dir)?;
            let mut file = smol::fs::File::from(temp.reopen()?);
            let data = serde_json::ser::to_vec_pretty(&self.config)?;
            file.write_all(&data).await?;
            file.close().await?;

            let path = Self::path();
            log::info!("Saving config file to: '{}'", path.display());
            temp.persist(&path)?;

            self.persisted_config = self.config.clone();

            Ok(())
        }
    }

    pub fn dir() -> PathBuf {
        let mut path = glib::user_config_dir();
        path.push(globals::APP_NAME);
        path
    }

    pub fn path() -> PathBuf {
        let mut path = Self::dir();
        path.push("config.json");
        path
    }

    pub fn accounts(&self) -> &[envelib::account::Account] {
        &self.accounts
    }

    pub fn accounts_mut(&mut self) -> &mut Vec<envelib::account::Account> {
        &mut self.accounts
    }
}
