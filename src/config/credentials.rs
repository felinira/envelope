// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::collections::HashMap;

use super::error::ConfigError;
use envelib::account;

// TODO: Fix hardcoding of xdg schema
const XDG_SCHEMA: &str = if cfg!(debug_assertions) {
    "app.drey.Envelope.Devel"
} else {
    "app.drey.Envelope"
};

pub trait SecretService {
    async fn load_secret(&mut self) -> Result<(), ConfigError>;
    async fn store_secret(
        &self,
        account: &account::Account,
        service_kind: account::ServiceType,
    ) -> Result<(), ConfigError>;
}

async fn connection() -> Result<oo7::Keyring, oo7::Error> {
    oo7::Keyring::new().await
}

impl SecretService for account::Service {
    async fn load_secret(&mut self) -> Result<(), ConfigError> {
        let connection = connection().await?;

        let mut attributes = HashMap::<&str, &str>::new();
        attributes.insert("xdg-schema", XDG_SCHEMA);
        attributes.insert("type", "credentials_v1");
        let service_id = self.service_id.to_string();
        attributes.insert("id", &service_id);

        let items = connection.search_items(&attributes).await?;

        if let Some(item) = items.first() {
            self.secret = serde_json::de::from_slice(&item.secret().await?)?;
            Ok(())
        } else {
            Err(ConfigError::CredentialsNotFound(self.service_id.clone()))
        }
    }

    async fn store_secret(
        &self,
        account: &account::Account,
        service_kind: account::ServiceType,
    ) -> Result<(), ConfigError> {
        let name = format!(
            "Envelope {} Credentials for “{}” on “{}”",
            service_kind,
            account.identity().address,
            &account
                .imap()
                .map(|s| s.server.clone())
                .unwrap_or("unknown".to_string())
        );

        let (_, service) = account
            .services
            .iter()
            .find(|(kind, _)| **kind == service_kind)
            .ok_or(ConfigError::ServiceNotFound(service_kind.to_string()))?;

        let connection = connection().await?;

        let mut attributes = HashMap::<&str, &str>::new();
        attributes.insert("xdg-schema", XDG_SCHEMA);
        attributes.insert("type", "credentials_v1");
        let service_id = self.service_id.to_string();
        attributes.insert("id", &service_id);

        let secret = serde_json::ser::to_vec(&service.secret)?;

        connection
            .create_item(&name, &attributes, secret, true)
            .await?;

        Ok(())
    }
}
