// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use envelib::account::ServiceId;
use thiserror::Error as ThisError;

#[derive(Debug, ThisError)]
pub enum ConfigError {
    #[error("I/O error: {0}")]
    Io(#[from] std::io::Error),
    #[error("Read error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("Service not found: {0}")]
    ServiceNotFound(String),
    #[error("Secret Service: {0}")]
    Secret(#[from] oo7::Error),
    #[error("Credentials not found")]
    CredentialsNotFound(ServiceId),
}

pub type ConfigResult<T> = Result<T, ConfigError>;
