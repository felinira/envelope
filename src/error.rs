// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

mod handle;

use thiserror::Error as ThisError;

pub use handle::*;

#[derive(Debug, ThisError)]
pub enum ConnectionTestError {
    #[error("Host is unreachable")]
    Unreachable,
    #[error("{0}")]
    Error(String),
    #[error("Authentication failed: {0}")]
    AuthenticationFailed(String),
    #[error("TLS: {0}")]
    Tls(String),
}

#[derive(Debug, ThisError)]
pub enum AppError {
    #[error("User canceled the operation")]
    Aborted,
    #[error("No user account configured")]
    NoAccount,
    #[error("Disconnected")]
    Disconnected,
    #[error("Mail Backend Error: {0}")]
    Mail(envelib::Error),
    #[error("An internal inconsistency has occurred. {0}")]
    Inconsistency(String),
    #[error("{0}")]
    Glib(#[from] glib::Error),
    #[error("Join Error: {0}")]
    JoinError(#[from] glib::JoinError),
    #[error("I/O Error: {0}")]
    Io(#[from] std::io::Error),
    #[error("{0}")]
    Anyhow(#[from] anyhow::Error),
    #[error("{0}")]
    Message(String),
    #[error("Configuration: {0}")]
    Config(#[from] crate::config::error::ConfigError),
}

impl From<envelib::Error> for AppError {
    fn from(value: envelib::Error) -> Self {
        match value.kind() {
            envelib::ErrorKind::Aborted => Self::Aborted,
            _ => Self::Mail(value),
        }
    }
}

impl AppError {
    pub fn msg(msg: impl AsRef<str>) -> Self {
        Self::Message(msg.as_ref().to_string())
    }
}

pub type AppResult<T> = std::result::Result<T, AppError>;
