// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::sync::LazyLock;

static_toml::static_toml! {
    static CARGO_TOML = include_toml!("Cargo.toml");
}

#[cfg(debug_assertions)]
pub static APP_ID: LazyLock<String> =
    LazyLock::new(|| CARGO_TOML.package.metadata.custom.app_id.to_string() + ".Devel");
#[cfg(not(debug_assertions))]
pub static APP_ID: LazyLock<String> =
    LazyLock::new(|| CARGO_TOML.package.metadata.custom.app_id.to_string());

pub static APP_NAME: &str = env!("CARGO_PKG_NAME");
pub static GETTEXT_PACKAGE: &str = APP_NAME;
pub static LOCALEDIR: LazyLock<&str> =
    LazyLock::new(|| option_env!("LOCALEDIR").unwrap_or("/usr/share/locale"));
pub static PKGDATADIR: LazyLock<&str> = LazyLock::new(|| {
    option_env!("PKGDATADIR").unwrap_or(concat!("/usr/share/", env!("CARGO_PKG_NAME")))
});
pub static DEBUG_BUILD: bool = cfg!(debug_assertions);
pub static PROFILE: &str = if cfg!(debug_assertions) {
    "Devel"
} else {
    "Release"
};
pub static GRESOURCE_DATA: &[u8] =
    gvdb_macros::include_gresource_from_dir!("/app/drey/Envelope/", "data/resources");
pub static VERSION: &str = env!("CARGO_PKG_VERSION");
