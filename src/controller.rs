// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use crate::error::*;
use crate::ui::application::EnvelopeApplication;
use crate::ui::utils::*;
use envelib::account;
use envelib::gettext::*;
use envelib::model;
use envelib::model::{MailboxExt, MailboxExtProps, MessageExtProps};
use envelib::service::MailService;
use envelib::service::delegate::*;
use envelib::service::incoming::IncomingMailServiceExt;
use envelib::types::*;

use std::cell::{Cell, OnceCell, RefCell};
use std::collections::HashSet;
use std::path::PathBuf;

use adw::prelude::*;
use adw::subclass::prelude::*;
use glib::SignalHandlerId;
use glib::closure_local;

mod imp {
    use std::marker::PhantomData;

    use crate::config::credentials::SecretService;

    use super::*;
    use envelib::service::incoming::{IncomingMailService, IncomingMailServiceExt};
    use glib::subclass::Signal;
    use std::sync::LazyLock;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Controller)]
    #[allow(clippy::type_complexity)]
    pub struct Controller {
        #[property(get, set, construct_only)]
        app: OnceCell<EnvelopeApplication>,
        #[property(get, set, construct, type = envelib::account::Account)]
        pub(super) account: RefCell<Option<envelib::account::Account>>,
        #[property(get)]
        connection_status: RefCell<ConnectionStatus>,
        #[property(get)]
        connection_active: Cell<bool>,
        #[property(get)]
        pub(super) service: OnceCell<MailService>,
        #[property(get = Self::incoming_service)]
        incoming_service: PhantomData<IncomingMailService>,

        /// The currently open mailbox
        #[property(get, set = Self::set_open_mailbox, nullable)]
        open_mailbox: RefCell<Option<model::Mailbox>>,

        /// The currently open message thread
        #[property(get, set = Self::set_open_thread, nullable)]
        open_thread: RefCell<Option<model::Thread>>,
        open_thread_signal_group: OnceCell<glib::SignalGroup>,

        /// The messages that are scrolled into view in the list
        visible_threads: RefCell<HashSet<model::Thread>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Controller {
        const NAME: &'static str = "EnvelopeController";
        type Type = super::Controller;
        type Interfaces = (ServiceDelegate,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for Controller {
        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> =
                LazyLock::new(|| vec![Signal::builder("shutdown").build()]);
            SIGNALS.as_ref()
        }
    }

    impl ServiceDelegateImpl for Controller {
        async fn ask_credentials(
            &self,
            service_id: account::ServiceId,
            previous: Option<account::Secret>,
            methods: enumset::EnumSet<account::AuthMethod>,
            message: &str,
        ) -> Option<account::Secret> {
            let secret =
                crate::ui::dialog::credentials::ask_for_credentials(previous, methods, message)
                    .await;

            let mut account = self.obj().account();
            let mut service = account.service_mut(&service_id);

            if let (Some((kind, s)), Some(secret)) = (&mut service, &secret) {
                // Save the secret in libsecret
                s.secret = secret.clone();
                s.store_secret(&self.obj().account(), **kind)
                    .await
                    .handle_app_result();
            }

            secret
        }
    }

    #[gtk::template_callbacks]
    impl Controller {
        fn service(&self) -> &MailService {
            self.service
                .get()
                .expect("Backend must be initialized by calling start")
        }

        fn incoming_service(&self) -> IncomingMailService {
            self.service().incoming()
        }

        pub(super) fn backend_constructed(&self) {
            self.incoming_service().set_delegate(self.obj().clone());
            let obj = self.obj();
            self.incoming_service()
                .connect_connection_status_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |backend| {
                        obj.imp()
                            .update_connection_status(backend.connection_status())
                    }
                ));

            self.set_connection_active(true);

            let obj = self.obj().clone();
            self.incoming_service()
                .connect_message_update(move |_, mailbox, update| {
                    obj.imp().send_new_messages_notification(&mailbox, &update);
                });

            self.init_open_thread();
        }

        fn init_open_thread(&self) {
            let obj = self.obj();
            let open_thread_signal_group = self
                .open_thread_signal_group
                .get_or_init(|| glib::SignalGroup::with_type(model::Thread::static_type()));

            open_thread_signal_group.connect_closure(
                "messages-changed",
                true,
                glib::closure_local!(
                    #[watch]
                    obj,
                    move |thread: model::Thread| {
                        let obj_ = obj.clone();
                        Async::spawn_local(async move {
                            obj_.imp().handle_open_thread_messages_changed(thread).await
                        });
                    }
                ),
            );
        }

        fn set_connection_active(&self, active: bool) {
            if self.connection_active.get() != active {
                self.connection_active.set(active);
                self.obj().notify_connection_active();
            }
        }

        pub(super) fn update_connection_status(&self, status: ConnectionStatus) {
            log::debug!("New connection status: {:?}", status);
            let old_active = self.connection_active.get();
            self.connection_status.replace(status.clone());

            self.obj().notify_connection_status();

            if !old_active && self.connection_active.get() {
                // Check if anything to do
                self.refresh_open_mailbox();
                self.refresh_open_thread();
            }
        }

        async fn handle_open_thread_messages_changed(
            &self,
            thread: model::Thread,
        ) -> AppResult<()> {
            self.incoming_service()
                .fetch_messages(
                    &thread.messages_without_body(),
                    model::MessageDetailLevel::Full,
                )
                .await?;

            Ok(())
        }

        pub(super) fn emit_shutdown(&self) {
            self.obj().emit_by_name::<()>("shutdown", &[]);
        }

        /// Sends the new messages notification given the provided messages
        ///
        /// No-Op if the messages list is empty
        fn send_new_messages_notification(&self, mailbox: &model::Mailbox, update: &MessageUpdate) {
            let message = update.notification_message();

            let subject = message.as_ref().and_then(|msg| msg.subject());
            let n = update.message_count() as u32;
            let is_inbox = mailbox.is_inbox();
            let unseen = update.unseen_count() as u32;
            let recent = update.recent_count() as u32;

            let title = match (subject, is_inbox, n, unseen, recent) {
                (Some(subject), _, _, _, _) => subject,
                (None, true, n, _, recent) if recent == n => {
                    // Translators: message count
                    ngettextf("New Message", "{} New Messages", n, &[&n])
                }
                (None, true, n, unseen, _) if unseen == n => {
                    // Translators: message count
                    ngettextf("Unread Message", "{} Unread Messages", n, &[&n])
                }
                (None, true, n, _, _) => {
                    // Translators: message count
                    ngettextf("Message", "{} Messages", n, &[&n])
                }
                (None, false, n, _, recent) if recent == n => {
                    ngettextf(
                        // Translators: 0: folder, 1: message count
                        "{0}: New Message",
                        "{0}: {1} New Messages",
                        n,
                        &[&mailbox.localized_name(), &n],
                    )
                }
                (None, false, n, unseen, _) if unseen == n => {
                    ngettextf(
                        // Translators: 0: folder, 1: message count
                        "{0}: Unread Message",
                        "{0}: {1} Unread Messages",
                        n,
                        &[&mailbox.localized_name(), &n],
                    )
                }
                (None, false, n, _, _) => {
                    ngettextf(
                        // Translators: 0: folder, 1: message count
                        "{0}: Message",
                        "{0}: {1} Messages",
                        n,
                        &[&mailbox.localized_name(), &n],
                    )
                }
            };

            let description = message
                .and_then(|msg| msg.preview_text())
                .unwrap_or(gettext("Open the App to view more"));

            let notification = gio::Notification::new(&title);
            notification.set_body(Some(&description));
            self.obj()
                .app()
                .send_notification(Some("new-message"), &notification);
        }

        fn refresh_open_mailbox(&self) {
            let Some(mailbox) = self.open_mailbox.borrow().clone() else {
                return;
            };

            let service = self.incoming_service().clone();
            Async::spawn_local(async move {
                service.open_mailbox(mailbox).await?;
                Ok(())
            });
        }

        fn set_open_mailbox(&self, mailbox: Option<model::Mailbox>) {
            let id = mailbox.as_ref().map(|mb| mb.id());

            if self.open_mailbox.borrow().as_ref().map(|mb| mb.id()) != id {
                self.open_mailbox.borrow_mut().clone_from(&mailbox);

                self.refresh_open_mailbox();
            }
        }

        fn refresh_open_thread(&self) {
            let Some(thread) = self.open_thread.borrow().clone() else {
                return;
            };

            if thread.is_loaded() {
                let messages = thread.messages_without_body();
                if !messages.is_empty() {
                    let service = self.incoming_service().clone();
                    Async::spawn_local(async move {
                        service
                            .fetch_messages(&messages, model::MessageDetailLevel::Full)
                            .await?;
                        Ok(())
                    });
                }
            }
        }

        fn set_open_thread(&self, thread: Option<model::Thread>) {
            if self.open_thread.borrow().as_ref() == thread.as_ref() {
                return;
            }

            if let Some(thread) = thread.as_ref() {
                self.mark_thread_visible(thread, true);
            }

            self.open_thread_signal_group
                .get()
                .unwrap()
                .set_target(thread.as_ref());
            *self.open_thread.borrow_mut() = thread;
            self.refresh_open_thread();
        }

        pub fn mark_thread_visible(&self, thread: &model::Thread, visible: bool) {
            if visible {
                self.visible_threads.borrow_mut().insert(thread.clone());
            } else {
                self.visible_threads.borrow_mut().remove(thread);
            }

            if !thread.is_loaded() {
                let service = self.incoming_service().clone();
                let obj = self.obj().clone();

                static RUNNING: smol::lock::Mutex<()> = smol::lock::Mutex::new(());
                thread_local! {
                    static QUEUED: Cell<bool> = const { Cell::new(false) }
                }

                if !QUEUED.get() {
                    QUEUED.set(true);
                    Async::spawn_local_with_priority(glib::Priority::HIGH, async move {
                        // Only one fetch to be queued at a time
                        let _lock = RUNNING.lock().await;
                        let threads: Vec<_> = obj
                            .imp()
                            .visible_threads
                            .borrow()
                            .iter()
                            .filter(|t| !t.is_loaded())
                            .cloned()
                            .collect();

                        // Set to false to allow the next batch to be queued up
                        QUEUED.set(false);
                        service.load_threads(&threads).await?;

                        Ok(())
                    });
                }
            }
        }

        /// Shutdown all backend connections
        pub async fn shutdown(&self) -> AppResult<()> {
            self.service().shutdown().await?;
            self.emit_shutdown();

            Ok(())
        }
    }
}

glib::wrapper! {
    pub struct Controller(ObjectSubclass<imp::Controller>)
    @implements ServiceDelegate;
}

impl Controller {
    pub fn new(app: EnvelopeApplication, account: envelib::account::Account) -> Self {
        glib::Object::builder()
            .property("app", app)
            .property("account", account)
            .build()
    }

    pub fn connect_shutdown<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn() + 'static,
    {
        self.connect_closure(
            "shutdown",
            false,
            closure_local!(|_: Controller| callback()),
        )
    }

    fn cache_dir(&self) -> PathBuf {
        let mut cache = glib::user_cache_dir();
        cache.push(crate::globals::APP_NAME);
        cache.push("accounts");
        cache.push(self.account().id.to_string());
        cache
    }

    pub async fn start(&self) -> AppResult<()> {
        // TODO: Error handling
        let account = self.account();
        let service = MailService::new(account.clone(), self.cache_dir()).await?;
        service.set_work_online(true);
        self.imp().service.set(service.clone()).unwrap();
        self.imp().backend_constructed();
        Ok(())
    }

    pub async fn connection_test(
        &self,
        timeout: std::time::Duration,
    ) -> Result<(), ConnectionTestError> {
        let service = MailService::new(self.account(), self.cache_dir())
            .await
            .map_err(|e| ConnectionTestError::Error(e.to_string()))?;
        let status = service.incoming().connection_test(timeout).await;

        match status {
            ConnectionStatus::Disconnected
            | ConnectionStatus::Connecting
            | ConnectionStatus::Unreachable => Err(ConnectionTestError::Unreachable),
            ConnectionStatus::Connected => Ok(()),
            ConnectionStatus::Error(err) => Err(ConnectionTestError::Error(err.to_string())),
            ConnectionStatus::AuthenticationFailed(err) => {
                Err(ConnectionTestError::AuthenticationFailed(err.to_owned()))
            }
            ConnectionStatus::TlsError(err) => Err(ConnectionTestError::Tls(err.to_owned())),
        }
    }

    /// Shutdown all backend connection. Will hold the running app to prevent it from quitting early.
    pub fn shutdown(&self) {
        let obj = self.clone();
        let guard = self.app().hold();
        Async::spawn_local(async move {
            obj.imp().shutdown().await?;
            drop(guard);
            Ok(())
        });
    }

    pub fn store_flags(
        &self,
        messages: Vec<model::Message>,
        flags: MessageFlags,
        change: MessageFlagsChange,
    ) {
        let service = self.incoming_service();
        Async::spawn_local(async move {
            service.store_flags(&messages, flags, change).await?;
            Ok(())
        });
    }

    pub fn move_to(&self, messages: Vec<model::Message>, destination: model::MailboxTarget) {
        let service = self.incoming_service();
        Async::spawn_local(async move {
            service.move_to(&messages, destination).await?;
            Ok(())
        });
    }

    pub fn copy_to(&self, messages: Vec<model::Message>, destination: model::MailboxTarget) {
        let service = self.incoming_service();
        Async::spawn_local(async move {
            service.copy_to(&messages, destination).await?;
            Ok(())
        });
    }

    pub fn delete_messages_permanently_confirm(
        &self,
        messages: Vec<model::Message>,
        parent: &impl IsA<gtk::Widget>,
    ) {
        let mut threads = HashSet::new();
        for message in &messages {
            threads.insert(message.thread());
        }

        let thread = if threads.len() == 1 {
            messages.first().and_then(|m| m.thread())
        } else {
            None
        };

        let dialog = if messages.len() > 1 {
            adw::AlertDialog::new(
                Some(&gettextf("Delete {} Messages?", &[&messages.len()])),
                Some(&if let Some(thread) = thread {
                    gettextf(
                        "Delete all {} messages in thread “{}” permanently?",
                        &[
                            &messages.len(),
                            &thread.thread_name().unwrap_or(gettext("No Subject")),
                        ],
                    )
                } else if messages.len() == threads.len() {
                    gettextf(
                        "Delete {} messages permanently?",
                        &[&messages.len(), &threads.len()],
                    )
                } else {
                    gettextf(
                        "Delete {} messages from {} threads permanently?",
                        &[&messages.len(), &threads.len()],
                    )
                }),
            )
        } else if let Some(message) = messages.first() {
            adw::AlertDialog::new(
                Some(&gettextf("Delete Message?", &[&messages.len()])),
                Some(&gettextf(
                    "Delete message “{}” permanently?",
                    &[&message.subject().unwrap_or(gettext("No subject"))],
                )),
            )
        } else {
            return;
        };

        dialog.add_responses(&[
            ("cancel", &gettext("Cancel")),
            ("delete", &gettext("Delete Permanently")),
        ]);

        dialog.set_response_appearance("delete", adw::ResponseAppearance::Destructive);
        let obj = self.clone();
        dialog.choose(parent, gio::Cancellable::NONE, move |response| {
            if response == "delete" {
                obj.delete_messages_permanently(messages);
            }
        });
    }

    pub fn delete_messages_permanently(&self, messages: Vec<model::Message>) {
        let service = self.incoming_service().clone();
        Async::spawn_local(async move {
            service.delete_messages(&messages).await?;
            Ok(())
        });
    }

    pub fn mark_thread_visible(&self, thread: &model::Thread, visible: bool) {
        self.imp().mark_thread_visible(thread, visible);
    }
}
