// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: GPL-3.0-or-later

use envelib::gettext::*;

use adw::prelude::*;
use std::sync::atomic;
use std::{future::Future, pin::Pin};

use crate::ui::application::EnvelopeApplication;

use super::{AppError, AppResult};

// Don't show more than one error dialog at the same time
static ERROR_DIALOG_ALREADY_SHOWING: atomic::AtomicBool = atomic::AtomicBool::new(false);

pub trait HandleError
where
    Self: std::fmt::Debug + Sized,
{
    /// In case of [`Result`] based types, this is the value of T
    type Output;

    fn gettext_error(&self) -> String;
    fn should_handle(&self) -> bool;
    fn should_panic(&self) -> bool;
    fn should_print(&self) -> bool;

    /// Returns this `HandleError` as a result
    ///
    /// Transforms error types into `Result<(), E>`
    fn output(self) -> Result<Self::Output, Self> {
        Err(self)
    }

    /// Returns a backtrace if available
    fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
        None
    }

    fn handle_basic(self) -> Result<Self::Output, Self> {
        if !self.should_handle() {
            return self.output();
        }

        if self.should_print() {
            if let Some(backtrace) = self.backtrace() {
                log::error!("{}\n{}", self.gettext_error(), backtrace);
            } else {
                log::error!("{}", self.gettext_error());
            }
        }

        // We can just panic if we can't display a proper error in the UI
        if !gtk::is_initialized() && self.should_panic() {
            panic!("An error occurred during application initialisation / exit: {self:?}",);
        }

        Err(self)
    }

    fn handle(mut self) -> Option<Self::Output> {
        self = match self.handle_basic() {
            Ok(output) => return Some(output),
            Err(this) => this,
        };

        if self.should_handle() && gtk::is_initialized() {
            // Run in background
            let error_fut = self.show_error_dialog();
            glib::MainContext::default().spawn_local(error_fut);
        }

        None
    }

    fn handle_fatal(mut self) -> Self::Output {
        self = match self.handle_basic() {
            Ok(output) => return output,
            Err(this) => this,
        };

        let msg = self.gettext_error();
        let dialog_future = self.show_error_dialog();

        // We spawn a new main loop to handle our error message and then panic
        glib::MainContext::default().spawn_local(async move {
            dialog_future.await;
            panic!("Fatal Error: {msg}")
        });

        loop {
            glib::MainContext::default().iteration(true);
        }
    }

    fn show_error_dialog(&self) -> Pin<Box<dyn Future<Output = ()>>> {
        let window: Option<gtk::Window> = match gio::Application::default() {
            Some(app) => match app.downcast::<gtk::Application>() {
                Ok(app) => {
                    let windows = app.windows();
                    windows
                        .first()
                        .cloned()
                        .and_then(|window| window.downcast().ok())
                }
                Err(_) => None,
            },
            _ => None,
        };

        let msg1 = gettext("Error");
        let msg2 = self.gettext_error();

        Box::pin(async move {
            if let Some(window) = window {
                if ERROR_DIALOG_ALREADY_SHOWING
                    .compare_exchange(
                        false,
                        true,
                        atomic::Ordering::SeqCst,
                        atomic::Ordering::SeqCst,
                    )
                    .is_err()
                {
                    return;
                }

                let dialog = adw::AlertDialog::builder()
                    .heading(msg1)
                    .body(msg2)
                    .close_response("close")
                    .build();

                dialog.add_response("close", &gettext("_Close"));
                dialog.choose_future(&window).await;
                ERROR_DIALOG_ALREADY_SHOWING.store(false, atomic::Ordering::SeqCst);
            } else {
                let notification = gio::Notification::new(&msg1);
                notification.set_body(Some(&msg2));

                EnvelopeApplication::default().send_notification(None, &notification);
            }
        })
    }
}

impl HandleError for AppError {
    type Output = ();

    fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
        match self {
            AppError::Mail(error_ty) => error_ty.backtrace(),
            _ => None,
        }
    }

    fn gettext_error(&self) -> String {
        format!("{self}")
    }

    fn should_panic(&self) -> bool {
        matches!(self, AppError::Inconsistency(_))
    }

    fn should_handle(&self) -> bool {
        // Don't do anything here, the user canceled the operation or is offline
        !matches!(self, Self::Aborted | Self::Disconnected)
            && !matches!(self, Self::Glib(err) if err.matches(gtk::DialogError::Dismissed))
    }

    fn should_print(&self) -> bool {
        match self {
            AppError::Mail(err) => !matches!(
                err.kind(),
                envelib::ErrorKind::Disconnected | envelib::ErrorKind::Aborted
            ),
            _ => true,
        }
    }
}

impl HandleError for anyhow::Error {
    type Output = ();

    fn gettext_error(&self) -> String {
        if let Some(e) = self.root_cause().downcast_ref::<AppError>() {
            e.gettext_error()
        } else {
            format!("{self}")
        }
    }

    fn should_handle(&self) -> bool {
        if let Some(e) = self.root_cause().downcast_ref::<AppError>() {
            e.should_handle()
        } else {
            true
        }
    }

    fn should_panic(&self) -> bool {
        false
    }

    fn should_print(&self) -> bool {
        true
    }
}

impl<T: std::fmt::Debug> HandleError for AppResult<T> {
    type Output = T;

    fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
        match self {
            Ok(_) => None,
            Err(err) => HandleError::backtrace(err),
        }
    }

    fn gettext_error(&self) -> String {
        match self {
            Ok(_) => String::new(),
            Err(err) => format!("{err}"),
        }
    }

    fn should_handle(&self) -> bool {
        match self {
            Ok(_) => false,
            Err(err) => err.should_handle(),
        }
    }

    fn should_panic(&self) -> bool {
        match self {
            Ok(_) => false,
            Err(err) => err.should_panic(),
        }
    }

    fn should_print(&self) -> bool {
        true
    }

    fn output(self) -> Result<Self::Output, Self> {
        if let Ok(ok) = self { Ok(ok) } else { Err(self) }
    }
}

pub trait HandleAppResult<R> {
    fn handle_app_result(self) -> Option<R>;
    fn handle_app_result_fatal(self) -> R;
}

impl<R: std::fmt::Debug, T: Into<AppError>> HandleAppResult<R> for Result<R, T> {
    fn handle_app_result(self) -> Option<R> {
        let result: AppResult<R> = self.map_err(|err| err.into());
        result.handle()
    }

    fn handle_app_result_fatal(self) -> R {
        let result: AppResult<R> = self.map_err(|err| err.into());
        result.handle_fatal()
    }
}
