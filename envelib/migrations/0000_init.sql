-- SPDX-FileCopyrightText: The Envelope Developers
--
-- SPDX-License-Identifier: MPL-2.0

-- Add migration script here

CREATE TABLE mailbox (
    id INTEGER PRIMARY KEY NOT NULL,
    name TEXT NOT NULL,
    kind TEXT NOT NULL,
    delimiter TEXT NOT NULL,

    uidnext INTEGER NOT NULL,
    uidfirst INTEGER NOT NULL,
    uidvalidity INTEGER NOT NULL
);

CREATE UNIQUE INDEX idx_mailbox_name ON mailbox(name);

CREATE TABLE message (
    id INTEGER PRIMARY KEY NOT NULL,
    thread INTEGER NOT NULL,
    mailbox INTEGER NOT NULL,

    uid INTEGER NOT NULL,

    internal_date DATETIME NOT NULL,
    flag_seen BOOLEAN NOT NULL DEFAULT FALSE,
    flags TEXT NOT NULL,

    header TEXT,
    header_message_id TEXT NOT NULL,
    header_in_reply_to TEXT,

    attachment_count INTEGER NOT NULL,

    preview TEXT,
    body BLOB DEFAULT NULL,
    last_body_access DATETIME NOT NULL DEFAULT current_timestamp,

    FOREIGN KEY(mailbox) REFERENCES mailbox(id) ON DELETE CASCADE
);

CREATE UNIQUE INDEX idx_message_mailbox_uid ON message(mailbox, uid);
CREATE INDEX idx_message_mailbox_flag_seen ON message(mailbox, flag_seen) WHERE flag_seen = FALSE;

CREATE INDEX idx_message_thread ON message(thread);
CREATE INDEX idx_message_mailbox_thread_internal_date ON message(mailbox, thread, internal_date DESC);

CREATE INDEX idx_message_header_message_id ON message(header_message_id);
CREATE INDEX idx_message_header_in_reply_to ON message(header_in_reply_to);

CREATE TABLE message_header_reference (
    message INTEGER,

    reference TEXT NOT NULL,
    FOREIGN KEY(message) REFERENCES message(id) ON DELETE CASCADE
);

CREATE INDEX idx_message_header_reference_message ON message_header_reference(message);
CREATE INDEX idx_message_header_reference_reference ON message_header_reference(reference);
