// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod client;
mod error;
mod pool;
mod service;
mod session;
mod types;

use client::*;
pub(crate) use service::ImapService;
pub(crate) use types::*;

use glib::types::StaticTypeExt;

pub(crate) fn ensure_types() {
    ImapService::ensure_type();
}
