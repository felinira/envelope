// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

pub mod entity;
pub mod id;
pub mod result;

use super::ErrorKind;
use super::{Error, Result};
use std::borrow::Borrow;
use std::path::Path;
use std::path::PathBuf;
use std::pin::Pin;

use futures::{FutureExt, Stream, StreamExt, TryFutureExt, TryStreamExt};
use sqlx::sqlite::SqliteConnectOptions;

use crate::error::ErrorKindExt;
use crate::imap::*;
use crate::types::*;

use self::entity::MessageDataFields;
use self::entity::MessageEntity;
use self::id::IdGenerator;
use self::result::MessagesUpdated;

#[derive(Debug)]
pub(crate) struct DatabaseCache {
    threadid_gen: IdGenerator<ThreadId>,

    read_pool: sqlx::SqlitePool,
    write_pool: sqlx::SqlitePool,
}

impl DatabaseCache {
    fn database_path(mut cache_dir: PathBuf) -> std::path::PathBuf {
        cache_dir.push("cache.sqlite");
        cache_dir
    }

    fn connect_options(path: &Path) -> SqliteConnectOptions {
        log::info!("Using cache database {}", path.display());

        SqliteConnectOptions::new()
            .filename(path)
            .create_if_missing(true)
            .optimize_on_close(true, Some(10))
            .locking_mode(sqlx::sqlite::SqliteLockingMode::Normal)
            .busy_timeout(std::time::Duration::from_secs(300))
            .journal_mode(sqlx::sqlite::SqliteJournalMode::Wal)
    }

    async fn create_pool(path: &Path, read_only: bool) -> Result<sqlx::SqlitePool> {
        let options = sqlx::sqlite::SqlitePoolOptions::new()
            .acquire_timeout(std::time::Duration::from_secs(300));

        if read_only {
            // Create a read pool that isn't allowed to write
            let pool =
                options.connect_lazy_with(Self::connect_options(path).pragma("query_only", "TRUE"));
            Ok(pool)
        } else {
            smol::fs::create_dir_all(path.parent().unwrap()).await?;

            let connect = || async {
                options
                    .clone()
                    .max_connections(1)
                    .connect_with(Self::connect_options(path))
                    .await
            };

            let pool = connect().await?;
            Ok(pool)
        }
    }

    /// We ensure an exclusive write transaction by requiring `&mut self` here.
    ///
    /// Note that this is also ensured internally in the connection pool options. We just
    /// make it explicit to ensure we have an expressive external API.
    async fn write_tx(&mut self) -> Result<sqlx::Transaction<sqlx::Sqlite>> {
        Ok(self.write_pool.begin().await?)
    }

    pub async fn new(cache_dir: PathBuf) -> Result<Self> {
        let path = Self::database_path(cache_dir);

        // Open a write pool
        let mut write_pool = Self::create_pool(&path, false).await?;

        // Execute migrations
        let result = sqlx::migrate!().run(&write_pool).await;
        if let Err(err) = result {
            // Error while running migrations
            // Delete the database
            drop(write_pool);
            log::error!(
                "Error migrating the cache database: Deleting “{}”: {}",
                path.display(),
                err
            );
            smol::fs::remove_file(&path).await?;

            // Create a new connection and run migrations again
            write_pool = Self::create_pool(&path, false).await?;
            sqlx::migrate!().run(&write_pool).await?;
        }

        // Initialize thread ID
        let thread = sqlx::query_scalar!("SELECT MAX(thread) as thread from message")
            .fetch_optional(&write_pool)
            .await?
            .flatten()
            .unwrap_or(1);

        let threadid_gen = IdGenerator::new(ThreadId::new(thread));

        // Open a read pool
        let read_pool = Self::create_pool(&path, true).await?;

        Ok(Self {
            threadid_gen,
            read_pool,
            write_pool,
        })
    }

    pub async fn close(&mut self) {
        self.read_pool.close().await;
        self.write_pool.close().await;
    }
}

/// Mailbox operations
impl DatabaseCache {
    /// Update or create a mailbox
    pub async fn mailbox_update(
        &mut self,
        mailbox: &entity::MailboxImapData,
    ) -> Result<result::MailboxUpdated> {
        let mut transaction = self.write_tx().await?;

        let row = sqlx::query!(
            r#"SELECT id, uidvalidity FROM mailbox WHERE name = ?"#,
            mailbox.name
        )
        .fetch_optional(&mut *transaction)
        .await?;

        let mut uidvalidity_reset = false;
        let previous_mailbox_id = row.as_ref().map(|row| MailboxId::from(row.id));
        if let Some(previous_uidvalidity) = row.map(|row| UidValidity::from(row.uidvalidity)) {
            if previous_uidvalidity != mailbox.uidvalidity {
                uidvalidity_reset = true;
            }
        }

        if let Some(mailbox_id) = previous_mailbox_id {
            if uidvalidity_reset {
                // Delete all messages, for all we know this is a new mailbox
                sqlx::query!("DELETE FROM message WHERE mailbox = ?", mailbox_id,)
                    .execute(&mut *transaction)
                    .await?;
            }
        }

        // Commit the changes to the mailbox
        let id = sqlx::query_scalar!(
            "
            INSERT INTO mailbox (
                name, delimiter, kind, uidnext, uidfirst, uidvalidity
            )
            VALUES (?1, ?2, ?3, ?4, ?5, ?6)
            ON CONFLICT
                DO UPDATE
                SET (
                    name, delimiter, kind, uidnext, uidfirst, uidvalidity
                ) = (?1, ?2, ?3, ?4, ?5, ?6)
            RETURNING id
            ",
            mailbox.name,
            mailbox.delimiter,
            mailbox.kind,
            mailbox.uidnext,
            mailbox.uidfirst,
            mailbox.uidvalidity,
        )
        .fetch_one(&mut *transaction)
        .await?;

        transaction.commit().await?;

        Ok(result::MailboxUpdated {
            id: MailboxId::new(id),
            new: previous_mailbox_id.is_none(),
            uidvalidity_reset,
        })
    }

    /// Remove a mailbox and all associated messages
    pub(super) async fn mailbox_remove(&mut self, mailbox_id: MailboxId) -> Result<()> {
        let mut connection = self.write_tx().await?;

        // Delete mailbox
        sqlx::query!("DELETE FROM mailbox WHERE id = ?", mailbox_id)
            .execute(&mut *connection)
            .await?;

        // Delete all messages in mailbox
        sqlx::query!("DELETE FROM message WHERE mailbox = ?", mailbox_id)
            .execute(&mut *connection)
            .await?;

        connection.commit().await?;

        Ok(())
    }

    /// Reset mailbox (delete all messages)
    pub async fn mailbox_reset(&mut self, mailbox_id: MailboxId) -> Result<()> {
        let mut connection = self.write_tx().await?;

        // Delete all messages in mailbox cache
        log::debug!("Delete all messages in '{:?}'", mailbox_id);
        sqlx::query!("DELETE FROM message WHERE mailbox = ?", mailbox_id)
            .execute(&mut *connection)
            .await?;

        connection.commit().await?;

        Ok(())
    }

    #[allow(clippy::needless_lifetimes)]
    /// Fetch all mailboxes as a stream
    pub async fn mailboxes_fetch<'a>(
        &'a self,
    ) -> Result<impl Stream<Item = Result<entity::MailboxEntity>> + 'a> {
        Ok(sqlx::query_as("SELECT * FROM mailbox")
            .fetch(&self.read_pool)
            .map_err(Error::from))
    }
}

/// Message operations
impl DatabaseCache {
    /// Create or update a message from message data
    async fn message_update_with_thread(
        transaction: &mut sqlx::SqliteConnection,
        data: &MessageDataFields,
        references: &[MessageId],
        mailbox_id: MailboxId,
        thread: ThreadId,
    ) -> Result<MessageCacheId> {
        let row_id = sqlx::query_scalar!(
            "
            INSERT INTO message (
                thread,
                mailbox,
                uid,
                internal_date,
                flag_seen,
                flags,
                header,
                header_message_id,
                header_in_reply_to,
                attachment_count,
                preview
            ) VALUES (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)
            ON CONFLICT
            DO UPDATE
            SET (
                thread,
                mailbox,
                uid,
                internal_date,
                flag_seen,
                flags,
                header,
                header_message_id,
                header_in_reply_to,
                attachment_count,
                preview
            ) = (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11)
            RETURNING id
            ",
            thread,
            mailbox_id,
            data.uid,
            data.internal_date,
            data.flag_seen,
            data.flags,
            data.header,
            data.header_message_id,
            data.header_in_reply_to,
            data.attachment_count,
            data.preview,
        )
        .fetch_one(&mut *transaction)
        .await?;
        sqlx::query!(
            "DELETE FROM message_header_reference WHERE message = ?",
            row_id
        )
        .execute(&mut *transaction)
        .await?;

        for reference in references {
            sqlx::query!(
                "INSERT INTO message_header_reference (message, reference) VALUES (?1, ?2)",
                row_id,
                reference
            )
            .execute(&mut *transaction)
            .await?;
        }

        Ok(MessageCacheId::new(row_id))
    }

    /// Update / Create messages
    pub async fn messages_update(
        &mut self,
        mailbox_id: MailboxId,
        messages: impl IntoIterator<Item = &MessageData>,
    ) -> Result<MessagesUpdated> {
        let threadid_gen = self.threadid_gen.clone();
        let mut transaction = self.write_tx().await?;
        let mut messages_updated = MessagesUpdated::default();

        for data in messages {
            let fields = MessageDataFields::try_from(data)?;
            let message = data.message.as_ref().ok_or_else(|| {
                ErrorKind::Inconsistency
                    .with_message("Server did not provide us with a message body")
            })?;
            let references = message.parse_references();

            let thread_id = if let Some(thread_id) = data.thread_id {
                // Check if it exists
                if Self::thread_exists_with_id(&mut transaction, thread_id).await? {
                    data.thread_id
                } else {
                    None
                }
            } else {
                let thread_ids = Self::thread_ids_find_by_reference(
                    &mut transaction,
                    Some(&fields.header_message_id),
                    fields.header_in_reply_to.as_ref(),
                    references.as_slice(),
                )
                .await?;
                if let Some(root) = thread_ids.first() {
                    let others = &thread_ids[1..];
                    if !others.is_empty() {
                        // Merge the threads
                        Self::threads_merge(&mut transaction, root, others).await?;

                        // We need to keep our own bookkeeping right
                        for merged_thread in others {
                            messages_updated.threads.remove(merged_thread);
                            messages_updated.threads_vanished.push(*merged_thread);
                        }
                    }

                    Some(*root)
                } else {
                    None
                }
            };

            let thread_id = if let Some(thread_id) = thread_id {
                // Ensure our counter is up to date
                threadid_gen.new_max_thread_id(thread_id);
                thread_id
            } else {
                threadid_gen.generate()
            };

            let _id = Self::message_update_with_thread(
                &mut transaction,
                &fields,
                &references,
                mailbox_id,
                thread_id,
            )
            .await?;

            // Find the newest message
            let date = fields.internal_date;
            messages_updated
                .threads
                .entry(thread_id)
                .and_modify(|date_entry| {
                    if *date_entry < date {
                        *date_entry = date;
                    }
                })
                .or_insert(date);
        }

        transaction.commit().await?;

        // TODO: What happens when an error occurs within the transaction?
        Ok(messages_updated)
    }

    /// Update messages
    pub async fn messages_commit(
        &mut self,
        messages: impl IntoIterator<Item = MessageEntity>,
    ) -> Result<()> {
        let mut transaction = self.write_tx().await?;

        for entity in messages {
            sqlx::query_scalar!(
                "
                UPDATE message
                SET (
                    thread,
                    mailbox,
                    uid,
                    internal_date,
                    flag_seen,
                    flags,
                    header,
                    header_message_id,
                    header_in_reply_to,
                    attachment_count,
                    preview,
                    body
                ) = (?1, ?2, ?3, ?4, ?5, ?6, ?7, ?8, ?9, ?10, ?11, coalesce(?12, body))
                WHERE id = ?13
                ",
                entity.thread,
                entity.mailbox,
                entity.fields.uid,
                entity.fields.internal_date,
                entity.fields.flag_seen,
                entity.fields.flags,
                entity.fields.header,
                entity.fields.header_message_id,
                entity.fields.header_in_reply_to,
                entity.fields.attachment_count,
                entity.fields.preview,
                entity.body,
                entity.id,
            )
            .execute(&mut *transaction)
            .await?;
        }

        transaction.commit().await?;

        Ok(())
    }

    /// Set new flags on (existing) messages. Returns messages that were missing from cache
    pub async fn messages_flags_update<T: Borrow<Uid>, F: Borrow<MessageFlags>>(
        &mut self,
        mailbox_id: &MailboxId,
        update: impl IntoIterator<Item = (T, F)>,
    ) -> Result<Vec<Uid>> {
        let mut missing_messages = Vec::new();
        let mut transaction = self.write_tx().await?;

        for (message_uid, flags) in update {
            let (message_uid, flags) = (message_uid.borrow(), flags.borrow());

            let (seen, flags) = (flags.contains(&MessageFlag::Seen), flags.to_string());

            let result = sqlx::query!(
                "UPDATE message
                SET (flag_seen, flags) = (?1, ?2)
                WHERE mailbox = ?3 AND uid = ?4",
                seen,
                flags,
                mailbox_id,
                message_uid
            )
            .execute(transaction.as_mut())
            .await?;

            if result.rows_affected() == 0 {
                missing_messages.push(*message_uid);
            }
        }

        transaction.commit().await?;
        Ok(missing_messages)
    }

    /// Remove messages
    pub(super) async fn messages_remove<T: Borrow<Uid>>(
        &mut self,
        mailbox_id: MailboxId,
        messages: impl IntoIterator<Item = T>,
    ) -> Result<result::MessagesRemoved> {
        let mut transaction = self.write_tx().await?;
        let mut threads_to_check = Vec::new();

        let mut messages_removed = Vec::new();
        let mut threads_vanished = Vec::new();

        for message_uid in messages {
            let message_uid = message_uid.borrow();
            let (mailbox_id, uid) = (mailbox_id, message_uid);

            // Delete message
            let row = sqlx::query!(
                "DELETE FROM message WHERE mailbox = ? AND uid = ? RETURNING message.thread, message.id",
                mailbox_id,
                uid
            )
            .fetch_optional(transaction.as_mut())
            .await?;

            if let Some(row) = row {
                log::trace!("Deleted message {} from {:?}", uid, mailbox_id);
                messages_removed.push(MessageCacheId::new(row.id));
                threads_to_check.push(ThreadId::new(row.thread));
            }
        }

        for thread_id in threads_to_check {
            let thread_mailboxes = sqlx::query_scalar!(
                "SELECT DISTINCT mailbox FROM message WHERE thread = ?",
                thread_id
            )
            .fetch_all(transaction.as_mut())
            .await?;

            if thread_mailboxes.iter().all(|id| id != mailbox_id.as_ref()) {
                threads_vanished.push(thread_id);
            }
        }

        transaction.commit().await?;

        Ok(result::MessagesRemoved {
            messages_removed,
            threads_vanished,
        })
    }

    /// Fetch all messages belonging to a thread
    pub async fn messages_fetch_for_threads<'a>(
        &'a self,
        thread_ids: impl IntoIterator<Item = ThreadId> + 'a,
    ) -> Result<Pin<Box<impl Stream<Item = Result<(ThreadId, entity::MessageEntity)>> + 'a>>> {
        Ok(Box::pin(
            futures::stream::iter(thread_ids).flat_map_unordered(10, move |thread_id| {
                sqlx::query_as(
                    "
                    SELECT
                        id,
                        thread,
                        mailbox,
                        uid,
                        internal_date,
                        flag_seen,
                        flags,
                        header,
                        header_message_id,
                        header_in_reply_to,
                        attachment_count,
                        preview
                    FROM message WHERE thread = ?",
                )
                .bind(thread_id)
                .fetch(&self.read_pool)
                .map_ok(move |message| (thread_id, message))
                .map_err(Error::from)
            }),
        ))
    }

    /// Fetch the body from
    pub async fn messages_fetch_body<ID: IntoIterator<Item = MessageCacheId>>(
        &mut self,
        message_ids: ID,
        update_last_access: bool,
    ) -> Result<Pin<Box<impl Stream<Item = Result<(MessageCacheId, InternetMessage)>> + use<'_, ID>>>>
    {
        let mut iter = message_ids.into_iter();

        if update_last_access {
            self.messages_update_body_last_access(iter.by_ref()).await?;
        }

        Ok(Box::pin(futures::stream::iter(iter).filter_map(
            |message_id| {
                sqlx::query_scalar(
                    "
                    SELECT
                        body
                    FROM message WHERE id = ?",
                )
                .bind(message_id)
                .fetch_one(&self.read_pool)
                .map_ok(move |opt: Option<InternetMessage>| opt.map(|msg| (message_id, msg)))
                .map_err(Error::from)
                .map(|res| res.transpose())
            },
        )))
    }

    /// Update the last access date of the message body for cache garbage collection purposes
    pub async fn messages_update_body_last_access<ID: IntoIterator<Item = MessageCacheId>>(
        &mut self,
        message_ids: ID,
    ) -> Result<()> {
        let mut tx = self.write_tx().await?;

        for message_id in message_ids {
            sqlx::query(
                r#"
                UPDATE
                    message set last_body_access = datetime('now')
                WHERE id = ?
                "#,
            )
            .bind(message_id)
            .execute(&mut *tx)
            .await?;
        }

        tx.commit().await?;

        Ok(())
    }

    /// Remove message bodies that were not visited within `duration`
    pub async fn messages_garbage_collect(&mut self, days: u32) -> Result<u64> {
        let mut tx = self.write_tx().await?;

        let result = sqlx::query!(
            r#"
            UPDATE
                message set body = null
            WHERE body is not null
            AND julianday(last_body_access) < julianday('now') - ?
            "#,
            days
        )
        .execute(&mut *tx)
        .await?;

        tx.commit().await?;

        Ok(result.rows_affected())
    }

    /// Count all messages in a mailbox
    pub async fn message_count_for_mailbox(&self, mailbox_id: &MailboxId) -> Result<usize> {
        let (count,): (i32,) = sqlx::query_as("SELECT COUNT(*) FROM message WHERE mailbox = ?")
            .bind(mailbox_id)
            .fetch_one(&self.read_pool)
            .await?;

        Ok(count as usize)
    }
}

/// Thread operations
impl DatabaseCache {
    /// Remove all threads in `threads` and put the messages under `root`
    async fn threads_merge(
        transaction: &mut sqlx::SqliteConnection,
        root: &ThreadId,
        threads: &[ThreadId],
    ) -> Result<()> {
        for thread in threads {
            log::debug!("Merging threads {:?} and {:?}", root, thread);

            // Remove this thread from the cache and replace it with root
            sqlx::query!(
                "UPDATE message SET thread = ?1 WHERE thread = ?2",
                root,
                thread
            )
            .execute(&mut *transaction)
            .await?;
        }

        Ok(())
    }

    /// Find threads based on related messages
    async fn thread_ids_find_by_reference(
        transaction: &mut sqlx::SqliteConnection,
        message_id: Option<&MessageId>,
        in_reply_to: Option<&MessageId>,
        references: &[MessageId],
    ) -> Result<Vec<ThreadId>> {
        // IDs to look for
        let reference_ids = if references.is_empty() {
            // Create a one / zero element slice from option
            in_reply_to.map(std::slice::from_ref).unwrap_or_default()
        } else {
            references
        };

        // This is a rather complicated query:
        //
        // Check for:
        // * Our message ID being in the in_reply_to header of another message
        // * Their message ID being in one of our references or in_reply_to
        // * Their references being in one of our references (common ancestor that we don't know about)
        //
        // SELECT DISTINCT message.thread
        // FROM message
        // WHERE message.header_in_reply_to = $message_id
        // OR message.header_message_id IN ($references)
        // OR message.id IN (
        //     SELECT message_header_reference.message
        //     FROM message_header_reference
        //     WHERE message_header_reference.reference IN ($message_id, $references));
        let mut builder =
            sqlx::QueryBuilder::new("SELECT DISTINCT message.thread FROM message WHERE");

        if let Some(message_id) = &message_id {
            builder.push(" message.header_in_reply_to = ");
            builder.push_bind(message_id);
            builder.push(" OR");
        }

        if !reference_ids.is_empty() {
            builder.push(" message.header_message_id IN (");
            let mut separated = builder.separated(", ");
            for id in reference_ids {
                separated.push_bind(id);
            }
            separated.push_unseparated(") ");
            builder.push(" OR");
        }

        builder.push(
            " message.id IN (
            SELECT message_header_reference.message
            FROM message_header_reference
            WHERE message_header_reference.reference IN (",
        );
        let mut separated = builder.separated(", ");
        for id in reference_ids {
            separated.push_bind(id);
        }
        if let Some(message_id) = &message_id {
            separated.push_bind(message_id);
        }
        separated.push_unseparated(")) ");

        let query = builder.build_query_scalar();

        let threads = query.fetch_all(&mut *transaction).await?;

        Ok(threads)
    }

    /// Fetch a thread with an existing ID
    async fn thread_exists_with_id(
        transaction: &mut sqlx::SqliteConnection,
        thread: ThreadId,
    ) -> Result<bool> {
        Ok(sqlx::query_scalar!(
            "SELECT id
            FROM message
            WHERE thread = ?",
            thread
        )
        .fetch_optional(&mut *transaction)
        .await?
        .is_some())
    }

    /// Fetch all threads without the messages
    pub async fn threads_fetch<'a>(
        &'a self,
        mailbox_id: &'a MailboxId,
    ) -> Result<impl futures::Stream<Item = Result<entity::ThreadEntity>> + 'a> {
        log::debug!("Loading cached thread info for mailbox '{:?}'", mailbox_id);

        Ok(sqlx::query_as(
            r#"
            SELECT message.thread as "id", MAX(internal_date) as internal_date_newest
            FROM message
            WHERE mailbox = ?
            GROUP BY message.thread
            "#,
        )
        .bind(mailbox_id)
        .fetch(&self.read_pool)
        .map_err(Error::from))
    }
}
