// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::cell::RefCell;
use std::collections::{BTreeMap, HashMap};

use crate::{imap::*, model::*, types::*};

use glib::prelude::*;
use glib::subclass::prelude::*;

mod imp {
    use std::cell::OnceCell;

    use glib::object::WeakRefNotify;

    use super::*;

    #[derive(Default)]
    pub(crate) struct MemoryCache {
        pub(super) mailboxes_by_imap_name: RefCell<HashMap<MailboxName, ImapMailbox>>,
        pub(super) mailboxes_by_kind: RefCell<HashMap<MailboxKind, Mailbox>>,
        mailboxes: OnceCell<MailboxStore>,

        pub(super) messages_imap: RefCell<BTreeMap<MailboxId, BTreeMap<Uid, MessageCacheId>>>,
        pub(super) messages: RefCell<BTreeMap<MessageCacheId, WeakRefNotify<Message>>>,

        pub(super) threads: RefCell<BTreeMap<ThreadId, WeakRefNotify<Thread>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MemoryCache {
        const NAME: &'static str = "EnvelopeMemoryCache";
        type Type = super::MemoryCache;
    }

    impl ObjectImpl for MemoryCache {}

    impl MemoryCache {
        pub(super) fn mailboxes(&self) -> &MailboxStore {
            self.mailboxes.get_or_init(MailboxStore::default)
        }
    }
}

glib::wrapper! {
    pub(crate) struct MemoryCache(ObjectSubclass<imp::MemoryCache>);
}

impl MemoryCache {
    // Mailbox

    pub fn mailbox_add(&self, mailbox: impl IsA<Mailbox>) {
        if let Some(imap_mailbox) = mailbox.dynamic_cast_ref::<ImapMailbox>() {
            self.imp()
                .mailboxes_by_imap_name
                .borrow_mut()
                .insert(imap_mailbox.imap_name(), imap_mailbox.clone());
        }

        self.imp()
            .mailboxes()
            .insert(mailbox.id(), mailbox.clone().upcast());

        if mailbox.kind() != MailboxKind::Regular {
            self.imp()
                .mailboxes_by_kind
                .borrow_mut()
                .insert(mailbox.kind(), mailbox.upcast());
        }
    }

    pub fn mailbox_remove(&self, mailbox_id: MailboxId) -> Option<Mailbox> {
        let mailbox: Option<Mailbox> = self.imp().mailboxes().remove(mailbox_id);

        if let Some(mailbox) = mailbox.as_ref() {
            if let Some(imap_mailbox) = mailbox.downcast_ref::<ImapMailbox>() {
                self.imp()
                    .mailboxes_by_imap_name
                    .borrow_mut()
                    .remove(&imap_mailbox.imap_name());
            }

            let kind = mailbox.kind();
            if kind != MailboxKind::Regular {
                self.imp().mailboxes_by_kind.borrow_mut().remove(&kind);
            }
        }

        self.imp().messages_imap.borrow_mut().remove(&mailbox_id);

        mailbox
    }

    pub fn mailbox_reset(&self, mailbox: &Mailbox) {
        if let Some(imap_mailbox) = mailbox.downcast_ref::<ImapMailbox>() {
            imap_mailbox.set_imap_uidnext(Uid::NULL);
        }

        mailbox.threads().remove_all();
        mailbox.set_threads_mapped(false);
    }

    pub fn mailbox_by_imap_name(&self, mailbox_name: &MailboxName) -> Option<ImapMailbox> {
        self.imp()
            .mailboxes_by_imap_name
            .borrow()
            .get(mailbox_name)
            .cloned()
    }

    pub fn mailbox_by_kind(&self, mailbox_kind: &MailboxKind) -> Option<Mailbox> {
        self.imp()
            .mailboxes_by_kind
            .borrow()
            .get(mailbox_kind)
            .cloned()
    }

    pub fn mailbox_by_id(&self, mailbox_id: MailboxId) -> Option<Mailbox> {
        self.imp().mailboxes().get(mailbox_id)
    }

    pub fn imap_mailboxes(&self) -> Vec<ImapMailbox> {
        self.imp()
            .mailboxes_by_imap_name
            .borrow()
            .values()
            .cloned()
            .collect()
    }

    pub fn mailboxes(&self) -> MailboxStore {
        self.imp().mailboxes().to_owned()
    }

    // Message

    /// Adds a message to the in-memory weak ref message store
    pub fn message_add(&self, message: &Message) -> Option<Message> {
        let obj = self.clone();
        let mailbox_id = message.mailbox_id();
        let cache_id = message.cache_id();
        let uid = message.downcast_ref::<ImapMessage>().map(|m| m.imap_uid());

        let notify = message.add_weak_ref_notify_local(move || {
            if obj.imp().messages.borrow_mut().remove(&cache_id).is_some() {
                uid.inspect(|uid| {
                    obj.imp()
                        .messages_imap
                        .borrow_mut()
                        .get_mut(&mailbox_id)
                        .map(|m| m.remove(uid));
                });
            }
        });

        if let Some(imap_message) = message.downcast_ref::<ImapMessage>() {
            let mut messages_borrow = self.imp().messages_imap.borrow_mut();
            let entry = messages_borrow.entry(message.mailbox_id()).or_default();
            entry.insert(imap_message.imap_uid(), message.cache_id());
        }

        self.imp()
            .messages
            .borrow_mut()
            .insert(message.cache_id(), notify)
            .and_then(|weak_ref| weak_ref.upgrade())
    }

    /// Gets a message from the in-memory weak ref thread store
    pub fn message_by_imap_uid(
        &self,
        mailbox_id: MailboxId,
        message_uid: Uid,
    ) -> Option<ImapMessage> {
        if let Some(mailbox_store) = self.imp().messages_imap.borrow().get(&mailbox_id) {
            if let Some(message) = mailbox_store.get(&message_uid) {
                return self.message_by_id(*message).and_downcast();
            }
        }

        None
    }

    pub fn message_by_id(&self, message_id: MessageCacheId) -> Option<Message> {
        if let Some(message) = self.imp().messages.borrow().get(&message_id) {
            return message.upgrade();
        }

        None
    }

    // Thread

    /// Adds a thread to the in-memory weak ref thread store
    /// If the store contained an old reference, returns it
    pub fn thread_add(&self, thread: &Thread) -> Option<Thread> {
        let obj = self.clone();
        let thread_id = thread.thread_id();

        let notify = thread.add_weak_ref_notify_local(move || {
            let mut thread_borrow = obj.imp().threads.borrow_mut();
            if thread_borrow
                .get(&thread_id)
                .is_some_and(|weak_ref| weak_ref.upgrade().is_none())
            {
                thread_borrow.remove(&thread_id);
            }
        });

        self.imp()
            .threads
            .borrow_mut()
            .insert(thread_id, notify)
            .and_then(|weak_ref| weak_ref.upgrade())
    }

    /// Gets a message from the in-memory weak ref thread store
    pub fn thread_by_id(&self, thread_id: &ThreadId) -> Option<Thread> {
        if let Some(thread) = self.imp().threads.borrow().get(thread_id) {
            return thread.upgrade();
        }

        None
    }
}

impl std::default::Default for MemoryCache {
    fn default() -> Self {
        glib::Object::new()
    }
}
