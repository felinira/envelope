// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::collections::BTreeMap;

use crate::types::*;

#[must_use]
pub(crate) struct MailboxUpdated {
    pub id: MailboxId,
    pub new: bool,
    pub uidvalidity_reset: bool,
}

#[must_use]
pub(crate) struct MessagesRemoved {
    pub messages_removed: Vec<MessageCacheId>,
    pub threads_vanished: Vec<ThreadId>,
}

#[derive(Default)]
#[must_use]
pub(crate) struct MessagesUpdated {
    pub threads: BTreeMap<ThreadId, time::OffsetDateTime>,
    pub threads_vanished: Vec<ThreadId>,
}
