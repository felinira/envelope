// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::marker::PhantomData;
use std::sync::Arc;
use std::sync::atomic::AtomicI64;

#[derive(Debug, Clone)]
pub(crate) struct IdGenerator<T: AsRef<i64> + From<i64>>(Arc<AtomicI64>, PhantomData<T>);

impl<T: AsRef<i64> + From<i64>> IdGenerator<T> {
    /// Creates a new thread counter with the current maximum value
    pub fn new(max_id: T) -> Self {
        Self(Arc::new(AtomicI64::new(max_id.as_ref() + 1)), PhantomData)
    }

    /// We generate thread IDs by incrementing an internal counter.
    ///
    /// As we are the only ones writing this cache this is fine and saves us round trips to the database.
    pub fn generate(&self) -> T {
        // This returns the *previous* value so we need to make sure we always store the *next* thread id here
        T::from(self.0.fetch_add(1, std::sync::atomic::Ordering::SeqCst))
    }

    pub fn new_max_thread_id(&self, max_value: T) {
        self.0
            .fetch_max(max_value.as_ref() + 1, std::sync::atomic::Ordering::SeqCst);
    }
}
