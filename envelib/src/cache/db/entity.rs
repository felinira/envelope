// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use serde::{Deserialize, Serialize};

use crate::{cache::ErrorKind, error::ErrorKindExt, imap::*, model::*, types::*};

#[derive(Debug, Clone, sqlx::FromRow)]
pub(crate) struct MailboxImapData {
    pub name: MailboxName,
    pub kind: MailboxKind,
    pub delimiter: String,

    pub uidnext: Uid,
    pub uidfirst: Uid,
    pub uidvalidity: UidValidity,
}

#[derive(Debug, Clone, sqlx::FromRow)]
pub(crate) struct MailboxEntity {
    pub id: MailboxId,
    #[sqlx(flatten)]
    pub data: MailboxImapData,
}

#[derive(Debug, Clone, sqlx::FromRow)]
pub struct ThreadEntity {
    pub id: ThreadId,
    pub internal_date_newest: time::OffsetDateTime,
}

#[derive(Debug, Default, Clone, Serialize, Deserialize)]
pub(crate) struct Header {
    #[serde(default)]
    pub participants: Participants,
    #[serde(default)]
    pub subject: Option<String>,
    #[serde(default)]
    pub date: Option<time::OffsetDateTime>,
}

#[derive(Debug, Clone, sqlx::FromRow)]
pub(crate) struct MessageDataFields {
    pub uid: Uid,
    pub internal_date: time::OffsetDateTime,
    pub flag_seen: bool,
    pub flags: String,
    pub header: String,
    pub header_message_id: MessageId,
    pub header_in_reply_to: Option<MessageId>,
    pub attachment_count: i64,
    pub preview: Option<String>,
}

#[derive(Debug, Clone, sqlx::FromRow)]
pub(crate) struct MessageEntity {
    pub id: MessageCacheId,
    pub thread: ThreadId,
    pub mailbox: MailboxId,
    #[sqlx(flatten)]
    pub fields: MessageDataFields,
    #[sqlx(default)]
    pub body: Option<InternetMessage>,
}

impl From<String> for MailboxKind {
    fn from(value: String) -> Self {
        match value.as_str() {
            "inbox" => Self::Inbox,
            "drafts" => Self::Drafts,
            "sent" => Self::Sent,
            "junk" => Self::Junk,
            "trash" => Self::Trash,
            "outbox" => Self::Outbox,
            "all" => Self::All,
            "archive" => Self::Archive,
            _ => Self::Regular,
        }
    }
}

impl From<i64> for Uid {
    fn from(value: i64) -> Self {
        Uid::new(value as u32)
    }
}

impl From<i64> for UidValidity {
    fn from(value: i64) -> Self {
        UidValidity::new(value as u32)
    }
}

impl From<MailboxEntity> for ImapMailbox {
    fn from(value: MailboxEntity) -> Self {
        let mailbox = Self::from_imap(
            value.id,
            MailboxInfo {
                name: value.data.name,
                delimiter: Some(value.data.delimiter),
                kind: value.data.kind,
                // TODO: Store in database
                selectable: true,
                no_inferiors: false,
                marked: None,
            },
        );

        mailbox.set_imap_uidvalidity(value.data.uidvalidity);
        mailbox.set_imap_uidnext(value.data.uidnext);
        mailbox.set_imap_uidfirst(value.data.uidfirst);
        mailbox
    }
}

impl From<&ImapMailbox> for MailboxImapData {
    fn from(value: &ImapMailbox) -> Self {
        Self {
            name: value.imap_name(),
            kind: value.kind(),
            delimiter: value.delimiter(),
            uidnext: value.imap_uidnext(),
            uidfirst: value.imap_uidfirst(),
            uidvalidity: value.imap_uidvalidity(),
        }
    }
}

impl From<MailboxInfo> for MailboxImapData {
    fn from(value: MailboxInfo) -> Self {
        Self {
            name: value.name,
            kind: value.kind,
            delimiter: value.delimiter.unwrap_or_default(),
            uidnext: Uid::NULL,
            uidvalidity: UidValidity::NULL,
            uidfirst: Uid::NULL,
        }
    }
}

impl From<ThreadEntity> for Thread {
    fn from(value: ThreadEntity) -> Self {
        Self::new(&value.id, &value.internal_date_newest.to_glib())
    }
}

impl From<MessageEntity> for ImapMessage {
    fn from(value: MessageEntity) -> Self {
        let preview = value.fields.preview;

        let message = Self::from_imap(
            value.id,
            value.mailbox,
            value.fields.uid,
            value.thread,
            value.fields.internal_date.to_glib(),
        );

        let header: Header = serde_json::from_str(&value.fields.header).unwrap_or_default();

        message.set_participants(header.participants);
        message.set_subject(header.subject);
        message.set_date(header.date.map(|d| d.to_glib()));

        message.update_flags(&value.fields.flags.as_str().into());
        message.set_message_id_header(value.fields.header_message_id);
        message.set_attachment_count(value.fields.attachment_count as u32);
        message.set_is_unread(!value.fields.flag_seen);
        message.set_attachment_count(value.fields.attachment_count as u32);
        message.set_preview_text(preview);

        message
    }
}

impl TryFrom<&MessageData> for MessageDataFields {
    type Error = crate::cache::Error;

    fn try_from(value: &MessageData) -> Result<Self, Self::Error> {
        let message = value.message.as_ref().ok_or_else(|| {
            ErrorKind::Inconsistency.with_message("Server did not provide us with a message body")
        })?;

        let headers = Header {
            participants: message.parse_participants(),
            subject: Some(message.parse_subject()),
            date: message.parse_date(),
        };

        let headers_json = serde_json::to_string(&headers).map_err(|err| {
            ErrorKind::Inconsistency.with_message(format!("Serializing headers: {err}"))
        })?;

        let flags = value.flags.as_ref().ok_or_else(|| {
            ErrorKind::Inconsistency.with_message("Server did not provide us with message flags")
        })?;

        Ok(Self {
            uid: value.uid,
            internal_date: value.internal_date.ok_or_else(|| {
                ErrorKind::Inconsistency
                    .with_message("Server did not provide us with a message date")
            })?,
            flag_seen: flags.is_seen(),
            flags: flags.to_string(),
            header: headers_json,
            header_message_id: message.parse_message_id(),
            header_in_reply_to: message.parse_in_reply_to(),
            attachment_count: value.attachment_count.unwrap_or_default() as i64,
            preview: message.parse_preview_text(),
        })
    }
}

impl From<&ImapMessage> for MessageDataFields {
    fn from(value: &ImapMessage) -> Self {
        let headers = Header {
            participants: value.participants(),
            subject: value.subject(),
            date: value.date().map(|d| d.to_offset_dt()),
        };

        let headers_json = serde_json::to_string(&headers).unwrap_or_else(|err| {
            log::error!("Serializing headers: {}", err);
            String::new()
        });

        Self {
            uid: value.imap_uid(),
            internal_date: value.internal_date().to_offset_dt(),
            flag_seen: value.is_read(),
            flags: value.flags().to_string(),
            header: headers_json,
            header_message_id: value.message_id_header(),
            header_in_reply_to: value.message_content().and_then(|c| c.parse_in_reply_to()),
            attachment_count: value.attachment_count().into(),
            preview: value.preview_text(),
        }
    }
}

impl From<&ImapMessage> for MessageEntity {
    fn from(value: &ImapMessage) -> Self {
        Self {
            id: value.cache_id(),
            thread: value.thread_id(),
            mailbox: value.mailbox_id(),
            fields: value.into(),
            body: value.message_content(),
        }
    }
}
