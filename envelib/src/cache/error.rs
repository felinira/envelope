// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use crate::{error::ErrorKindExt, gettext::*};
use std::fmt::Display;

pub(crate) type Error = crate::error::ErrorTy<ErrorKind>;
pub(crate) type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Clone, Copy, glib::ErrorDomain)]
#[error_domain(name = "EnvelopeCache")]
pub enum ErrorKind {
    Sqlx,
    SqlxMigrate,
    Io,
    Inconsistency,
}

impl ErrorKindExt for ErrorKind {}
impl Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ErrorKind::Sqlx => pgettext("cache::error", "Database"),
                ErrorKind::SqlxMigrate => pgettext("cache::error", "Database Consistency"),
                ErrorKind::Io => pgettext("cache::error", "Database I/O"),
                ErrorKind::Inconsistency => pgettext("cache::error", "Internal Inconsistency"),
            }
        )
    }
}

// Conversions

impl From<sqlx::Error> for Error {
    fn from(value: sqlx::Error) -> Self {
        ErrorKind::Sqlx.with_source(value)
    }
}

impl From<sqlx::migrate::MigrateError> for Error {
    fn from(value: sqlx::migrate::MigrateError) -> Self {
        ErrorKind::SqlxMigrate.with_source(value)
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        ErrorKind::Io.with_source(value)
    }
}
