// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::fmt::{Display, Formatter};

use crate::{gettext::*, model::TaskStatus, types::ConnectionStatus};

pub trait ErrorKindExt:
    glib::prelude::ErrorDomain + Display + std::fmt::Debug + Clone + Copy + Send + 'static
{
    fn with_source(self, source: impl std::error::Error + Send + 'static) -> ErrorTy<Self> {
        ErrorTy::from(self).with_source(source)
    }

    fn with_message(self, message: impl ToString) -> ErrorTy<Self> {
        ErrorTy::from(self).with_message(message)
    }

    fn with_backtrace(self, backtrace: Option<std::backtrace::Backtrace>) -> ErrorTy<Self> {
        ErrorTy::from(self).with_backtrace(backtrace)
    }
}

#[derive(Debug)]
/// The shared error kind for envelib
pub struct ErrorTy<T: ErrorKindExt> {
    kind: T,
    source: Option<Box<dyn std::error::Error + Send>>,
    #[allow(dead_code)]
    backtrace: Option<std::backtrace::Backtrace>,
}

impl<T: ErrorKindExt> Display for ErrorTy<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if let Some(source) = &self.source {
            write!(f, "{} ({})", self.kind, source)
        } else {
            write!(f, "{}", self.kind)
        }
    }
}

impl<T: ErrorKindExt> std::error::Error for ErrorTy<T> {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(self.source.as_deref()?)
    }
}

impl<T: ErrorKindExt> From<T> for ErrorTy<T> {
    fn from(kind: T) -> Self {
        Self {
            kind,
            source: None,
            backtrace: Some(std::backtrace::Backtrace::capture()),
        }
    }
}

impl<T: ErrorKindExt> ErrorTy<T> {
    pub fn with_source(mut self, source: impl std::error::Error + Send + 'static) -> Self {
        self.source = Some(Box::new(source));
        self
    }

    pub fn with_message(self, message: impl ToString) -> Self {
        self.with_source(ErrorMessage(message.to_string()))
    }

    pub fn with_backtrace(mut self, backtrace: Option<std::backtrace::Backtrace>) -> Self {
        self.backtrace = backtrace;
        self
    }

    pub fn backtrace(&self) -> Option<&std::backtrace::Backtrace> {
        self.backtrace.as_ref()
    }
}

impl<T: ErrorKindExt> ErrorTy<T> {
    pub fn kind(&self) -> T {
        self.kind
    }
}

impl<T: ErrorKindExt> From<ErrorTy<T>> for glib::Error {
    fn from(value: ErrorTy<T>) -> Self {
        glib::Error::new(
            value.kind,
            &value
                .source
                .as_ref()
                .map(ToString::to_string)
                .unwrap_or_default(),
        )
    }
}

#[derive(Debug)]
pub struct ErrorMessage(String);

impl Display for ErrorMessage {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl std::error::Error for ErrorMessage {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        None
    }
}

pub type Error = ErrorTy<ErrorKind>;
pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, glib::ErrorDomain)]
#[error_domain(name = "EnvelopeBackend")]
pub enum ErrorKind {
    /// Aborted by the user, doesn't need to be handled
    Aborted,
    /// An error ocurred from the account configuration
    AccountConfig,
    /// An error occurred in the cache component
    Cache,
    /// Something went wrong when trying to establish a connection
    Connection,
    /// An operation was performed that requires a connection, but we don't have any.
    ///
    /// This is different from `Connection`, which is an error that occurs *when connecting*.
    /// Disconnected will be returned when we don't even try to connect, to signal to the caller
    /// that calling this method is currently not possible.
    ///
    /// This will also be thrown when the client is in shutdown and the operation is no longer available.
    Disconnected,
    /// An internal inconsistency ocurred. In contract to a panic which would be thrown due to a
    /// bug in the program, this will be returned in cases where the inconsistency comes from external
    /// factors, such as the server returning confusing or garbage results, IDs not matching to each
    /// other etc.
    Inconsistency,
    /// The supplied credentials were wrong, and there is no way to recover. This will be thrown instead
    /// of calling the delegate, whenever we can't do anything about it anymore.
    InvalidCredentials,
}

impl ErrorKindExt for ErrorKind {}
impl Display for ErrorKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ErrorKind::Aborted => pgettext("error", "Aborted"),
                ErrorKind::AccountConfig => pgettext("error", "Account Configuration"),
                ErrorKind::Cache => pgettext("error", "Cache Error"),
                ErrorKind::Connection => pgettext("error", "Connection Error"),
                ErrorKind::Disconnected => pgettext("error", "Disconnected"),
                ErrorKind::Inconsistency => pgettext("error", "Internal Inconsistency"),
                ErrorKind::InvalidCredentials => pgettext("error", "Invalid Credentials"),
            }
        )
    }
}

// Conversions

impl From<crate::account::AccountConfigError> for Error {
    fn from(value: crate::account::AccountConfigError) -> Self {
        ErrorKind::AccountConfig.with_source(value)
    }
}

impl From<crate::cache::Error> for Error {
    fn from(mut value: crate::cache::Error) -> Self {
        ErrorKind::Cache
            .with_backtrace(value.backtrace.take())
            .with_source(value)
    }
}

impl From<std::io::Error> for Error {
    fn from(value: std::io::Error) -> Self {
        ErrorKind::Connection.with_source(value)
    }
}

impl From<&Error> for TaskStatus {
    fn from(value: &Error) -> Self {
        if matches!(value.kind(), ErrorKind::Aborted) {
            TaskStatus::Aborted
        } else if matches!(
            value.kind(),
            ErrorKind::Connection | ErrorKind::Disconnected
        ) {
            TaskStatus::ConnectionError(ConnectionStatus::Error(value.to_string()))
        } else {
            TaskStatus::Error(value.to_string())
        }
    }
}
