// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#![allow(unused)]

pub use gettextrs::*;
use std::fmt::Display;

use crate::model::{ToGlibDate, ToOffsetDateTime};

fn fmt(mut format: String, args: &[&dyn Display]) -> String {
    for arg in args {
        format = format.replacen("{}", &arg.to_string(), 1);
    }

    for (i, arg) in args.iter().enumerate() {
        format = format.replace(&format!("{{{i}}}"), &arg.to_string());
    }

    format
}

pub fn gettextf(msgid: &str, args: &[&dyn Display]) -> String {
    fmt(gettext(msgid), args)
}

pub fn pgettextf(msgctxt: &str, msgid: &str, args: &[&dyn Display]) -> String {
    fmt(pgettext(msgctxt, msgid), args)
}

pub fn ngettextf(
    msgid: &str,
    msgid_plural: &str,
    n: impl TryInto<u32>,
    args: &[&dyn Display],
) -> String {
    fmt(
        ngettext(msgid, msgid_plural, n.try_into().unwrap_or(u32::MAX)),
        args,
    )
}

pub fn ngettextf_(msgid: &str, msgid_plural: &str, n: impl Copy + TryInto<u32>) -> String {
    ngettextf(
        msgid,
        msgid_plural,
        n.try_into().unwrap_or(u32::MAX),
        &[&n.try_into().unwrap_or(u32::MAX)],
    )
}

pub fn npgettextf(
    msgctxt: &str,
    msgid: &str,
    msgid_plural: &str,
    n: impl TryInto<u32>,
    args: &[&dyn Display],
) -> String {
    fmt(
        npgettext(
            msgctxt,
            msgid,
            msgid_plural,
            n.try_into().unwrap_or(u32::MAX),
        ),
        args,
    )
}

pub fn gettext_short_date(date: Option<&glib::DateTime>) -> String {
    if let Some(date) = date.map(|d| d.to_offset_dt()) {
        let month = match date.month() {
            time::Month::January => gettext("Jan"),
            time::Month::February => gettext("Feb"),
            time::Month::March => gettext("Mar"),
            time::Month::April => gettext("Apr"),
            time::Month::May => gettext("May"),
            time::Month::June => gettext("Jun"),
            time::Month::July => gettext("Jul"),
            time::Month::August => gettext("Aug"),
            time::Month::September => gettext("Sep"),
            time::Month::October => gettext("Oct"),
            time::Month::November => gettext("Nov"),
            time::Month::December => gettext("Dec"),
        };

        if date.year() == time::OffsetDateTime::now_utc().year() {
            // Translators: date format: {0} = day, {1} = month name
            gettextf("{1} {0}", &[&date.day().to_string(), &month])
        } else {
            gettextf(
                // Translators: date format: {0} = day, {1} = month name, {2} = year
                "{1} {0} {2}",
                &[&date.day().to_string(), &month, &date.year().to_string()],
            )
        }
    } else {
        gettext("Unknown Date")
    }
}
