// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod db;
mod error;
mod mem;

use crate::error::ErrorKindExt;
pub use error::*;

use crate::account;
use crate::imap;
use crate::model::*;
use crate::types::*;
use crate::utils::DisplayLog;
use db::*;
use mem::*;

use futures::TryStreamExt;
use gio::prelude::*;
use gio::subclass::prelude::*;
use std::{
    borrow::Borrow,
    cell::{OnceCell, RefCell},
    collections::BTreeMap,
    future::Future,
    path::PathBuf,
};

mod imp {
    use std::{cell::Cell, collections::HashSet, sync::Arc};

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Cache)]
    pub(crate) struct Cache {
        #[property(get, set = Self::set_account, construct)]
        account: RefCell<Option<crate::account::Account>>,
        #[property(get, construct_only)]
        cache_dir: OnceCell<PathBuf>,
        #[property(get, set, construct)]
        body_cache_days: Cell<u32>,

        pub(super) mem: MemoryCache,
        pub(super) db: OnceCell<smol::lock::RwLock<DatabaseCache>>,

        self_addrs: RefCell<Arc<HashSet<Address>>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Cache {
        const NAME: &'static str = "EnvelopeCache";
        type Type = super::Cache;
        type Interfaces = (gio::AsyncInitable,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for Cache {
        fn constructed(&self) {
            self.parent_constructed();

            // TODO: Make configurable
            self.body_cache_days.set(30);
        }
    }

    impl AsyncInitableImpl for Cache {
        fn init_future(
            &self,
            _io_priority: glib::Priority,
        ) -> std::pin::Pin<
            Box<dyn Future<Output = std::prelude::v1::Result<(), glib::Error>> + 'static>,
        > {
            let imp = self.ref_counted();

            Box::pin(glib::clone!(
                #[strong]
                imp,
                async move {
                    // Initialize the database cache
                    let cache_dir = imp.cache_dir.get().unwrap().clone();
                    let db = DatabaseCache::new(cache_dir).await?;

                    // Fetch all mailboxes so that all other methods don't need to care about it
                    let mut mailboxes = db.mailboxes_fetch().await?;
                    while let Some(mailbox) = mailboxes.try_next().await? {
                        imp.mem.mailbox_add(ImapMailbox::from(mailbox))
                    }

                    drop(mailboxes);
                    imp.db
                        .set(smol::lock::RwLock::new(db))
                        .expect("OnceCell must not be initialized yet");

                    Ok(())
                }
            ))
        }
    }

    impl Cache {
        fn set_account(&self, account: account::Account) {
            let self_addrs: Arc<HashSet<Address>> =
                Arc::new(account.own_addrs().into_iter().map(Into::into).collect());
            self.self_addrs.replace(self_addrs);
            self.account.replace(Some(account));
        }

        async fn db_read(&self) -> smol::lock::RwLockReadGuard<'_, DatabaseCache> {
            self.db.get().unwrap().read().await
        }

        async fn db_write(&self) -> smol::lock::RwLockWriteGuard<'_, DatabaseCache> {
            self.db.get().unwrap().write().await
        }

        pub async fn shutdown(&self) -> Result<()> {
            // TODO: Should we run this at every shutdown? Might make sense to limit execution time
            self.garbage_collect().await?;
            self.db_write().await.close().await;
            Ok(())
        }

        pub fn mailboxes(&self) -> MailboxStore {
            self.mem.mailboxes()
        }

        pub fn imap_mailboxes(&self) -> Vec<ImapMailbox> {
            self.mem.imap_mailboxes()
        }

        pub fn mailbox_by_id(&self, mailbox_id: MailboxId) -> Option<Mailbox> {
            self.mem.mailbox_by_id(mailbox_id)
        }

        pub fn mailbox_by_kind(&self, mailbox_kind: MailboxKind) -> Option<Mailbox> {
            self.mem.mailbox_by_kind(&mailbox_kind)
        }

        pub(super) async fn mailbox_reset(&self, mailbox: &Mailbox) -> Result<()> {
            log::warn!("Resetting Mailbox '{:?}'", mailbox.id());
            self.db_write().await.mailbox_reset(mailbox.id()).await?;
            self.mem.mailbox_reset(mailbox);

            Ok(())
        }

        async fn load_mailbox_threads(
            &self,
            mailbox_id: MailboxId,
            store: ThreadStore,
        ) -> Result<()> {
            let db = self.db_write().await;

            let mut stream = db
                .threads_fetch(&mailbox_id)
                .await?
                .map_ok(move |row| {
                    if let Some(thread) = self.mem.thread_by_id(&row.id) {
                        thread
                    } else {
                        let thread = Thread::new(&row.id, &row.internal_date_newest.to_glib());
                        self.mem.thread_add(&thread);
                        thread
                    }
                })
                .try_chunks(500);

            while let Some(threads) = stream.try_next().await.map_err(|e| e.1)? {
                store.extend(threads.into_iter().map(|t| (t.thread_id(), t)));
            }

            store.set_finished_loading(true);

            Ok(())
        }

        pub fn mailbox_map_threads(&self, mailbox: &Mailbox) {
            log::debug!("Loading thread store for mailbox '{:?}'", mailbox.id());

            let store = mailbox.threads();
            if !mailbox.threads_mapped() {
                mailbox.set_threads_mapped(true);
                let id = mailbox.id();
                let imp = self.ref_counted();

                glib::spawn_future_local(glib::clone!(
                    #[strong]
                    store,
                    async move {
                        if let Err(err) = imp.load_mailbox_threads(id, store).await {
                            log::error!("Error loading threads from cache: {}", err);
                        }
                    }
                ));
            }
        }

        pub async fn threads_load_full<T: AsRef<Thread>>(
            &self,
            threads: impl IntoIterator<Item = T>,
        ) -> Result<()> {
            let time: std::time::Instant = std::time::Instant::now();

            let threads: BTreeMap<ThreadId, T> = threads
                .into_iter()
                .filter_map(|thread| {
                    let t = thread.as_ref();
                    if !t.is_loading() && !t.is_loaded() {
                        t.set_is_loading(true);
                        Some((t.thread_id(), thread))
                    } else {
                        None
                    }
                })
                .collect();

            let thread_ids = threads.keys().cloned().collect::<Vec<_>>();
            let db = self.db_read().await;
            let mut stream = db.messages_fetch_for_threads(thread_ids).await?;

            while let Some((thread_id, entity)) = stream.try_next().await? {
                if let Some(thread) = threads.get(&thread_id).map(AsRef::as_ref) {
                    let message = if let Some(message) = self
                        .mem
                        .message_by_imap_uid(entity.mailbox, entity.fields.uid)
                    {
                        message
                    } else {
                        let message = ImapMessage::from(entity);
                        let Some(mailbox) = self.mem.mailbox_by_id(message.mailbox_id()) else {
                            return Err(ErrorKind::Inconsistency.with_message("Found message that does not belong to any mailbox still known to us"));
                        };

                        message.set_mailbox(mailbox);
                        message.set_self_addresses(self.self_addrs.borrow().clone());

                        self.mem.message_add(message.upcast_ref());
                        message
                    };

                    thread.add_message(message);
                    thread.set_is_loading(false);
                    thread.set_is_loaded(true);
                }
            }

            if time.elapsed() > std::time::Duration::from_millis(50) {
                log::debug!(
                    "Loading thread messages for {} threads took {}",
                    threads.len(),
                    time.elapsed().display()
                );
            }

            Ok(())
        }

        pub async fn message_count_for_mailbox(&self, mailbox_id: &MailboxId) -> Result<usize> {
            self.db_read()
                .await
                .message_count_for_mailbox(mailbox_id)
                .await
        }

        pub(super) async fn imap_mailbox_update(&self, mailbox: &ImapMailbox) -> Result<()> {
            let updated = self
                .db_write()
                .await
                .mailbox_update(&mailbox.into())
                .await?;
            mailbox.set_id(updated.id);

            if updated.uidvalidity_reset {
                self.mem.mailbox_reset(mailbox.upcast_ref());
            } else if updated.new {
                self.mem
                    .mailbox_add(mailbox.upcast_ref::<Mailbox>().clone());
            }

            Ok(())
        }

        pub(super) async fn mailbox_update_info(&self, info: imap::MailboxInfo) -> Result<()> {
            if let Some(mailbox) = self.mem.mailbox_by_imap_name(&info.name) {
                mailbox.set_imap_info(info);
                self.imap_mailbox_update(&mailbox).await?;
            } else {
                // We don't know about this mailbox yet
                let updated = self
                    .db_write()
                    .await
                    .mailbox_update(&info.clone().into())
                    .await?;
                let mailbox = ImapMailbox::from_imap(updated.id, info);
                self.mem.mailbox_add(mailbox.clone());
            }

            Ok(())
        }

        pub(super) async fn mailbox_remove(&self, mailbox_id: MailboxId) -> Result<()> {
            self.mem.mailbox_remove(mailbox_id);
            self.db_write().await.mailbox_remove(mailbox_id).await?;
            Ok(())
        }

        pub(super) async fn messages_remove<T: Borrow<imap::Uid>>(
            &self,
            mailbox: &Mailbox,
            messages: impl IntoIterator<Item = T>,
        ) -> Result<()> {
            let result = self
                .db_write()
                .await
                .messages_remove(mailbox.id(), messages)
                .await?;

            for message_id in result.messages_removed {
                if let Some(message) = self.mem.message_by_id(message_id) {
                    if let Some(thread) = message.thread() {
                        thread.remove_message(&message);
                        mailbox.threads().update(thread.thread_id());
                    }
                }
            }

            for thread_id in result.threads_vanished {
                mailbox.threads().remove(thread_id);
            }

            Ok(())
        }

        pub(super) async fn messages_update(
            &self,
            messages: impl IntoIterator<Item = impl AsRef<ImapMessage>>,
        ) -> Result<()> {
            self.db_write()
                .await
                .messages_commit(
                    messages
                        .into_iter()
                        .map(|m| entity::MessageEntity::from(m.as_ref())),
                )
                .await?;

            Ok(())
        }

        pub(super) async fn messages_load_full(
            &self,
            messages: impl IntoIterator<Item = impl AsRef<Message>>,
        ) -> Result<()> {
            let messages = messages
                .into_iter()
                .map(|msg| msg.as_ref().clone())
                .filter(|msg| msg.detail_level() < MessageDetailLevel::Full)
                .map(|msg| msg.cache_id());

            let mut db = self.db_write().await;
            let mut bodies = db.messages_fetch_body(messages, true).await?;

            while let Some((id, imf)) = bodies.try_next().await? {
                // If message found
                if let Some(msg) = self.mem.message_by_id(id) {
                    log::debug!("Fetched {} from disk cache", msg.message_id_header());
                    msg.set_message_body(Some(imf));
                }
            }

            Ok(())
        }
    }

    /// IMAP specific extensions
    impl Cache {
        pub fn mailbox_by_imap_name(
            &self,
            mailbox_name: &imap::MailboxName,
        ) -> Option<ImapMailbox> {
            self.mem.mailbox_by_imap_name(mailbox_name)
        }

        pub async fn imap_message_data_update(
            &self,
            mailbox_name: &imap::MailboxName,
            message_data: impl IntoIterator<Item = &imap::MessageData>,
        ) -> Result<()> {
            let timer = std::time::Instant::now();
            let mut n = 0;

            let Some(mailbox) = self.mem.mailbox_by_imap_name(mailbox_name) else {
                return Err(ErrorKind::Inconsistency.with_message(format!(
                    "Mailbox {} not found in memory cache",
                    mailbox_name
                )));
            };

            // As we check here whether to update the flags or not, aquire db lock already.
            // This ensures no concurrent reads are in progress while we update the message data
            let mut db = self.db_write().await;

            // If the message already exists, we update it with the new info
            let message_data = message_data.into_iter().inspect(|data| {
                n += 1;
                if let Some(message) = self.mem.message_by_imap_uid(mailbox.id(), data.uid) {
                    message.update_data(data);
                    message.set_mailbox(mailbox.clone());
                }
            });

            // Commit the update to cache
            let update_result = db.messages_update(mailbox.id(), message_data).await?;

            // We don't need the database anymore
            drop(db);

            let store = mailbox.threads();
            let mut threads_added = Vec::new();
            let mut threads_to_load = Vec::new();

            for (thread_id, date) in update_result.threads {
                let date = date.to_glib();

                if let Some(thread) = self.mem.thread_by_id(&thread_id) {
                    if thread.sort_date().as_ref() < Some(&date) || thread.sort_date().is_none() {
                        thread.set_sort_date(date);
                        store.update(thread.thread_id());
                    }

                    if thread.is_loaded() {
                        thread.set_is_loaded(false);
                        threads_to_load.push(thread.clone());
                    }
                } else {
                    let thread = Thread::new(&thread_id, &date);

                    self.mem.thread_add(&thread);
                    threads_added.push(thread);
                }
            }

            // Load threads that were loaded before
            self.threads_load_full(threads_to_load).await?;

            // Insert threads in sorted order
            threads_added.sort_unstable_by_key(|t| std::cmp::Reverse(t.sort_date()));

            store.extend(threads_added.into_iter().map(|t| (t.thread_id(), t)));

            for thread in update_result.threads_vanished {
                store.remove(thread);
            }

            log::trace!(
                "message_data_update took {} for {} messages",
                timer.elapsed().display(),
                n
            );

            // TODO
            Ok(())
        }

        pub async fn imap_messages_update_flags<T: Borrow<imap::Uid>, F: Borrow<MessageFlags>>(
            &self,
            mailbox_name: &imap::MailboxName,
            update: impl IntoIterator<Item = (T, F)>,
        ) -> Result<Vec<imap::Uid>> {
            let instant_start = std::time::Instant::now();

            let Some(mailbox) = self.mem.mailbox_by_imap_name(mailbox_name) else {
                return Err(ErrorKind::Inconsistency.with_message(format!(
                    "Mailbox {} not found in memory cache",
                    mailbox_name
                )));
            };

            // As we check here whether to update the flags or not, aquire db lock already
            let mut db = self.db_write().await;

            // TODO: Ignore "unimportant" flags that we don't care about
            let iter = update.into_iter().filter(|(message_uid, flags)| {
                if let Some(message) = self
                    .mem
                    .message_by_imap_uid(mailbox.id(), *message_uid.borrow())
                {
                    // If the in-memory version didn't have any changes we can skip the update
                    message.update_flags(flags.borrow())
                } else {
                    // Always update the unavailable messages to keep an up-to-date cache
                    true
                }
            });

            let missing_messages = db.messages_flags_update(&mailbox.id(), iter).await?;

            log::debug!(
                "Updating flags for {} took {}",
                mailbox_name,
                instant_start.elapsed().display()
            );

            Ok(missing_messages)
        }

        /// Run periodic garbage collection activities
        pub async fn garbage_collect(&self) -> Result<()> {
            log::debug!("Running message cache gc");
            let now = std::time::Instant::now();
            let days = self.body_cache_days.get();

            let mut db = self.db_write().await;
            let rows_affected = db.messages_garbage_collect(days).await?;
            log::debug!(
                "Message cache gc took {:.2} seconds and cleared {} messages older than {} days",
                now.elapsed().as_secs_f32(),
                rows_affected,
                days
            );
            Ok(())
        }
    }
}

glib::wrapper! {
    pub(crate) struct Cache(ObjectSubclass<imp::Cache>)
    @implements gio::AsyncInitable;
}

impl Cache {
    /// Create a new cache via gio::AsyncInitable
    pub async fn new(
        account: account::Account,
        cache_dir: PathBuf,
    ) -> std::result::Result<Self, glib::Error> {
        gio::AsyncInitable::builder()
            .property("account", account)
            .property("cache_dir", cache_dir)
            .build_future(glib::Priority::DEFAULT)
            .await
    }

    /// Shutdown all connections. This will make the [`Cache`] unusable afterwards.
    pub async fn shutdown(&self) -> Result<()> {
        self.imp().shutdown().await
    }

    /// All [mailboxes](Mailbox) known to the cache.
    pub fn mailboxes(&self) -> MailboxStore {
        self.imp().mailboxes()
    }

    pub fn imap_mailboxes(&self) -> Vec<ImapMailbox> {
        self.imp().imap_mailboxes()
    }

    /// Gets a [`Mailbox`] by [id](MailboxId).
    pub fn mailbox(&self, mailbox_id: MailboxId) -> Option<Mailbox> {
        self.imp().mailbox_by_id(mailbox_id)
    }

    pub fn imap_mailbox(&self, mailbox_id: MailboxId) -> Option<ImapMailbox> {
        self.mailbox(mailbox_id).and_downcast()
    }

    /// Lookup a [`Mailbox`] by specific kind.
    pub fn mailbox_of_kind(&self, mailbox_kind: MailboxKind) -> Option<Mailbox> {
        self.imp().mailbox_by_kind(mailbox_kind)
    }

    /// Commits changes to a [`Mailbox`].
    ///
    /// # Returns
    ///
    /// - cache update of type [`CacheUpdate::MailboxAdded`] if the mailbox isn't already known.
    /// - cache update of type [`CacheUpdate::MailboxRest`] if the [`UidValidity`] has changed.
    pub async fn imap_mailbox_update(&self, mailbox: &ImapMailbox) -> Result<()> {
        if mailbox.uncommitted() {
            self.imp().imap_mailbox_update(mailbox).await?;
        }

        Ok(())
    }

    /// Update or create a [`Mailbox`] based on new [`MailboxInfo`].
    ///
    /// # Returns
    ///
    /// - cache update of type [`CacheUpdate::MailboxAdded`] if the mailbox isn't already known.
    /// - cache update of type [`CacheUpdate::MailboxRest`] if the [`UidValidity`] has changed.
    pub async fn mailbox_update_info(&self, info: imap::MailboxInfo) -> Result<()> {
        self.imp().mailbox_update_info(info).await
    }

    /// Delete all cached messages in a [`Mailbox`]
    ///
    /// # Returns
    ///
    /// Returns a cache update of type [`CacheUpdate::MailboxReset`]
    #[allow(dead_code)]
    pub async fn mailbox_reset(&self, mailbox: &Mailbox) -> Result<()> {
        self.imp().mailbox_reset(mailbox).await
    }

    /// Remove any knowledge of a [`Mailbox`].
    ///
    /// # Returns
    ///
    /// Returns a cache update of type [`CacheUpdate::MailboxRemoved`]
    pub async fn mailbox_remove(&self, mailbox_id: MailboxId) -> Result<()> {
        self.imp().mailbox_remove(mailbox_id).await
    }

    /// Initialize the thread list of the specified mailbox if not already done.
    ///
    /// Returns the thread store.
    pub fn mailbox_map_threads(&self, mailbox: &Mailbox) {
        self.imp().mailbox_map_threads(mailbox);
    }

    /// Load all messages in `thread` and set them on their respective [`Thread`].
    pub async fn threads_load_full(
        &self,
        threads: impl IntoIterator<Item = impl AsRef<Thread>>,
    ) -> Result<()> {
        self.imp().threads_load_full(threads).await
    }

    /// Returns the number of messages in `mailbox_name`.
    pub async fn message_count_for_mailbox(&self, mailbox_id: &MailboxId) -> Result<usize> {
        self.imp().message_count_for_mailbox(mailbox_id).await
    }

    /// Remove the message from [`Cache`]
    ///
    /// Returns a cache update of type [`CacheUpdate::Threads`]
    ///
    /// - `threads_vanished` if threads have been merged.
    pub async fn message_remove(&self, mailbox: &Mailbox, message_uid: imap::Uid) -> Result<()> {
        self.messages_remove(mailbox, std::iter::once(message_uid))
            .await
    }

    /// Remove the messages from [`Cache`]
    ///
    /// Returns a cache update of type [`CacheUpdate::Threads`]
    ///
    /// - `threads_vanished` if threads have been merged.
    pub async fn messages_remove<T: Borrow<imap::Uid>>(
        &self,
        mailbox: &Mailbox,
        messages: impl IntoIterator<Item = T>,
    ) -> Result<()> {
        self.imp().messages_remove(mailbox, messages).await
    }

    /// Sets the preview text for a message
    pub(super) async fn messages_update(
        &self,
        messages: impl IntoIterator<Item = impl AsRef<ImapMessage>>,
    ) -> Result<()> {
        self.imp().messages_update(messages).await
    }

    /// Loads the full message text from cache, if available
    pub(super) async fn messages_load_full(
        &self,
        messages: impl IntoIterator<Item = impl AsRef<Message>>,
    ) -> Result<()> {
        self.imp().messages_load_full(messages).await
    }
}

/// IMAP specific extensions
impl Cache {
    /// Gets a [`Mailbox`] by [name](MailboxName).
    pub fn mailbox_by_imap_name(&self, mailbox_name: &imap::MailboxName) -> Option<ImapMailbox> {
        self.imp().mailbox_by_imap_name(mailbox_name)
    }

    /// Commits incoming [`MessageData`] to the cache.
    ///
    /// Returns all newly created [messages](`crate::model::Message`).
    ///
    /// # Returns
    ///
    /// Returns a cache update of type [`CacheUpdate::Threads`]
    ///
    /// - `threads_added` for any new / updated threads.
    /// - `threads_vanished` if threads have been merged.
    pub async fn imap_message_data_update(
        &self,
        mailbox_name: &imap::MailboxName,
        messages: impl IntoIterator<Item = &imap::MessageData>,
    ) -> Result<()> {
        self.imp()
            .imap_message_data_update(mailbox_name, messages)
            .await
    }

    /// Updates the flags on multiple messages.
    ///
    /// Returns a [`Vec<MessageUid>`] with 'rejected' messages that weren't found in the cache.
    pub async fn imap_messages_update_flags<T: Borrow<imap::Uid>, F: Borrow<MessageFlags>>(
        &self,
        mailbox_name: &imap::MailboxName,
        update: impl IntoIterator<Item = (T, F)>,
    ) -> Result<Vec<imap::Uid>> {
        self.imp()
            .imap_messages_update_flags(mailbox_name, update)
            .await
    }

    /// Updates the flags on a single message.
    ///
    /// See also [`messages_update_flags`](Self::messages_update_flags).
    pub async fn imap_message_update_flags(
        &self,
        mailbox_name: &imap::MailboxName,
        message_uid: &imap::Uid,
        flags: &MessageFlags,
    ) -> Result<()> {
        self.imp()
            .imap_messages_update_flags(mailbox_name, std::iter::once((message_uid, flags)))
            .await?;
        Ok(())
    }
}

impl std::default::Default for Cache {
    fn default() -> Self {
        glib::Object::new()
    }
}

pub(crate) fn ensure_types() {
    Cache::ensure_type();
    MemoryCache::ensure_type();
}
