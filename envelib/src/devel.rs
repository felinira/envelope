// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod docker;
pub mod test_server;

pub use docker::TEST_SERVER_PORTS;

use crate::error::ErrorKindExt;

#[derive(thiserror::Error, Debug)]
#[error(
    "Error launching test server: {0}\n\nTo use the test server a docker/podman daemon must be running on the local machine."
)]
pub enum Error {
    Docker(#[from] bollard::errors::Error),
    Io(#[from] std::io::Error),
    Tokio(#[from] tokio::task::JoinError),
    Msg(String),
}

impl From<Error> for crate::Error {
    fn from(value: Error) -> Self {
        crate::ErrorKind::Inconsistency.with_source(value)
    }
}

impl From<docker::Error> for crate::Error {
    fn from(value: docker::Error) -> Self {
        Error::from(value).into()
    }
}

impl From<docker::Error> for Error {
    fn from(value: docker::Error) -> Self {
        match value {
            docker::Error::Docker(docker) => Self::Docker(docker),
            docker::Error::Io(io) => Self::Io(io),
        }
    }
}

impl From<tokio::task::JoinError> for crate::Error {
    fn from(value: tokio::task::JoinError) -> Self {
        Error::from(value).into()
    }
}
