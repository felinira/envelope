// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#![allow(async_fn_in_trait)]
#![feature(return_type_notation)]
#![feature(async_fn_traits)]
#![feature(unboxed_closures)]

pub mod account;
mod cache;
#[cfg(feature = "devel")]
pub mod devel;
mod error;
pub mod gettext;
mod imap;
pub mod model;
pub mod service;
pub mod types;
pub mod utils;

pub use error::{Error, ErrorKind, Result};
use imap_client::imap_types;

pub fn init() {
    cache::ensure_types();
    imap::ensure_types();
    model::ensure_types();
    service::ensure_types();
}
