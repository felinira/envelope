// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::prelude::*;
use glib::subclass::prelude::*;

use crate::types::imf;

mod imp {
    use std::cell::{OnceCell, RefCell};

    use crate::types::MimeAttachment;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Attachment)]
    pub struct Attachment {
        /// The icon for the attachment
        #[property(get, nullable)]
        pub(super) gicon: RefCell<Option<gio::Icon>>,

        /// Name of the attachment file
        #[property(get)]
        pub(super) filename: RefCell<Option<String>>,

        /// MIME type
        #[property(get)]
        pub(super) content_type: RefCell<String>,

        /// The raw MIME attachment data
        pub(super) mime_attachment: OnceCell<MimeAttachment>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Attachment {
        const NAME: &'static str = "EnvelopeAttachment";
        type Type = super::Attachment;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Attachment {}

    impl Attachment {}
}

glib::wrapper! {
    pub struct Attachment(ObjectSubclass<imp::Attachment>);
}

impl Attachment {
    pub fn data(&self) -> Option<&[u8]> {
        self.imp().mime_attachment.get().and_then(|a| a.data())
    }
}

impl From<imf::MimeAttachment> for Attachment {
    fn from(value: imf::MimeAttachment) -> Self {
        let obj: Attachment = glib::Object::new();
        let imp = obj.imp();

        imp.gicon
            .replace(Some(gio::content_type_get_icon(&value.content_type)));
        imp.content_type.replace(value.content_type.clone());
        imp.filename.replace(value.filename.clone());
        imp.mime_attachment.set(value).expect("Cell to be empty");

        obj
    }
}
