// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::prelude::*;
use glib::subclass::prelude::*;

use super::Thread;
use crate::model::IndexStoreExt;
use crate::model::index_store::{IndexStore, IndexStoreIter, IndexStoreKey};
use crate::types::ThreadId;

impl From<IndexStoreKey> for ThreadId {
    fn from(value: IndexStoreKey) -> Self {
        ThreadId::new(value.into())
    }
}

impl From<ThreadId> for IndexStoreKey {
    fn from(value: ThreadId) -> Self {
        IndexStoreKey::from(*value.as_ref())
    }
}

mod imp {
    use crate::model::index_store::{IndexStoreClassExt, IndexStoreImpl};

    use super::*;
    use std::cell::Cell;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::ThreadStore)]
    pub struct ThreadStore {
        #[property(get)]
        pub(super) finished_loading: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ThreadStore {
        const NAME: &'static str = "EnvelopeThreadStore";
        type Type = super::ThreadStore;
        type ParentType = IndexStore;

        fn class_init(klass: &mut Self::Class) {
            klass.init_item_type();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for ThreadStore {}
    impl IndexStoreImpl for ThreadStore {
        type Key = ThreadId;
        type Item = Thread;
    }
}

glib::wrapper! {
    pub struct ThreadStore(ObjectSubclass<imp::ThreadStore>)
    @extends IndexStore,
    @implements gio::ListModel;
}

impl ThreadStore {
    pub(crate) fn set_finished_loading(&self, finished_loading: bool) {
        self.imp().finished_loading.set(finished_loading);
        self.notify_finished_loading();
    }
}

impl std::default::Default for ThreadStore {
    fn default() -> Self {
        ThreadStore::new()
    }
}

impl FromIterator<Thread> for ThreadStore {
    fn from_iter<T: IntoIterator<Item = Thread>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter.into_iter().map(|thread| (thread.thread_id(), thread)))
    }
}

impl FromIterator<(ThreadId, Thread)> for ThreadStore {
    fn from_iter<T: IntoIterator<Item = (ThreadId, Thread)>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter)
    }
}

impl<'a> IntoIterator for &'a ThreadStore {
    type Item = (ThreadId, Thread);
    type IntoIter = IndexStoreIter<'a, ThreadStore>;

    fn into_iter(self) -> Self::IntoIter {
        IndexStoreExt::iter(self)
    }
}
