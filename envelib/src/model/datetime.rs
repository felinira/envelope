// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

/// Convert [`time::OffsetDateTime`] to [`glib::DateTime`]
pub(crate) trait ToGlibDate {
    fn to_glib(&self) -> glib::DateTime;
}

impl ToGlibDate for time::OffsetDateTime {
    /// Convert [`time::OffsetDateTime`] to [`glib::DateTime`]
    fn to_glib(&self) -> glib::DateTime {
        glib::DateTime::new(
            &glib::TimeZone::from_offset(self.offset().whole_seconds()),
            self.year(),
            self.month() as i32,
            self.day().into(),
            self.hour().into(),
            self.minute().into(),
            self.second() as f64,
        )
        .unwrap()
    }
}

/// Convert [`glib::DateTime`] to [`time::OffsetDateTime`]
pub(crate) trait ToOffsetDateTime {
    fn to_offset_dt(&self) -> time::OffsetDateTime;
}

impl ToOffsetDateTime for glib::DateTime {
    /// Convert [`glib::DateTime`] to [`time::OffsetDateTime`]
    fn to_offset_dt(&self) -> time::OffsetDateTime {
        time::OffsetDateTime::new_in_offset(
            time::Date::from_calendar_date(
                self.year(),
                time::Month::try_from(self.month() as u8).unwrap(),
                self.day_of_month() as u8,
            )
            .unwrap(),
            time::Time::from_hms(self.hour() as u8, self.minute() as u8, self.second() as u8)
                .unwrap(),
            time::UtcOffset::from_whole_seconds(self.utc_offset().as_seconds() as i32).unwrap(),
        )
    }
}
