// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use gio::prelude::*;
use gio::subclass::prelude::*;

use crate::gettext::*;
use crate::types::ConnectionStatus;

#[derive(Debug, Default, Clone, glib::Boxed)]
#[boxed_type(name = "EnvelopeTaskStatus")]
pub enum TaskStatus {
    /// Task has been enqueued and will wait for execution
    #[default]
    Initial,

    /// Task is running right now
    Running,

    /// Task is running but is an idle task (doesn't show a spinner)
    Idle,

    /// Task was aborted by user action
    Aborted,

    /// An error occurred with the underlying connection
    ConnectionError(ConnectionStatus),

    /// An error occurred
    Error(String),

    /// The task was completed successfully and a result is available
    Completed,
}

impl std::fmt::Display for TaskStatus {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TaskStatus::Initial => gettext("Pending"),
                TaskStatus::Running => gettext("Running"),
                TaskStatus::Idle => gettext("Background task"),
                TaskStatus::Aborted => gettext("Aborted"),
                TaskStatus::ConnectionError(_error) => gettext("Connection Error"),
                TaskStatus::Error(msg) => gettextf("Error: {}", &[msg]),
                TaskStatus::Completed => gettext("Completed"),
            }
        )
    }
}

mod imp {
    use std::{
        cell::{OnceCell, RefCell},
        marker::PhantomData,
    };

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Task)]
    pub struct Task {
        /// The task will be aborted when this cancellable is canceled
        #[property(get)]
        cancellable: gio::Cancellable,

        /// Name of the task
        #[property(get, construct_only)]
        name: RefCell<String>,

        /// Accumulated warnings
        #[property(get, nullable)]
        warnings: RefCell<Option<String>>,

        /// The current task status
        #[property(get, set = Self::set_status)]
        status: RefCell<TaskStatus>,

        #[property(get = Self::running)]
        running: PhantomData<bool>,

        /// When this task was initially created
        pub(super) creation_instant: OnceCell<std::time::Instant>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Task {
        const NAME: &'static str = "EnvelopeTask";
        type Type = super::Task;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Task {
        fn constructed(&self) {
            self.parent_constructed();

            let obj = self.obj();
            self.cancellable.connect_cancelled_local(glib::clone!(
                #[weak]
                obj,
                move |_| {
                    obj.set_status(TaskStatus::Aborted);
                }
            ));

            self.creation_instant
                .set(std::time::Instant::now())
                .unwrap();
        }
    }

    impl Task {
        fn running(&self) -> bool {
            matches!(
                *self.status.borrow(),
                TaskStatus::Running | TaskStatus::Idle
            )
        }

        fn set_status(&self, status: TaskStatus) {
            self.status.replace(status);
            self.obj().notify_running();
        }

        pub(super) fn add_warning(&self, warning: String) {
            let mut warnings_borrow = self.warnings.borrow_mut();
            if let Some(warnings) = &mut *warnings_borrow {
                warnings.push_str(&format!("; {warning}"));
            } else {
                warnings_borrow.replace(warning);
            }

            self.obj().notify_status();
        }
    }
}

glib::wrapper! {
    pub struct Task(ObjectSubclass<imp::Task>);
}

impl Task {
    pub fn new(name: String) -> Self {
        glib::Object::builder().property("name", name).build()
    }

    pub fn cancel(&self) {
        self.cancellable().cancel();
    }

    pub fn add_warning(&self, warning: String) {
        self.imp().add_warning(warning);
    }

    pub fn creation_instant(&self) -> &std::time::Instant {
        self.imp().creation_instant.get().unwrap()
    }

    pub fn cmp_prio(&self, other: &Task) -> std::cmp::Ordering {
        let calc = |t: &Task| match t.status() {
            TaskStatus::Initial => 0,
            TaskStatus::Running => 1,
            TaskStatus::Idle => 1,
            TaskStatus::Aborted => 3,
            TaskStatus::ConnectionError(_) => 2,
            TaskStatus::Error(_) => 2,
            TaskStatus::Completed => 3,
        };

        let points_a = calc(self);
        let points_b = calc(other);
        if points_a == points_b {
            other.creation_instant().cmp(self.creation_instant())
        } else {
            points_b.cmp(&points_a)
        }
    }
}
