// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::subclass::prelude::*;

use crate::model::index_store::{IndexStore, IndexStoreExt, IndexStoreIter, IndexStoreKey};

use super::{Mailbox, MailboxExtProps, MailboxId};

impl From<MailboxId> for IndexStoreKey {
    fn from(value: MailboxId) -> Self {
        Self::from(*value.as_ref())
    }
}

impl From<IndexStoreKey> for MailboxId {
    fn from(value: IndexStoreKey) -> Self {
        MailboxId::new(value.into())
    }
}

mod imp {
    use crate::model::index_store::{IndexStoreClassExt, IndexStoreImpl};

    use super::*;

    #[derive(Default)]
    pub struct MailboxStore {}

    #[glib::object_subclass]
    impl ObjectSubclass for MailboxStore {
        const NAME: &'static str = "EnvelopeMailboxStore";
        type Type = super::MailboxStore;
        type ParentType = IndexStore;

        fn class_init(klass: &mut Self::Class) {
            klass.init_item_type();
        }
    }

    impl ObjectImpl for MailboxStore {}
    impl IndexStoreImpl for MailboxStore {
        type Key = MailboxId;
        type Item = Mailbox;
    }
    impl MailboxStore {}
}

glib::wrapper! {
    pub struct MailboxStore(ObjectSubclass<imp::MailboxStore>)
    @extends IndexStore,
    @implements gio::ListModel;
}

impl std::default::Default for MailboxStore {
    fn default() -> Self {
        MailboxStore::new()
    }
}

impl FromIterator<Mailbox> for MailboxStore {
    fn from_iter<T: IntoIterator<Item = Mailbox>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter.into_iter().map(|mailbox| (mailbox.id(), mailbox)))
    }
}

impl FromIterator<(MailboxId, Mailbox)> for MailboxStore {
    fn from_iter<T: IntoIterator<Item = (MailboxId, Mailbox)>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter)
    }
}

impl<'a> IntoIterator for &'a MailboxStore {
    type Item = (MailboxId, Mailbox);
    type IntoIter = IndexStoreIter<'a, MailboxStore>;

    fn into_iter(self) -> Self::IntoIter {
        IndexStoreExt::iter(self)
    }
}
