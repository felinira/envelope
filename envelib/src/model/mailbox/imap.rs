// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use crate::imap;

use glib::prelude::*;
use glib::subclass::prelude::*;

use super::*;

mod imp {
    use crate::model::mailbox::MailboxImpl;

    use super::*;
    use std::cell::{Cell, RefCell};

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::ImapMailbox)]
    pub(crate) struct ImapMailbox {
        /// The path delimiter
        #[property(get, set = Self::set_delimiter)]
        delimiter: RefCell<String>,

        /// The unique mailbox name.
        /// It is made sure no two distinct mailboxes exist with the same id.
        #[property(get, set = Self::set_imap_name)]
        pub(super) imap_name: RefCell<imap::MailboxName>,

        // The uidnext value that corresponds to the latest message fetched and cached on the client
        #[property(get, set = Self::set_imap_uidnext)]
        pub(super) imap_uidnext: Cell<imap::Uid>,

        // The uidfirst value corresponds to the first message we have ever tried to fetch
        #[property(get, set = Self::set_imap_uidfirst)]
        pub(super) imap_uidfirst: Cell<imap::Uid>,

        #[property(get, set = Self::set_imap_uidvalidity)]
        pub(super) imap_uidvalidity: Cell<imap::UidValidity>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImapMailbox {
        const NAME: &'static str = "EnvelopeImapMailbox";
        type Type = super::ImapMailbox;
        type ParentType = crate::model::Mailbox;
    }

    #[glib::derived_properties]
    impl ObjectImpl for ImapMailbox {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }
    impl MailboxImpl for ImapMailbox {}

    impl ImapMailbox {
        pub(super) fn set_imap_name(&self, name: imap::MailboxName) {
            self.imap_name.replace(name);
            self.update_name_components();
        }

        fn set_delimiter(&self, delimiter: String) {
            self.delimiter.replace(delimiter);
            self.update_name_components();
        }

        fn update_name_components(&self) {
            let name = self.imap_name.borrow().clone();
            let delimiter = self.delimiter.borrow().clone();

            if name == imap::MailboxName::default() {
                return;
            }

            // TODO: utf7
            let components = if delimiter.is_empty() {
                vec![name.to_string()]
            } else {
                self.obj()
                    .imap_name()
                    .as_ref()
                    .split(&delimiter)
                    .map(ToOwned::to_owned)
                    .collect()
            };

            self.parent_set_name_components(components);
        }

        /// Update the highest cached UID value
        pub(super) fn set_imap_uidnext(&self, uidnext: imap::Uid) {
            if self.imap_uidnext.get() != uidnext {
                self.parent_set_uncommitted(true);
                self.imap_uidnext.replace(uidnext);
            }
        }

        /// Update the lowest cached UID value
        pub(super) fn set_imap_uidfirst(&self, uidfirst: imap::Uid) {
            if self.imap_uidfirst.get() != uidfirst {
                self.parent_set_uncommitted(true);
                self.imap_uidfirst.replace(uidfirst);
            }
        }

        /// Update the UIDVALIDITY value of this mailbox
        pub(super) fn set_imap_uidvalidity(&self, uidvalidity: imap::UidValidity) {
            if self.imap_uidvalidity.get() != uidvalidity {
                self.parent_set_uncommitted(true);
                self.imap_uidvalidity.set(uidvalidity);
                self.imap_uidnext.set(imap::Uid::NULL);
            }
        }
    }
}

glib::wrapper! {
    pub(crate) struct ImapMailbox(ObjectSubclass<imp::ImapMailbox>)
    @extends crate::model::Mailbox;
}

/// Private to the library crate
impl ImapMailbox {
    /// Create a new mailbox from the provided [`imap::MailboxInfo`]
    pub(crate) fn from_imap(id: MailboxId, info: imap::MailboxInfo) -> Self {
        let mailbox: Self = glib::Object::builder()
            .property("id", id)
            .property("imap-name", info.name)
            .property("delimiter", info.delimiter)
            .property("kind", info.kind)
            .build();
        mailbox
    }

    /// Updates this mailbox with information from a LIST response
    pub(crate) fn set_imap_info(&self, info: imap::MailboxInfo) {
        self.set_imap_name(info.name);
        self.set_delimiter(info.delimiter.unwrap_or_default());
        self.set_kind(info.kind);
    }

    /// Updates this mailbox with information from a SELECT response
    pub(crate) fn set_imap_selection(&self, selection: &imap::MailboxSelection) {
        self.set_message_count(selection.message_count);
        self.set_recent_count(selection.recent_count);
        self.set_imap_uidvalidity(selection.uidvalidity);
    }

    /// Updates this mailbox with information from a STATUS response
    pub(crate) fn set_imap_status(&self, status: &imap::MailboxStatus) {
        self.set_message_count(status.message_count);
        self.set_recent_count(status.recent_count);
        self.set_imap_uidvalidity(status.uidvalidity);

        if let Some(count) = status.unseen_count {
            self.set_unread_count(count);
        }
    }
}
