// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod imap;
mod store;

use crate::types::*;
use glib::prelude::*;
use glib::subclass::prelude::*;

pub use self::imp::MailboxExtProps;
pub(crate) use imap::*;
pub use store::MailboxStore;

pub enum MailboxTarget {
    Mailbox(Mailbox),
    Kind(MailboxKind),
}

impl From<Mailbox> for MailboxTarget {
    fn from(value: Mailbox) -> Self {
        Self::Mailbox(value)
    }
}

impl From<MailboxKind> for MailboxTarget {
    fn from(value: MailboxKind) -> Self {
        Self::Kind(value)
    }
}

mod imp {
    use std::{
        cell::{Cell, RefCell},
        marker::PhantomData,
    };

    use crate::model::ThreadStore;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Mailbox, ext_trait = MailboxExtProps)]
    pub struct Mailbox {
        #[property(get, set)]
        id: Cell<MailboxId>,

        /// The path delimiter
        #[property(get, set = Self::set_delimiter, nullable)]
        delimiter: RefCell<Option<String>>,

        /// The amount of messages
        #[property(get, set = Self::set_message_count)]
        message_count: Cell<u32>,

        /// The amount of recent messages
        #[property(get, set = Self::set_recent_count)]
        recent_count: Cell<u32>,

        /// The amount of unread messages
        #[property(get, set = Self::set_unread_count)]
        unread_count: Cell<u32>,

        /// Determines what the mailbox is supposed to be used for, such as inbox, drafts etc.
        #[property(get, set = Self::set_kind, builder(MailboxKind::default()))]
        kind: Cell<MailboxKind>,

        /// The icon of the mailbox
        #[property(get = Self::icon_name)]
        icon_name: PhantomData<String>,

        /// The UI name. This is translated except for custom mailboxes
        #[property(get = Self::localized_name)]
        localized_name: PhantomData<String>,

        /// The last component of the name, aka the mailbox name inside the hierarchy
        #[property(get = Self::local_name)]
        local_name: PhantomData<String>,

        /// This is the mailbox name, split in components (folders)
        /// To be used to group mailboxes by folders
        name_components: RefCell<Vec<String>>,

        /// The counter to be displayed in the UI
        #[property(get = Self::counter)]
        counter: PhantomData<u32>,

        /// Whether the UI counter is visible
        #[property(get = Self::counter_visible)]
        counter_visible: PhantomData<bool>,

        /// Whether the UI counter is deemed important
        #[property(get = Self::counter_important)]
        counter_important: PhantomData<bool>,

        /// When this mailbox was last updated
        pub(super) last_update: Cell<Option<std::time::Instant>>,

        /// When messages in this mailbox were last fetched
        pub(super) last_sync: Cell<Option<std::time::Instant>>,

        /// Whether this mailbox has uncommitted changes that have not been flushed to the cache
        #[property(get)]
        uncommitted: Cell<bool>,

        #[property(get)]
        threads: ThreadStore,

        /// The mailbox is considered mapped when the threads are loaded / are in the process of being loaded
        #[property(get, set)]
        threads_mapped: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Mailbox {
        const NAME: &'static str = "EnvelopeMailbox";
        type Type = super::Mailbox;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Mailbox {}

    /// Virtual methods
    pub trait MailboxImpl: ObjectImpl {}

    /// Protected methods
    pub(crate) trait MailboxImplExt: MailboxImpl
    where
        <Self as ObjectSubclass>::Type: IsA<super::Mailbox>,
    {
        fn parent_set_uncommitted(&self, uncommitted: bool) {
            self.obj()
                .upcast_ref::<super::Mailbox>()
                .imp()
                .set_uncommitted(uncommitted);
        }

        fn parent_set_name_components(&self, name_components: Vec<String>) {
            self.obj()
                .upcast_ref::<super::Mailbox>()
                .imp()
                .set_name_components(name_components);
        }
    }

    impl<T: MailboxImpl> MailboxImplExt for T where <Self as ObjectSubclass>::Type: IsA<super::Mailbox> {}

    impl Mailbox {
        fn set_uncommitted(&self, uncommitted: bool) {
            self.uncommitted.set(uncommitted);
            self.obj().notify_uncommitted();
        }

        pub(super) fn set_name_components(&self, name_components: Vec<String>) {
            self.name_components.replace(name_components);

            self.obj().notify_local_name();
            self.obj().notify_localized_name();
        }

        pub(super) fn name_components(&self) -> Vec<String> {
            self.name_components.borrow().clone()
        }

        /// Set the delimiter (path-separator)
        fn set_delimiter(&self, delimiter: Option<String>) {
            if *self.delimiter.borrow() != delimiter {
                self.set_uncommitted(true);
                self.delimiter.replace(delimiter);

                self.obj().notify_local_name();
                self.obj().notify_localized_name();
            }
        }

        /// Set the designation of this mailbox
        fn set_kind(&self, kind: MailboxKind) {
            if self.kind.get() != kind {
                self.set_uncommitted(true);
                self.kind.replace(kind);

                self.obj().notify_icon_name();
                self.obj().notify_localized_name();
                self.obj().notify_counter();
                self.obj().notify_counter_visible();
                self.obj().notify_counter_important();
            }
        }

        /// Update the unread counter
        fn set_unread_count(&self, unread: u32) {
            if self.unread_count.get() != unread {
                self.set_uncommitted(true);
                self.unread_count.set(unread);

                self.obj().notify_counter();
                self.obj().notify_counter_visible();
            }
        }

        /// Update the recent counter
        fn set_recent_count(&self, recent: u32) {
            if self.recent_count.get() != recent {
                self.set_uncommitted(true);
                self.recent_count.set(recent);

                self.obj().notify_counter();
                self.obj().notify_counter_visible();
                self.obj().notify_counter_important();
            }
        }

        /// Update total number of messages
        fn set_message_count(&self, messages: u32) {
            if self.message_count.get() != messages {
                self.set_uncommitted(true);
                self.message_count.set(messages);

                self.obj().notify_counter();
                self.obj().notify_counter_visible();
            }
        }

        /// A human-readable name of this mailbox
        fn localized_name(&self) -> String {
            if let Some(name) = self.kind.get().localized_name() {
                name
            } else {
                self.local_name()
            }
        }

        /// The name after path separators
        pub fn local_name(&self) -> String {
            self.name_components
                .borrow()
                .last()
                .expect("name to not be empty")
                .to_owned()
        }

        /// Name for the symbolic of this mailbox
        fn icon_name(&self) -> String {
            self.kind.get().icon_name().to_string()
        }

        /// The number of unread messages
        ///
        /// The Drafts and Outbox mailboes are special and will show *all* messages in the mailbox
        fn counter(&self) -> u32 {
            if self.kind.get().is_drafts() {
                self.obj().message_count()
            } else if self.obj().unread_count() > 0 {
                self.obj().unread_count()
            } else {
                0
            }
        }

        /// Whether to show a counter in the mailbox list
        fn counter_visible(&self) -> bool {
            !matches!(self.kind.get(), MailboxKind::Junk)
                && (self.kind.get().is_drafts() || self.obj().counter() > 0)
        }

        /// Whether to highlight the counter in the mailbox list
        fn counter_important(&self) -> bool {
            self.recent_count.get() > 0
                && !matches!(
                    self.kind.get(),
                    MailboxKind::Junk | MailboxKind::Drafts | MailboxKind::Sent
                )
        }
    }
}

glib::wrapper! {
    pub struct Mailbox(ObjectSubclass<imp::Mailbox>);
}

pub(crate) use imp::{MailboxImpl, MailboxImplExt};
unsafe impl<Obj: MailboxImpl> IsSubclassable<Obj> for Mailbox {}

pub trait MailboxExt: IsA<Mailbox> {
    /// Returns whether this mailbox is INBOX
    fn is_inbox(&self) -> bool {
        self.kind() == MailboxKind::Inbox
    }

    /// Returns whether this mailbox is designated for Trash
    fn is_trash(&self) -> bool {
        self.kind() == MailboxKind::Trash
    }

    /// Returns whether this mailbox is designated for Junk E-Mail
    fn is_junk(&self) -> bool {
        self.kind() == MailboxKind::Junk
    }

    /// Set the last time this mailbox had a status update
    fn set_last_update(&self, instant: std::time::Instant) {
        self.upcast_ref().imp().last_update.set(Some(instant));
    }

    /// Set the last time this mailbox was synchronized
    fn set_last_sync(&self, instant: std::time::Instant) {
        self.upcast_ref().imp().last_sync.set(Some(instant));
    }

    fn cmp_kind_name(&self, other: &Mailbox) -> std::cmp::Ordering {
        match self.kind().cmp(&other.kind()) {
            std::cmp::Ordering::Equal => {}
            ord => return ord,
        }

        self.name_components().cmp(&other.name_components())
    }

    /// Returns the last time this mailbox was refreshed from the backend
    fn duration_since_update(&self) -> std::time::Duration {
        if let Some(last_update) = self.upcast_ref().imp().last_update.get() {
            last_update.elapsed()
        } else {
            std::time::Duration::MAX
        }
    }

    /// Returns whether the last message refresh occurred before the provided time
    fn last_update_earlier_than(&self, instant: std::time::Instant) -> bool {
        if let Some(last_update) = self.upcast_ref().imp().last_update.get() {
            last_update.saturating_duration_since(instant).is_zero()
        } else {
            true
        }
    }

    /// Returns whether the last complete mailbox sync occurred before the provided time
    fn last_sync_earlier_than(&self, instant: std::time::Instant) -> bool {
        if let Some(last_sync) = self.upcast_ref().imp().last_sync.get() {
            last_sync.saturating_duration_since(instant).is_zero()
        } else {
            true
        }
    }

    fn name_components(&self) -> Vec<String> {
        self.upcast_ref().imp().name_components()
    }
}

impl<T: IsA<Mailbox>> MailboxExt for T {}
