// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod imap;
mod part;
mod store;

use header::id::MidCidUri;
pub(crate) use imap::ImapMessage;
pub use imp::MessageExtProps;
use mime::ContentTypeToString;
pub use part::MessagePart;
pub use store::MessageStore;

use std::collections::HashSet;
use std::sync::Arc;

use glib::prelude::*;
use glib::subclass::prelude::*;

use crate::gettext::*;
use crate::types::*;

use super::attachment::Attachment;
use super::*;

#[repr(transparent)]
pub struct MessageOrderedByDateDescending(Message);

impl PartialEq for MessageOrderedByDateDescending {
    fn eq(&self, other: &Self) -> bool {
        self.0.cache_id().eq(&other.0.cache_id())
    }
}

impl Eq for MessageOrderedByDateDescending {}

impl PartialOrd for MessageOrderedByDateDescending {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl Ord for MessageOrderedByDateDescending {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.0
            .internal_date()
            .cmp(&other.0.internal_date())
            .reverse()
    }
}

#[derive(Debug, Clone, Default, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, glib::Enum)]
#[enum_type(name = "EnvelopeMessageDetailLevel")]
pub enum MessageDetailLevel {
    #[default]
    Headers,
    HeadersAndPreview,
    Full,
}

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        marker::PhantomData,
        sync::Arc,
    };

    use glib::WeakRef;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Message, ext_trait = MessageExtProps)]
    pub struct Message {
        /// Which parts of the message have been loaded
        #[property(get, set, construct, builder(MessageDetailLevel::Headers))]
        detail_level: Cell<MessageDetailLevel>,

        /// The unique cache id
        #[property(get, set, construct)]
        cache_id: Cell<MessageCacheId>,

        /// The id of the mailbox that contains this message
        #[property(get, set, construct)]
        mailbox_id: Cell<MailboxId>,

        /// The mailbox this message is contained within
        #[property(get, set)]
        mailbox: WeakRef<Mailbox>,

        /// The Message-Id header field
        #[property(get, set)]
        message_id_header: RefCell<MessageId>,

        /// The id of the containing thread
        #[property(get, set)]
        thread_id: RefCell<ThreadId>,

        /// A weak reference to the containing thread
        #[property(get, set = Self::set_thread)]
        thread: glib::WeakRef<Thread>,

        /// Detailed info on all participants of the message
        #[property(get, set = Self::set_participants)]
        pub(super) participants: RefCell<Participants>,

        /// All the mail addresses that belong to the user
        self_addresses: RefCell<Arc<HashSet<Address>>>,

        /// Short string of all participants of the mail conversation
        /// Will skip the `envelope_to` addresses as those are most like the user's own addresses
        #[property(get)]
        short_participant_names: RefCell<Option<String>>,

        /// The subject of the message
        /// None if the subject is empty string or missing
        #[property(get, set = Self::set_subject, nullable)]
        subject: RefCell<Option<String>>,

        /// How many attachments the message has
        #[property(get, set)]
        attachment_count: Cell<u32>,

        /// The partial text, for displaying in lists
        #[property(get = Self::preview_text, set = Self::set_preview_text, nullable)]
        preview_text: RefCell<Option<String>>,

        pub(super) flags: RefCell<MessageFlags>,

        /// The date from the date field of the message. This is set by the composing email client.
        #[property(get, set = Self::set_date, nullable)]
        pub(super) date: RefCell<Option<glib::DateTime>>,

        /// The date from the mail server. This is set by eg. INTERNALDATE on IMAP servers.
        #[property(get, construct_only)]
        pub(super) internal_date: OnceCell<glib::DateTime>,

        #[property(get)]
        pub(super) short_date_str: RefCell<String>,

        #[property(get)]
        avatar_text: RefCell<Option<String>>,

        #[property(get = Self::is_read)]
        is_read: PhantomData<bool>,

        /// Exposes the unread flag from flags for quick access. Other flags can be accessed through [Self::flags]
        #[property(get = Self::is_unread, set = Self::set_is_unread, default = true)]
        is_unread: PhantomData<bool>,

        /// The full message
        pub(super) message_content: RefCell<Option<InternetMessage>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Message {
        const NAME: &'static str = "Message";
        type Type = super::Message;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Message {}

    /// Virtual methods
    pub trait MessageImpl: ObjectImpl {}

    impl Message {
        fn set_date(&self, date: Option<glib::DateTime>) {
            self.short_date_str
                .replace(gettext_short_date(date.as_ref()));
            self.date.replace(date);

            self.obj().notify_short_date_str();
        }

        /// Mark the message as read/unread
        fn set_is_unread(&self, is_unread: bool) {
            self.flags.borrow_mut().set(MessageFlag::Seen, !is_unread);
            self.notify_flags_changed();
        }

        /// Returns the unread state of this message
        fn is_unread(&self) -> bool {
            self.flags.borrow().is_unseen()
        }

        /// Returns the read state of this message
        fn is_read(&self) -> bool {
            self.flags.borrow().is_seen()
        }

        /// Notify flag changes
        fn notify_flags_changed(&self) {
            self.obj().notify_is_unread();
            self.obj().notify_is_read();
        }

        /// Set a weak reference to the thread this message belongs to
        pub fn set_thread(&self, thread: &Thread) {
            self.thread.set(Some(thread));
            self.obj().set_thread_id(thread.thread_id());
        }

        /// Update flags
        pub fn set_flags(&self, flags: &MessageFlags) {
            self.flags.replace(flags.clone());
            self.notify_flags_changed();
        }

        /// Set the message subject
        fn set_subject(&self, subject: Option<String>) {
            self.subject.replace(
                subject
                    .map(|s| s.trim().to_string())
                    .filter(|s| !s.is_empty()),
            );
        }

        /// Sets the addresses belonging to 'self'
        pub(crate) fn set_self_addresses(&self, addrs: Arc<HashSet<Address>>) {
            self.self_addresses.replace(addrs);

            self.update_short_participant_names();
            self.update_avatar_text();
        }

        /// A set containing all participants
        fn participant_set(&self) -> HashSet<Address> {
            let participants = self.participants.borrow();
            participants
                .from
                .iter()
                .chain(participants.to.iter())
                .chain(participants.cc.iter())
                .chain(participants.bcc.iter())
                .cloned()
                .collect()
        }

        fn participants_except_self(&self) -> HashSet<Address> {
            let set = self.participant_set();
            let set_without_self: HashSet<_> = set
                .difference(&self.self_addresses.borrow())
                .cloned()
                .collect();

            if set_without_self.is_empty() {
                set
            } else {
                set_without_self
            }
        }

        /// Refresh the short participant names from message headers
        fn update_short_participant_names(&self) {
            let participants = self.participants_except_self();

            self.short_participant_names
                .replace(if participants.is_empty() {
                    None
                } else {
                    Some(
                        participants
                            .iter()
                            .map(|addr| {
                                if let Some(name) = addr.name() {
                                    name
                                } else {
                                    addr.address()
                                }
                            })
                            .collect::<Vec<&str>>()
                            // Translators: Separation character for list of mail correspondant names
                            .join(&gettext(", ")),
                    )
                });
        }

        /// Set the avatar text depending on who sent this message
        fn update_avatar_text(&self) {
            self.avatar_text.replace(
                self.obj()
                    .addr_from()
                    .and_then(|from| from.name().map(ToString::to_string)),
            );
        }

        /// Set the participants of this message
        fn set_participants(&self, participants: Participants) {
            self.participants.replace(participants);

            self.update_short_participant_names();
            self.update_avatar_text();
        }

        /// Sets a preview text for message lists
        fn set_preview_text(&self, text: Option<String>) {
            let text = text.filter(|t| !t.is_empty());
            if self.preview_text.replace(text) != *self.preview_text.borrow() {
                self.update_fetch_state();
            }
        }

        /// Gets the message preview text, either by parsing the body, or by returning the preview text
        fn preview_text(&self) -> Option<String> {
            if self.preview_text.borrow().is_none() {
                self.preview_text.replace(
                    self.message_content
                        .borrow()
                        .as_ref()
                        .and_then(|msg| msg.parse_preview_text()),
                );
            }

            self.preview_text.borrow().clone()
        }

        /// Examine how much of this message has been loaded and update fetch_state accordingly
        pub(super) fn update_fetch_state(&self) {
            self.obj().set_detail_level(
                if self
                    .message_content
                    .borrow()
                    .as_ref()
                    .is_some_and(|body| body.is_full_message())
                {
                    MessageDetailLevel::Full
                } else if self
                    .message_content
                    .borrow()
                    .as_ref()
                    .is_some_and(|msg| msg.has_body())
                {
                    MessageDetailLevel::HeadersAndPreview
                } else {
                    MessageDetailLevel::Headers
                },
            );
        }

        /// Updates the message with information from the RFC 822 body
        pub(super) fn update_message_body(&self) {
            if let Some(msg) = &*self.message_content.borrow() {
                let obj = self.obj();

                obj.set_message_id_header(msg.parse_message_id());
                obj.set_participants(msg.parse_participants());
                obj.set_subject(Some(msg.parse_subject()));
                obj.set_date(msg.parse_date().map(|d| d.to_glib()));

                if msg.has_body() {
                    // Reload preview text. We prefer to generate it ourselves,
                    // instead of the server generating it for us
                    self.preview_text.take();
                    obj.notify_preview_text();
                }
            }
        }
    }
}

glib::wrapper! {
    pub struct Message(ObjectSubclass<imp::Message>);
}

pub(crate) use imp::MessageImpl;
unsafe impl<Obj: MessageImpl> IsSubclassable<Obj> for Message {}

pub trait MessageExt: IsA<Message> + MessageExtProps {
    /// The address of the sender of the message
    ///
    /// Note that in the very rare case of multiple senders this will only return one.
    /// They can still be accessed by [Self::participants}.from
    fn addr_from(&self) -> Option<Address> {
        self.upcast_ref::<Message>()
            .imp()
            .participants
            .borrow()
            .from
            .first()
            .cloned()
    }

    fn message_content(&self) -> Option<InternetMessage> {
        self.upcast_ref::<Message>()
            .imp()
            .message_content
            .borrow()
            .clone()
    }

    fn set_self_addresses(&self, addresses: Arc<HashSet<Address>>) {
        self.upcast_ref::<Message>()
            .imp()
            .set_self_addresses(addresses);
    }

    fn set_message_body(&self, message_body: Option<InternetMessage>) {
        let imp = self.upcast_ref::<Message>().imp();
        imp.message_content.replace(message_body);
        imp.update_fetch_state();
        imp.update_message_body();
    }

    fn inline_bodies(&self) -> Vec<TextPart<'static>> {
        // TODO: Don't clone here
        self.upcast_ref::<Message>()
            .imp()
            .message_content
            .borrow()
            .clone()
            .map(|msg| msg.inline_bodies().map(|p| p.into_owned()).collect())
            .unwrap_or_default()
    }

    fn html_body_owned(&self) -> Option<TextPart<'static>> {
        self.upcast_ref::<Message>()
            .imp()
            .message_content
            .borrow()
            .as_ref()
            .and_then(|imf| imf.html_body().map(|b| b.into_owned()))
    }

    fn attachments(&self) -> Vec<Attachment> {
        self.upcast_ref::<Message>()
            .imp()
            .message_content
            .borrow()
            .as_ref()
            .map(|msg| msg.attachments().map(Attachment::from).collect())
            .unwrap_or_default()
    }

    fn flags(&self) -> MessageFlags {
        self.upcast_ref::<Message>().imp().flags.borrow().clone()
    }

    /// Returns true when flags have changed
    fn update_flags(&self, flags: &MessageFlags) -> bool {
        let imp = self.upcast_ref::<Message>().imp();

        if flags == &*imp.flags.borrow() {
            false
        } else {
            imp.set_flags(flags);
            true
        }
    }

    /// Returns data and mime-type
    fn text_body_by_message_id(
        &self,
        message_id: &str,
    ) -> Option<(TextPart<'static>, Option<String>)> {
        if self.message_id_header().as_ref() == message_id {
            self.html_body_owned()
        } else if let Some(imf) = self.message_content().and_then(|imf| {
            imf.embedded_messages()
                .find(|imf| imf.parse_message_id().as_ref() == message_id)
        }) {
            imf.html_body_owned()
        } else {
            None
        }
        .map(|b| (b.into_owned(), Some("message/rfc822".to_string())))
    }

    /// Returns a mime-type and body part by mid: or cid: URI
    fn message_part_by_uri(&self, uri: &str) -> Option<MessagePart> {
        let uri: MidCidUri = uri.parse().inspect_err(|err| log::error!("{}", err)).ok()?;

        let message: Arc<mail_parser::Message<'static>> = self.message_content()?.inner();
        let message = uri
            .message_id()
            .and_then(|mid| message.message_by_mid(mid))
            .unwrap_or(&message);

        if let Some(cid) = uri.content_id() {
            message.part_id_by_content_id(cid).map(|part| {
                MessagePart::from_message_part(Arc::new(message.clone().into_owned()), part)
            })
        } else {
            Some(message.into())
        }
    }

    /// Sorter that sorts messages by date ascending
    fn cmp_date(&self, other: &Self) -> std::cmp::Ordering {
        match self.date().cmp(&other.date()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        match self.cache_id().cmp(&other.cache_id()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        self.cmp(other)
    }

    /// Sorter that sorts messages by internal date ascending
    fn cmp_internal_date(&self, other: &Self) -> std::cmp::Ordering {
        match self.internal_date().cmp(&other.internal_date()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        match self.cache_id().cmp(&other.cache_id()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        self.cmp(other)
    }

    fn is_newest_in_thread(&self) -> bool {
        self.thread()
            .is_some_and(|t| t.newest_message().is_some_and(|msg| &msg == self))
    }
}

impl<T: IsA<Message>> MessageExt for T {}
