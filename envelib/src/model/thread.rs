// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod store;

pub use store::*;

use crate::model::{IndexStoreExt, MessageExt, MessageExtProps};
use crate::types::*;
use glib::subclass::prelude::*;
use glib::{SignalHandlerId, closure_local, prelude::*};
use std::sync::LazyLock;

use super::{Message, MessageDetailLevel, MessageStore};

mod imp {
    use std::{
        cell::{Cell, RefCell},
        collections::BTreeMap,
        marker::PhantomData,
    };

    use glib::subclass::Signal;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::Thread)]
    pub struct Thread {
        /// Sorted list of messages
        pub(super) messages: MessageStore,
        handlers: RefCell<BTreeMap<MessageCacheId, Vec<glib::SignalHandlerId>>>,

        /// The unique id of this thread
        #[property(get, set, construct_only)]
        thread_id: RefCell<ThreadId>,

        /// The date that should be used for sorting this thread. Refers to the date of the newest message.
        ///
        /// This must not be updated by the thread itself. As this invalidated the sort position in lists,
        /// we need to coordinate this change with items_changed notifications in the corresponding list model.
        #[property(get, set, construct)]
        sort_date: RefCell<Option<glib::DateTime>>,

        /// This thread is currently fetching messages from cache
        #[property(get, set)]
        is_loading: Cell<bool>,

        /// This thread is finished fetching messages from cache
        #[property(get, set)]
        is_loaded: Cell<bool>,

        /// The number of messages in this thread
        #[property(get)]
        message_count: Cell<u32>,

        /// A property to fetch the most recent message
        #[property(get = Self::newest_message)]
        newest_message: PhantomData<Option<Message>>,

        /// The amount of unread messages
        #[property(get)]
        unread_count: Cell<u32>,

        /// Whether this thread has at least one unread message
        #[property(get)]
        is_unread: Cell<bool>,

        /// The accumulated number of attachments in this thread
        #[property(get)]
        attachment_count: Cell<u32>,

        /// The cleaned subject of the most recent message without any 'Re:', 'Fwd' etc
        #[property(get)]
        thread_name: RefCell<Option<String>>,

        /// The preview of the of the most recent message
        #[property(get)]
        preview_text: RefCell<Option<String>>,

        /// The text to be used for avatars
        #[property(get)]
        avatar_text: RefCell<Option<String>>,

        /// A generated string of all participants to this thread
        #[property(get)]
        participant_names: RefCell<Option<String>>,

        /// A short string that represents the most recent date
        #[property(get)]
        short_date_str: RefCell<Option<String>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for Thread {
        const NAME: &'static str = "EnvelopeThread";
        type Type = super::Thread;
    }

    #[glib::derived_properties]
    impl ObjectImpl for Thread {
        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> =
                LazyLock::new(|| vec![Signal::builder("messages-changed").build()]);
            SIGNALS.as_ref()
        }
    }

    impl Thread {
        /// Emit this signal when the message list has changed
        pub(super) fn emit_messages_changed(&self) {
            self.obj().emit_by_name::<()>("messages-changed", &[]);
        }

        pub(super) fn position(&self, message: &Message) -> Result<usize, usize> {
            self.messages
                .binary_search_by(|_key, msg| message.cmp_internal_date(msg).reverse())
        }

        /// Return the newest message in this thread
        fn newest_message(&self) -> Option<Message> {
            self.messages
                .values()
                .fold(None, |acc: Option<Message>, msg| {
                    if acc.map(|m| m.date()) < Some(msg.date()) {
                        Some(msg.clone())
                    } else {
                        None
                    }
                })
        }

        /// Update the number of messages in this thread by counting the message list
        pub(super) fn update_message_count(&self) {
            self.message_count.set(self.messages.len() as u32);
            self.obj().notify_message_count();
        }

        /// Set how many messages in this thread are unread
        fn set_unread_count(&self, count: u32) {
            self.unread_count.replace(count);
            self.obj().notify_unread_count();

            self.is_unread.set(self.unread_count.get() > 0);
            self.obj().notify_is_unread();
        }

        /// Set the number of attachments this message has
        pub(super) fn set_attachment_count(&self, count: u32) {
            self.attachment_count.replace(count);
            self.obj().notify_attachment_count();
        }

        /// The newest message will determine many properties that are going to be displayed in a message list.
        fn set_newest_message(&self, message: &Message) {
            let obj = self.obj();

            // Thread name
            let new_thread_name = message
                .subject()
                .map(|s| mail_parser::parsers::fields::thread::thread_name(&s).to_owned());
            if self.thread_name.replace(new_thread_name) != *self.thread_name.borrow() {
                obj.notify_thread_name();
            }

            // Preview text
            let new_preview_text = message.preview_text();
            if self.preview_text.replace(new_preview_text) != *self.preview_text.borrow() {
                obj.notify_preview_text();
            }

            // Avatar text
            let new_avatar_text = message.avatar_text();
            if self.avatar_text.replace(new_avatar_text) != *self.avatar_text.borrow() {
                obj.notify_avatar_text();
            }

            // Participants
            let new_participants = message.short_participant_names();
            if self.participant_names.replace(new_participants) != *self.participant_names.borrow()
            {
                obj.notify_participant_names();
            }

            // Date string
            let short_date_str = message.short_date_str();
            if self.short_date_str.replace(Some(short_date_str)) != *self.short_date_str.borrow() {
                obj.notify_short_date_str();
            }

            obj.notify_newest_message();
        }

        /// Recalculate message counters
        fn update_counts(&self) {
            self.update_message_count();
            let mut attachment_count = 0;
            let mut unread_count = 0;

            for message in self.messages.values() {
                attachment_count += message.attachment_count();
                if message.is_unread() {
                    unread_count += 1;
                }
            }

            self.set_attachment_count(attachment_count);
            self.set_unread_count(unread_count);
        }

        /// Add a message to this thread. Updates message counts and possibly the newest message.
        pub(super) fn add_message(&self, message: Message) {
            if message.thread().as_ref() != Some(&self.obj()) {
                message.set_thread(self.obj().clone());

                // Sort by internal date, not by actual date
                let pos = self.position(&message);

                let pos = match pos {
                    Ok(_) => {
                        // Don't insert a message twice
                        return;
                    }
                    Err(pos) => {
                        // Insert here
                        pos
                    }
                };

                self.messages
                    .insert_at(pos, message.cache_id(), message.clone());

                let mut handlers = Vec::new();

                // Connect to signals
                let obj = self.obj();
                handlers.push(message.connect_attachment_count_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |_| {
                        obj.imp().update_counts();
                        obj.imp().emit_messages_changed();
                    }
                )));

                handlers.push(message.connect_is_unread_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |_| {
                        obj.imp().update_counts();
                        obj.imp().emit_messages_changed();
                    }
                )));

                handlers.push(message.connect_preview_text_notify(glib::clone!(
                    #[weak]
                    obj,
                    move |msg| {
                        if obj
                            .newest_message()
                            .is_some_and(|m| m.cache_id() == msg.cache_id())
                        {
                            obj.imp().preview_text.replace(msg.preview_text());
                            obj.notify_preview_text();
                            obj.imp().emit_messages_changed();
                        }
                    }
                )));

                self.handlers
                    .borrow_mut()
                    .insert(message.cache_id(), handlers);

                // Update internal values
                if pos == 0 {
                    // First message has changed: Update the relevant fields
                    self.set_newest_message(&message);
                }

                self.update_counts();
                self.emit_messages_changed();
            }
        }

        /// Remove the specified message from the thread
        pub(super) fn remove_message(&self, message: &Message) {
            if let Some((pos, cache_id, message)) = self.messages.remove_full(message.cache_id()) {
                for handler in self
                    .handlers
                    .borrow_mut()
                    .remove(&cache_id)
                    .unwrap_or_default()
                {
                    message.disconnect(handler)
                }

                // Update internal values
                if pos == 0 {
                    // First message has changed: Update the relevant fields
                    if let Some(first) = self.messages.first() {
                        self.set_newest_message(&first);
                    }
                }

                // Will always change
                self.update_counts();
                self.emit_messages_changed();
            }
        }
    }
}

glib::wrapper! {
    pub struct Thread(ObjectSubclass<imp::Thread>);
}

impl Thread {
    pub fn new(thread_id: &ThreadId, date_newest_message: &glib::DateTime) -> Self {
        glib::Object::builder()
            .property("thread-id", thread_id)
            .property("sort-date", date_newest_message)
            .build()
    }

    /// Add a message to this thread
    pub fn add_message(&self, message: impl IsA<Message>) {
        self.imp().add_message(message.upcast());
    }

    /// Remove a message from this thread
    pub fn remove_message(&self, message: &impl IsA<Message>) {
        self.imp().remove_message(message.upcast_ref());
    }

    /// Returns true if this thread does not contain any messages
    pub fn is_empty(&self) -> bool {
        self.imp().messages.is_empty()
    }

    /// All messages in this thread
    pub fn messages(&self) -> MessageStore {
        self.imp().messages.clone()
    }

    /// Gets emitted messages have been added to or removed from this thread
    pub fn connect_messages_changed<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(Self) + 'static,
    {
        self.connect_closure(
            "messages-changed",
            false,
            closure_local!(|thread: Thread| callback(thread)),
        )
    }

    /// Returns true if `message` is part of this thread
    pub fn contains(&self, message: &Message) -> bool {
        self.imp().position(message).is_ok()
    }

    /// Returns a list of messages that don't have the full body loaded
    pub fn messages_without_body(&self) -> Vec<Message> {
        self.imp()
            .messages
            .iter()
            .filter_map(|(_key, msg)| {
                if msg.detail_level() < MessageDetailLevel::Full {
                    Some(msg.clone())
                } else {
                    None
                }
            })
            .collect()
    }

    pub fn messages_filter(&self, filter: impl Fn(&Message) -> bool) -> Vec<Message> {
        self.imp()
            .messages
            .iter()
            .filter_map(|(_key, msg)| filter(&msg).then_some(msg))
            .collect()
    }

    /// Clear out any RFC 822 bodies from this thread
    pub fn clear_message_bodies(&self) {
        self.imp()
            .messages
            .iter()
            .for_each(|(_key, msg)| msg.set_message_body(None));
    }

    pub fn cmp_date(&self, other: &Thread) -> std::cmp::Ordering {
        match self.sort_date().cmp(&other.sort_date()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        match self.thread_id().cmp(&other.thread_id()) {
            std::cmp::Ordering::Equal => {}
            o => return o,
        }

        self.cmp(other)
    }
}
