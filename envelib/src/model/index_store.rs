// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::borrow::Cow;
use std::cell::{Cell, RefCell};
use std::collections::VecDeque;
use std::rc::Rc;

use gio::prelude::ListModelExt;
use glib::object::ObjectSubclassIs;
use glib::subclass::prelude::*;
use glib::{SignalHandlerId, closure_local, prelude::*};

const INVALID_OBJ_MSG: &str = "Invalid item type for IndexStore";

#[derive(Debug, Clone, Copy, Hash, PartialEq, Eq)]
#[repr(transparent)]
pub struct IndexStoreKey(i64);

impl From<i64> for IndexStoreKey {
    fn from(value: i64) -> Self {
        Self(value)
    }
}

impl From<IndexStoreKey> for i64 {
    fn from(value: IndexStoreKey) -> Self {
        value.0
    }
}

pub trait IndexStoreClassExt {
    fn init_item_type(&mut self);
}

impl<T: ClassStruct> IndexStoreClassExt for T
where
    T::Type: IndexStoreImpl,
{
    fn init_item_type(&mut self) {
        let item_type = <<Self as ClassStruct>::Type as IndexStoreImpl>::Item::static_type();
        unsafe {
            let class = self as *const _ as *mut imp::IndexStoreClass;
            (*class).set_item_type(item_type)
        }
    }
}

mod imp {
    use gio::subclass::prelude::ListModelImpl;
    use glib::subclass::Signal;
    use indexmap::{IndexMap, map::Entry};
    use std::sync::LazyLock;

    use super::*;

    #[repr(C)]
    pub struct IndexStoreClass {
        parent_class: <<<Self as ClassStruct>::Type as ObjectSubclass>::ParentType as ObjectType>::GlibClassType,
        item_type: glib::Type,
    }

    unsafe impl ClassStruct for IndexStoreClass {
        type Type = IndexStore;
    }

    impl IndexStoreClass {
        pub(crate) fn item_type(&self) -> glib::Type {
            self.item_type
        }

        pub(crate) fn set_item_type(&mut self, item_type: glib::Type) {
            self.item_type = item_type;
        }
    }

    #[derive(Debug)]
    pub struct IndexStore {
        item_type: Cell<glib::Type>,
        list: RefCell<IndexMap<IndexStoreKey, glib::Object>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for IndexStore {
        const NAME: &'static str = "EnvelopeIndexStore";
        const ABSTRACT: bool = true;
        type Type = super::IndexStore;
        type Interfaces = (gio::ListModel,);
        type Class = IndexStoreClass;

        fn class_init(klass: &mut Self::Class) {
            klass.item_type = glib::Type::INVALID;
        }

        fn with_class(klass: &Self::Class) -> Self {
            assert_ne!(
                klass.item_type(),
                glib::Type::INVALID,
                "the object type of IndexStore must be initialized in `class_init()` of the subclass"
            );

            Self {
                item_type: Cell::new(klass.item_type()),
                list: RefCell::default(),
            }
        }
    }

    impl ObjectImpl for IndexStore {
        fn signals() -> &'static [glib::subclass::Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> =
                LazyLock::new(|| vec![Signal::builder("before-mutation").build()]);
            SIGNALS.as_ref()
        }
    }

    impl ListModelImpl for IndexStore {
        fn item_type(&self) -> glib::Type {
            self.item_type.get()
        }

        fn n_items(&self) -> u32 {
            self.list.borrow().len() as u32
        }

        fn item(&self, position: u32) -> Option<glib::Object> {
            self.get_index(position as usize)
                .map(|(_index, index)| index.clone().upcast())
        }
    }

    impl IndexStore {
        /// Emitted directly before the list store will be mutated
        fn emit_before_mutate(&self) {
            self.obj().emit_by_name::<()>("before-mutation", &[])
        }

        fn position(&self, key: impl Into<IndexStoreKey>) -> Option<usize> {
            self.list.borrow().get_index_of(&key.into())
        }

        pub(super) fn contains_key(&self, key: &IndexStoreKey) -> bool {
            self.list.borrow().contains_key(key)
        }

        pub(super) fn get(&self, key: IndexStoreKey) -> Option<glib::Object> {
            self.list.borrow().get(&key).cloned()
        }

        pub(super) fn get_index(&self, position: usize) -> Option<(IndexStoreKey, glib::Object)> {
            self.list
                .borrow()
                .get_index(position)
                .map(|(key, value)| (*key, value.to_owned()))
        }

        pub(super) fn binary_search_by<F>(&self, f: F) -> Result<usize, usize>
        where
            F: FnMut(&IndexStoreKey, &glib::Object) -> std::cmp::Ordering,
        {
            let list = self.list.borrow();
            list.binary_search_by(f)
        }

        pub(super) fn retain<F>(&self, keep: F)
        where
            F: FnMut(&IndexStoreKey, &mut glib::Object) -> bool,
        {
            self.emit_before_mutate();
            self.list.borrow_mut().retain(keep);
        }

        pub(super) fn extend(
            &self,
            objects: impl IntoIterator<Item = (IndexStoreKey, glib::Object)>,
        ) {
            self.emit_before_mutate();
            let mut last_index = self.list.borrow().len();
            let mut added = 0;

            for (key, object) in objects {
                // This will insert either at the end, or replace an item further back and return its index
                //let (index, old_object) = self.list.borrow_mut().insert_full(key, object.clone());
                let mut list = self.list.borrow_mut();
                let entry = list.entry(key);

                match entry {
                    Entry::Occupied(entry) => {
                        if entry.get() == &object {
                            // This is the exact same object. Nothing to do.
                            continue;
                        }

                        // An item will be replaced in the middle
                        drop(list);
                        if added > 0 {
                            // First commit the changes we have.
                            self.obj().items_changed(last_index as u32, 0, added as u32);

                            // Reset our counters
                            last_index += added;
                            added = 0;
                        }

                        // Insert the item at its location in the middle
                        let (index, _) = self.list.borrow_mut().insert_full(key, object.clone());
                        self.obj().items_changed(index as u32, 1, 1);
                    }
                    Entry::Vacant(entry) => {
                        // Insert the item at the end
                        entry.insert(object);
                        added += 1;
                    }
                }
            }

            if added > 0 {
                self.obj().items_changed(last_index as u32, 0, added as u32);
            }
        }

        pub(super) fn insert(&self, key: IndexStoreKey, object: glib::Object) {
            self.extend(std::iter::once((key, object)))
        }

        pub(super) fn insert_at(&self, pos: usize, key: IndexStoreKey, object: glib::Object) {
            self.emit_before_mutate();

            // TODO optimize this a bit more
            // First remove the object if it exists
            let remove = self.list.borrow_mut().shift_remove_full(&key);
            if let Some((old_index, _, _)) = remove {
                // Emit the removal
                self.obj().items_changed(old_index as u32, 1, 0);
            }

            // Now insert it at pos
            self.list.borrow_mut().shift_insert(pos, key, object);
            self.obj().items_changed(pos as u32, 0, 1);
        }

        pub(super) fn update(&self, key: IndexStoreKey) -> u32 {
            self.emit_before_mutate();
            if let Some(pos) = self.position(key) {
                self.obj().items_changed(pos as u32, 1, 1);
                pos as u32
            } else {
                u32::MAX
            }
        }

        pub(super) fn remove_full(
            &self,
            key: IndexStoreKey,
        ) -> Option<(usize, IndexStoreKey, glib::Object)> {
            self.emit_before_mutate();
            let removed = self.list.borrow_mut().shift_remove_full(&key);

            if let Some((idx, key, object)) = removed {
                self.obj().items_changed(idx as u32, 1, 0);
                Some((idx, key, object))
            } else {
                None
            }
        }

        pub(super) fn remove_all(&self) {
            self.emit_before_mutate();
            let len = self.list.borrow().len();
            self.list.borrow_mut().clear();
            self.obj().items_changed(0, len as u32, 0);
        }
    }
}

glib::wrapper! {
    pub struct IndexStore(ObjectSubclass<imp::IndexStore>)
    @implements gio::ListModel;
}

/// Virtual methods
pub trait IndexStoreImpl: ObjectImpl {
    type Key: Into<IndexStoreKey> + From<IndexStoreKey> + 'static;
    type Item: IsA<glib::Object> + ObjectType;

    fn item_type() -> &'static str {
        <Self::Item as glib::types::StaticType>::static_type().name()
    }
}

unsafe impl<Obj: IndexStoreImpl> IsSubclassable<Obj> for IndexStore {}

#[allow(dead_code)]
pub trait IndexStoreExt:
    IsA<IndexStore> + IsA<gio::ListModel> + ObjectSubclassIs + IsA<glib::Object> + glib::object::IsClass
{
    type Key: Into<IndexStoreKey> + From<IndexStoreKey> + 'static;
    type Item: IsA<glib::Object> + ObjectType;

    fn new() -> Self {
        glib::Object::new()
    }

    /// Create an object of this type from an iterator
    fn from_iter(objects: impl IntoIterator<Item = (Self::Key, Self::Item)>) -> Self {
        let store = Self::new();
        store.extend(objects);
        store
    }

    /// This signal is emitted *before* any mutation occurs. This can be used to
    /// save the state before the mutation, or do any last-minute items fetching
    /// before indexes might become invalidated
    fn connect_before_mutation<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(Self) + 'static,
    {
        self.connect_closure(
            "before-mutation",
            false,
            closure_local!(|store: Self| callback(store)),
        )
    }

    fn contains_key(&self, key: Self::Key) -> bool {
        self.upcast_ref::<IndexStore>()
            .imp()
            .contains_key(&key.into())
    }

    fn get(&self, key: Self::Key) -> Option<Self::Item> {
        self.upcast_ref::<IndexStore>()
            .imp()
            .get(key.into())
            .map(|o| o.dynamic_cast().expect(INVALID_OBJ_MSG))
    }

    fn get_index(&self, position: usize) -> Option<(Self::Key, Self::Item)> {
        self.upcast_ref::<IndexStore>()
            .imp()
            .get_index(position)
            .map(|(k, o)| (k.into(), o.dynamic_cast().expect(INVALID_OBJ_MSG)))
    }

    /// Binary searches the store with a comparator function
    ///
    /// Returns `Ok(index)` with the index of the matching element if it was found
    /// Returns `Èrr(index)` with the index of a possible insertion position if the
    /// element was not found
    fn binary_search_by<F>(&self, mut f: F) -> Result<usize, usize>
    where
        F: FnMut(&Self::Key, &Self::Item) -> std::cmp::Ordering,
    {
        self.upcast_ref::<IndexStore>().imp().binary_search_by(
            move |key: &IndexStoreKey, obj: &glib::Object| {
                f(
                    &Self::Key::from(*key),
                    obj.dynamic_cast_ref().expect(INVALID_OBJ_MSG),
                )
            },
        )
    }

    fn retain<F>(&self, mut keep: F)
    where
        F: FnMut(Self::Key, &Self::Item) -> bool,
    {
        self.upcast_ref::<IndexStore>().imp().retain(|key, item| {
            keep(
                (*key).into(),
                item.dynamic_cast_ref().expect(INVALID_OBJ_MSG),
            )
        });
    }

    fn extend(&self, objects: impl IntoIterator<Item = (Self::Key, Self::Item)>) {
        self.upcast_ref::<IndexStore>()
            .imp()
            .extend(objects.into_iter().map(|(k, o)| (k.into(), o.upcast())))
    }

    fn insert(&self, key: Self::Key, object: Self::Item) {
        self.upcast_ref::<IndexStore>()
            .imp()
            .insert(key.into(), object.upcast())
    }

    /// Insert the item at the specified index
    ///
    /// Panics if index is out of bounds
    fn insert_at(&self, pos: usize, key: Self::Key, object: Self::Item) {
        self.upcast_ref::<IndexStore>()
            .imp()
            .insert_at(pos, key.into(), object.upcast());
    }

    fn update(&self, key: Self::Key) -> u32 {
        self.upcast_ref::<IndexStore>().imp().update(key.into())
    }

    fn remove(&self, key: Self::Key) -> Option<Self::Item> {
        self.remove_full(key).map(|(_, _, o)| o)
    }

    fn remove_full(&self, key: Self::Key) -> Option<(usize, Self::Key, Self::Item)> {
        self.upcast_ref::<IndexStore>()
            .imp()
            .remove_full(key.into())
            .map(|(pos, key, o)| (pos, key.into(), o.downcast().expect(INVALID_OBJ_MSG)))
    }

    fn remove_all(&self) {
        self.upcast_ref::<IndexStore>().imp().remove_all()
    }

    fn iter(&self) -> IndexStoreIter<'_, Self> {
        IndexStoreIter::new(Cow::Borrowed(self))
    }

    fn keys(&self) -> IndexStoreKeysIter<'_, Self> {
        IndexStoreKeysIter::new(Cow::Borrowed(self))
    }

    fn values(&self) -> IndexStoreValuesIter<'_, Self> {
        IndexStoreValuesIter::new(Cow::Borrowed(self))
    }

    /// Return the item at the given `position`
    fn value(&self, position: usize) -> Option<Self::Item> {
        self.item(position as u32)
            .map(|o| o.downcast().expect(INVALID_OBJ_MSG))
    }

    /// Same as [`ListModelExt::n_items()`] but returns a usize as is common for rust len methods
    fn len(&self) -> usize {
        self.n_items() as usize
    }

    /// Checks whether the store is empty
    fn is_empty(&self) -> bool {
        self.n_items() == 0
    }

    /// Returns the first item, if it exists
    fn first(&self) -> Option<Self::Item> {
        self.item(0)
            .map(|o| o.dynamic_cast().expect(INVALID_OBJ_MSG))
    }

    /// Returns the last item, if it exists
    fn last(&self) -> Option<Self::Item> {
        let size = self.n_items();
        if size > 0 {
            self.item(size - 1)
                .map(|o| o.dynamic_cast().expect(INVALID_OBJ_MSG))
        } else {
            None
        }
    }
}

impl<T> IndexStoreExt for T
where
    Self: ObjectSubclassIs + ObjectType + IsA<IndexStore> + IsA<gio::ListModel> + IsA<glib::Object>,
    <Self as ObjectSubclassIs>::Subclass: IndexStoreImpl,
{
    type Key = <<Self as ObjectSubclassIs>::Subclass as IndexStoreImpl>::Key;
    type Item = <<Self as ObjectSubclassIs>::Subclass as IndexStoreImpl>::Item;
}

#[derive(Debug)]
struct IndexStoreIterInner<T> {
    idx: usize,
    ridx: usize,
    buf: Option<VecDeque<T>>,
}

pub struct IndexStoreIter<'a, S: IndexStoreExt> {
    store: Cow<'a, S>,
    signal_id: Option<SignalHandlerId>,
    inner: Rc<RefCell<IndexStoreIterInner<<Self as Iterator>::Item>>>,
}

impl<'a, S: IsA<IndexStore> + IndexStoreExt> IndexStoreIter<'a, S> {
    pub fn new(store: Cow<'a, S>) -> Self {
        // Like any ListModel we have the issue that GObjects have interior mutability and thus
        // our list model can change during iteration. To safeguard against this, [`IndexStore`]
        // has a signal `before-mutation` that will be called *before* any change to the internal
        // list has occured. We connect to this signal and use this opportunity to collect all
        // items in the store into a temporay `Vec`.
        //
        // We could of course do this from the beginning, because ListModel is !Send. But this has
        // a performance penalty.
        let len = store.len();
        let inner = Rc::new(RefCell::new(IndexStoreIterInner {
            idx: 0,
            ridx: len,
            buf: None,
        }));

        let signal_id = Some(store.connect_before_mutation({
            let inner = inner.clone();

            move |store| {
                // Consume the ListModel and put the remaining items in buf
                let mut inner = inner.borrow_mut();

                // Only run this once
                if inner.buf.is_none() {
                    let mut vec =
                        VecDeque::with_capacity(inner.ridx - inner.idx);

                    for n in inner.idx..inner.ridx {
                        let item = store.get_index(n).expect("Invalid ListModel implementation, reported length higher than actual length");
                        vec.push_back(item);
                    }

                    inner.buf = Some(vec);
                }
            }
        }));

        IndexStoreIter {
            store,
            signal_id,
            inner,
        }
    }

    pub fn is_buffered(&self) -> bool {
        self.inner.borrow().buf.is_some()
    }

    pub fn into_owned(mut self) -> IndexStoreIter<'static, S> {
        IndexStoreIter {
            store: Cow::Owned(self.store.clone().into_owned()),
            signal_id: self.signal_id.take(),
            inner: self.inner.clone(),
        }
    }
}

impl<S: IndexStoreExt> Iterator for IndexStoreIter<'_, S> {
    type Item = (<S as IndexStoreExt>::Key, <S as IndexStoreExt>::Item);

    fn size_hint(&self) -> (usize, Option<usize>) {
        let inner = self.inner.borrow();
        let len = match &inner.buf {
            Some(buf) => buf.len(),
            None => inner.ridx - inner.idx,
        };

        (len, Some(len))
    }

    fn next(&mut self) -> Option<Self::Item> {
        let mut inner = self.inner.borrow_mut();
        if inner.idx >= inner.ridx {
            return None;
        }

        let (key, value) = match &mut inner.buf {
            Some(buf) => buf
                .pop_front()
                .expect("buffer size must be equal to length"),
            None => self
                .store
                .get_index(inner.idx)
                .expect("reported length should be equal to actual length"),
        };

        inner.idx += 1;
        Some((key, value))
    }
}

impl<S: IndexStoreExt> DoubleEndedIterator for IndexStoreIter<'_, S> {
    fn next_back(&mut self) -> Option<Self::Item> {
        let mut inner = self.inner.borrow_mut();
        if inner.idx >= inner.ridx {
            return None;
        }

        inner.ridx -= 1;
        let (key, value) = match &mut inner.buf {
            Some(buf) => buf.pop_back().expect("buffer size must be equal to length"),
            None => self
                .store
                .get_index(inner.ridx)
                .expect("reported length should be equal to actual length"),
        };

        Some((key, value))
    }
}

impl<S: IndexStoreExt> ExactSizeIterator for IndexStoreIter<'_, S> {}

impl<S: IndexStoreExt> Drop for IndexStoreIter<'_, S> {
    fn drop(&mut self) {
        if let Some(s) = self.signal_id.take() {
            self.store.disconnect(s);
        }
    }
}

pub struct IndexStoreKeysIter<'a, S: IndexStoreExt>(IndexStoreIter<'a, S>);

impl<'a, S: IsA<IndexStore> + IndexStoreExt> IndexStoreKeysIter<'a, S> {
    pub fn new(store: Cow<'a, S>) -> Self {
        Self(IndexStoreIter::new(store))
    }

    pub fn is_buffered(&self) -> bool {
        self.0.is_buffered()
    }
}

impl<S: IndexStoreExt> Iterator for IndexStoreKeysIter<'_, S> {
    type Item = <S as IndexStoreExt>::Key;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|(key, _value)| key)
    }
}

impl<S: IndexStoreExt> DoubleEndedIterator for IndexStoreKeysIter<'_, S> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0.next_back().map(|(key, _value)| key)
    }
}

impl<S: IndexStoreExt> ExactSizeIterator for IndexStoreKeysIter<'_, S> {}

pub struct IndexStoreValuesIter<'a, S: IndexStoreExt>(IndexStoreIter<'a, S>);

impl<'a, S: IsA<IndexStore> + IndexStoreExt> IndexStoreValuesIter<'a, S> {
    pub fn new(store: Cow<'a, S>) -> Self {
        Self(IndexStoreIter::new(store))
    }

    pub fn is_buffered(&self) -> bool {
        self.0.is_buffered()
    }
}

impl<S: IndexStoreExt> Iterator for IndexStoreValuesIter<'_, S> {
    type Item = <S as IndexStoreExt>::Item;

    fn next(&mut self) -> Option<Self::Item> {
        self.0.next().map(|(_key, value)| value)
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        self.0.size_hint()
    }
}

impl<S: IndexStoreExt> DoubleEndedIterator for IndexStoreValuesIter<'_, S> {
    fn next_back(&mut self) -> Option<Self::Item> {
        self.0.next_back().map(|(_key, value)| value)
    }
}

impl<S: IndexStoreExt> ExactSizeIterator for IndexStoreValuesIter<'_, S> {}

#[cfg(test)]
mod test {
    use super::*;

    use gio::prelude::*;

    mod imp {
        use super::*;

        #[derive(Default)]
        pub(super) struct TestStore {}

        #[glib::object_subclass]
        impl ObjectSubclass for TestStore {
            const NAME: &'static str = "TestIndexStore";
            type Type = super::TestStore;
            type ParentType = IndexStore;

            fn class_init(klass: &mut Self::Class) {
                klass.init_item_type();
            }
        }
        impl ObjectImpl for TestStore {}
        impl IndexStoreImpl for TestStore {
            type Key = i64;
            type Item = gio::File;
        }
        impl TestStore {}
    }
    glib::wrapper! {
        struct TestStore(ObjectSubclass<imp::TestStore>)
        @extends IndexStore,
        @implements gio::ListModel;
    }
    impl TestStore {}

    #[test]
    fn store() {
        let store = TestStore::new();

        let files = (0..10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);

        assert_eq!(store.n_items(), 10);
        assert_eq!(store.get(3).unwrap().uri(), "test://3/");
        store.remove(3);
        assert_eq!(store.n_items(), 9);
        assert_eq!(store.get(3), None);
        assert_eq!(store.get(4).unwrap().uri(), "test://4/");
        assert_eq!(store.get(9).unwrap().uri(), "test://9/");
        let mutated = Rc::new(Cell::new(false));
        let id = store.connect_before_mutation(glib::clone!(
            #[strong]
            mutated,
            #[strong]
            store,
            move |_| {
                mutated.set(true);

                // During signal emission nothing has been mutated yet
                assert_eq!(store.n_items(), 9);
            }
        ));
        assert!(!mutated.get());
        store.remove(1);
        assert_eq!(store.n_items(), 8);
        assert!(mutated.get());
        store.disconnect(id);
    }

    #[test]
    fn iter() {
        let store: TestStore = TestStore::new();

        let files = (0..10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);
        for (i, value) in store.values().enumerate() {
            assert_eq!(value.uri(), format!("test://{}/", i));
        }
    }

    #[test]
    fn iter_mutate() {
        let store = TestStore::new();

        let files = (0..10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);
        let mut last_i = 0;
        for (i, value) in store.values().enumerate() {
            // Let's mutate the store after the third item
            if i == 3 {
                store.remove(9);
            }

            assert_eq!(value.uri(), format!("test://{}/", i));
            last_i = i;
        }

        assert_eq!(last_i, 9);
    }

    #[test]
    fn iter_rev() {
        let store = TestStore::new();

        let files = (0..=10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);

        let mut iter = store.values();
        let mut i = 11;

        while let Some(file) = iter.next_back() {
            i -= 1;
            assert_eq!(file.uri(), format!("test://{}/", i));
        }

        assert_eq!(i, 0);
    }

    #[test]
    fn iter_mutate_rev() {
        let store = TestStore::new();

        let files = (0..10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);

        let mut iter = store.values();
        let mut i = 10;

        while let Some(file) = iter.next_back() {
            i -= 1;
            // mutate the store after 2 items
            if i == 7 {
                store.remove_all();
            }

            assert_eq!(file.uri(), format!("test://{}/", i));

            let front = iter.next();
            assert_eq!(front.unwrap().uri(), format!("test://{}/", 9 - i))
        }

        // As we were iterating from both sides, i should be n / 2 = 5
        assert_eq!(i, 5);
    }

    #[test]
    fn iter_exact_size() {
        let store: TestStore = TestStore::new();

        let files = (0..10).map(|i| (i, gio::File::for_uri(&format!("test://{}/", i))));
        store.extend(files);
        for (i, value) in store.values().rev().enumerate() {
            assert_eq!(value.uri(), format!("test://{}/", 9 - i));
        }

        let mut iter = store.values();
        assert_eq!(iter.len(), 10);
        assert_eq!(iter.nth(3).unwrap().uri(), format!("test://{}/", 3));
        assert_eq!(iter.len(), 6);
        assert!(!iter.is_buffered());
        store.insert(11, gio::File::for_uri(&format!("test://{}/", 11)));
        // Store has been mutated
        assert!(iter.is_buffered());
        assert_eq!(iter.len(), 6);
        assert_eq!(iter.next().unwrap().uri(), format!("test://{}/", 4));
        assert_eq!(iter.next_back().unwrap().uri(), format!("test://{}/", 9));
    }
}
