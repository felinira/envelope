// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::prelude::*;
use glib::subclass::prelude::*;

use super::index_store::IndexStoreIter;
use super::{Message, MessageExtProps};
use crate::model::IndexStoreExt;
use crate::model::index_store::{IndexStore, IndexStoreKey};
use crate::types::MessageCacheId;

impl From<IndexStoreKey> for MessageCacheId {
    fn from(value: IndexStoreKey) -> Self {
        MessageCacheId::new(value.into())
    }
}

impl From<MessageCacheId> for IndexStoreKey {
    fn from(value: MessageCacheId) -> Self {
        IndexStoreKey::from(*value.as_ref())
    }
}

mod imp {
    use crate::model::index_store::{IndexStoreClassExt, IndexStoreImpl};

    use super::*;
    use std::cell::Cell;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::MessageStore)]
    pub struct MessageStore {
        #[property(get)]
        pub(super) finished_loading: Cell<bool>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MessageStore {
        const NAME: &'static str = "EnvelopeMessageStore";
        type Type = super::MessageStore;
        type ParentType = IndexStore;

        fn class_init(klass: &mut Self::Class) {
            klass.init_item_type();
        }
    }

    #[glib::derived_properties]
    impl ObjectImpl for MessageStore {}
    impl IndexStoreImpl for MessageStore {
        type Key = MessageCacheId;
        type Item = Message;
    }
}

glib::wrapper! {
    pub struct MessageStore(ObjectSubclass<imp::MessageStore>)
    @extends IndexStore,
    @implements gio::ListModel;
}

impl MessageStore {}

impl std::default::Default for MessageStore {
    fn default() -> Self {
        MessageStore::new()
    }
}

impl FromIterator<Message> for MessageStore {
    fn from_iter<T: IntoIterator<Item = Message>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter.into_iter().map(|msg| (msg.cache_id(), msg)))
    }
}

impl FromIterator<(MessageCacheId, Message)> for MessageStore {
    fn from_iter<T: IntoIterator<Item = (MessageCacheId, Message)>>(iter: T) -> Self {
        IndexStoreExt::from_iter(iter)
    }
}

impl<'a> IntoIterator for &'a MessageStore {
    type Item = (MessageCacheId, Message);
    type IntoIter = IndexStoreIter<'a, MessageStore>;

    fn into_iter(self) -> Self::IntoIter {
        IndexStoreExt::iter(self)
    }
}
