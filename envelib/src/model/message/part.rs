// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::{borrow::Cow, sync::Arc};

use mail_parser::MimeHeaders;

use crate::{gettext::*, model::Attachment};

use super::{ContentTypeToString, InternetMessage, MimeAttachment, TextPart};

pub enum MessagePart {
    Text(TextPart<'static>),
    Message(InternetMessage),
    Binary(Attachment),
    Multipart(String, Vec<MessagePart>),
}

impl MessagePart {
    pub fn from_message_part(
        message: Arc<mail_parser::Message<'static>>,
        part_id: mail_parser::MessagePartId,
    ) -> Self {
        let part = &message.as_ref().parts[part_id];
        let content_type = part
            .content_type()
            .map(ContentTypeToString::to_string)
            .unwrap_or("mixed".to_string());

        match &part.body {
            mail_parser::PartType::Text(cow) => {
                Self::Text(TextPart::text(Cow::from(cow.clone().into_owned())))
            }
            mail_parser::PartType::Html(cow) => {
                Self::Text(TextPart::html(Cow::Owned(cow.clone().into_owned())))
            }
            mail_parser::PartType::InlineBinary(_) | mail_parser::PartType::Binary(_) => {
                let filename = part.attachment_name().map(ToOwned::to_owned);
                let encoding = part.encoding;
                let mime = MimeAttachment {
                    message: message.clone(),
                    part_id,
                    filename,
                    encoding,
                    content_type,
                };
                Self::Binary(Attachment::from(mime))
            }
            mail_parser::PartType::Message(message) => Self::from(message),
            mail_parser::PartType::Multipart(ids) => Self::Multipart(
                content_type,
                ids.iter()
                    .map(|id| MessagePart::from_message_part(message.clone(), *id))
                    .collect(),
            ),
        }
    }

    pub fn message(&self) -> Option<&InternetMessage> {
        match self {
            MessagePart::Message(internet_message) => Some(internet_message),
            _ => None,
        }
    }

    pub fn text(&self) -> Option<&TextPart> {
        match self {
            MessagePart::Text(text_part) => Some(text_part),
            _ => None,
        }
    }

    pub fn binary(&self) -> Option<Attachment> {
        match self {
            MessagePart::Binary(attachment) => Some(attachment.clone()),
            _ => None,
        }
    }

    pub fn bytes(&self) -> Option<&[u8]> {
        match self {
            MessagePart::Text(text_part) => Some(text_part.as_ref().as_bytes()),
            MessagePart::Message(internet_message) => Some(internet_message.as_ref()),
            MessagePart::Binary(attachment) => attachment.data(),
            MessagePart::Multipart(_, _) => None,
        }
    }

    /// MIME type
    pub fn content_type(&self) -> String {
        match self {
            MessagePart::Message(_) => "message/rfc822".to_string(),
            MessagePart::Text(text_part) => text_part.content_type().to_string(),
            MessagePart::Binary(attachment) => attachment.content_type(),
            MessagePart::Multipart(kind, _) => format!("multipart/{}", kind),
        }
    }

    pub fn content_disposition(&self) -> Option<String> {
        match self {
            MessagePart::Text(_) => None,
            MessagePart::Message(_) => Some("attachment".to_string()),
            // TODO: Format filename (urlencoded in filename*)
            MessagePart::Binary(_) => Some("attachment".to_string()),
            MessagePart::Multipart(_, _) => None,
        }
    }

    /// A suggested filename if one were to save this message part to disk
    pub fn filename(&self) -> String {
        match self {
            MessagePart::Text(text_part) => {
                match text_part.kind() {
                    super::text::TextKind::Text => {
                        // Translators: A generic fallback filename for plain message text
                        gettext("Message Content.txt")
                    }
                    super::text::TextKind::Html => {
                        // Translators: A generic fallback filename for HTML message text
                        gettext("Message Content.html")
                    }
                }
            }
            MessagePart::Message(internet_message) => {
                let subject = internet_message.parse_subject();
                if subject.is_empty() {
                    // Translators: A generic fallback filename for an email message
                    gettext("Message.eml")
                } else {
                    format!("{}.eml", subject.trim())
                }
            }
            MessagePart::Binary(attachment) => {
                attachment.filename().unwrap_or({
                    // Translators: A generic fallback filename for an email attachment
                    gettext("Attachment.bin")
                })
            }
            MessagePart::Multipart(_, _) => gettext("Attachment.bin"),
        }
    }
}

impl From<&mail_parser::Message<'_>> for MessagePart {
    fn from(value: &mail_parser::Message<'_>) -> Self {
        Self::Message(InternetMessage::new(
            value.clone().into_owned().to_owned(),
            true,
        ))
    }
}

impl From<InternetMessage> for MessagePart {
    fn from(value: InternetMessage) -> Self {
        Self::Message(value)
    }
}
