// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::prelude::*;
use glib::subclass::prelude::*;

pub(crate) use crate::imap::*;

use super::{MailboxId, Message, MessageCacheId, MessageExt, MessageExtProps, ThreadId};

mod imp {
    use crate::model::message::MessageImpl;

    use super::*;
    use std::cell::Cell;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::ImapMessage)]
    pub(crate) struct ImapMessage {
        /// The IMAP UID for this message
        #[property(get, set)]
        imap_uid: Cell<Uid>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImapMessage {
        const NAME: &'static str = "EnvelopeImapMessage";
        type Type = super::ImapMessage;
        type ParentType = crate::model::Message;
    }

    #[glib::derived_properties]
    impl ObjectImpl for ImapMessage {}
    impl MessageImpl for ImapMessage {}

    impl ImapMessage {}
}

glib::wrapper! {
    pub(crate) struct ImapMessage(ObjectSubclass<imp::ImapMessage>)
    @extends Message;
}

/// Private to the library crate
impl ImapMessage {
    pub(crate) fn from_imap(
        cache_id: MessageCacheId,
        mailbox_id: MailboxId,
        imap_uid: Uid,
        thread_id: ThreadId,
        internal_date: glib::DateTime,
    ) -> Self {
        glib::Object::builder()
            .property("cache-id", cache_id)
            .property("mailbox-id", mailbox_id)
            .property("imap-uid", imap_uid)
            .property("thread-id", thread_id)
            .property("internal-date", internal_date)
            .build()
    }

    pub(crate) fn update_data(&self, data: &MessageData) {
        if let Some(flags) = &data.flags {
            self.update_flags(flags);
        }

        if let Some(attachment_count) = data.attachment_count {
            self.set_attachment_count(attachment_count as u32);
        }

        if let Some(body) = &data.message {
            self.set_message_body(Some(body.clone()));
        }
    }
}
