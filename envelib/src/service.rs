// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#![allow(dead_code)]

pub mod delegate;
pub mod incoming;

use crate::imap::ImapService;
use crate::types::ConnectionStatus;
use std::path::PathBuf;

use gio::prelude::*;
use gio::subclass::prelude::*;

use self::incoming::IncomingMailServiceExt;

mod imp {
    use std::cell::{Cell, OnceCell, RefCell};

    use crate::{account, cache::Cache};

    use self::incoming::IncomingMailService;

    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::MailService)]
    pub struct MailService {
        /// The account property can be changed, but must never be null
        #[property(get, set = Self::set_account, construct)]
        account: RefCell<Option<account::Account>>,
        #[property(get, construct_only)]
        cache_dir: OnceCell<PathBuf>,
        #[property(get, set = Self::set_work_online, explicit_notify)]
        work_online: Cell<bool>,
        #[property(get, explicit_notify)]
        connection_status: RefCell<ConnectionStatus>,
        #[property(get)]
        uses_test_server: Cell<bool>,
        cache: OnceCell<Cache>,

        #[property(get)]
        mailboxes: OnceCell<gio::ListStore>,

        #[property(get)]
        incoming: OnceCell<IncomingMailService>,
        outgoing: RefCell<Option<()>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MailService {
        const NAME: &'static str = "EnvelopeMailService";
        type Type = super::MailService;
        type Interfaces = (gio::AsyncInitable,);
    }

    #[glib::derived_properties]
    impl ObjectImpl for MailService {}

    impl AsyncInitableImpl for MailService {
        fn init_future(
            &self,
            _io_priority: glib::Priority,
        ) -> std::pin::Pin<
            Box<dyn std::future::Future<Output = std::result::Result<(), glib::Error>> + 'static>,
        > {
            let imp = self.ref_counted();
            Box::pin(glib::clone!(
                #[strong]
                imp,
                async move {
                    let account = imp.account.borrow().clone().unwrap();
                    let cache_dir = imp.cache_dir.get().unwrap().clone();

                    let cache = Cache::new(account.clone(), cache_dir).await?;
                    imp.cache.set(cache.clone()).unwrap();

                    #[cfg(feature = "devel")]
                    if account.is_test_server() {
                        log::info!("Using test server");
                        imp.uses_test_server.set(true);
                        imp.obj().notify_uses_test_server();

                        if crate::devel::test_server::test_connection() {
                            log::info!("Test server already running. Reusing connection.");
                        } else {
                            crate::devel::test_server::start(true, false)
                                .await
                                .map_err(crate::Error::from)??;
                        }
                    }

                    let imap = ImapService::new(account, cache).await?;
                    let incoming = imp.incoming.get_or_init(|| imap.upcast());

                    incoming.connect_work_online_notify(glib::clone!(
                        #[weak]
                        imp,
                        move |incoming| {
                            let new = incoming.work_online();
                            if new != imp.work_online.get() {
                                imp.set_work_online(new)
                            }
                        }
                    ));

                    incoming.connect_connection_status_notify(glib::clone!(
                        #[weak]
                        imp,
                        move |incoming| {
                            let new = incoming.connection_status();
                            if new != *imp.connection_status.borrow() {
                                imp.set_connection_status(new)
                            }
                        }
                    ));

                    Ok(())
                }
            ))
        }
    }

    impl MailService {
        fn cache(&self) -> Cache {
            self.cache
                .get()
                .expect("Cache must be initialized in constructor")
                .clone()
        }

        fn incoming(&self) -> IncomingMailService {
            self.incoming
                .get()
                .expect("MailService can't be intialized without an incoming service configured")
                .clone()
        }

        fn set_account(&self, account: crate::account::Account) {
            if let Some(incoming) = self.incoming.get() {
                incoming.set_account(account.clone());
            }

            self.account.replace(Some(account));
        }

        fn set_work_online(&self, work_online: bool) {
            self.incoming().set_work_online(work_online);

            if self.work_online.replace(work_online) != work_online {
                self.obj().notify_work_online();
            }
        }

        /// Sets connection status and notifies the property
        fn set_connection_status(&self, status: ConnectionStatus) {
            if *self.connection_status.borrow() != status {
                self.connection_status.replace(status);
                self.obj().notify_connection_status();
            }
        }
    }
}

glib::wrapper! {
    /// The main service class is used to call into delegated implementations.
    pub struct MailService(ObjectSubclass<imp::MailService>)
    @implements gio::AsyncInitable;
}

impl MailService {
    /// Create a new MailService via gio::AsyncInitable
    pub async fn new(
        account: crate::account::Account,
        cache_dir: PathBuf,
    ) -> std::result::Result<Self, glib::Error> {
        gio::AsyncInitable::builder()
            .property("account", account)
            .property("cache-dir", cache_dir)
            .build_future(glib::Priority::DEFAULT)
            .await
    }

    /// Finish all outstanding actions, shutdown all sessions
    pub async fn shutdown(&self) -> crate::Result<()> {
        self.incoming().shutdown().await?;

        #[cfg(feature = "devel")]
        if self.uses_test_server() {
            // Try to stop the server
            crate::devel::test_server::stop().await??;
        }

        Ok(())
    }
}

pub(crate) fn ensure_types() {
    MailService::ensure_type();
    delegate::ServiceDelegate::ensure_type();
    incoming::IncomingMailService::ensure_type();
}
