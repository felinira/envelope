// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use futures::Future;

use crate::{
    error::{ErrorKind, ErrorKindExt, Result},
    gettext::*,
};

pub(crate) trait StrExt {
    fn ensure_no_nul(self) -> String;
    fn unquote(self) -> Self;
}

impl StrExt for &str {
    fn ensure_no_nul(self) -> String {
        self.replace('\0', "\u{2400}")
    }

    fn unquote(self) -> Self {
        if (self.starts_with('\"') && self.ends_with('\"'))
            || (self.starts_with('\'') && self.ends_with('\''))
        {
            &self[1..self.len() - 1]
        } else {
            self
        }
    }
}

impl StrExt for String {
    fn ensure_no_nul(self) -> String {
        if self.contains('\0') {
            self.replace('\0', "\u{2400}")
        } else {
            self
        }
    }

    fn unquote(self) -> Self {
        if (self.starts_with('\"') && self.ends_with('\"'))
            || (self.starts_with('\'') && self.ends_with('\''))
        {
            self[1..self.len() - 1].to_string()
        } else {
            self
        }
    }
}

/// Utility function to add a timeout to a future
#[allow(dead_code)]
pub(crate) fn with_timeout<'a, R, F: std::future::Future<Output = Result<R>> + 'a>(
    timeout: std::time::Duration,
    future: F,
) -> impl Future<Output = Result<R>> + 'a {
    let timeout_future = async move {
        smol::Timer::after(timeout).await;
        Err(ErrorKind::Connection.with_message(gettext("Timed out")))
    };

    smol::future::or(future, timeout_future)
}

pub(crate) fn chrono_to_time(
    date_time: &chrono::DateTime<chrono::FixedOffset>,
) -> time::OffsetDateTime {
    time::OffsetDateTime::parse(
        &date_time.to_rfc3339(),
        &time::format_description::well_known::Rfc3339,
    )
    .expect("RFC3339 should be compatible between chrono and time")
}

#[expect(dead_code)]
pub(crate) fn time_to_chrono(
    date_time: &time::OffsetDateTime,
) -> chrono::DateTime<chrono::FixedOffset> {
    chrono::DateTime::parse_from_rfc3339(
        &date_time
            .format(&time::format_description::well_known::Rfc3339)
            .expect("RFC3339 should be compatible between chrono and time"),
    )
    .expect("RFC3339 should be compatible between chrono and time")
}

pub(crate) fn date_to_chrono(date: &time::Date) -> chrono::NaiveDate {
    let (y, o) = date.to_ordinal_date();
    chrono::NaiveDate::from_yo_opt(y, o.into())
        .expect("Ordinal date should be compatible between chrono and time")
}

pub(crate) trait DisplayLog {
    fn display(&self) -> String;
}

impl DisplayLog for std::time::Duration {
    fn display(&self) -> String {
        if self.as_secs() >= 3600 {
            format!(
                "{} h {:02} m {:02} s",
                self.as_secs() / 60 / 60,
                self.as_secs() / 60 % 60,
                self.as_secs() % 60
            )
        } else if self.as_secs() >= 60 {
            format!("{} m {:02} s", self.as_secs() / 60, self.as_secs() % 60)
        } else if self.as_millis() >= 1000 {
            format!("{:.2} s", self.as_secs_f32())
        } else if self.as_micros() >= 1000 {
            format!("{} ms", self.as_millis())
        } else {
            format!("{} µs", self.as_micros())
        }
    }
}

#[cfg(test)]
mod test {
    use std::time::Duration;

    use super::DisplayLog;

    #[test]
    fn duration() {
        assert_eq!(Duration::from_secs(30242).display(), "8 h 24 m 02 s");
        assert_eq!(Duration::from_secs(302).display(), "5 m 02 s");
        assert_eq!(Duration::from_millis(12023).display(), "12.02 s");
        assert_eq!(Duration::from_micros(1002).display(), "1 ms");
        assert_eq!(Duration::from_nanos(34000).display(), "34 µs");
    }
}
