// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::SignalHandlerId;
use glib::closure_local;
use glib::prelude::*;
use glib::subclass::Signal;
use glib::subclass::prelude::*;
use std::sync::LazyLock;

use futures::future::LocalBoxFuture;

use crate::account;
use crate::model;
use crate::model::MessageDetailLevel;
use crate::types::ConnectionStatus;
use crate::types::MessageFlags;
use crate::types::MessageFlagsChange;
use crate::types::MessageUpdate;

use super::delegate::ServiceDelegate;

/// The ffi module includes the exported C API of the interface.
#[allow(dead_code)]
pub mod ffi {
    use super::*;

    /// The instance pointer is used for references to the interface.
    #[repr(C)]
    pub struct Instance(std::ffi::c_void);

    #[derive(Copy, Clone)]
    #[repr(C)]
    #[allow(clippy::type_complexity)]
    pub struct Interface {
        parent_type: glib::gobject_ffi::GTypeInterface,

        pub(super) connection_test: for<'a> fn(
            &'a super::IncomingMailService,
            timeout: std::time::Duration,
        ) -> LocalBoxFuture<'a, ConnectionStatus>,

        pub(super) shutdown:
            for<'a> fn(&'a super::IncomingMailService) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) open_mailbox: for<'a> fn(
            &'a super::IncomingMailService,
            mailbox: model::Mailbox,
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) fetch_messages: for<'a> fn(
            &'a super::IncomingMailService,
            messages: &'a [model::Message],
            detail_level: MessageDetailLevel,
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) store_flags: for<'a> fn(
            &'a super::IncomingMailService,
            messages: &'a [model::Message],
            flags: MessageFlags,
            change: MessageFlagsChange,
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) copy_to: for<'a> fn(
            &'a super::IncomingMailService,
            messages: &'a [model::Message],
            destination: model::MailboxTarget,
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) move_to: for<'a> fn(
            &'a super::IncomingMailService,
            messages: &'a [model::Message],
            destination: model::MailboxTarget,
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) delete_messages: for<'a> fn(
            &'a super::IncomingMailService,
            messages: &'a [model::Message],
        ) -> LocalBoxFuture<'a, crate::Result<()>>,

        pub(super) load_threads: for<'a> fn(
            &'a super::IncomingMailService,
            threads: &'a [model::Thread],
        ) -> LocalBoxFuture<'a, crate::Result<()>>,
    }

    unsafe impl glib::subclass::types::InterfaceStruct for Interface {
        type Type = super::iface::IncomingMailService;
    }
}

/// The private module includes the interface default methods and ties together class and public type
mod iface {
    use glib::{ParamSpec, ParamSpecBoolean, ParamSpecBoxed, ParamSpecObject, ParamSpecString};

    use crate::account;
    use crate::service::delegate::ServiceDelegate;
    use crate::types::ConnectionStatus;
    use crate::{model, types::MessageUpdate};

    use super::*;

    pub struct IncomingMailService {}

    #[glib::object_interface]
    impl ObjectInterface for IncomingMailService {
        const NAME: &'static str = "EnvelopeIncomingMailService";
        type Instance = super::ffi::Instance;
        type Interface = super::ffi::Interface;
        type Prerequisites = (glib::Object,);

        fn interface_init(klass: &mut Self::Interface) {
            klass.connection_test = |_, _| unimplemented!();
            klass.shutdown = |_| unimplemented!();
            klass.open_mailbox = |_, _| unimplemented!();
            klass.fetch_messages = |_, _, _| unimplemented!();
            klass.store_flags = |_, _, _, _| unimplemented!();
            klass.copy_to = |_, _, _| unimplemented!();
            klass.move_to = |_, _, _| unimplemented!();
            klass.delete_messages = |_, _| unimplemented!();
            klass.load_threads = |_, _| unimplemented!();
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("new-task")
                        .param_types([model::Task::static_type()])
                        .build(),
                    Signal::builder("message-fetch-complete")
                        .param_types([model::Mailbox::static_type()])
                        .build(),
                    Signal::builder("message-update")
                        .param_types([model::Mailbox::static_type(), MessageUpdate::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: LazyLock<Vec<ParamSpec>> = LazyLock::new(|| {
                vec![
                    ParamSpecObject::builder::<model::Mailbox>("mailboxes").build(),
                    ParamSpecBoxed::builder::<account::Account>("account")
                        .construct()
                        .build(),
                    ParamSpecObject::builder::<ServiceDelegate>("delegate").build(),
                    ParamSpecBoolean::builder("work-online").build(),
                    ParamSpecBoxed::builder::<ConnectionStatus>("connection-status").build(),
                    ParamSpecString::builder("connection-debug-info").build(),
                ]
            });
            PROPERTIES.as_ref()
        }
    }

    impl super::ffi::Interface {}
}

glib::wrapper! {
    pub struct IncomingMailService(ObjectInterface<iface::IncomingMailService>);
}

/// The `IncomingMailServiceExt` trait contains public methods of all [`IncomingMailService`] objects
///
/// These methods need to call the appropriate vfunc from the vtable.
pub trait IncomingMailServiceExt: IsA<IncomingMailService> {
    /// Try to establish a connection with the service.
    async fn connection_test(&self, timeout: std::time::Duration) -> ConnectionStatus {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        ((class.as_ref().connection_test)(this, timeout)).await
    }

    /// Finish all outstanding actions, shutdown all sessions
    async fn shutdown(&self) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().shutdown)(this).await
    }

    /// Set the mailbox as 'open'
    ///
    /// This results in the following:
    /// - Map the thread list to the mailbox, loading it from cache if required
    /// - Synchronize the mailbox from the server, checking for recent changes
    /// - TODO: Continually monitor the mailbox for changes until the corresponding call to close_mailbox()
    async fn open_mailbox(&self, mailbox: model::Mailbox) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().open_mailbox)(this, mailbox).await
    }

    /// Fetch messages up to the specified detail level
    async fn fetch_messages(
        &self,
        messages: &[model::Message],
        detail_level: MessageDetailLevel,
    ) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().fetch_messages)(this, messages, detail_level).await
    }

    /// Store flag updates on the server
    async fn store_flags(
        &self,
        messages: &[model::Message],
        flags: MessageFlags,
        change: MessageFlagsChange,
    ) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().store_flags)(this, messages, flags, change).await
    }

    /// Copy the messages from this mailbox to the destination
    async fn copy_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().copy_to)(this, messages, destination).await
    }

    /// Move the messages from this mailbox to the destination
    async fn move_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().move_to)(this, messages, destination).await
    }

    /// Delete the messages permanently
    async fn delete_messages(&self, messages: &[model::Message]) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().delete_messages)(this, messages).await
    }

    /// Load all message headers in `thread` and set them on their respective [`Thread`].
    async fn load_threads(&self, threads: &[model::Thread]) -> crate::Result<()> {
        let this = self.upcast_ref::<IncomingMailService>();
        let class = this.interface::<IncomingMailService>().unwrap();
        (class.as_ref().load_threads)(this, threads).await
    }

    /// Gets emitted when a new task is running.
    ///
    /// This can be used to indicate running operations.
    fn connect_new_task<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService, model::Task) + 'static,
    {
        self.connect_closure(
            "new-task",
            false,
            closure_local!(|service: IncomingMailService, task: model::Task| callback(
                service, task
            )),
        )
    }

    /// Gets emitted when a fetch operation has been completed.
    ///
    /// This can be used to indicate the "loading" process is now finished.
    fn connect_message_fetch_complete<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService, model::Mailbox) + 'static,
    {
        self.connect_closure(
            "message-fetch-complete",
            false,
            closure_local!(
                |service: IncomingMailService, mailbox: model::Mailbox| callback(service, mailbox)
            ),
        )
    }

    /// Gets emitted when new (as in, not previously cached) messages have been fetched from the server.
    ///
    /// Use this for new-message notifications.
    fn connect_message_update<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService, model::Mailbox, MessageUpdate) + 'static,
    {
        self.connect_closure(
            "message-update",
            false,
            closure_local!(|service: IncomingMailService,
                            mailbox: model::Mailbox,
                            update: MessageUpdate| callback(
                service, mailbox, update
            )),
        )
    }

    /// A [`MailboxStore`](model::MailboxStore) with a list of all mailboxes.
    ///
    /// It is expected that this list be automatically fetched upon creation of the service.
    fn mailboxes(&self) -> model::MailboxStore {
        self.property("mailboxes")
    }

    /// The stored [`Account`](account::Account) for the service.
    fn account(&self) -> account::Account {
        self.property("account")
    }

    /// The account information corresponding to this service.
    ///
    /// Set this property if the account data has been changed.
    fn set_account(&self, account: account::Account) {
        self.set_property("account", account);
    }

    /// The service delegate is an object that implements auxilliary tasks for the backend.
    ///
    /// It will be called when the credentials are invalid, so that they can be updated without
    /// losing any connection state.
    fn delegate(&self) -> ServiceDelegate {
        self.property("delegate")
    }

    /// The service delegate is an object that implements auxilliary tasks for the backend.
    ///
    /// It will be called when the credentials are invalid, so that they can be updated without
    /// losing any connection state.
    fn set_delegate(&self, delegate: impl IsA<ServiceDelegate>) {
        self.set_property("delegate", delegate.upcast())
    }

    /// Whether to work online and establish a network connection, or to work completely offline
    ///
    /// When working offline, only cached messages are available.
    fn work_online(&self) -> bool {
        self.property("work-online")
    }

    /// Whether to work online and establish a network connection, or to work completely offline
    ///
    /// When working offline, only cached messages are available.
    fn set_work_online(&self, work_online: bool) {
        self.set_property("work-online", work_online);
    }

    /// Whether to work online and establish a network connection, or to work completely offline
    fn connect_work_online_notify<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService) + 'static,
    {
        self.connect_closure(
            "notify::work-online",
            false,
            closure_local!(|service: IncomingMailService, _: glib::Value| callback(service)),
        )
    }

    /// The currently determined connection state of the service.
    ///
    /// This will be set by connection errors and by monitoring the network.
    fn connection_status(&self) -> ConnectionStatus {
        self.property("connection-status")
    }

    /// The currently determined connection state of the service.
    fn connect_connection_status_notify<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService) + 'static,
    {
        self.connect_closure(
            "notify::connection-status",
            false,
            closure_local!(|service: IncomingMailService, _: glib::Value| callback(service)),
        )
    }

    /// A debug string containing additional information about the connection
    fn connection_debug_info(&self) -> String {
        self.property("connection-debug-info")
    }

    /// A debug string containing additional information about the connection
    fn connect_connection_debug_info_notify<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(IncomingMailService) + 'static,
    {
        self.connect_closure(
            "notify::connection-debug-info",
            false,
            closure_local!(|service: IncomingMailService, _: glib::Value| callback(service)),
        )
    }
}

impl<T: IsA<IncomingMailService>> IncomingMailServiceExt for T {}

/// The `IncomingMailServiceImpl` trait contains virtual function definitions for [`IncomingMailService`] objects.
pub trait IncomingMailServiceImpl: ObjectImpl {
    /// Try to establish a connection with the service.
    async fn connection_test(&self, timeout: std::time::Duration) -> ConnectionStatus {
        self.parent_connection_test(timeout).await
    }

    async fn shutdown(&self) -> crate::Result<()> {
        self.parent_shutdown().await
    }

    async fn open_mailbox(&self, mailbox: model::Mailbox) -> crate::Result<()> {
        self.parent_open_mailbox(mailbox).await
    }

    async fn fetch_messages(
        &self,
        messages: &[model::Message],
        detail_level: MessageDetailLevel,
    ) -> crate::Result<()> {
        self.parent_fetch_messages(messages, detail_level).await
    }

    /// Store flag updates on the server
    async fn store_flags(
        &self,
        messages: &[model::Message],
        flags: MessageFlags,
        change: MessageFlagsChange,
    ) -> crate::Result<()> {
        self.parent_store_flags(messages, flags, change).await
    }

    /// Copy the messages from this mailbox to the destination
    async fn copy_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        self.parent_copy_to(messages, destination).await
    }

    /// Move the messages from this mailbox to the destination
    async fn move_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        self.parent_move_to(messages, destination).await
    }

    /// Delete the messages permanently
    async fn delete_messages(&self, messages: &[model::Message]) -> crate::Result<()> {
        self.parent_delete_messages(messages).await
    }

    /// Load all message headers in `thread` and set them on their respective [`Thread`].
    async fn load_threads(&self, threads: &[model::Thread]) -> crate::Result<()> {
        self.parent_load_threads(threads).await
    }
}

/// The `IncomingMailServiceImplExt` trait contains non-overridable methods for subclasses to use.
///
/// These are supposed to be called only from inside implementations of `Pet` subclasses.
pub(crate) trait IncomingMailServiceImplExt: IncomingMailServiceImpl {
    async fn parent_connection_test(&self, timeout: std::time::Duration) -> ConnectionStatus {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let connection_test = parent_class.connection_test;
        let obj = self.obj();

        connection_test(unsafe { obj.unsafe_cast_ref() }, timeout).await
    }

    async fn parent_shutdown(&self) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let shutdown = parent_class.shutdown;
        let obj = self.obj();

        shutdown(unsafe { obj.unsafe_cast_ref() }).await
    }

    async fn parent_open_mailbox(&self, mailbox: model::Mailbox) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let open_mailbox = parent_class.open_mailbox;
        let obj = self.obj();

        open_mailbox(unsafe { obj.unsafe_cast_ref() }, mailbox).await
    }

    async fn parent_fetch_messages(
        &self,
        messages: &[model::Message],
        detail_level: MessageDetailLevel,
    ) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let fetch_messages = parent_class.fetch_messages;
        let obj = self.obj();

        fetch_messages(unsafe { obj.unsafe_cast_ref() }, messages, detail_level).await
    }

    async fn parent_store_flags(
        &self,
        messages: &[model::Message],
        flags: MessageFlags,
        change: MessageFlagsChange,
    ) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let store_flags = parent_class.store_flags;
        let obj = self.obj();

        store_flags(unsafe { obj.unsafe_cast_ref() }, messages, flags, change).await
    }

    async fn parent_copy_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let copy_to = parent_class.copy_to;
        let obj = self.obj();

        copy_to(unsafe { obj.unsafe_cast_ref() }, messages, destination).await
    }

    async fn parent_move_to(
        &self,
        messages: &[model::Message],
        destination: model::MailboxTarget,
    ) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let move_to = parent_class.move_to;
        let obj = self.obj();

        move_to(unsafe { obj.unsafe_cast_ref() }, messages, destination).await
    }

    async fn parent_delete_messages(&self, messages: &[model::Message]) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let delete_messages = parent_class.delete_messages;
        let obj = self.obj();

        delete_messages(unsafe { obj.unsafe_cast_ref() }, messages).await
    }

    async fn parent_load_threads(&self, threads: &[model::Thread]) -> crate::Result<()> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<IncomingMailService>() as *const ffi::Interface)
        };
        let load_threads = parent_class.load_threads;
        let obj = self.obj();

        load_threads(unsafe { obj.unsafe_cast_ref() }, threads).await
    }

    // Emit Signals
    /// A new task has been launched
    fn emit_new_task(&self, task: &model::Task) {
        self.obj().emit_by_name::<()>("new-task", &[task])
    }

    /// A fetch operation has been completed.
    fn emit_message_fetch_complete(&self, mailbox: &model::Mailbox) {
        self.obj()
            .emit_by_name::<()>("message-fetch-complete", &[mailbox]);
    }

    /// New (as in, not previously cached) messages have been fetched from the server.
    fn emit_message_update(&self, mailbox: &model::Mailbox, update: &MessageUpdate) {
        self.obj()
            .emit_by_name::<()>("message-update", &[mailbox, update]);
    }

    fn notify_work_online(&self) {
        self.obj().notify("work-online");
    }

    fn notify_connection_status(&self) {
        self.obj().notify("connection-status")
    }

    fn notify_connection_debug_info(&self) {
        self.obj().notify("connection-debug-info")
    }
}

/// The `IncomingMailServiceImplExt` trait is implemented for all classes that implement [`IncomingMailService`].
impl<T: IncomingMailServiceImpl> IncomingMailServiceImplExt for T {}

/// To make this interface implementable we need to implement [`IsImplementable`]
unsafe impl<Obj: IncomingMailServiceImpl> IsImplementable<Obj> for IncomingMailService {
    fn interface_init(iface: &mut glib::Interface<Self>) {
        let klass = iface.as_mut();

        klass.connection_test = |obj, timeout| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::connection_test(this, timeout))
        };

        klass.shutdown = |obj| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::shutdown(this))
        };

        klass.open_mailbox = |obj, mailbox_id| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::open_mailbox(this, mailbox_id))
        };

        klass.fetch_messages = |obj, messages, detail_level| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::fetch_messages(
                this,
                messages,
                detail_level,
            ))
        };

        klass.store_flags = |obj, messages, flags, change| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::store_flags(
                this, messages, flags, change,
            ))
        };

        klass.copy_to = |obj, messages, destination| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::copy_to(
                this,
                messages,
                destination,
            ))
        };

        klass.move_to = |obj, messages, destination| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::move_to(
                this,
                messages,
                destination,
            ))
        };

        klass.delete_messages = |obj, messages| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::delete_messages(this, messages))
        };

        klass.load_threads = |obj, threads| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(IncomingMailServiceImpl::load_threads(this, threads))
        };
    }
}
