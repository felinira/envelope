// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use glib::prelude::*;
use glib::subclass::prelude::*;

use crate::account;
use enumset::EnumSet;
use futures::future::LocalBoxFuture;

/// The ffi module includes the exported C API of the interface.
#[allow(dead_code)]
pub mod ffi {
    use super::*;

    /// The instance pointer is used for references to the interface.
    #[repr(C)]
    pub struct Instance(std::ffi::c_void);

    #[derive(Copy, Clone)]
    #[repr(C)]
    #[allow(clippy::type_complexity)]
    pub struct Interface {
        parent_type: glib::gobject_ffi::GTypeInterface,
        pub(super) ask_credentials: for<'a> fn(
            &'a super::ServiceDelegate,
            service_id: account::ServiceId,
            previous: Option<account::Secret>,
            methods: EnumSet<account::AuthMethod>,
            message: &'a str,
        )
            -> LocalBoxFuture<'a, Option<account::Secret>>,
    }

    unsafe impl glib::subclass::types::InterfaceStruct for Interface {
        type Type = super::iface::ServiceDelegate;
    }
}

/// The private module includes the interface default methods and ties together class and public type
mod iface {
    use glib::subclass::prelude::*;

    pub struct ServiceDelegate {}

    #[glib::object_interface]
    impl ObjectInterface for ServiceDelegate {
        const NAME: &'static str = "EnvelopeServiceDelegate";
        type Instance = super::ffi::Instance;
        type Interface = super::ffi::Interface;
        type Prerequisites = (glib::Object,);

        fn interface_init(klass: &mut Self::Interface) {
            klass.ask_credentials = |_, _, _, _, _| unimplemented!();
        }
    }

    impl ServiceDelegate {}
}

glib::wrapper! {
    pub struct ServiceDelegate(ObjectInterface<iface::ServiceDelegate>);
}

/// The `ServiceDelegateExt` trait contains public methods of all [`ServiceDelegate`] objects
///
/// These methods need to call the appropriate vfunc from the vtable.
pub trait ServiceDelegateExt: IsA<ServiceDelegate> {
    async fn ask_credentials(
        &self,
        service_id: account::ServiceId,
        previous: Option<account::Secret>,
        methods: EnumSet<account::AuthMethod>,
        message: &str,
    ) -> Option<account::Secret> {
        let this = self.upcast_ref::<ServiceDelegate>();
        let class = this.interface::<ServiceDelegate>().unwrap();
        (class.as_ref().ask_credentials)(this, service_id, previous, methods, message).await
    }
}

impl<T: IsA<ServiceDelegate>> ServiceDelegateExt for T {}

/// The `ServiceDelegateImpl` trait contains virtual function definitions for [`ServiceDelegate`] objects.
pub trait ServiceDelegateImpl: ObjectImpl {
    async fn ask_credentials(
        &self,
        service_id: account::ServiceId,
        previous: Option<account::Secret>,
        methods: EnumSet<account::AuthMethod>,
        message: &str,
    ) -> Option<account::Secret> {
        self.parent_ask_credentials(service_id, previous, methods, message)
            .await
    }
}

/// The `ServiceDelegateImplExt` trait contains non-overridable methods for subclasses to use.
///
/// These are supposed to be called only from inside implementations of `Pet` subclasses.
pub trait ServiceDelegateImplExt: ServiceDelegateImpl {
    async fn parent_ask_credentials(
        &self,
        service_id: account::ServiceId,
        previous: Option<account::Secret>,
        methods: EnumSet<account::AuthMethod>,
        message: &str,
    ) -> Option<account::Secret> {
        let data = Self::type_data();
        let parent_class = unsafe {
            &*(data.as_ref().parent_interface::<ServiceDelegate>() as *const ffi::Interface)
        };
        let ask_credentials = parent_class.ask_credentials;
        let obj = self.obj();

        ask_credentials(
            unsafe { obj.unsafe_cast_ref() },
            service_id,
            previous,
            methods,
            message,
        )
        .await
    }
}

/// The `ServiceDelegateImplExt` trait is implemented for all classes that implement [`ServiceDelegate`].
impl<T: ServiceDelegateImpl> ServiceDelegateImplExt for T {}

/// To make this interface implementable we need to implement [`IsImplementable`]
unsafe impl<Obj: ServiceDelegateImpl> IsImplementable<Obj> for ServiceDelegate {
    fn interface_init(iface: &mut glib::Interface<Self>) {
        let klass = iface.as_mut();

        // Set the virtual method
        klass.ask_credentials = |obj, service_id, previous, methods, message| {
            // (Down-)cast the object as the concrete type
            let this = unsafe { obj.unsafe_cast_ref::<<Obj as ObjectSubclass>::Type>().imp() };
            // Call the trait method
            Box::pin(ServiceDelegateImpl::ask_credentials(
                this, service_id, previous, methods, message,
            ))
        };
    }
}
