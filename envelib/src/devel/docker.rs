// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::net::IpAddr;
use std::{collections::HashMap, io::Write};

use bollard::image::CreateImageOptions;
use crossterm::queue;
use futures::StreamExt;

pub const TEST_SERVER_PORTS: [u16; 6] = [3025, 3110, 3143, 3465, 3993, 3995];
const IMAGE_NAME: &str = "docker.io/greenmail/standalone:latest";
const CONTAINER_NAME: &str = "envelope_test_server";

fn docker() -> Result<bollard::Docker, bollard::errors::Error> {
    if let Ok(docker) = bollard::Docker::connect_with_unix_defaults() {
        return Ok(docker);
    }

    bollard::Docker::connect_with_unix("/run/docker.sock", 10, bollard::API_DEFAULT_VERSION)
}

#[expect(dead_code)]
pub struct ContainerInfo {
    ip_address: Option<IpAddr>,
}

#[derive(thiserror::Error, Debug)]
#[error("Error when launching docker container: {0}")]
pub enum Error {
    Docker(#[from] bollard::errors::Error),
    Io(#[from] std::io::Error),
}

async fn handle_progress_stdout(
    stream: impl futures::Stream<
        Item = Result<bollard::models::CreateImageInfo, bollard::errors::Error>,
    >,
) -> Result<(), Error> {
    let mut stdout = std::io::stdout();
    let mut lines = Vec::new();

    let mut stream = std::pin::pin!(stream);

    while let Some(info) = stream.next().await {
        let info = info?;
        let Some(id) = info.id else {
            continue;
        };

        let line = if let Some(line) = lines.iter().position(|s| *s == id) {
            line
        } else {
            lines.push(id.clone());
            queue!(stdout, crossterm::style::Print("\n"))?;
            lines.len() - 1
        };

        // Move back to the respective line
        let up = lines.len().saturating_sub(1).saturating_sub(line);
        queue!(stdout, crossterm::cursor::SavePosition)?;
        if up > 0 {
            queue!(stdout, crossterm::cursor::MoveUp(up as u16))?;
        }

        queue!(stdout, crossterm::cursor::MoveToColumn(0))?;
        queue!(
            stdout,
            crossterm::terminal::Clear(crossterm::terminal::ClearType::CurrentLine)
        )?;

        if let Some(status) = info.status {
            if let Some(progress) = info.progress {
                queue!(
                    stdout,
                    crossterm::style::Print(format!("{}: {} {}\n", id, status, progress))
                )?;
            } else {
                queue!(
                    stdout,
                    crossterm::style::Print(format!("{}: {}\n", id, status))
                )?;
            }
        }

        queue!(stdout, crossterm::cursor::RestorePosition)?;
        std::io::stdout().flush()?;
    }

    Ok(())
}

pub async fn start(bind_host_ports: bool, progress_stdout: bool) -> Result<(), Error> {
    let docker = docker()?;

    println!("Starting greenmail server container");
    let stream = docker.create_image(
        Some(CreateImageOptions {
            from_image: IMAGE_NAME,
            ..Default::default()
        }),
        None,
        None,
    );

    if progress_stdout {
        handle_progress_stdout(stream).await?;
    }

    let ports = TEST_SERVER_PORTS
        .iter()
        .map(|port| (port.to_string(), HashMap::new()))
        .collect();

    let port_bindings: Option<HashMap<_, _>> = bind_host_ports.then_some(
        TEST_SERVER_PORTS
            .iter()
            .map(|port| {
                (
                    port.to_string(),
                    Some(vec![bollard::models::PortBinding {
                        host_ip: Some("127.0.0.1".to_string()),
                        host_port: Some(port.to_string()),
                    }]),
                )
            })
            .collect(),
    );

    // Try to remove a previous container
    if docker
        .remove_container(
            CONTAINER_NAME,
            Some(bollard::container::RemoveContainerOptions {
                v: true,
                force: true,
                link: false,
            }),
        )
        .await
        .is_ok()
    {
        log::debug!("Removed previous lingering test server container");
    };

    docker.create_container(Some(bollard::container::CreateContainerOptions {
        name: CONTAINER_NAME,
        ..Default::default()
    }), bollard::container::Config {
        image: Some(IMAGE_NAME.to_string()),
        exposed_ports: Some(ports),
        host_config: Some(bollard::models::HostConfig {
            port_bindings,
            ..Default::default()
        }),
        env: Some(vec![
            "GREENMAIL_OPTS=-Dgreenmail.setup.test.all -Dgreenmail.hostname=0.0.0.0 -Dgreenmail.auth.disabled -Dgreenmail.verbose".to_string(),
        ]),
        ..Default::default()
    }).await?;

    docker
        .start_container::<String>(CONTAINER_NAME, None)
        .await?;

    Ok(())
}

pub async fn info() -> Result<ContainerInfo, Error> {
    let docker = docker()?;
    let inspect = docker.inspect_container(CONTAINER_NAME, None).await?;

    let ip_address = inspect
        .network_settings
        .and_then(|n| n.ip_address)
        .and_then(|i| i.parse().ok());
    println!("Container IP address: {:?}", ip_address);

    Ok(ContainerInfo { ip_address })
}

pub async fn attach_stdout() -> Result<(), Error> {
    let docker = docker()?;

    let mut results = docker
        .attach_container::<String>(
            CONTAINER_NAME,
            Some(bollard::container::AttachContainerOptions {
                stdin: Some(true),
                stdout: Some(true),
                stderr: Some(true),
                stream: Some(true),
                logs: Some(true),
                detach_keys: None,
            }),
        )
        .await?;

    while let Some(line) = results.output.next().await {
        let line = line.unwrap();
        std::io::stdout().write_all(line.as_ref()).unwrap();
    }

    Ok(())
}

pub async fn stop() -> Result<(), Error> {
    let docker = docker()?;

    println!("Stopping container");

    // For some reason greenmail does not quit properly when receiving SIGTERM.
    // Therefore we set the timeout to 0 to immediately kill the container
    let res = docker
        .stop_container(
            CONTAINER_NAME,
            Some(bollard::container::StopContainerOptions { t: 0 }),
        )
        .await;

    if let Err(err) = res {
        if !matches!(err, bollard::errors::Error::DockerResponseServerError { status_code, .. } if status_code == 404)
        {
            return Err(err.into());
        }
    }

    println!("Removing Container");
    let res = docker
        .remove_container(
            CONTAINER_NAME,
            Some(bollard::container::RemoveContainerOptions {
                force: true,
                ..Default::default()
            }),
        )
        .await;

    if let Err(err) = res {
        if !matches!(err, bollard::errors::Error::DockerResponseServerError { status_code, .. } if status_code == 404)
        {
            return Err(err.into());
        }
    }

    Ok(())
}
