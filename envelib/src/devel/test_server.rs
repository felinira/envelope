// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use super::docker;

use lettre::Transport as _;

static TOKIO: std::sync::LazyLock<tokio::runtime::Runtime> =
    std::sync::LazyLock::new(|| tokio::runtime::Runtime::new().unwrap());

pub fn start(
    bind_host_ports: bool,
    progress_stdout: bool,
) -> tokio::task::JoinHandle<crate::Result<docker::ContainerInfo>> {
    TOKIO.spawn(async move {
        if !test_connection() {
            docker::start(bind_host_ports, progress_stdout).await?;
            wait_for_server().await?;
        }

        let info = docker::info().await?;
        Ok(info)
    })
}

pub fn attach_stdout() -> tokio::task::JoinHandle<crate::Result<()>> {
    TOKIO.spawn(async move { Ok(docker::attach_stdout().await?) })
}

pub fn stop() -> tokio::task::JoinHandle<crate::Result<()>> {
    TOKIO.spawn(async move { Ok(docker::stop().await?) })
}

fn mailer() -> lettre::SmtpTransport {
    lettre::SmtpTransport::builder_dangerous("127.0.0.1")
        .port(3025)
        .build()
}

pub fn test_connection() -> bool {
    let mailer = mailer();
    mailer.test_connection().is_ok_and(|c| c)
}

async fn wait_for_server() -> crate::Result<()> {
    println!("Waiting for the server to be ready");

    while !test_connection() {
        tokio::time::sleep(std::time::Duration::from_millis(200)).await
    }

    Ok(())
}

pub fn send_emails() -> tokio::task::JoinHandle<crate::Result<()>> {
    TOKIO.spawn(async move {
        wait_for_server().await?;

        println!("Connected");
        let mailer = mailer();

        loop {
            println!("Sending an email");
            let email = lettre::Message::builder()
                .from("Me <test_server_sender@envelope.test>".parse().unwrap())
                .to("app_receiver@envelope.test".parse().unwrap())
                .subject("Fwd: SMTP TEST")
                .header(lettre::message::header::ContentType::TEXT_PLAIN)
                .body(String::from("Message content here"))
                .unwrap();

            match mailer.send(&email) {
                Ok(_) => println!("New email added"),
                Err(e) => println!("Could not send email: {e:?}"),
            }

            tokio::time::sleep(std::time::Duration::from_secs(10)).await;
        }
    })
}
