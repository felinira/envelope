// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#[derive(Default, Clone, Debug, PartialEq, PartialOrd, Eq, Ord, glib::Boxed)]
#[boxed_type(name = "EnvelopeConnectionStatus")]
pub enum ConnectionStatus {
    /// Disconnected (Initial state / manually disconnected)
    #[default]
    Disconnected,
    /// Trying to establish a connection
    Connecting,
    /// TCP socket is open
    Connected,
    /// Network disconnected, server unreachable
    Unreachable,
    /// Server is reachable, but connecting to it caused an error
    Error(String),
    /// Authentication error
    AuthenticationFailed(String),
    /// Error in establishing a TLS connection
    TlsError(String),
}

impl ConnectionStatus {
    pub fn can_schedule_tasks(&self) -> bool {
        matches!(
            self,
            ConnectionStatus::Connecting | ConnectionStatus::Connected
        )
    }

    pub fn is_connected(&self) -> bool {
        matches!(self, ConnectionStatus::Connected)
    }

    pub fn is_disconnected(&self) -> bool {
        matches!(
            self,
            ConnectionStatus::Disconnected
                | ConnectionStatus::Unreachable
                | ConnectionStatus::Error(_)
                | ConnectionStatus::AuthenticationFailed(_)
                | ConnectionStatus::TlsError(_)
        )
    }

    pub fn is_error(&self) -> bool {
        matches!(
            self,
            ConnectionStatus::Error(_)
                | ConnectionStatus::TlsError(_)
                | ConnectionStatus::Unreachable
                | ConnectionStatus::AuthenticationFailed(_)
        )
    }
}
