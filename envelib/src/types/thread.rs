// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::fmt::Display;

#[derive(
    Debug,
    Default,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    glib::ValueDelegate,
    sqlx::Type,
)]
#[sqlx(transparent)]
pub struct ThreadId(i64);

impl ThreadId {
    pub fn new(id: i64) -> Self {
        Self(id)
    }
}

impl From<i64> for ThreadId {
    fn from(value: i64) -> Self {
        ThreadId(value)
    }
}

impl AsRef<i64> for ThreadId {
    fn as_ref(&self) -> &i64 {
        &self.0
    }
}

impl Display for ThreadId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}
