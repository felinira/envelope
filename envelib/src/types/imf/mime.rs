// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

pub mod attachment;

pub(crate) trait ContentTypeToString {
    fn to_string(&self) -> String;
}

impl ContentTypeToString for mail_parser::ContentType<'_> {
    fn to_string(&self) -> String {
        if let Some(subtype) = &self.c_subtype {
            format!("{}/{}", self.c_type, subtype)
        } else {
            self.c_type.to_string()
        }
    }
}
