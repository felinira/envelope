// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::{borrow::Borrow, sync::Arc};

#[derive(Debug)]
pub struct MimeAttachment {
    pub message: Arc<mail_parser::Message<'static>>,
    pub part_id: mail_parser::MessagePartId,
    pub filename: Option<String>,
    pub encoding: mail_parser::Encoding,
    pub content_type: String,
}

impl MimeAttachment {
    pub fn data(&self) -> Option<&[u8]> {
        self.message
            .part(self.part_id)
            .and_then(|part| match &part.body {
                mail_parser::PartType::Text(_) => None,
                mail_parser::PartType::Html(_) => None,
                mail_parser::PartType::Binary(data) => Some(data.borrow()),
                mail_parser::PartType::InlineBinary(data) => Some(data.borrow()),
                mail_parser::PartType::Message(_) => None,
                mail_parser::PartType::Multipart(_) => None,
            })
    }
}
