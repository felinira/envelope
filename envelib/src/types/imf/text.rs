// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::borrow::Cow;

use crate::gettext::*;

#[derive(Debug, Clone)]
pub struct TextPart<'a> {
    pub(super) text: Cow<'a, str>,
    pub(super) kind: TextKind,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum TextKind {
    Text,
    Html,
}

impl<'a> TextPart<'a> {
    pub fn text(text: Cow<'a, str>) -> Self {
        TextPart {
            text,
            kind: TextKind::Text,
        }
    }

    pub fn html(text: Cow<'a, str>) -> Self {
        TextPart {
            text,
            kind: TextKind::Html,
        }
    }

    pub fn kind(&self) -> TextKind {
        self.kind
    }

    pub fn into_owned(self) -> TextPart<'static> {
        TextPart {
            text: Cow::from(self.text.into_owned()),
            kind: self.kind,
        }
    }

    pub fn push_body_part(&mut self, other: &TextPart) {
        match (&self.kind, other.kind) {
            (TextKind::Text, TextKind::Text) => {
                // Combine text and text -> just concatenate them with a newline
                self.text = Cow::from(format!("{}\n{}", self.text, other.text));
            }
            _ => {
                // Convert both parts to HTML and concatenate
                let html1 = self.to_html_string();
                let html2 = other.to_html_string();

                // Combine two HTML bodies - let webkit deal with it and just concatenate them
                self.text = Cow::Owned(format!("{html1}{html2}"));
                self.kind = TextKind::Html;
            }
        }
    }

    pub fn to_html_string(&self) -> String {
        match self.kind {
            TextKind::Html => self.text.clone().into_owned(),
            TextKind::Text => {
                if self.text.trim().is_empty() {
                    let missing_body_str = gettext("This Message has no content");
                    format!(
                        r#"<html class="email-plain-text"><body><div><p><i>{missing_body_str}</i></p></div></body></html>"#
                    )
                } else {
                    let link_finder = linkify::LinkFinder::new();
                    let text = link_finder
                        .spans(&self.text)
                        .map(|chunk| match chunk.kind() {
                            Some(kind) => match kind {
                                linkify::LinkKind::Url => {
                                    format!(
                                        r#"<a href="{}">{}</a>"#,
                                        chunk.as_str(),
                                        chunk.as_str()
                                    )
                                }
                                linkify::LinkKind::Email => format!(
                                    r#"<a href="mailto:{}">{}</a>"#,
                                    chunk.as_str(),
                                    chunk.as_str()
                                ),
                                _ => chunk.as_str().to_owned(),
                            },
                            None => chunk.as_str().to_owned(),
                        })
                        .collect::<String>();

                    let mut html_text = String::new();
                    let mut quote_level = 0;
                    let mut quote_block_count = 0;

                    for line in text.lines() {
                        if line.starts_with('>') && quote_level == 0 {
                            let expand_quote_str = gettext("Quoted Message");
                            html_text.push_str(&format!(
                            r#"<div class="quote"><details><summary><span>{expand_quote_str}</span></summary>"#
                        ));
                            quote_block_count += 1;
                        } else if !line.starts_with('>') && quote_level > 0 {
                            html_text.push_str("</details></div>");
                            quote_level = 0;
                        }

                        let new_line = line.trim_start_matches('>');
                        let new_quote_level = line.len() - new_line.len();

                        while new_quote_level > quote_level {
                            html_text.push_str(r#"<blockquote>"#);
                            quote_level += 1;
                        }

                        while new_quote_level < quote_level {
                            html_text.push_str(r#"</blockquote>"#);
                            quote_level -= 1;
                        }

                        //let escaped_line = html_escape::encode_safe(new_line);
                        let escaped_line = new_line;
                        html_text.push_str(&format!(r#"<p>{escaped_line}</p>"#));
                    }

                    // Open all details tags except for the last one if the email ended with it (which is presumably a fullquote)
                    if quote_block_count > 0 {
                        let count = if quote_level > 0 {
                            quote_block_count - 1
                        } else {
                            quote_block_count
                        };

                        // TODO this is a hack
                        html_text = html_text.replacen("<details>", "<details open>", count);
                    }

                    format!(
                        r#"<html class="email-plain-text"><body><div style="word-wrap: break-word; white-space: pre-wrap;">{html_text}</div></body></html>"#
                    )
                }
            }
        }
    }

    pub fn content_type(&self) -> &str {
        match self.kind {
            TextKind::Text => "text/plain",
            TextKind::Html => "text/html",
        }
    }
}

impl<'a> FromIterator<TextPart<'a>> for Option<TextPart<'a>> {
    fn from_iter<T: IntoIterator<Item = TextPart<'a>>>(iter: T) -> Self {
        iter.into_iter().reduce(|mut acc, next| {
            acc.push_body_part(&next);
            acc
        })
    }
}

impl AsRef<str> for TextPart<'_> {
    fn as_ref(&self) -> &str {
        &self.text
    }
}
