// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::str::FromStr;

use crate::utils::StrExt;

/// Normalizes `Message-ID` and `Content-ID` string.
fn normalize_id(mut id: &str) -> String {
    // We need to normalize the message id
    let mut new_id = String::new();
    let mut quoted = false;
    let mut escape = false;

    // Ensure no angle brackets
    if id.starts_with('<') && id.ends_with('>') {
        id = &id[1..id.len() - 1];
    }

    // Remove quotes, unless they are escaped
    for char in id.chars() {
        if !escape && char == '\"' {
            // skip quotes
            quoted = !quoted;
            continue;
        }

        if escape {
            escape = false;
        } else if quoted && char == '\\' {
            escape = true;
        }

        new_id.push(char);
    }

    // Ensure no nul byte in the string
    new_id.ensure_no_nul()
}

/// A message id from the Message-ID header field
///
/// ## Relevant grammar from RFC 2822
///
/// ```text
/// msg-id = [CFWS] "<" id-left "@" id-right ">" [CFWS]
/// id-left = dot-atom-text / no-fold-quote / obs-id-left
/// id-right = dot-atom-text / no-fold-literal / obs-id-right
///
/// obs-id-left = dot-atom / quoted-string / obs-local-part
/// obs-local-part = word *("." word)
///
/// obs-id-right = domain
/// domain = dot-atom / domain-literal / obs-domain
/// domain-literal = [CFWS] "[" *([FWS] dcontent) [FWS] "]" [CFWS]
/// obs-domain = atom *("." atom)
///
/// no-fold-literal = "[" *(dtext / quoted-pair) "]"
/// no-fold-quote = DQUOTE *(qtext / quoted-pair) DQUOTE
/// qtext = NO-WS-CTL /     ; Non white space controls
///         %d33 /          ; The rest of the US-ASCII
///         %d35-91 /       ;  characters not including "\"
///         %d93-126        ;  or the quote character
///
/// atom = [CFWS] 1*atext [CFWS]
/// dot-atom-text = 1*atext *("." 1*atext)
/// atext = ALPHA / DIGIT / "!" / "#" / "$" / "%" / "&" / "'" /
///   "*" / "+" / "-" / "/" / "=" / "?" / "^" / "_" / "`" / "{" /
///   "|" / "}" / "~"
/// dtext = NO-WS-CTL /     ; Non white space controls
///         %d33-90 /       ; The rest of the US-ASCII
///         %d94-126        ;  characters not including "[",
///                         ;  "]", or "\"
/// ```
///
/// ## Implementation
///
/// In practise we can mostly treat them as verbatim strings. We do need to normalize
/// them however. This grammar makes this very difficult to achieve.
///
/// - Remove any angle brackets
/// - Find quotes and then remove them. The quotes will only appear
///   between the beginning of the `Message-Id` and before the @ character.
/// - Replace any `\0` bytes from the string and replace them with `\u{FFFD}` as those are incompatible with glib
///
/// From [RFC 5256](https://www.rfc-editor.org/rfc/rfc5256#section-3)
///
/// > ```text
/// > For example, the msg-id
/// >    <"01KF8JCEOCBS0045PS"@xxx.yyy.com>
/// > and the msg-id
/// >    <01KF8JCEOCBS0045PS@xxx.yyy.com>
/// > MUST be interpreted as being the same Message ID.
/// > ```
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, glib::ValueDelegate, sqlx::Type)]
#[value_delegate(nullable)]
#[sqlx(transparent)]
pub struct MessageId(String);

impl AsRef<str> for MessageId {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<&str> for MessageId {
    /// Normalizes `Message-ID` string.
    fn from(value: &str) -> Self {
        // We need to normalize the message id
        Self(normalize_id(value))
    }
}

impl From<String> for MessageId {
    fn from(value: String) -> Self {
        Self::from(value.as_ref())
    }
}

impl std::fmt::Display for MessageId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Default for MessageId {
    fn default() -> Self {
        let uuid = uuid::Uuid::new_v4();
        MessageId(format!("<{}@app.drey.Envelope.MessageIdRandom>", uuid))
    }
}

/// A MIME Content-ID
///
/// This is very similar to a Message-ID for all intents and purposes, but identifies a MIME
/// message part.
///
/// See [`MessageId`] for a detailed description of the format.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, glib::ValueDelegate, sqlx::Type)]
#[value_delegate(nullable)]
#[sqlx(transparent)]
pub struct ContentId(String);

impl AsRef<str> for ContentId {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<&str> for ContentId {
    /// Normalizes `Content-ID` string.
    fn from(value: &str) -> Self {
        // We need to normalize the content-id
        Self(normalize_id(value))
    }
}

impl From<String> for ContentId {
    fn from(value: String) -> Self {
        Self::from(value.as_ref())
    }
}

impl std::fmt::Display for ContentId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// An mid: URI as defined in [RFC 2392](https://www.rfc-editor.org/rfc/rfc2392)
pub struct MidCidUri(Option<MessageId>, Option<ContentId>);

impl MidCidUri {
    const MID_SCHEME: &fluent_uri::component::Scheme =
        fluent_uri::component::Scheme::new("mid").unwrap();
    const CID_SCHEME: &fluent_uri::component::Scheme =
        fluent_uri::component::Scheme::new("cid").unwrap();

    pub fn mid_cid(message_id: MessageId, content_id: ContentId) -> Self {
        Self(Some(message_id), Some(content_id))
    }

    pub fn mid(message_id: MessageId) -> Self {
        Self(Some(message_id), None)
    }

    pub fn cid(content_id: ContentId) -> Self {
        Self(None, Some(content_id))
    }

    pub fn scheme(&self) -> &str {
        if self.0.is_some() { "mid" } else { "cid" }
    }

    pub fn message_id(&self) -> Option<&MessageId> {
        self.0.as_ref()
    }

    pub fn content_id(&self) -> Option<&ContentId> {
        self.1.as_ref()
    }
}

impl fluent_uri::encoding::Encoder for MidCidUri {
    // These characters are also allowed according to RFC 2111
    const TABLE: &'static fluent_uri::encoding::Table =
        &fluent_uri::encoding::encoder::Data::TABLE.or(&fluent_uri::encoding::Table::new(b"@*"));
}

impl std::fmt::Display for MidCidUri {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut path_str = fluent_uri::encoding::EString::new();

        if let Some(message_id) = &self.0 {
            path_str.encode::<Self>(message_id.as_ref());
        }

        if let Some(content_id) = &self.1 {
            if !path_str.is_empty() {
                path_str.push('/');
            }

            path_str.encode::<Self>(content_id.as_ref());
        }

        let uri = fluent_uri::Uri::builder()
            .scheme(if self.0.is_some() {
                Self::MID_SCHEME
            } else {
                Self::CID_SCHEME
            })
            .path(&path_str)
            .build()
            .unwrap_or_else(|_| {
                panic!(
                    "Error URI encoding message ID {:?} and content ID {:?}",
                    self.message_id(),
                    self.content_id()
                )
            });

        write!(f, "{uri}")
    }
}

#[derive(Debug, thiserror::Error)]
pub enum MidCidUriParseError {
    #[error("The provided URI is not an mid: or cid: URI")]
    InvalidScheme,
    #[error(
        "An mid: URI needs to have at least one and a maximum of 2 path segments. A cid: URI is required to have exactly one path segment"
    )]
    InvalidPathSegmentCount,
    #[error("Error parsing mid: or cid: URI: {0}")]
    Url(#[from] fluent_uri::error::ParseError),
    #[error("Error decoding UTF8")]
    Utf8(#[from] std::string::FromUtf8Error),
}

impl FromStr for MidCidUri {
    type Err = MidCidUriParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let url = fluent_uri::Uri::parse(s)?;
        let is_mid = url.scheme() == Self::MID_SCHEME;
        let is_cid = url.scheme() == Self::CID_SCHEME;
        if !is_mid && !is_cid {
            return Err(MidCidUriParseError::InvalidScheme);
        }

        let mut segments = url.path().split('/');
        let message_id = if is_mid {
            // message-id is required for mid URIs
            Some(MessageId::from(
                segments
                    .next()
                    .ok_or(MidCidUriParseError::InvalidPathSegmentCount)?
                    .decode()
                    .into_string()?
                    .into_owned(),
            ))
        } else {
            // message-id doesn't exist for cid URIs
            None
        };

        let content_id = segments
            .next()
            .map(|s| s.decode().into_string())
            .transpose()?
            .map(|cid| ContentId::from(&*cid));

        if is_cid && content_id.is_none() {
            // content-id is required for cid URIs
            return Err(MidCidUriParseError::InvalidPathSegmentCount);
        };

        Ok(Self(message_id, content_id))
    }
}

#[cfg(test)]
mod tests {
    pub use super::*;

    #[test]
    fn test_msg_id_normalize() {
        // Regular quoted case
        assert_eq!(
            MessageId::from(r#"<"01KF8JCEOCBS0045PS"@xxx.yyy.com>"#),
            MessageId::from(r#"<01KF8JCEOCBS0045PS@xxx.yyy.com>"#)
        );

        // Chars longer than 1 byte
        assert_eq!(
            MessageId::from(r#"<"プログラマーズ"@xxx.yyy.com>"#),
            MessageId::from(r#"<プログラマーズ@xxx.yyy.com>"#)
        );

        // Escaped characters (unsure whether this is legal)
        assert_eq!(
            MessageId::from(r#"<"01KF8J\@CEOCB\"S0045PS"@[xxx@yyy.com]>"#),
            MessageId::from(r#"<01KF8J\@CEOCB"\""S0045PS@[xxx@yyy.com]>"#)
        );

        // Remove angle brackets
        assert_eq!(
            MessageId::from(r#"<foo4%foo1@bar.net>"#).as_ref(),
            "foo4%foo1@bar.net"
        );
    }

    #[test]
    fn mid_cid_uri() {
        // https://www.rfc-editor.org/rfc/rfc2392
        let cid = ContentId::from("<foo4%foo1@bar.net>");
        let uri = MidCidUri::cid(cid);
        assert_eq!(&uri.to_string(), "cid:foo4%25foo1@bar.net");
        assert_eq!(
            MidCidUri::from_str(&uri.to_string())
                .unwrap()
                .content_id()
                .unwrap()
                .as_ref(),
            "foo4%foo1@bar.net"
        );
        assert_eq!(uri.content_id().unwrap().as_ref(), "foo4%foo1@bar.net");

        let cid = ContentId::from("foo4*foo1@bar.net");
        let uri = MidCidUri::cid(cid);
        assert_eq!(&uri.to_string(), "cid:foo4*foo1@bar.net");
        assert_eq!(uri.content_id().unwrap().as_ref(), "foo4*foo1@bar.net");

        let mid = MessageId::from("<960830.1639@XIson.com>");
        let cid = ContentId::from("partA.960830.1639@XIson.com");
        let uri = MidCidUri::mid_cid(mid, cid);
        assert_eq!(
            &uri.to_string(),
            "mid:960830.1639@XIson.com/partA.960830.1639@XIson.com"
        );
        assert_eq!(uri.message_id().unwrap().as_ref(), "960830.1639@XIson.com");
        assert_eq!(
            uri.content_id().unwrap().as_ref(),
            "partA.960830.1639@XIson.com"
        );
    }
}
