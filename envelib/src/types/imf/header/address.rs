// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use serde::{Deserialize, Serialize};

use crate::utils::StrExt;

#[derive(Debug, Clone, Eq, Serialize, Deserialize)]
pub struct Address {
    name: Option<String>,
    address: String,
}

impl PartialEq for Address {
    fn eq(&self, other: &Self) -> bool {
        self.address == other.address
    }
}

impl std::hash::Hash for Address {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.address.hash(state);
    }
}

impl std::fmt::Display for Address {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(name) = &self.name {
            write!(f, "{} <{}>", name, self.address)
        } else {
            write!(f, "{}", self.address)
        }
    }
}

impl Address {
    pub fn new(name: Option<impl ToString>, address: impl ToString) -> Self {
        Self {
            name: name.map(|n| n.to_string()),
            address: address.to_string(),
        }
    }

    pub fn name(&self) -> Option<&str> {
        self.name.as_deref()
    }

    pub fn address(&self) -> &str {
        &self.address
    }

    // Formats either the name or the address as fallback
    pub fn format_short(&self) -> &str {
        if let Some(name) = &self.name {
            name
        } else {
            &self.address
        }
    }
}

impl TryFrom<&mail_parser::Addr<'_>> for Address {
    type Error = ();

    fn try_from(value: &mail_parser::Addr) -> Result<Self, Self::Error> {
        if let Some(addr) = &value.address {
            Ok(Self {
                name: value
                    .name
                    .as_deref()
                    .map(str::trim)
                    .map(StrExt::unquote)
                    .map(StrExt::ensure_no_nul),
                address: addr.trim().ensure_no_nul().to_ascii_lowercase(),
            })
        } else {
            Err(())
        }
    }
}

#[derive(Clone, Default, Debug, PartialEq, Eq, glib::Boxed, Deserialize, Serialize)]
#[boxed_type(name = "EnvelopeParticipants")]
pub struct Participants {
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub from: Vec<Address>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub to: Vec<Address>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub cc: Vec<Address>,
    #[serde(default, skip_serializing_if = "Vec::is_empty")]
    pub bcc: Vec<Address>,
}

impl From<&mail_parser::Message<'_>> for Participants {
    fn from(value: &mail_parser::Message) -> Self {
        let parse_header = |addresses: Option<&mail_parser::Address>| {
            let mut addrs = Vec::new();
            match addresses {
                Some(mail_parser::Address::Group(groups)) => {
                    for group in groups {
                        for addr in &group.addresses {
                            if let Ok(addr) = Address::try_from(addr) {
                                addrs.push(addr)
                            }
                        }
                    }
                }
                Some(mail_parser::Address::List(list)) => {
                    for addr in list {
                        if let Ok(addr) = Address::try_from(addr) {
                            addrs.push(addr)
                        }
                    }
                }
                None => {}
            }

            addrs
        };

        Participants {
            from: parse_header(value.from()),
            to: parse_header(value.to()),
            cc: parse_header(value.cc()),
            bcc: parse_header(value.bcc()),
        }
    }
}
