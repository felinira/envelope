// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod sync_mode;

use std::fmt::Display;

use crate::gettext::*;
pub use sync_mode::*;

#[derive(
    Debug,
    Default,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    glib::ValueDelegate,
    sqlx::Type,
)]
#[sqlx(transparent)]
pub struct MailboxId(i64);

impl MailboxId {
    pub const NONE: Self = Self(0);

    pub fn new(value: i64) -> Self {
        Self(value)
    }
}

impl From<i64> for MailboxId {
    fn from(value: i64) -> Self {
        MailboxId(value)
    }
}

impl AsRef<i64> for MailboxId {
    fn as_ref(&self) -> &i64 {
        &self.0
    }
}

impl Display for MailboxId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

#[derive(
    Debug, sqlx::Type, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, glib::Enum, Hash,
)]
#[enum_type(name = "EnvelopeMailboxKind")]
#[sqlx(rename_all = "lowercase")]
pub enum MailboxKind {
    Inbox,
    Drafts,
    Sent,
    Junk,
    Trash,
    Outbox,
    All,
    Archive,

    #[default]
    #[sqlx(rename = "")]
    Regular,
}

impl MailboxKind {
    pub fn icon_name(&self) -> &'static str {
        match self {
            MailboxKind::Inbox => "inbox-symbolic",
            MailboxKind::Drafts => "drafts-symbolic",
            MailboxKind::Sent => "sent-symbolic",
            MailboxKind::Junk => "junk-symbolic",
            MailboxKind::Trash => "user-trash-symbolic",
            MailboxKind::Outbox => "outbox-symbolic",
            MailboxKind::All => "archive-symbolic",
            MailboxKind::Archive => "archive-symbolic",
            MailboxKind::Regular => "labels-symbolic",
        }
    }

    pub fn is_below_separator(&self) -> bool {
        self == &MailboxKind::Regular
    }

    pub fn is_drafts(&self) -> bool {
        self == &MailboxKind::Drafts
    }

    pub fn localized_name(&self) -> Option<String> {
        match self {
            MailboxKind::Inbox => Some(gettext("Inbox")),
            MailboxKind::Drafts => Some(gettext("Drafts")),
            MailboxKind::Sent => Some(gettext("Sent")),
            MailboxKind::Junk => Some(gettext("Junk")),
            MailboxKind::Trash => Some(gettext("Trash")),
            MailboxKind::Outbox => Some(gettext("Outbox")),
            MailboxKind::All => Some(gettext("All Mail")),
            MailboxKind::Archive => Some(gettext("Archive")),
            MailboxKind::Regular => None,
        }
    }
}
