// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

pub mod header;
pub mod mime;
pub mod text;

pub use header::address::Address;
pub use header::address::Participants;
use header::id::ContentId;
pub use header::id::MessageId;
use mail_parser::HeaderName;
pub use mime::attachment::MimeAttachment;
use text::TextKind;
pub use text::TextPart;

use mail_parser::MimeHeaders;
use std::borrow::Cow;
use std::fmt::Display;
use std::sync::Arc;

use crate::{imap, utils::*};

use self::mime::ContentTypeToString;

/// The Internet Message Format (IMF)
///
/// See [RFC 5322](https://www.rfc-editor.org/rfc/rfc5322#section-2.3)
#[derive(Debug, Clone)]
pub struct InternetMessage {
    inner: Arc<mail_parser::Message<'static>>,
    full: bool,
}

impl InternetMessage {
    pub fn new(body: mail_parser::Message<'static>, full: bool) -> Self {
        Self {
            inner: Arc::new(body),
            full,
        }
    }

    pub fn inner(&self) -> Arc<mail_parser::Message<'static>> {
        self.inner.clone()
    }

    pub fn is_full_message(&self) -> bool {
        self.full
    }

    pub fn has_body(&self) -> bool {
        !self.inner.text_body.is_empty() || !self.inner.html_body.is_empty()
    }

    pub fn parse_subject(&self) -> String {
        self.inner.subject().unwrap_or_default().ensure_no_nul()
    }

    pub fn parse_participants(&self) -> Participants {
        Participants::from(&*self.inner)
    }

    pub fn parse_thread_name(&self) -> String {
        self.inner.thread_name().unwrap_or_default().ensure_no_nul()
    }

    pub fn parse_date(&self) -> Option<time::OffsetDateTime> {
        self.inner
            .date()
            .and_then(|date| imap::DateTime::try_from(date).ok())
            .map(|date| date.into_inner())
    }

    /// For cases where a message id is not set, generate one by hashing a few common header fields
    fn generate_message_id(&self) -> MessageId {
        let mut checksum = glib::Checksum::new(glib::ChecksumType::Sha256).unwrap();
        for header in [
            HeaderName::Date,
            HeaderName::From,
            HeaderName::Sender,
            HeaderName::ReplyTo,
            HeaderName::To,
            HeaderName::Cc,
            HeaderName::Bcc,
            HeaderName::InReplyTo,
            HeaderName::References,
            HeaderName::Subject,
        ] {
            self.inner
                .header_raw(header)
                .inspect(|d| checksum.update(d.as_bytes()));
        }

        MessageId::from(format!(
            "<{}@app.drey.Envelope.MessageId>",
            checksum.string().unwrap()
        ))
    }

    pub fn parse_message_id(&self) -> MessageId {
        self.inner
            .message_id()
            .map(MessageId::from)
            .unwrap_or_else(|| self.generate_message_id())
    }

    pub fn parse_references(&self) -> Vec<MessageId> {
        self.inner
            .references()
            .as_text_list()
            .unwrap_or_default()
            .iter()
            .map(|id| MessageId::from(id.as_ref()))
            .collect()
    }

    pub fn parse_in_reply_to(&self) -> Option<MessageId> {
        self.inner.in_reply_to().as_text().map(MessageId::from)
    }

    pub fn parse_preview_text(&self) -> Option<String> {
        self.inner
            .body_preview(200)
            .map(|text| text.trim().replace('\r', "").replace('\n', " "))
            .filter(|text| !text.trim().is_empty())
    }

    pub fn inline_parts(&self) -> impl Iterator<Item = &mail_parser::MessagePart> {
        self.inner.parts.iter()
    }

    pub fn inline_bodies(&self) -> impl Iterator<Item = TextPart<'_>> + use<'_> {
        self.inner.html_bodies().filter_map(|body| {
            body.text_contents().map(|text| TextPart {
                text: Cow::Borrowed(text),
                kind: if body.is_text_html() {
                    TextKind::Html
                } else {
                    TextKind::Text
                },
            })
        })
    }

    pub fn html_body(&self) -> Option<TextPart<'_>> {
        self.inline_bodies()
            .collect::<Option<TextPart>>()
            .map(|bp| bp.into_owned())
    }

    pub fn html_body_owned(&self) -> Option<TextPart<'static>> {
        self.html_body().map(|b| b.into_owned())
    }

    pub fn embedded_messages(&self) -> impl Iterator<Item = InternetMessage> + use<'_> {
        self.inner.parts.iter().filter_map(|part| {
            part.message()
                .map(|msg| InternetMessage::new(msg.clone(), true))
        })
    }

    pub fn attachments(&self) -> impl Iterator<Item = MimeAttachment> + use<'_> {
        self.inner
            .attachments()
            .enumerate()
            .map(|(pos, part)| MimeAttachment {
                message: self.inner.clone(),
                part_id: self.inner.attachments[pos],
                filename: part.attachment_name().map(ToOwned::to_owned),
                encoding: part.encoding,
                content_type: part
                    .content_type()
                    .map(ContentTypeToString::to_string)
                    .unwrap_or_default(),
            })
    }

    /// Returns data and mime-type
    pub fn body_part_by_content_id(
        &self,
        content_id: &str,
    ) -> Option<(Cow<'_, [u8]>, Option<String>)> {
        self.inner
            .parts
            .iter()
            .find(|part| {
                if let Some(id) = part.content_id() {
                    id == content_id
                } else {
                    false
                }
            })
            .map(|part| {
                (
                    Cow::Borrowed(part.contents()),
                    part.content_type().map(ContentTypeToString::to_string),
                )
            })
    }
}

impl Display for InternetMessage {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            String::from_utf8_lossy(self.inner.raw_message()).into_owned()
        )
    }
}

impl AsRef<[u8]> for InternetMessage {
    fn as_ref(&self) -> &[u8] {
        &self.inner.raw_message
    }
}

pub(crate) trait MailParserExt {
    /// Returns a message by the specified Message-ID Header.
    ///
    /// This might be the root message, or it could be an embedded message hidden inside
    /// the MIME content of the message
    fn message_by_mid(&self, message_id: &MessageId) -> Option<&mail_parser::Message>;

    /// Returns a [`mail_parser::MessagePartId`] for the specified content ID.
    fn part_id_by_content_id(&self, content_id: &ContentId) -> Option<mail_parser::MessagePartId>;
}

impl MailParserExt for mail_parser::Message<'_> {
    /// Returns a message by the specified Message-ID Header.
    ///
    /// This might be the root message, or it could be an embedded message hidden inside
    /// the MIME content of the message
    fn message_by_mid(&self, message_id: &MessageId) -> Option<&mail_parser::Message> {
        if self
            .message_id()
            .is_some_and(|id| id == message_id.as_ref())
        {
            Some(self)
        } else {
            self.parts
                .iter()
                .find_map(|part: &mail_parser::MessagePart<'_>| {
                    if let Some(message) = part.message() {
                        if message.message_id() == Some(message_id.as_ref()) {
                            Some(message)
                        } else {
                            // Recurse into the nested message
                            message.message_by_mid(message_id)
                        }
                    } else {
                        None
                    }
                })
        }
    }

    /// Returns a [`mail_parser::MessagePartId`] for the specified content ID.
    fn part_id_by_content_id(&self, content_id: &ContentId) -> Option<mail_parser::MessagePartId> {
        self.parts.iter().enumerate().find_map(|(id, part)| {
            if part
                .content_id()
                .is_some_and(|cid| cid == content_id.as_ref())
            {
                Some(id)
            } else {
                None
            }
        })
    }
}

impl<DB: sqlx::Database> sqlx::Type<DB> for InternetMessage
where
    [u8]: sqlx::Type<DB>,
{
    fn type_info() -> <DB as sqlx::Database>::TypeInfo {
        <[u8] as sqlx::Type<DB>>::type_info()
    }
}

impl<'a, DB: sqlx::Database> sqlx::Decode<'a, DB> for InternetMessage
where
    &'a [u8]: sqlx::Decode<'a, DB>,
{
    fn decode(
        value: <DB as sqlx::Database>::ValueRef<'a>,
    ) -> Result<Self, sqlx::error::BoxDynError> {
        let bytes = <&'a [u8] as sqlx::Decode<DB>>::decode(value)?;
        let parser = mail_parser::MessageParser::default();
        let value = parser.parse(bytes).ok_or(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "Error decoding message from database",
        ))?;

        Ok(InternetMessage::new(value.into_owned(), true))
    }
}

impl<'q, DB: sqlx::Database> sqlx::Encode<'q, DB> for InternetMessage
where
    Vec<u8>: sqlx::Encode<'q, DB>,
{
    fn encode_by_ref(
        &self,
        buf: &mut <DB as sqlx::Database>::ArgumentBuffer<'q>,
    ) -> Result<sqlx::encode::IsNull, sqlx::error::BoxDynError> {
        <Vec<u8> as sqlx::Encode<'q, DB>>::encode(self.inner.raw_message.clone().to_vec(), buf)
    }
}
