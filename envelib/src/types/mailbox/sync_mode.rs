// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

/// The mode to use when resynchronizing a mailbox from a server.
///
/// Used when requesting a resynchronisation of a particular mailbox, eg. when switching to it in the user interface.-
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MailboxSyncMode {
    /// Only resynchronize flags
    FlagsOnly,
    /// Only check if there are new messages in this mailbox
    NewMessagesOnly,
    /// Sync all messages
    All,
}
