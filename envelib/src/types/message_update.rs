// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use super::*;
use crate::model;

#[derive(Debug, Clone, glib::Boxed)]
#[boxed_type(name = "EnvelopeMessageUpdate")]
pub struct MessageUpdate {
    pub new_messages: Vec<model::Message>,
    pub message_count: usize,
    pub unseen_count: usize,
    pub recent_count: usize,
}

impl MessageUpdate {
    pub fn new() -> Self {
        Self {
            new_messages: Vec::new(),
            message_count: 0,
            unseen_count: 0,
            recent_count: 0,
        }
    }

    pub fn set_messages(&mut self, messages: Vec<model::Message>) {
        self.new_messages = messages;
    }

    pub fn new_messages(&self) -> &[model::Message] {
        self.new_messages.as_slice()
    }

    pub fn notification_message(&self) -> Option<model::Message> {
        if self.message_count == 1 && self.new_messages.len() == 1 {
            self.new_messages.first().cloned()
        } else {
            None
        }
    }

    pub fn message_count(&self) -> usize {
        self.message_count
    }

    pub fn unseen_count(&self) -> usize {
        self.unseen_count
    }

    pub fn recent_count(&self) -> usize {
        self.recent_count
    }

    pub fn add_data<I: IntoIterator<Item = T>, T: Into<Self>>(&mut self, other: I) {
        other.into_iter().for_each(|update| {
            self.combine(update.into());
        });
    }

    pub fn combine(&mut self, other: Self) {
        self.message_count += other.message_count;
        self.unseen_count += other.unseen_count;
        self.recent_count += other.recent_count;

        self.new_messages.extend(other.new_messages)
    }
}

impl From<&MessageFlags> for MessageUpdate {
    fn from(value: &MessageFlags) -> Self {
        Self {
            new_messages: Vec::new(),
            message_count: 1,
            unseen_count: if value.is_unseen() { 1 } else { 0 },
            recent_count: if value.is_recent() { 1 } else { 0 },
        }
    }
}

impl<D: Into<MessageUpdate>> FromIterator<D> for MessageUpdate {
    fn from_iter<T: IntoIterator<Item = D>>(iter: T) -> Self {
        let mut update = MessageUpdate::new();
        update.add_data(iter);
        update
    }
}

impl Default for MessageUpdate {
    fn default() -> Self {
        Self::new()
    }
}
