// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::{borrow::Cow, collections::HashSet, fmt::Display};

use imap_client::imap_types::{self, IntoStatic, flag::StoreType};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum MessageFlag {
    Seen,
    Answered,
    Flagged,
    Deleted,
    Draft,
    Recent,
    Custom(String),
}

impl From<&str> for MessageFlag {
    fn from(value: &str) -> Self {
        match value {
            "\\Seen" => Self::Seen,
            "\\Answered" => Self::Answered,
            "\\Flagged" => Self::Flagged,
            "\\Deleted" => Self::Deleted,
            "\\Draft" => Self::Draft,
            "\\Recent" => Self::Recent,
            s => Self::Custom(s.to_string()),
        }
    }
}

impl From<&Cow<'_, str>> for MessageFlag {
    fn from(value: &Cow<'_, str>) -> Self {
        Self::from(value.as_ref())
    }
}

impl From<imap_types::flag::Flag<'_>> for MessageFlag {
    fn from(value: imap_types::flag::Flag<'_>) -> Self {
        match &value {
            imap_types::flag::Flag::Answered => Self::Answered,
            imap_types::flag::Flag::Deleted => Self::Deleted,
            imap_types::flag::Flag::Draft => Self::Draft,
            imap_types::flag::Flag::Flagged => Self::Flagged,
            imap_types::flag::Flag::Seen => Self::Seen,
            imap_types::flag::Flag::Extension(_) | imap_types::flag::Flag::Keyword(_) => {
                Self::Custom(value.to_string())
            }
        }
    }
}

impl From<MessageFlag> for imap_types::flag::Flag<'static> {
    fn from(value: MessageFlag) -> Self {
        match value {
            MessageFlag::Seen => Self::Seen,
            MessageFlag::Answered => Self::Answered,
            MessageFlag::Flagged => Self::Flagged,
            MessageFlag::Deleted => Self::Deleted,
            MessageFlag::Draft => Self::Draft,
            _ => imap_types::flag::Flag::try_from(&*value.to_string())
                .unwrap()
                .into_static(),
        }
    }
}

impl std::fmt::Display for MessageFlag {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "\\{}",
            match self {
                MessageFlag::Seen => "Seen",
                MessageFlag::Answered => "Answered",
                MessageFlag::Flagged => "Flagged",
                MessageFlag::Deleted => "Deleted",
                MessageFlag::Draft => "Draft",
                MessageFlag::Recent => "Recent",
                MessageFlag::Custom(flag) => flag.strip_prefix('\\').unwrap_or(flag),
            }
        )
    }
}

#[derive(Clone, Debug, Default, PartialEq, Eq)]
pub struct MessageFlags(HashSet<MessageFlag>);

impl std::ops::Deref for MessageFlags {
    type Target = HashSet<MessageFlag>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::ops::DerefMut for MessageFlags {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.0
    }
}

impl FromIterator<MessageFlag> for MessageFlags {
    fn from_iter<T: IntoIterator<Item = MessageFlag>>(iter: T) -> Self {
        Self(HashSet::from_iter(iter))
    }
}

impl IntoIterator for MessageFlags {
    type Item = MessageFlag;
    type IntoIter = std::collections::hash_set::IntoIter<MessageFlag>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl Display for MessageFlags {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0
                .iter()
                .map(|flag| flag.to_string())
                .reduce(|a, b| a + " " + &b)
                .unwrap_or("".to_string())
        )
    }
}

impl From<&str> for MessageFlags {
    fn from(value: &str) -> Self {
        value.split_whitespace().map(MessageFlag::from).collect()
    }
}

impl MessageFlags {
    pub fn flag(flag: MessageFlag) -> Self {
        let mut set = HashSet::new();
        set.insert(flag);
        Self(set)
    }

    pub fn set(&mut self, flag: MessageFlag, set: bool) {
        if set {
            self.0.insert(flag);
        } else {
            self.0.remove(&flag);
        }
    }

    pub fn extend(&mut self, other: impl IntoIterator<Item = MessageFlag>) {
        self.0.extend(other);
    }

    pub fn seen() -> Self {
        MessageFlags::flag(MessageFlag::Seen)
    }

    pub fn is_seen(&self) -> bool {
        self.contains(&MessageFlag::Seen)
    }

    pub fn is_unseen(&self) -> bool {
        !self.is_seen()
    }

    pub fn answered() -> Self {
        MessageFlags::flag(MessageFlag::Answered)
    }

    pub fn is_answered(&self) -> bool {
        self.contains(&MessageFlag::Answered)
    }

    pub fn flagged() -> Self {
        MessageFlags::flag(MessageFlag::Flagged)
    }

    pub fn is_flagged(&self) -> bool {
        self.contains(&MessageFlag::Flagged)
    }

    pub fn deleted() -> Self {
        MessageFlags::flag(MessageFlag::Deleted)
    }

    pub fn is_deleted(&self) -> bool {
        self.contains(&MessageFlag::Deleted)
    }

    pub fn draft() -> Self {
        MessageFlags::flag(MessageFlag::Draft)
    }

    pub fn is_draft(&self) -> bool {
        self.contains(&MessageFlag::Draft)
    }

    pub fn recent() -> Self {
        MessageFlags::flag(MessageFlag::Recent)
    }

    pub fn is_recent(&self) -> bool {
        self.contains(&MessageFlag::Recent)
    }

    pub fn custom(flag: impl ToString) -> Self {
        MessageFlags::flag(MessageFlag::Custom(flag.to_string()))
    }

    pub fn is_custom(&self, flag: impl ToString) -> bool {
        self.contains(&MessageFlag::Custom(flag.to_string()))
    }

    pub fn union(&self, other: &MessageFlags) -> Self {
        MessageFlags(self.0.union(&other.0).cloned().collect())
    }

    pub fn difference(&self, other: &MessageFlags) -> Self {
        MessageFlags(self.0.difference(&other.0).cloned().collect())
    }
}

impl From<MessageFlags> for Vec<imap_types::flag::Flag<'_>> {
    fn from(value: MessageFlags) -> Self {
        value.into_iter().map(Into::into).collect()
    }
}

impl From<imap_types::flag::FlagFetch<'_>> for MessageFlag {
    fn from(value: imap_types::flag::FlagFetch) -> Self {
        match value {
            imap_types::flag::FlagFetch::Flag(flag) => MessageFlag::from(flag),
            imap_types::flag::FlagFetch::Recent => MessageFlag::Recent,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum MessageFlagsChange {
    Add,
    Remove,
    Replace,
}

impl From<MessageFlagsChange> for StoreType {
    fn from(value: MessageFlagsChange) -> Self {
        match value {
            MessageFlagsChange::Add => StoreType::Add,
            MessageFlagsChange::Remove => StoreType::Remove,
            MessageFlagsChange::Replace => StoreType::Replace,
        }
    }
}

impl From<StoreType> for MessageFlagsChange {
    fn from(value: StoreType) -> Self {
        match value {
            StoreType::Add => MessageFlagsChange::Add,
            StoreType::Remove => MessageFlagsChange::Remove,
            StoreType::Replace => MessageFlagsChange::Replace,
        }
    }
}
