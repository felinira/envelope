// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod flag;

pub use flag::*;

#[derive(
    Debug,
    Default,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    glib::ValueDelegate,
    sqlx::Type,
)]
#[sqlx(transparent)]
pub struct MessageCacheId(i64);

impl MessageCacheId {
    pub fn new(value: i64) -> Self {
        Self(value)
    }
}

impl AsRef<i64> for MessageCacheId {
    fn as_ref(&self) -> &i64 {
        &self.0
    }
}

impl From<i64> for MessageCacheId {
    fn from(value: i64) -> Self {
        Self(value)
    }
}
