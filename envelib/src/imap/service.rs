// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use gio::prelude::*;
use gio::subclass::prelude::*;

use super::error::ClientError;
use super::pool;
use super::session::*;
use super::*;
use crate::cache::Cache;
use crate::error::{ErrorKind, ErrorKindExt};
use crate::gettext::*;
use crate::model::MailboxStore;
use crate::model::{
    self, MailboxExt, MailboxExtProps, MessageDetailLevel, MessageExt, MessageExtProps,
};
use crate::service::{delegate::*, incoming::IncomingMailService};
use crate::types::*;

/// How often we should refresh the mailbox list
const LIST_MAILBOXES_INTERVAL: std::time::Duration = std::time::Duration::from_secs(600);
const INVALID_PASSWORD_MAX_RETRY_COUNT: usize = 5;
const INVALID_PASSWORD_WAIT_TIME: std::time::Duration = std::time::Duration::from_secs(5);
const CHUNK_SIZE: usize = 200;

mod imp {
    use std::{
        cell::{Cell, OnceCell, RefCell},
        collections::{BTreeMap, BTreeSet, HashSet},
        marker::PhantomData,
        ops::AsyncFnOnce,
        pin::pin,
        rc::Rc,
    };

    use enumset::EnumSet;
    use futures::{FutureExt, future::LocalBoxFuture, select};
    use smol::lock::Semaphore;
    use zeroize::Zeroizing;

    use crate::{
        account,
        service::incoming::{IncomingMailServiceImpl, IncomingMailServiceImplExt},
        utils::DisplayLog,
    };

    use self::model::TaskStatus;

    use super::*;

    /// High-Level IMAP / cache operations and interface
    ///
    /// This serves as the main GObject based entrypoint into the IMAP backend.
    ///
    /// The IMAP backend is responsible for managing connections in a pool,
    /// cache management, mailbox synchronisation etc.
    ///
    /// We perform conversions between model objects and internal IMAP structures,
    /// acting as an API boundary between both domains.
    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = service::ImapService)]
    pub(crate) struct ImapService {
        /// The account property can be changed, but must never be null
        #[property(get, set = Self::set_account, override_interface = IncomingMailService, construct)]
        account: RefCell<Option<account::Account>>,
        #[property(get, set, override_interface = IncomingMailService, construct)]
        delegate: RefCell<Option<ServiceDelegate>>,
        #[property(get)]
        shutdown_cancellable: gio::Cancellable,
        #[property(get, set = Self::set_work_online, override_interface = IncomingMailService, explicit_notify)]
        work_online: Cell<bool>,
        work_online_semaphore: OnceCell<Rc<Semaphore>>,
        #[property(get, override_interface = IncomingMailService)]
        connection_status: RefCell<ConnectionStatus>,
        #[property(get, override_interface = IncomingMailService)]
        connection_debug_info: RefCell<String>,

        // Pool for IMAP sessions
        session_pool: pool::SessionPool,

        /// Memory and database backed cache
        #[property(construct_only)]
        pub(super) cache: OnceCell<Cache>,
        #[property(get = Self::mailboxes, override_interface = IncomingMailService)]
        mailboxes: PhantomData<MailboxStore>,

        /// The mailboxes that are currently being fetched.
        /// We store this to ensure we don't fetch the same mailbox twice.
        pub(super) syncing_mailboxes: RefCell<BTreeSet<MailboxId>>,

        /// A map with cancellables for every IMAP IDLE mailbox
        ///
        /// This does not include other background tasks.
        idle_cancellables: RefCell<BTreeMap<MailboxId, gio::Cancellable>>,

        /// The last time we updated the list of mailboxes
        last_mailbox_refresh: Cell<Option<std::time::Instant>>,

        messages_without_previews:
            RefCell<BTreeMap<MailboxId, BTreeMap<MessageCacheId, model::Message>>>,

        ask_credentials_shared: RefCell<
            Option<futures::future::Shared<LocalBoxFuture<'static, Option<account::Secret>>>>,
        >,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ImapService {
        const NAME: &'static str = "EnvelopeImapService";
        type Type = service::ImapService;
        type Interfaces = (gio::AsyncInitable, IncomingMailService);
    }

    #[glib::derived_properties]
    impl ObjectImpl for ImapService {
        fn constructed(&self) {
            let obj = self.obj();

            self.work_online_semaphore
                .get_or_init(|| Rc::new(Semaphore::new(1)));

            let session_debug_callback = glib::clone!(
                #[weak]
                obj,
                move |pool: &pool::SessionPool| {
                    let count = pool.session_count();
                    let claimed = pool.session_claimed_count();
                    let waiting = pool.session_wait_count();
                    if waiting > 0 {
                        obj.imp()
                            .connection_debug_info
                            .replace(format!("Sessions: {claimed}/{count} ({waiting} waiting)"));
                    } else {
                        obj.imp()
                            .connection_debug_info
                            .replace(format!("Sessions: {claimed}/{count}"));
                    }

                    obj.imp().notify_connection_debug_info();
                }
            );

            self.session_pool
                .connect_session_count_notify(session_debug_callback.clone());
            self.session_pool
                .connect_session_claimed_count_notify(session_debug_callback.clone());
            self.session_pool
                .connect_session_wait_count_notify(session_debug_callback);
            self.session_pool.connect_session_idle(glib::clone!(
                #[weak]
                obj,
                move |mailbox_name, cancellable| obj.imp().session_idle(mailbox_name, cancellable)
            ));
        }
    }

    impl AsyncInitableImpl for ImapService {
        fn init_future(
            &self,
            _io_priority: glib::Priority,
        ) -> std::pin::Pin<
            Box<dyn std::future::Future<Output = std::result::Result<(), glib::Error>> + 'static>,
        > {
            let imp = self.ref_counted();
            Box::pin(glib::clone!(
                #[strong]
                imp,
                async move {
                    glib::spawn_future_local(glib::clone!(
                        #[strong]
                        imp,
                        async move {
                            imp.event_loop().await;
                        }
                    ));

                    gio::NetworkMonitor::default().connect_network_changed(glib::clone!(
                        #[weak]
                        imp,
                        move |monitor, available| imp.network_changed(monitor, available)
                    ));

                    Ok(())
                }
            ))
        }
    }

    impl IncomingMailServiceImpl for ImapService {
        async fn connection_test(&self, timeout: std::time::Duration) -> ConnectionStatus {
            let account = self.account.borrow().clone().unwrap();
            let mut timeout_future = glib::timeout_future(timeout).fuse();
            let mut client = pin!(super::client::from_account(account, None).fuse());

            select! {
                res = client => {
                    match res {
                        Ok(_) => ConnectionStatus::Connected,
                        Err(err) => ConnectionStatus::Error(err.to_string())
                    }
                },
                _ = timeout_future => { ConnectionStatus::Unreachable }
            }
        }

        /// Shutdown all sessions
        async fn shutdown(&self) -> crate::Result<()> {
            self.shutdown_cancellable.cancel();
            self.session_pool.shutdown().await;
            self.cache().shutdown().await?;

            Ok(())
        }

        /// Set the mailbox as 'open'
        ///
        /// This results in the following:
        /// - Map the thread list to the mailbox, loading it from cache if required
        /// - Synchronize the mailbox from the server, checking for recent changes
        /// - TODO: Continually monitor the mailbox for changes until the corresponding call to close_mailbox()
        async fn open_mailbox(&self, mailbox: model::Mailbox) -> crate::Result<()> {
            self.cache().mailbox_map_threads(&mailbox);
            if self.syncing_mailboxes.borrow().contains(&mailbox.id()) {
                return Ok(());
            }

            let obj = self;

            // We need to pin this future because we can't prove to the solver that this future can never be recursive
            Box::pin(self.with_session(
                gettext("Reload Folder"),
                Some(mailbox.id()),
                pool::Priority::Normal,
                async |mut session| obj.resync_mailbox(&mut session, MailboxSyncMode::All).await,
            ))
            .await?;

            Ok(())
        }

        /// Fetch messages up to the specified detail level
        async fn fetch_messages(
            &self,
            messages: &[model::Message],
            detail_level: MessageDetailLevel,
        ) -> crate::Result<()> {
            // First check if we have the messages cached
            self.cache().messages_load_full(messages).await?;

            let map = messages
                .iter()
                .filter(|msg| msg.detail_level() < detail_level)
                .collect_mailbox_map();

            futures::future::try_join_all(map.into_iter().map(async |(mailbox, messages)| {
                let imp = self.ref_counted();

                if detail_level == MessageDetailLevel::HeadersAndPreview {
                    self.with_session(
                        ngettextf_(
                            "Fetch Message Preview",
                            "Fetch {} Message Previews",
                            messages.len(),
                        ),
                        Some(mailbox),
                        pool::Priority::Important,
                        async |mut session| imp.fetch_previews(None, &mut session, messages).await,
                    )
                    .await
                } else if detail_level == MessageDetailLevel::Full {
                    self.with_session(
                        ngettextf_("Fetch Message", "Fetch {} Messages", messages.len()),
                        Some(mailbox),
                        pool::Priority::Important,
                        async |mut session| imp.fetch_body(&mut session, messages).await,
                    )
                    .await
                } else {
                    Ok(())
                }
            }))
            .await?;

            Ok(())
        }

        /// Store flag updates on the server
        async fn store_flags(
            &self,
            messages: &[model::Message],
            flags: MessageFlags,
            change: MessageFlagsChange,
        ) -> crate::Result<()> {
            let map = messages.iter().collect_mailbox_map();

            futures::future::try_join_all(map.into_iter().map(|(mailbox, messages)| {
                let flags = flags.clone();

                self.with_session(
                    gettext("Store Message Updates"),
                    Some(mailbox),
                    pool::Priority::Important,
                    async |mut session| {
                        self.store_flags(&mut session, messages, flags, change)
                            .await
                    },
                )
            }))
            .await?;

            Ok(())
        }

        /// Copy the messages from this mailbox to the destination
        async fn copy_to(
            &self,
            messages: &[model::Message],
            destination: model::MailboxTarget,
        ) -> crate::Result<()> {
            let destination = match destination {
                model::MailboxTarget::Mailbox(mailbox) => mailbox,
                model::MailboxTarget::Kind(kind) => {
                    self.cache().mailbox_of_kind(kind).ok_or_else(|| {
                        ErrorKind::Inconsistency.with_message(gettextf(
                            "Mailbox “{}” not found",
                            &[&kind.localized_name().unwrap_or_default()],
                        ))
                    })?
                }
            };

            let map = messages.iter().collect_mailbox_map();
            let destination = destination.downcast::<model::ImapMailbox>().map_err(|_| {
                ErrorKind::Inconsistency.with_message("Can't copy messages to a non-imap mailbox")
            })?;

            futures::future::try_join_all(map.into_iter().map(|(mailbox, messages)| {
                let destination = destination.clone();

                self.with_session(
                    ngettextf_("Copy One Message", "Copy {} Messages", messages.len()),
                    Some(mailbox),
                    pool::Priority::Low,
                    async |mut session| self.copy_to(&mut session, messages, destination).await,
                )
            }))
            .await?;

            Ok(())
        }

        /// Move the messages from this mailbox to the destination
        async fn move_to(
            &self,
            messages: &[model::Message],
            destination: model::MailboxTarget,
        ) -> crate::Result<()> {
            let destination = match destination {
                model::MailboxTarget::Mailbox(mailbox) => mailbox,
                model::MailboxTarget::Kind(kind) => {
                    self.cache().mailbox_of_kind(kind).ok_or_else(|| {
                        ErrorKind::Inconsistency.with_message(gettextf(
                            "Mailbox “{}” not found",
                            &[&kind.localized_name().unwrap_or_default()],
                        ))
                    })?
                }
            };

            let map = messages.iter().collect_mailbox_map();
            let destination = destination.downcast::<model::ImapMailbox>().map_err(|_| {
                ErrorKind::Inconsistency.with_message("Can't move messages to a non-imap mailbox")
            })?;

            futures::future::try_join_all(map.into_iter().map(|(mailbox, messages)| {
                let destination = destination.clone();

                self.with_session(
                    ngettextf_("Move One Message", "Move {} Messages", messages.len()),
                    Some(mailbox),
                    pool::Priority::Low,
                    async |mut session| self.move_to(&mut session, messages, destination).await,
                )
            }))
            .await?;

            Ok(())
        }

        /// Delete the messages permanently
        async fn delete_messages(&self, messages: &[model::Message]) -> crate::Result<()> {
            let map = messages.iter().collect_mailbox_map();

            futures::future::try_join_all(map.into_iter().map(|(mailbox, messages)| {
                self.with_session(
                    ngettextf_("Delete One Message", "Delete {} Messages", messages.len()),
                    Some(mailbox),
                    pool::Priority::Low,
                    async |mut session| {
                        self.delete_messages_permanently(&mut session, messages)
                            .await
                    },
                )
            }))
            .await?;

            Ok(())
        }

        /// Load all message headers in `thread` and set them on their respective [`Thread`].
        async fn load_threads(&self, threads: &[model::Thread]) -> crate::Result<()> {
            self.cache().threads_load_full(threads).await?;

            let mut mailboxes = BTreeSet::new();
            for msg in threads
                .iter()
                .filter(|t| t.preview_text().is_none())
                .flat_map(|t| t.newest_message())
            {
                mailboxes.insert(msg.mailbox_id());
                self.messages_without_previews
                    .borrow_mut()
                    .entry(msg.mailbox_id())
                    .or_default()
                    .insert(msg.cache_id(), msg);
            }

            for mailbox in mailboxes {
                self.idle_cancellables
                    .borrow()
                    .get(&mailbox)
                    .inspect(|c| c.cancel());
            }

            Ok(())
        }
    }

    /// Properties
    impl ImapService {
        fn mailboxes(&self) -> MailboxStore {
            self.cache().mailboxes()
        }

        pub(super) fn cache(&self) -> &Cache {
            self.cache
                .get()
                .expect("Cache must be initialized by AsyncInitable")
        }

        fn set_account(&self, account: crate::account::Account) {
            if let Some(cache) = self.cache.get() {
                cache.set_account(account.clone());
            }

            self.session_pool.set_account(account.clone());
            self.account.replace(Some(account));
        }

        fn set_work_online(&self, work_online: bool) {
            // The replacement does not need any locking. We only set this from one thread, and the last call wins.
            if self.work_online.replace(work_online) != work_online {
                self.obj().notify("work-online");

                glib::spawn_future_local(glib::clone!(
                    #[weak(rename_to = imp)]
                    self,
                    async move {
                        let lock = imp.work_online_semaphore.get().unwrap().acquire().await;

                        // Retrieve the last value of the property
                        let new_work_online = imp.work_online.get();
                        if new_work_online != work_online {
                            // This has changed in-between spawning the future
                            return;
                        }

                        if work_online {
                            // Go online
                            imp.set_connection_status(ConnectionStatus::Connecting);
                            imp.ensure_recent_mailbox_list();
                        } else {
                            // Go offline
                            imp.session_pool.shutdown().await;
                            imp.set_connection_status(ConnectionStatus::Disconnected);
                        }

                        drop(lock);
                    }
                ));
            }
        }

        /// Sets connection status and notifies the property
        fn set_connection_status(&self, status: ConnectionStatus) {
            if *self.connection_status.borrow() != status {
                // TODO: do we need to handle this in the pool at all?
                self.connection_status.replace(status);
                self.notify_connection_status()
            }
        }
    }

    /// Call delegate
    impl ImapService {
        async fn delegate_ask_credentials(
            &self,
            account: &account::Account,
            previous: Option<account::Secret>,
            methods: EnumSet<account::AuthMethod>,
            message: String,
        ) -> crate::Result<account::Secret> {
            let Some(delegate) = self.delegate.borrow().clone() else {
                return Err(ErrorKind::InvalidCredentials.into());
            };

            // Ensure we only ask our delegate once at a time per service type (IMAP, SMTP, ...)
            let shared = self.ask_credentials_shared.borrow().clone();
            let service_id = account.imap()?.service_id.clone();

            if let Some(shared) = shared {
                shared.await.ok_or(ErrorKind::Aborted.into())
            } else {
                let secret = if account.is_test_server() {
                    Some(account::Secret::new_basic(
                        "testuser".to_string(),
                        Zeroizing::new("password".to_string()),
                    ))
                } else {
                    let fut = async move {
                        delegate
                            .ask_credentials(service_id, previous, methods, &message)
                            .await
                    }
                    .boxed_local()
                    .shared();

                    self.ask_credentials_shared
                        .borrow_mut()
                        .replace(fut.clone());

                    fut.await
                };

                self.ask_credentials_shared.borrow_mut().take();

                secret.ok_or(ErrorKind::Aborted.into())
            }
        }
    }

    impl ImapService {
        /// Returns the mailbox contained in the selection or an error
        fn expect_mailbox(
            &self,
            selection: &MailboxSelection,
        ) -> crate::Result<model::ImapMailbox> {
            if let Some(mailbox) = self.cache().mailbox_by_imap_name(&selection.mailbox_name) {
                Ok(mailbox)
            } else {
                Err(ErrorKind::Inconsistency.with_message(format!(
                    "Perform action for unknown mailbox {:?}.",
                    selection.mailbox_name
                )))
            }
        }

        /// Signal handler for [`gio::NetworkMonitor`] `network-changed`
        fn network_changed(&self, monitor: &gio::NetworkMonitor, available: bool) {
            if !available && self.work_online.get() {
                self.set_connection_status(ConnectionStatus::Unreachable);
            } else if *self.connection_status.borrow() != ConnectionStatus::Connected {
                let Some(account) = self.account.borrow().clone() else {
                    self.set_connection_status(ConnectionStatus::Disconnected);
                    return;
                };

                let Ok(imap) = account.imap().cloned() else {
                    self.set_connection_status(ConnectionStatus::Disconnected);
                    return;
                };

                // See if we can reach the network now
                glib::spawn_future_local(glib::clone!(
                    #[strong(rename_to = imp)]
                    self.ref_counted(),
                    #[strong]
                    monitor,
                    async move {
                        let addr = gio::NetworkAddress::new(&imap.server, imap.port);
                        match monitor.can_reach_future(&addr).await {
                            Ok(()) => {
                                if !imp.work_online.get() {
                                    imp.set_connection_status(ConnectionStatus::Disconnected);
                                } else if !imp.connection_status.borrow().is_connected() {
                                    imp.set_connection_status(ConnectionStatus::Connecting);
                                }
                            }
                            Err(_) => {
                                imp.set_connection_status(ConnectionStatus::Unreachable);
                            }
                        }
                    }
                ));
            }
        }

        /// Waits for unsolicited responses in a loop
        async fn event_loop(&self) {
            log::debug!("Waiting for unsolicited responses");

            let Some(unsolicited_receiver) = self.session_pool.take_unsolicited_response_receiver()
            else {
                log::error!("Event loop can't run more than once");
                let _ = self.shutdown().await;
                return;
            };

            let _ = gio::CancellableFuture::new(
                async {
                    loop {
                        match unsolicited_receiver.recv().await {
                            Ok(response) => {
                                if let Err(err) = self.handle_unsolicited_response(response).await {
                                    log::error!("Error handling unsolicited response: {}", err);
                                }
                            }
                            Err(err) => {
                                log::error!("Unsolicited response receiver error: {:?}", err);
                                break;
                            }
                        }
                    }
                },
                self.shutdown_cancellable.clone(),
            )
            .await;
        }

        /// Process a single unsolicited response
        async fn handle_unsolicited_response(
            &self,
            response: UnsolicitedResponse,
        ) -> crate::Result<()> {
            let Some(mailbox) = self.cache().mailbox_by_imap_name(&response.mailbox_name) else {
                // Mailbox unknown
                log::info!(
                    "Received unsolicited response about unknown mailbox: {}",
                    response.mailbox_name
                );
                return Ok(());
            };

            let mut fetch_flags_ids = BTreeSet::new();
            let mut resync_mailbox = false;
            let mut fetch_new_messages = false;

            for attribute in &response.attributes {
                match attribute {
                    MailboxAttribute::HighestModSeq(_n) => {
                        // TODO: QRESYNC
                    }
                    MailboxAttribute::Messages(n) => {
                        if self
                            .cache()
                            .message_count_for_mailbox(&mailbox.id())
                            .await
                            .unwrap_or(0)
                            != *n as usize
                        {
                            resync_mailbox = true;
                        }
                    }
                    MailboxAttribute::MailboxFlagsChanged => resync_mailbox = true,
                    MailboxAttribute::Recent(_n) => {
                        // TODO
                    }
                    MailboxAttribute::UidNext(uidnext) => {
                        if self
                            .cache()
                            .mailbox_by_imap_name(&response.mailbox_name)
                            .is_none_or(|mailbox| mailbox.imap_uidnext() >= *uidnext)
                        {
                            resync_mailbox = true;
                        }
                    }
                    MailboxAttribute::UidValidity(uidvalidity) => {
                        if self
                            .cache()
                            .mailbox_by_imap_name(&response.mailbox_name)
                            .is_none_or(|mailbox| mailbox.imap_uidvalidity() >= *uidvalidity)
                        {
                            self.uidvalidity_reset(&mailbox, *uidvalidity).await?;
                        }
                    }
                    MailboxAttribute::Exists(_seqnum) => {
                        fetch_new_messages = true;
                    }
                    MailboxAttribute::Expunge { seqnum, uid } => {
                        if let Some(uid) = uid {
                            self.cache()
                                .message_remove(mailbox.upcast_ref(), *uid)
                                .await?;
                        } else {
                            log::debug!(
                                "Unsolicited EXPUNGE without uid: {seqnum:?} for mailbox {}",
                                mailbox.imap_name()
                            );
                            resync_mailbox = true;
                        }
                    }
                    MailboxAttribute::Fetch {
                        seqnum, uid, fetch, ..
                    } => {
                        if let Some(uid) = uid {
                            match fetch {
                                FetchData::Flags(flags) => {
                                    self.cache()
                                        .imap_message_update_flags(
                                            &response.mailbox_name,
                                            uid,
                                            flags,
                                        )
                                        .await?;
                                }
                                FetchData::Unknown => {
                                    fetch_flags_ids.insert(uid);
                                }
                            }
                        } else {
                            log::debug!(
                                "Unsolicited FETCH without uid: {}: {fetch:?} for mailbox {}",
                                seqnum,
                                mailbox.imap_name()
                            );
                            resync_mailbox = true;
                        }
                    }
                    MailboxAttribute::Unseen(_n) => {
                        resync_mailbox = true;
                    }
                    MailboxAttribute::Other => {}
                }
            }

            // We only update the mailbox if it wasn't already updated after this unsolicited response was generated
            if (resync_mailbox || fetch_new_messages || !fetch_flags_ids.is_empty())
                && mailbox.last_sync_earlier_than(response.time)
            {
                self.with_session(
                    gettext("Fetching Updated Mailbox Data"),
                    Some(mailbox.id()),
                    pool::Priority::Normal,
                    async |mut session| {
                        if resync_mailbox {
                            self.resync_mailbox(&mut session, MailboxSyncMode::All)
                                .await?
                        } else {
                            if fetch_new_messages {
                                self.resync_mailbox(&mut session, MailboxSyncMode::NewMessagesOnly)
                                    .await?
                            }

                            if !fetch_flags_ids.is_empty() {
                                self.fetch_flags(
                                    &mut session,
                                    fetch_flags_ids.into_iter().cloned().collect::<Vec<_>>(),
                                )
                                .await?;
                            }
                        }

                        Ok(())
                    },
                )
                .await?;
            }

            Ok(())
        }

        /// Called whenever a mailbox is selected
        async fn new_selection(&self, selection: Option<&MailboxSelection>) -> crate::Result<()> {
            let Some(selection) = selection else {
                return Ok(());
            };

            log::debug!(
                "Selected mailbox '{}': UIDVALIDITY: {}, UIDNEXT: {} (count: {}, recent: {})",
                selection.mailbox_name,
                selection.uidvalidity,
                selection.uidnext,
                selection.message_count,
                selection.recent_count,
            );

            let Some(mailbox) = self.cache().mailbox_by_imap_name(&selection.mailbox_name) else {
                log::warn!("Selected unknown mailbox {}", selection.mailbox_name);
                return Ok(());
            };

            mailbox.set_imap_selection(selection);
            if mailbox.imap_uidvalidity() != selection.uidvalidity {
                self.uidvalidity_reset(&mailbox, selection.uidvalidity)
                    .await?;
            }

            self.cache().imap_mailbox_update(&mailbox).await?;

            Ok(())
        }

        /// Called whenever a new STATUS response is received
        async fn mailbox_new_status(
            &self,
            mailbox: &model::ImapMailbox,
            status: &MailboxStatus,
        ) -> crate::Result<()> {
            mailbox.set_imap_status(status);

            if mailbox.imap_uidvalidity() != status.uidvalidity {
                self.uidvalidity_reset(mailbox, status.uidvalidity).await?;
            } else {
                self.cache().imap_mailbox_update(mailbox).await?;
            }

            if status.uidnext > mailbox.imap_uidnext() {
                // We have new messages in the mailbox
                // TODO: resync
            }

            Ok(())
        }

        /// Fetch message headers in the background
        async fn idle_fetch_messages(
            &self,
            session: &mut SessionClaim,
            mailbox: model::ImapMailbox,
            cancellable: gio::Cancellable,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            // After the sync our mailbox looks like this:
            // 0 .. uidfirst: We don't have any messages here
            // uidfirst .. uidnext: We have all messages
            // uidnext .. selection.uidnext: We don't have any messages here
            while !cancellable.is_cancelled() {
                if selection.uidnext > mailbox.imap_uidnext() {
                    // We prioritize fetching the new messages
                    let top = selection
                        .uidnext
                        .min(mailbox.imap_uidnext() + Uid::new(CHUNK_SIZE as u32));
                    let chunk = mailbox.imap_uidnext()..top;

                    let _update = self.fetch_message_headers(&mut *session, chunk).await?;
                    mailbox.set_imap_uidnext(top);
                } else if mailbox.imap_uidfirst() > Uid::NULL {
                    // Otherwise we fill our archive of old messages
                    let first = mailbox.imap_uidfirst() - Uid::new(CHUNK_SIZE as u32);
                    let first = if first == Uid::FIRST {
                        Uid::NULL
                    } else {
                        first
                    };
                    let chunk = first..mailbox.imap_uidfirst();

                    let _update = self.fetch_message_headers(&mut *session, chunk).await?;
                    mailbox.set_imap_uidfirst(first);
                } else {
                    // Done
                    break;
                };

                self.cache().imap_mailbox_update(&mailbox).await?;
            }

            if !cancellable.is_cancelled() && mailbox.uncommitted() {
                // We have finished. Resync the mailbox to fix final message counts etc.
                // This should be fast, as we only need to fetch flags
                self.resync_mailbox(&mut *session, MailboxSyncMode::FlagsOnly)
                    .await?;
            }

            self.cache().imap_mailbox_update(&mailbox).await?;

            if cancellable.is_cancelled() {
                return Err(ErrorKind::Aborted.into());
            }

            Ok(())
        }

        /// Fetch previews in the background
        async fn idle_fetch_previews(
            &self,
            session: &mut SessionClaim,
            mailbox: model::ImapMailbox,
            cancellable: gio::Cancellable,
        ) -> crate::Result<()> {
            if cancellable.is_cancelled() {
                return Err(ErrorKind::Aborted.into());
            }

            let messages = self
                .messages_without_previews
                .borrow()
                .get(&mailbox.id())
                .cloned()
                .unwrap_or_default();

            self.fetch_previews(Some(&cancellable), &mut *session, messages.values())
                .await?;

            if let Some(map) = self
                .messages_without_previews
                .borrow_mut()
                .get_mut(&mailbox.id())
            {
                for id in messages.keys() {
                    map.remove(id);
                }
            }

            Ok(())
        }

        /// We only start IDLE when nothing is going on anymore.
        /// Once the queue is finished with the current mailbox it will notify us.
        /// Then we can spawn an IDLE session if desired.
        async fn try_start_imap_idle(
            &self,
            session: &mut SessionClaim,
            cancellable: gio::Cancellable,
        ) -> crate::Result<()> {
            let Some(selection) = session.selection() else {
                return Ok(());
            };

            log::debug!("Starting IDLE for mailbox {}", selection.mailbox_name);

            let res = self.imap_idle_loop(&mut *session, cancellable).await;
            session.idle_done().await;

            if let Err(err) = &res {
                if err.kind() != ErrorKind::Aborted {
                    log::error!("Error during IDLE session: {}", err);
                }
            }

            res
        }

        /// The inner IDLE loop. Waits for a response, calls the callback, repeat.
        async fn imap_idle_loop(
            &self,
            session: &mut SessionClaim,
            cancellable: gio::Cancellable,
        ) -> crate::Result<()> {
            loop {
                let res = session
                    .with_imap_client(
                        async |client| {
                                if cancellable.is_cancelled() {
                                    // We were asked to stop
                                    return Err(ClientError::Aborted);
                                }

                                let mut handle = client.start_idle()?;

                                let res = {
                                    futures::select_biased! {
                                        res = handle.idle().fuse() => res,
                                        _ = cancellable.future().fuse() => Ok(IdleResponse::ManualInterrupt),
                                    }
                                };

                                handle.done().await?;
                                res
                        }
                    )
                    .await?;

                match res {
                    IdleResponse::ManualInterrupt => break,
                    IdleResponse::Response(_) | IdleResponse::Timeout => {
                        // TODO: Resync and continue, because we can't differentiate between timeout and responses
                        self.resync_mailbox(&mut *session, MailboxSyncMode::NewMessagesOnly)
                            .await?;
                    }
                }
            }

            Ok(())
        }

        /// Execute background tasks.
        ///
        /// This will do the following, in order:
        /// - Fetch any missing messages
        /// - Fetch any missing message preview texts
        /// - Run the IMAP IDLE command
        ///
        /// All methods called from here must ensure they properly cancel their operation if the passed
        /// cancellable is cancelled.
        async fn do_session_background_tasks(
            &self,
            mailbox: model::ImapMailbox,
            cancellable: gio::Cancellable,
        ) -> crate::Result<()> {
            loop {
                if mailbox.imap_uidfirst() > Uid::NULL {
                    let cancellable = cancellable.clone();
                    let mailbox = mailbox.clone();

                    self.with_session(
                        gettext("Load Messages"),
                        Some(mailbox.id()),
                        pool::Priority::Idle,
                        async |mut session| {
                            self.idle_fetch_messages(&mut session, mailbox, cancellable)
                                .await
                        },
                    )
                    .await?;
                }

                // While missing previews
                while !cancellable.is_cancelled()
                    && self
                        .messages_without_previews
                        .borrow()
                        .get(&mailbox.id())
                        .is_some_and(|m| !m.is_empty())
                {
                    let cancellable = cancellable.clone();
                    let mailbox = mailbox.clone();

                    self.with_session(
                        gettext("Load Previews"),
                        Some(mailbox.id()),
                        pool::Priority::Idle,
                        async |mut session| {
                            self.idle_fetch_previews(
                                &mut session,
                                mailbox.clone(),
                                cancellable.clone(),
                            )
                            .await
                        },
                    )
                    .await?;
                }

                // Create a sub-cancellable that will be used only to cancel the IMAP IDLE task, and nothing else
                let idle_cancel = gio::Cancellable::new();
                cancellable.connect_cancelled(glib::clone!(
                    #[weak]
                    idle_cancel,
                    move |_| idle_cancel.cancel()
                ));

                self.idle_cancellables
                    .borrow_mut()
                    .insert(mailbox.id(), idle_cancel.clone());

                // We've made it this far, the only thing that is left is actually calling IMAP IDLE
                let res = self
                    .with_session(
                        gettext("Monitoring Folder"),
                        Some(mailbox.id()),
                        pool::Priority::Idle,
                        async |mut session| {
                            self.try_start_imap_idle(&mut session, idle_cancel.clone())
                                .await
                        },
                    )
                    .await;

                if cancellable.is_cancelled() {
                    return Err(ErrorKind::Aborted.into());
                }

                if let Err(err) = res {
                    if !idle_cancel.is_cancelled() {
                        // Some proper error occurred
                        return Err(err);
                    }
                }

                // Only the IDLE task was cancelled, we might want to fetch previews
                self.idle_cancellables.borrow_mut().remove(&mailbox.id());
            }
        }

        /// Called when a session is free of any claims. We use this opportunity to enter IMAP IDLE.
        fn session_idle(&self, mailbox_name: MailboxName, cancellable: gio::Cancellable) {
            log::debug!("Run idle tasks for mailbox: {mailbox_name:?}");
            let Some(mailbox) = self.cache().mailbox_by_imap_name(&mailbox_name) else {
                return;
            };

            let imp = self.ref_counted().clone();
            glib::spawn_future_local(async move {
                let result = imp.do_session_background_tasks(mailbox, cancellable).await;

                if let Err(err) = result {
                    if err.kind() == ErrorKind::Aborted {
                        log::debug!("Idle process interrupted: {}", mailbox_name);
                    } else {
                        log::error!("Error fetching messages in background: {}", err);
                    }
                }
            });
        }

        /// Ensures the mailbox list has been refreshed since [`LIST_MAILBOXES_INTERVAL`]
        ///
        /// Otherwise, spawn a task to refresh the mailbox list
        fn ensure_recent_mailbox_list(&self) {
            if self
                .last_mailbox_refresh
                .get()
                .is_none_or(|time| time.elapsed() >= LIST_MAILBOXES_INTERVAL)
            {
                let obj = self.obj().clone();
                glib::spawn_future_local(async move {
                    let imp = obj.imp();
                    imp.last_mailbox_refresh
                        .set(Some(std::time::Instant::now()));
                    let result = imp
                        .with_session(
                            gettext("Reload Folder List"),
                            None,
                            pool::Priority::Normal,
                            async |mut session| imp.refresh_mailbox_list(&mut session).await,
                        )
                        .await;

                    if let Err(err) = result {
                        log::error!("Error refreshing recent mailbox list: {}", err);
                    }
                });
            }
        }

        /// Return a mailbox object for a given name
        ///
        /// Internally triggers a mailbox fetch whenever it is deemed necessary
        #[expect(dead_code)]
        pub(super) fn mailbox(
            &self,
            mailbox_id: MailboxId,
        ) -> crate::Result<Option<model::Mailbox>> {
            // Trigger a network refresh if the cached data is unvailable or deemed too old
            self.ensure_recent_mailbox_list();
            let mailbox = self.cache().mailbox(mailbox_id);

            Ok(mailbox)
        }

        /// Used when an internal inconsistency was detected.
        ///
        /// This schedules a mailbox resync at the earliest opportunity.
        fn trigger_resync(&self, mailbox: model::ImapMailbox, mode: MailboxSyncMode) {
            glib::spawn_future_local(glib::clone!(
                #[strong(rename_to = imp)]
                self.ref_counted(),
                async move {
                    imp.clone()
                        .with_session(
                            gettext("Fetching Updated Mailbox Data"),
                            Some(mailbox.id()),
                            pool::Priority::Normal,
                            |mut session| async move {
                                imp.resync_mailbox(&mut session, mode).await
                            },
                        )
                        .await
                }
            ));
        }

        /// Perform the synchronisation process as detailed in RFC 4549 Section 3
        pub(super) async fn resync_mailbox(
            &self,
            session: &mut SessionClaim,
            mode: MailboxSyncMode,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            log::debug!(
                "Syncing mailbox state for '{}' (RFC 4549)",
                selection.mailbox_name
            );

            let start_instant = std::time::Instant::now();

            if self.syncing_mailboxes.borrow().contains(&mailbox.id()) {
                // We are already in a resync. Nothing to be done.
                log::debug!("Multiple mailbox synchronisations happening at the same time");
                return Ok(());
            }

            // This needs to be after the check for syncing_mailboxes,
            // otherwise we would be removing it right after
            scopeguard::defer! {
                mailbox.set_last_sync(start_instant);

                self.syncing_mailboxes
                    .borrow_mut()
                    .remove(&mailbox.id());

                self.emit_message_fetch_complete(mailbox.upcast_ref());
            }

            self.syncing_mailboxes.borrow_mut().insert(mailbox.id());

            // c Perform client-to-server synchronization TODO

            // d) Perform Server-to-client synchronization
            // 1) UID VALIDITY: Selecting a mailbox will already clear out the cache if uidvalidity has changed
            let resync = mailbox.imap_uidnext() > Uid::NULL;

            // 2) Fetch the current "descriptors";

            // Only fetch top CHUNK_SIZE full message headers at a time
            // There are different scenarios that we handle:
            //
            // 1. The mailbox has never been opened before. uidnext and uidfirst are both 0:
            //    - Start from the top (most recent messages first) to ensure fast response time for huge mailboxes
            //    - Only sync a maximum of CHUNK_SIZE messages, don't sync flags
            // 2. The mailbox has been opened before.
            //    - Start from the bottom of the unsynced area (oldest unknown messages first)
            //    - Sync the next chunk from uidnext up
            // 2.a) uidfirst == 0 (a full sync has occured in the past):
            //    - Sync flags for all messages uidfirst .. uidnext
            // 2.b) uidfirst != 0 (a partial sync has occured in the past):
            //    - Sync flags for all messages < uidnext
            //    - Don't synchronize anything lower than uidfirst
            //
            // After the sync our mailbox looks like this:
            // 0 .. uidfirst: We don't have any messages here
            // uidfirst .. uidnext: We have all messages
            // uidnext .. selection.uidnext: We don't have any messages here
            let (bottom, mid, top) = if resync {
                (
                    mailbox.imap_uidfirst(),
                    mailbox.imap_uidnext(),
                    selection
                        .uidnext
                        .min(mailbox.imap_uidnext() + Uid::new(CHUNK_SIZE as u32)),
                )
            } else {
                let mid = selection.uidnext - Uid::new(CHUNK_SIZE as u32);
                (mid, mid, selection.uidnext)
            };

            let headers_range = mid..top;
            let flags_range = bottom..top;

            let mut update = MessageUpdate::new();

            if mode != MailboxSyncMode::FlagsOnly && !headers_range.is_empty() {
                // I)  Discover new messages.
                update.combine(self.fetch_message_headers(session, headers_range).await?);
                log::debug!("{update:?}");

                self.cache().imap_mailbox_update(&mailbox).await?;

                if resync {
                    // Send notifications for any new messages
                    self.emit_message_update(mailbox.upcast_ref(), &update);
                }
            }

            mailbox.set_imap_uidnext(top);
            mailbox.set_imap_uidfirst(bottom);

            // II) Discover changes to old messages.
            if mode != MailboxSyncMode::NewMessagesOnly {
                if !flags_range.is_empty() {
                    update.combine(self.fetch_flags(session, flags_range).await?);
                    log::debug!("{update:?}");
                }

                // Only update the mailbox if this is a full sync
                mailbox.set_message_count(update.message_count() as u32);
                mailbox.set_unread_count(update.unseen_count() as u32);
                mailbox.set_recent_count(update.recent_count() as u32);

                self.cache().imap_mailbox_update(&mailbox).await?;
            }

            log::info!(
                "Finished syncing mailbox state for '{}' (RFC 4549) (time: {})",
                selection.mailbox_name,
                start_instant.elapsed().display()
            );
            log::debug!(
                "{}: num: {}, unread: {}, recent: {}",
                selection.mailbox_name,
                mailbox.message_count(),
                mailbox.unread_count(),
                mailbox.recent_count()
            );

            self.cache().imap_mailbox_update(&mailbox).await?;

            Ok(())
        }

        /// Fetch messages in the specified `range` from the server
        async fn fetch_message_headers(
            &self,
            session: &mut SessionClaim,
            range: impl UidRange,
        ) -> crate::Result<MessageUpdate> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            if range.is_empty() {
                log::debug!("Nothing to fetch, everything up to date");
                return Ok(MessageUpdate::new());
            }

            log::debug!(
                "Fetch messages for {} (cached: {}..{}, server: {})",
                selection.mailbox_name,
                mailbox.imap_uidfirst(),
                mailbox.imap_uidnext(),
                selection.uidnext
            );

            let mut update = MessageUpdate::new();
            for uids in range.chunks(CHUNK_SIZE) {
                let chunk = session
                    .exec(async move |session: &mut Client| {
                        let mut list = session
                            .fetch_headers(&*uids)
                            .await?
                            .into_values()
                            .collect::<Vec<MessageData>>();

                        list.sort_by(|a, b| b.internal_date.cmp(&a.internal_date));
                        Ok(list)
                    })
                    .await?;

                update.add_data(&chunk);
                self.cache()
                    .imap_message_data_update(&selection.mailbox_name, &chunk)
                    .await?;
            }

            if let Some(message) = &update.notification_message() {
                // We need the preview for the notification
                // In any other case we'd just trigger the queue to do it, but in this case we need it immediately
                if message.preview_text().is_none() {
                    self.fetch_previews(None, session, std::iter::once(message))
                        .await?;
                }
            };

            Ok(update)
        }

        /// RFC 4549
        /// 4.3.1. Discovering New Messages and Changes to Old Messages
        ///
        /// part 2
        async fn fetch_flags(
            &self,
            session: &mut SessionClaim,
            range: impl UidRange,
        ) -> crate::Result<MessageUpdate> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            log::debug!("Fetch flags for {:?} in '{}'", range, mailbox.imap_name());

            let uids = range.to_owned();
            let map = session
                .exec(async move |client| client.fetch_flags(&*uids).await)
                .await?;

            let update = MessageUpdate::from_iter(map.values());

            let missing_messages = self
                .cache()
                .imap_messages_update_flags(&selection.mailbox_name, &map)
                .await?;

            // Fetch messages that were apparently missing in our cache
            // This should usually not happen, but better be safe than miss messages
            if !missing_messages.is_empty() {
                if missing_messages.len() > 5 * CHUNK_SIZE {
                    // This is too big for a single transaction, something is seriously wrong here.
                    // We reset the mailbox, then trigger a resync.
                    log::error!(
                        "Restoring cache integrity: {} messages are inexplicably missing from the cache. Reloading mailbox",
                        missing_messages.len()
                    );

                    self.uidvalidity_reset(&mailbox, selection.uidvalidity)
                        .await?;

                    // Resync the entire mailbox
                    self.trigger_resync(mailbox, MailboxSyncMode::All);

                    return Err(ErrorKind::Inconsistency.with_message(format!(
                        "Inexplicably missing {} messages",
                        missing_messages.len()
                    )));
                } else {
                    // This is not too bad, we just fetch the messages and continue
                    log::warn!(
                        "Restoring cache integrity: Fetching {} messages that are inexplicably missing from the cache",
                        missing_messages.len()
                    );

                    self.fetch_message_headers(session, missing_messages)
                        .await?;
                }
            }

            if range.is_contiguous() {
                let fetched_range: &dyn UidRange = if range.last() == Uid::MAX {
                    &(range.first()..mailbox.imap_uidnext())
                } else {
                    &range
                };

                // We know that all missing messages don't exist anymore. Expunge them.
                let mut expunge = Vec::new();

                for id in fetched_range {
                    if !map.contains_key(&id) {
                        expunge.push(id);
                    }
                }

                self.cache()
                    .messages_remove(mailbox.upcast_ref(), expunge)
                    .await?;
            }

            log::debug!("Fetch flags complete");

            Ok(update)
        }

        /// Store flag updates on the server
        pub(super) async fn store_flags(
            &self,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
            flags: MessageFlags,
            change: MessageFlagsChange,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            let messages = messages
                .into_iter()
                .try_collect_uid_map(&selection.mailbox_name)?;
            let uids = messages.to_uid_range();

            log::debug!(
                "Store flags for {} messages in {}",
                messages.len(),
                mailbox.imap_name()
            );

            let new_flags = flags.clone();
            let result = session
                .exec(async move |client| client.store_flags(&uids, new_flags, change).await)
                .await?;

            if result.len() < messages.len() {
                // Something failed. Maybe our flags were outdated. We do a flags check.
                log::debug!("Store flags had insufficient result: Assuming fetch went through");
                for message in messages.values() {
                    match change {
                        MessageFlagsChange::Add => {
                            let new_flags = message.flags().union(&flags);
                            message.update_flags(&new_flags);
                        }
                        MessageFlagsChange::Remove => {
                            let new_flags = message.flags().difference(&flags);
                            message.update_flags(&new_flags);
                        }
                        MessageFlagsChange::Replace => {
                            message.update_flags(&flags);
                        }
                    }
                }

                return Ok(());
            }

            for (uid, message) in messages {
                if let Some(flags) = result.get(&uid) {
                    message.update_flags(flags);
                }
            }

            Ok(())
        }

        /// RFC 4549 - Synchronization Operations for Disconnected IMAP4 Clients
        ///
        /// If UIDVALIDITY value returned by the server differs, the client
        /// MUST
        ///
        /// * empty the local cache of that mailbox;
        /// * remove any pending "actions" that refer to UIDs in that
        ///   mailbox and consider them failed; and
        /// * skip step 2-II.
        async fn uidvalidity_reset(
            &self,
            mailbox: &model::ImapMailbox,
            uidvalidity: UidValidity,
        ) -> crate::Result<()> {
            log::info!("UIDVALIDITY changed for mailbox {}", mailbox.imap_name());
            mailbox.set_imap_uidvalidity(uidvalidity);
            mailbox.set_imap_uidnext(Uid::NULL);
            mailbox.set_imap_uidfirst(Uid::NULL);
            self.cache().imap_mailbox_update(mailbox).await?;

            Ok(())
        }

        /// Fetch the body of messages
        pub(super) async fn fetch_body(
            &self,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;

            let messages = messages
                .into_iter()
                .try_collect_uid_map(&selection.mailbox_name)?;
            let uids = messages.to_uid_range();

            log::debug!(
                "Fetch bodies for {} messages in {:?}",
                messages.len(),
                selection.mailbox_name
            );

            for chunk in uids.chunks(CHUNK_SIZE) {
                let bodies = session
                    .exec(async move |client| client.fetch_body(&*chunk).await)
                    .await?;

                for (uid, body) in bodies {
                    if let Some(msg) = messages.get(&uid) {
                        msg.set_message_body(Some(body));
                    }
                }
            }

            self.cache().messages_update(messages.values()).await?;

            Ok(())
        }

        /// Fetch a preview of the body.
        pub(super) async fn fetch_previews(
            &self,
            cancellable: Option<&gio::Cancellable>,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let messages = messages
                .into_iter()
                .try_collect_uid_map(&selection.mailbox_name)?;

            if messages.is_empty() {
                return Ok(());
            }

            log::debug!(
                "Fetch previews for {} messages in {:?}",
                messages.len(),
                selection.mailbox_name
            );

            for chunk in messages.to_uid_range().chunks(CHUNK_SIZE) {
                if cancellable.is_some_and(|c| c.is_cancelled()) {
                    return Err(ErrorKind::Aborted.into());
                }

                let previews = session
                    .exec(async move |session| session.fetch_preview_text(&*chunk).await)
                    .await?;

                let updated = previews.into_iter().flat_map(|(uid, preview)| {
                    let msg = messages.get(&uid);
                    if let Some(msg) = msg {
                        msg.set_preview_text(Some(preview));
                    }

                    msg
                });

                self.cache().messages_update(updated).await?;
            }

            log::debug!("Done fetching previews");

            Ok(())
        }

        /// Iterate through all known mailboxes and send a STATUS command
        pub(super) async fn fetch_all_mailbox_status(
            &self,
            session: &mut SessionClaim,
            mailboxes: impl IntoIterator<Item = model::ImapMailbox>,
        ) -> crate::Result<()> {
            log::debug!("Updating mailbox status");

            for mailbox in mailboxes {
                mailbox.set_last_update(session.time().to_owned());
                let name = mailbox.imap_name();

                let status_res = session
                    .exec(async move |client| client.mailbox_status(&name).await)
                    .await;

                match status_res {
                    Ok(status) => {
                        log::debug!("STATUS {}: {:?}", mailbox.imap_name(), status);
                        self.mailbox_new_status(&mailbox, &status).await?;
                    }
                    Err(err) => {
                        log::warn!(
                            "Error retrieving mailbox status for '{}': {}",
                            mailbox.imap_name(),
                            err
                        );
                    }
                }
            }

            Ok(())
        }

        /// Reload the list of known mailboxes
        async fn refresh_mailbox_list(&self, session: &mut SessionClaim) -> crate::Result<()> {
            log::debug!("List mailboxes");

            let info_list: Vec<_> = session
                .exec(async |client| {
                    Ok(client
                        .list("")
                        .await?
                        .into_iter()
                        .filter(|mailbox| mailbox.selectable)
                        .collect())
                })
                .await?;

            log::debug!("List: {:?}", info_list);

            let mut new_mailbox_names = HashSet::new();

            for info in info_list {
                new_mailbox_names.insert(info.name.clone());
                self.cache().mailbox_update_info(info).await?;
            }

            // Remove deleted mailboxes
            let mailboxes = self.cache().imap_mailboxes();
            for mailbox in mailboxes
                .iter()
                .flat_map(|m| m.downcast_ref::<model::ImapMailbox>())
            {
                if !new_mailbox_names.contains(&mailbox.imap_name()) {
                    self.cache().mailbox_remove(mailbox.id()).await?;
                }
            }

            self.last_mailbox_refresh
                .set(Some(std::time::Instant::now()));

            self.fetch_all_mailbox_status(session, mailboxes).await?;

            Ok(())
        }

        /// Move the messages from this mailbox to the destination
        pub(super) async fn move_to(
            &self,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
            destination: model::ImapMailbox,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            let uids = messages
                .into_iter()
                .try_collect_uid_range(&selection.mailbox_name)?;
            let imap_destination = destination.imap_name();

            log::debug!(
                "Move {} messages from {:?} to {}",
                uids.len(),
                mailbox,
                destination.imap_name()
            );

            let range = uids.clone();
            session
                .exec(async move |client| {
                    client.move_to(&range, &imap_destination).await?;
                    Ok(())
                })
                .await?;

            for uid in uids {
                self.cache()
                    .message_remove(mailbox.upcast_ref(), uid)
                    .await?;
            }

            Ok(())
        }

        /// Copy the messages from this mailbox to the destination
        pub(super) async fn copy_to(
            &self,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
            destination: model::ImapMailbox,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            let uids = messages
                .into_iter()
                .try_collect_uid_range(&selection.mailbox_name)?;
            let imap_destination = destination.imap_name();

            log::debug!(
                "Copy {} messages from {:?} to {}",
                uids.len(),
                mailbox,
                destination.imap_name()
            );

            session
                .exec(async move |client| {
                    client.copy_to(&uids, &imap_destination).await?;
                    Ok(())
                })
                .await?;

            Ok(())
        }

        /// Delete the messages permanently
        pub(super) async fn delete_messages_permanently(
            &self,
            session: &mut SessionClaim,
            messages: impl IntoIterator<Item = impl AsRef<model::Message>>,
        ) -> crate::Result<()> {
            let selection = session.expect_selection()?;
            let mailbox = self.expect_mailbox(&selection)?;

            let uids = messages
                .into_iter()
                .try_collect_uid_range(&selection.mailbox_name)?;

            let range = uids.clone();
            session
                .exec(async move |client| client.delete_messages(&uids).await)
                .await?;

            for uid in range {
                self.cache()
                    .message_remove(mailbox.upcast_ref(), uid)
                    .await?;
            }

            Ok(())
        }

        async fn connect_retry(&self, session: &mut SessionClaim) -> crate::Result<()> {
            for _ in 0..INVALID_PASSWORD_MAX_RETRY_COUNT {
                let time = std::time::Instant::now();

                let mut account = self
                    .account
                    .borrow()
                    .clone()
                    .expect("Account config must be set at this point");

                // Initiate the connection
                let connect_result = session.connect().await;

                match connect_result {
                    Ok(session) => {
                        self.set_connection_status(ConnectionStatus::Connected);
                        return Ok(session);
                    }
                    Err(
                        ref err @ ClientError::Authenticate {
                            ref secret,
                            server_mechanisms,
                            ref message,
                        },
                    ) => {
                        log::info!("Authentication error: {}", message);
                        self.set_connection_status(err.into());

                        let new_secret = self
                            .delegate_ask_credentials(
                                &account,
                                secret.clone(),
                                server_mechanisms,
                                message.clone(),
                            )
                            .await?;

                        let imap = account.imap_mut()?;
                        imap.secret = new_secret;

                        // TODO: What happens if SMTP password is different?
                        // Or what happens if it *becomes* different?
                        // Maybe set SMTP password here and if that was a wrong guess we can catch the error in SMTP?
                        // Retry
                    }
                    Err(err) => {
                        if !err.is_aborted() {
                            self.set_connection_status((&err).into());
                        }

                        return Err(err.into());
                    }
                }

                // Will retry with new credentials after timeout
                let elapsed = time.elapsed();
                if elapsed < INVALID_PASSWORD_WAIT_TIME {
                    glib::timeout_future(INVALID_PASSWORD_WAIT_TIME - elapsed).await;
                }
            }

            Err(ErrorKind::Disconnected
                .with_message(gettext("Maximum connection retries exhausted")))
        }

        /// Claim a new session designated to the specified (`mailbox`)[MailboxName]
        async fn claim_session(
            &self,
            mailbox: Option<MailboxName>,
            priority: pool::Priority,
        ) -> crate::Result<SessionClaim> {
            // Wait for a session to be free
            let mut session = self
                .session_pool
                .claim_session(mailbox.clone(), priority)
                .await?;

            // Connect and authenticate
            self.connect_retry(&mut session).await?;

            // Now select the mailbox
            session.select_mailbox(mailbox).await?;

            if session.is_new_selection() {
                self.new_selection(session.selection().as_deref()).await?;
            }

            Ok(session)
        }

        /// Run the callback with a newly aquired session
        pub(super) async fn with_session<F, R>(
            &self,
            name: String,
            mailbox_id: Option<MailboxId>,
            priority: pool::Priority,
            func: F,
        ) -> crate::Result<R>
        where
            F: AsyncFnOnce(SessionClaim) -> crate::Result<R>,
        {
            let mailbox_name = if let Some(mailbox_id) = mailbox_id {
                Some(
                    self.cache()
                        .imap_mailbox(mailbox_id)
                        .map(|m| m.imap_name())
                        .ok_or(ClientError::Inconsistency(format!(
                            "Invalid backend: Mailbox {mailbox_id:?} not found"
                        )))?,
                )
            } else {
                None
            };

            if !self.work_online.get() {
                return Err(crate::ErrorKind::Disconnected.into());
            }

            let task = model::Task::new(name);
            self.emit_new_task(&task);

            let session = self.claim_session(mailbox_name, priority).await?;
            if priority > pool::Priority::Idle {
                task.set_status(TaskStatus::Running);
            } else {
                task.set_status(TaskStatus::Idle);
            }

            let mut fut = Box::pin(func(session)).fuse();
            let mut abort_future = task.cancellable().future().fuse();

            let result = futures::select! {
               res = fut => res,
               _ = abort_future => Err(ErrorKind::Aborted.into()),
            };

            if let Err(err) = &result {
                if task.cancellable().is_cancelled() {
                    task.set_status(TaskStatus::Aborted);
                } else if self.connection_status.borrow().can_schedule_tasks() {
                    task.set_status(TaskStatus::from(err))
                } else {
                    task.set_status(TaskStatus::ConnectionError(
                        self.connection_status.borrow().clone(),
                    ));
                }
            } else {
                task.set_status(TaskStatus::Completed);
            }

            result
        }
    }
}

glib::wrapper! {
    pub(crate) struct ImapService(ObjectSubclass<imp::ImapService>)
    @implements gio::AsyncInitable, IncomingMailService;
}

/// Signals
impl ImapService {
    /// Create a new Backend via gio::AsyncInitable
    pub(crate) async fn new(
        account: crate::account::Account,
        cache: Cache,
    ) -> std::result::Result<Self, glib::Error> {
        gio::AsyncInitable::builder()
            .property("account", account)
            .property("cache", cache)
            .build_future(glib::Priority::DEFAULT)
            .await
    }
}

impl ImapService {}

impl std::default::Default for ImapService {
    fn default() -> Self {
        glib::Object::new()
    }
}
