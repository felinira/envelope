// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod authenticated;
mod idle;
mod preauth;

pub(super) use authenticated::Client;
use idle::*;

use super::error::ClientResult;

pub(crate) async fn from_account(
    account: crate::account::Account,
    unsolicited_sender: Option<async_channel::Sender<crate::imap::UnsolicitedResponse>>,
) -> ClientResult<Client> {
    Client::from_account(account, unsolicited_sender).await
}
