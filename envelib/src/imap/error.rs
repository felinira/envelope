// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use enumset::EnumSet;
use imap_client::tasks::tasks::TaskError;

use crate::{error::ErrorKindExt, types::ConnectionStatus};

use super::{TryCollectUidRangeError, Utf7DecodeError};

#[derive(Debug, thiserror::Error)]
pub enum ClientError {
    #[error("The operation was aborted")]
    Aborted,
    #[error("Account configuration: {0}")]
    AccountConfig(#[from] crate::account::AccountConfigError),
    #[error("Disconnected")]
    Disconnected,
    #[error("Authentication error: {message}")]
    Authenticate {
        secret: Option<crate::account::Secret>,
        server_mechanisms: EnumSet<crate::account::AuthMethod>,
        message: String,
    },
    #[error("IMAP: {0}")]
    ImapClient(#[source] imap_client::client::tokio::ClientError),
    #[error("IMAP validation: {0}")]
    ImapValidation(#[from] imap_client::imap_types::error::ValidationError),
    #[error("Task Error: {0}")]
    ImapTaskError(#[from] imap_client::tasks::tasks::TaskError),
    #[error("Unexpected NO response: {0}")]
    No(String),
    #[error("Stream: {0}")]
    Stream(#[from] imap_next::stream::Error<imap_next::client::Error>),
    #[error("Protocol: {0}")]
    Protocol(#[from] imap_client::stream::Error<imap_client::imap_next::client::Error>),
    #[error("Date conversion Error: {0}")]
    DateConversion(#[from] imap_client::imap_types::datetime::error::NaiveDateError),
    #[error("Unexpected end of stream, expected '{0}'")]
    EndOfStream(String),
    #[error("I/O Error: {0}")]
    Io(#[from] std::io::Error),
    #[error("An internal inconsistency has occurred. {0}")]
    Inconsistency(String),
    #[error("The connection is too insecure to continue: {0}")]
    Insecure(String),
    #[error("Operation not supported: {0}")]
    Unsupported(String),
    #[error("The '{0}' field is missing in the response.")]
    MissingField(String),
    #[error("Wrong Mailbox selected: {0} (expected {1})")]
    SelectedWrongMailbox(String, String),
    #[error("Client Error: No mailbox has been selected")]
    NoMailboxSelected,
    #[error("Error decoding mailbox as valid UTF-7: {0}")]
    Utf7(#[from] Utf7DecodeError),
    #[error("Error parsing the BODY section as valid RFC 2822 message")]
    BodyParseError,
    #[error("TLS Error: {0}")]
    Tls(#[from] async_native_tls::Error),
    #[error("Date parse error: {0}")]
    Time(#[from] time::error::ComponentRange),
    #[error("Timed Out")]
    TimedOut,
    #[error("Session not available")]
    IdleSessionTaken,
    #[error("Internal Inconsistency: Wrong mailbox selected ({:?})", .0.selected)]
    TryCollectUidRangeError(#[from] TryCollectUidRangeError),
}

pub(super) type ClientResult<T> = Result<T, ClientError>;

impl From<imap_client::client::tokio::ClientError> for ClientError {
    fn from(value: imap_client::client::tokio::ClientError) -> Self {
        match value {
            imap_client::client::tokio::ClientError::ResolveTask(
                TaskError::UnexpectedNoResponse(err),
            ) => Self::No(format!("{err:?}")),
            imap_client::client::tokio::ClientError::ResolveTask(task_error) => {
                Self::ImapTaskError(task_error)
            }
            value => Self::ImapClient(value),
        }
    }
}

impl From<ClientError> for crate::Error {
    fn from(value: ClientError) -> Self {
        match value {
            ClientError::Aborted => crate::ErrorKind::Aborted.into(),
            ClientError::AccountConfig(_) => crate::ErrorKind::AccountConfig.with_source(value),
            ClientError::Disconnected => crate::ErrorKind::Disconnected.with_source(value),
            // General connection errors
            ClientError::Authenticate { .. }
            | ClientError::ImapClient(_)
            | ClientError::ImapValidation(_)
            | ClientError::DateConversion(_)
            | ClientError::ImapTaskError(_)
            | ClientError::No(_)
            | ClientError::Stream(_)
            | ClientError::Protocol(_)
            | ClientError::Inconsistency(_)
            | ClientError::TryCollectUidRangeError(_)
            | ClientError::Insecure(_)
            | ClientError::Unsupported(_)
            | ClientError::MissingField(_)
            | ClientError::SelectedWrongMailbox(_, _)
            | ClientError::NoMailboxSelected
            | ClientError::Utf7(_)
            | ClientError::BodyParseError
            | ClientError::Time(_) => crate::ErrorKind::Connection.with_source(value),
            // Socket errors
            ClientError::Tls(_)
            | ClientError::Io(_)
            | ClientError::TimedOut
            | ClientError::EndOfStream(_) => crate::ErrorKind::Connection.with_source(value),
            ClientError::IdleSessionTaken => {
                crate::ErrorKind::Inconsistency.with_message(value.to_string())
            }
        }
    }
}

impl From<&ClientError> for ConnectionStatus {
    fn from(value: &ClientError) -> Self {
        match value {
            ClientError::Disconnected => ConnectionStatus::Disconnected,
            // TODO: Should this just be "Ok" because it's just been aborted by user action?
            ClientError::Aborted => ConnectionStatus::Error("aborted".to_string()),
            ClientError::TimedOut => ConnectionStatus::Unreachable,
            ClientError::Tls(error) => ConnectionStatus::TlsError(error.to_string()),
            ClientError::Authenticate { message, .. } => {
                ConnectionStatus::AuthenticationFailed(message.clone())
            }
            err => ConnectionStatus::Error(err.to_string()),
        }
    }
}

impl ClientError {
    pub fn is_aborted(&self) -> bool {
        matches!(self, ClientError::Aborted)
    }
}
