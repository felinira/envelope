// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::cell::{Cell, RefCell};
use std::ops::AsyncFnOnce;
use std::rc::Rc;
use std::sync::Arc;
use std::sync::atomic::AtomicUsize;

use super::client::Client;
use super::error::{ClientError, ClientResult};
use super::types::SelectKind;
use crate::imap::{MailboxName, MailboxSelection, UnsolicitedResponse};
use crate::utils::DisplayLog;

use futures::Future;
use gio::prelude::*;
use smol::lock::Mutex;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SessionId(usize);

impl Default for SessionId {
    fn default() -> Self {
        Self::new()
    }
}

impl SessionId {
    pub fn new() -> Self {
        static NEXT: AtomicUsize = AtomicUsize::new(1);
        let next = NEXT.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
        Self(next)
    }
}

#[derive(Debug)]
pub(crate) struct SessionClient {
    session_id: SessionId,
    client: Option<Client>,
    unsolicited_response_sender: async_channel::Sender<UnsolicitedResponse>,
    shutdown: bool,
}

impl SessionClient {
    pub fn new(
        session_id: SessionId,
        unsolicited_response_sender: async_channel::Sender<UnsolicitedResponse>,
    ) -> Self {
        Self {
            session_id,
            client: None,
            unsolicited_response_sender,
            shutdown: false,
        }
    }

    pub async fn shutdown(&mut self) -> ClientResult<()> {
        self.shutdown = true;

        if let Some(mut session) = self.client.take() {
            log::debug!("Shutdown session {:?}", self.session_id);
            session.shutdown().await?;
        }

        Ok(())
    }

    pub async fn connect(
        &mut self,
        account: &crate::account::Account,
    ) -> ClientResult<&mut Client> {
        if self.shutdown {
            return Err(ClientError::Disconnected);
        }

        log::info!(
            "Spawn IMAP session for account {:?} with id: {:?}",
            account.id,
            self.session_id
        );

        let client = super::client::from_account(
            account.clone(),
            Some(self.unsolicited_response_sender.clone()),
        )
        .await?;

        self.client = Some(client);
        Ok(self.client.as_mut().unwrap())
    }

    pub fn client_mut(&mut self) -> Option<&mut Client> {
        self.client.as_mut()
    }

    pub fn take_client(&mut self) -> Option<Client> {
        self.client.take()
    }

    pub fn return_client(&mut self, client: Client) {
        assert!(self.client.is_none());
        self.client.replace(client);
    }
}

pub(crate) struct Session {
    account: crate::account::Account,
    session_id: SessionId,
    selection: RefCell<Option<Arc<MailboxSelection>>>,
    mailbox_reservation: RefCell<Option<MailboxName>>,
    inner: Arc<Mutex<SessionClient>>,
    claimed: Cell<bool>,
    idle_cancellable: RefCell<Option<gio::Cancellable>>,
}

impl Session {
    pub fn new(
        account: crate::account::Account,
        unsolicited_response_sender: async_channel::Sender<UnsolicitedResponse>,
    ) -> Self {
        let id = SessionId::new();
        let client = Arc::new(Mutex::new(SessionClient::new(
            id.clone(),
            unsolicited_response_sender,
        )));

        Self {
            account,
            session_id: id,
            selection: Default::default(),
            mailbox_reservation: Default::default(),
            inner: client,
            claimed: Default::default(),
            idle_cancellable: Default::default(),
        }
    }

    pub async fn shutdown(&self) -> ClientResult<()> {
        self.idle_abort();
        self.inner.lock().await.shutdown().await
    }

    pub fn selection(&self) -> Option<Arc<MailboxSelection>> {
        self.selection.borrow().clone()
    }

    pub fn is_reserved_for_mailbox(&self, mailbox: &MailboxName) -> bool {
        self.mailbox_reservation
            .borrow()
            .as_ref()
            .is_some_and(|mailbox_name| mailbox_name == mailbox)
    }

    pub fn is_reserved_for_inbox(&self) -> bool {
        self.mailbox_reservation
            .borrow()
            .as_ref()
            .is_some_and(|mailbox_name| mailbox_name.is_inbox())
    }

    pub fn reserve_for(&self, mailbox: Option<MailboxName>) {
        self.mailbox_reservation.replace(mailbox);
    }

    pub fn is_claimed(&self) -> bool {
        self.claimed.get()
    }

    pub fn idle_free(&self) {
        if self
            .idle_cancellable
            .borrow()
            .as_ref()
            .is_some_and(|c| c.is_cancelled())
        {
            self.idle_cancellable.take();
        }
    }

    /// Immediately marks the session as ready for idle.
    pub fn set_idle_cancellable(&self, cancellable: gio::Cancellable) {
        self.idle_cancellable.replace(Some(cancellable));
    }

    pub fn idle_abort(&self) {
        self.idle_cancellable
            .borrow()
            .as_ref()
            .inspect(|c| c.cancel());
    }

    pub fn is_idle(&self) -> bool {
        self.idle_cancellable.borrow().is_some()
    }

    pub fn claim(self: Rc<Self>, drop_callback: Option<SessionClaimDropCallback>) -> SessionClaim {
        assert!(!self.claimed.get());
        self.claimed.set(true);

        log::debug!("Claiming session {:?}", self.session_id,);

        SessionClaim {
            session: self.clone(),
            time: std::time::Instant::now(),
            drop_callback,
            is_new_selection: false,
        }
    }

    pub fn id(&self) -> SessionId {
        self.session_id.clone()
    }
}

type SessionClaimDropCallback = Box<dyn FnOnce(Rc<Session>)>;

pub(crate) struct SessionClaim {
    session: Rc<Session>,
    drop_callback: Option<SessionClaimDropCallback>,
    time: std::time::Instant,
    is_new_selection: bool,
}

impl SessionClaim {
    pub fn id(&self) -> SessionId {
        self.session.session_id.clone()
    }

    pub fn session(&self) -> Rc<Session> {
        self.session.clone()
    }

    pub fn time(&self) -> &std::time::Instant {
        &self.time
    }

    pub fn selection(&self) -> Option<Arc<MailboxSelection>> {
        self.session.selection.borrow().clone()
    }

    pub fn mailbox(&self) -> Option<MailboxName> {
        self.session
            .selection
            .borrow()
            .as_ref()
            .map(|s| s.mailbox_name.clone())
    }

    pub fn expect_selection(&self) -> ClientResult<Arc<MailboxSelection>> {
        self.selection().ok_or(ClientError::NoMailboxSelected)
    }

    pub async fn connect(&mut self) -> ClientResult<()> {
        let account = self.session.account.clone();
        let lock_fut = self.session.inner.lock_arc();

        let mut lock = lock_fut.await;
        if lock.client.is_none() {
            lock.connect(&account).await?;
        }

        Ok(())
    }

    pub async fn select_mailbox(&mut self, mailbox: Option<MailboxName>) -> ClientResult<bool> {
        let result = self
            .with_imap_client(async move |client| {
                // We check the selection of the connection here to sync it with ours
                let current_selection = client.selection();

                // Select the mailbox if needed
                if mailbox.as_ref() != current_selection.as_ref().map(|s| &s.mailbox_name) {
                    log::info!("Selecting mailbox: {:?}", mailbox);
                    let selection = client
                        .change_selection(mailbox.map(|m| SelectKind::select(m.clone())).as_ref())
                        .await?;
                    Ok((selection, true))
                } else {
                    Ok((current_selection, false))
                }
            })
            .await;

        match result {
            Ok((selection, new)) => {
                self.session.selection.replace(selection.clone());
                self.is_new_selection = new;
                Ok(new)
            }
            Err(err) => {
                self.session.selection.take();
                Err(err)
            }
        }
    }

    pub fn with_imap_client<F, R>(&self, f: F) -> impl Future<Output = ClientResult<R>> + use<F, R>
    where
        F: AsyncFnOnce(&mut Client) -> ClientResult<R>,
    {
        let account = self.session.account.clone();
        let lock_fut = self.session.inner.lock_arc();

        // This future does not borrow &self
        async move {
            let mut lock = lock_fut.await;
            let client = if let Some(session) = lock.client_mut() {
                session
            } else {
                lock.connect(&account).await?
            };

            f(&mut *client).await
        }
    }

    pub fn exec<F, R: Send + 'static>(
        &self,
        f: F,
    ) -> impl Future<Output = ClientResult<R>> + use<F, R>
    where
        F: AsyncFnOnce(&mut Client) -> ClientResult<R> + Send + 'static,
        // TODO: This is not ideal
        for<'a> <F as AsyncFnOnce<(&'a mut Client,)>>::CallOnceFuture: Send,
    {
        let account = self.session.account.clone();

        let lock_fut = self.session.inner.lock_arc();

        smol::spawn(async move {
            let mut lock = lock_fut.await;

            // We take the client by value, to ensure that if the future drops we force a reconnect
            let mut client = if let Some(session) = lock.take_client() {
                session
            } else {
                lock.connect(&account).await?;
                lock.take_client().unwrap()
            };

            let res = f(&mut client).await;
            lock.return_client(client);
            res
        })
    }

    pub async fn idle_done(&self) {
        self.session().idle_abort();
    }

    pub fn is_new_selection(&self) -> bool {
        self.is_new_selection
    }
}

impl Drop for SessionClaim {
    fn drop(&mut self) {
        let elapsed = self.time.elapsed();

        let level = if elapsed.as_secs() >= 1 {
            log::Level::Info
        } else {
            log::Level::Debug
        };

        if let Some(mailbox) = self.mailbox() {
            log::log!(
                level,
                "Releasing session {:?} for '{}' after {}",
                self.id(),
                mailbox,
                elapsed.display()
            );
        } else {
            log::log!(
                level,
                "Releasing session {:?} after {}",
                self.id(),
                elapsed.display()
            );
        }

        self.session.claimed.set(false);
        if let Some(c) = self.drop_callback.take() {
            c(self.session())
        }
    }
}
