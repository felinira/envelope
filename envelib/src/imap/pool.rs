// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use futures::{Future, FutureExt};
use gio::prelude::*;
use gio::subclass::prelude::*;
use glib::subclass::Signal;
use glib::{SignalHandlerId, closure_local};
use std::sync::LazyLock;
use std::{
    cell::OnceCell,
    collections::{BinaryHeap, HashMap},
    marker::PhantomData,
};
use std::{
    cell::{Cell, RefCell},
    rc::Rc,
};

use super::error::{ClientError, ClientResult};
use super::session::*;
use crate::imap::{MailboxName, UnsolicitedResponse};
use crate::utils::DisplayLog;

const MAX_SESSIONS: usize = 3;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default)]
pub(crate) enum Priority {
    /// A Special priority. Claims will only be answered when a session is currently doing nothing (not even IDLE)
    Idle,
    Low,
    #[default]
    Normal,
    Important,
}

impl Priority {
    const ORDER: [Priority; 4] = [
        Priority::Important,
        Priority::Normal,
        Priority::Low,
        Priority::Idle,
    ];
}

#[derive(Debug, Clone)]
pub(crate) struct QueueItem {
    priority: Priority,
    time: std::time::Instant,
    sender: async_channel::Sender<SessionClaim>,
}

impl Eq for QueueItem {}
impl PartialEq for QueueItem {
    fn eq(&self, other: &Self) -> bool {
        self.priority == other.priority && self.time == other.time
    }
}

impl Ord for QueueItem {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        match self.priority.cmp(&other.priority) {
            core::cmp::Ordering::Equal => {}
            ord => return ord,
        }
        // The oldest item wins
        self.time.cmp(&other.time).reverse()
    }
}

impl PartialOrd for QueueItem {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl QueueItem {
    fn new(priority: Priority, sender: async_channel::Sender<SessionClaim>) -> Self {
        Self {
            priority,
            time: std::time::Instant::now(),
            sender,
        }
    }
}

mod imp {
    use super::*;

    #[derive(Default, glib::Properties)]
    #[properties(wrapper_type = super::SessionPool)]
    pub(crate) struct SessionPool {
        #[property(get, set)]
        account: RefCell<Option<crate::account::Account>>,

        /// The available IMAP sessions. Will not exceed MAX_SESSIONS.
        sessions: RefCell<Vec<Rc<Session>>>,
        /// Waiting for session
        queue: RefCell<HashMap<Option<MailboxName>, BinaryHeap<QueueItem>>>,

        unsolicited_response_sender: OnceCell<async_channel::Sender<UnsolicitedResponse>>,
        unsolicited_response_receiver:
            RefCell<Option<async_channel::Receiver<UnsolicitedResponse>>>,

        #[property(get)]
        pub(super) session_count: Cell<u32>,
        #[property(get, set)]
        pub(super) session_claimed_count: Cell<u32>,
        #[property(get = Self::session_wait_count)]
        pub(super) session_wait_count: PhantomData<u32>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for SessionPool {
        const NAME: &'static str = "EnvelopeImapSessionPool";
        type Type = super::SessionPool;
    }

    #[glib::derived_properties]
    impl ObjectImpl for SessionPool {
        fn constructed(&self) {
            self.parent_constructed();

            let (sender, receiver) = async_channel::unbounded();
            self.unsolicited_response_sender.set(sender).unwrap();
            self.unsolicited_response_receiver.replace(Some(receiver));
        }

        fn signals() -> &'static [Signal] {
            static SIGNALS: LazyLock<Vec<Signal>> = LazyLock::new(|| {
                vec![
                    Signal::builder("session-idle")
                        .param_types([MailboxName::static_type(), gio::Cancellable::static_type()])
                        .build(),
                ]
            });
            SIGNALS.as_ref()
        }
    }

    impl SessionPool {
        fn emit_session_idle(&self, mailbox_name: &MailboxName, cancellable: &gio::Cancellable) {
            self.obj()
                .emit_by_name::<()>("session-idle", &[mailbox_name, cancellable]);
        }

        fn session_wait_count(&self) -> u32 {
            self.queue.borrow().values().map(|q| q.len()).sum::<usize>() as u32
        }

        fn session_drop_callback(&self, session: Rc<Session>) {
            self.obj()
                .set_session_claimed_count(self.obj().session_claimed_count() - 1);
            self.free_session(session);
        }

        fn do_claim_session(
            &self,
            session: Rc<Session>,
            mailbox: Option<MailboxName>,
        ) -> SessionClaim {
            assert!(!session.is_claimed());
            session.reserve_for(mailbox);

            self.session_claimed_count
                .set(self.session_claimed_count.get() + 1);
            let obj = self.obj().downgrade();
            session.claim(Some(Box::new(move |session| {
                if let Some(pool) = obj.upgrade() {
                    pool.imp().session_drop_callback(session);
                }
            })))
        }

        /// Create a new session if we are allowed more sessions and put it in the list
        fn new_session_if_allowed(&self) -> Option<Rc<Session>> {
            if self.sessions.borrow().len() < MAX_SESSIONS {
                let session = Rc::new(Session::new(
                    self.obj().account().expect("Account must be set"),
                    self.unsolicited_response_sender.get().unwrap().clone(),
                ));
                self.sessions.borrow_mut().push(session.clone());
                self.session_count.set(self.sessions.borrow().len() as u32);
                self.obj().notify_session_count();
                Some(session)
            } else {
                None
            }
        }

        /// This selects the next best and available session for the specified mailbox.
        ///
        /// # Returns
        ///
        /// The best available session to use for the mailbox, or None if we need to wait
        ///
        /// # Rules
        ///
        /// - IDLE sessions are treated as unused and will be canceled
        /// - Never unselect INBOX unless we are only allowed one session in total
        /// - Never select a mailbox twice. Some servers don't like it, and even if this weren't
        ///   an issue, we might get stalled by our own poor session management
        /// - If we already have a session running on this mailbox, select it
        /// - If that one is busy, wait
        /// - If we don't have a session for this mailbox, we choose any free session
        /// - If there isn't any we create a new one if we haven't reached MAX_SESSIONS
        fn try_claim_session_for_mailbox(
            &self,
            mailbox: Option<MailboxName>,
            priority: Priority,
        ) -> Option<SessionClaim> {
            let mut free = None;

            // This isn't slow, it's not like we ever have many sessions
            for session in &*self.sessions.borrow() {
                // This is our preferred session
                // We take it or leave it
                if mailbox
                    .as_ref()
                    .is_some_and(|m| session.is_reserved_for_mailbox(m))
                {
                    if !session.is_claimed() {
                        // Session already has the right mailbox selected and is free
                        return Some(self.do_claim_session(session.clone(), mailbox));
                    } else if session.is_idle() && priority != Priority::Idle {
                        // We can't have the session right away but by aborting idle we can get it very soon
                        session.idle_abort();
                    }

                    return None;
                }

                // Only select a session IF:
                // * It's free AND
                // * If it's an INBOX session IF EITHER:
                // ** We are only allowed one connection OR
                // ** We want INBOX
                //
                // This effectively prevents reuse of the INBOX session for anything else unless it's unavoidable
                if free.is_none()
                    && (!session.is_claimed() || (session.is_idle()))
                    && (MAX_SESSIONS <= 1
                        || !mailbox.as_ref().is_some_and(|m| m.is_inbox())
                        || !session.is_reserved_for_inbox())
                {
                    // This is just the first free session that fits all criteria
                    free = Some(session.clone());
                }
            }

            // We only take existing sessions that already have the desired mailbox for Idle priority
            if priority == Priority::Idle {
                return None;
            }

            if let Some(free) = free {
                if free.is_idle() {
                    free.idle_abort();
                    return None;
                }

                Some(free.clone())
            } else {
                self.new_session_if_allowed()
            }
            .map(|s| self.do_claim_session(s, mailbox))
        }

        /// Only claim one session per mailbox
        pub(super) fn claim_session(
            &self,
            mailbox: Option<MailboxName>,
            priority: Priority,
        ) -> impl Future<Output = ClientResult<SessionClaim>> + '_ {
            let claim = self.try_claim_session_for_mailbox(mailbox.clone(), priority);

            async move {
                let session_claim = if let Some(session) = claim {
                    log::debug!(
                        "Aquired {:?} for mailbox {:?}, priority {:?}",
                        session.id(),
                        mailbox,
                        priority
                    );
                    session
                } else {
                    if priority == Priority::Idle {
                        return Err(ClientError::Aborted);
                    }

                    // Keep track of how long we waited for the logs
                    let start_wait = std::time::Instant::now();
                    log::trace!(
                        "Aquire session for mailbox {:?}: All sessions occupied, waiting",
                        mailbox
                    );

                    let (sender, receiver) = async_channel::bounded(1);
                    self.queue
                        .borrow_mut()
                        .entry(mailbox.clone())
                        .or_default()
                        .push(QueueItem::new(priority, sender));
                    self.obj().notify_session_wait_count();

                    let mut warning_count = 0;

                    // Periodically wake up to be able to re-asses the situation
                    let session_claim = loop {
                        let session_claim = futures::select_biased! {
                        res = receiver.recv().fuse() => res.map(Some),
                        _ = glib::timeout_future(std::time::Duration::from_secs(10)).fuse() => Ok(None)
                    }
                    .map_err(|_| ClientError::Disconnected)?;

                        if let Some(session_claim) = session_claim {
                            break session_claim;
                        } else {
                            let elapsed = start_wait.elapsed();
                            if elapsed.as_secs() % 10 == 0 && elapsed.as_secs() / 10 > warning_count
                            {
                                warning_count += 1;
                                log::warn!(
                                    "Potential session stall: {} seconds elapsed waiting for session.",
                                    elapsed.as_secs(),
                                );
                            }
                        }
                    };

                    log::debug!(
                        "Aquired {:?} for mailbox {:?} in {}, priority {:?}",
                        session_claim.id(),
                        mailbox,
                        start_wait.elapsed().display(),
                        priority
                    );

                    session_claim
                };

                self.obj().notify_session_count();
                Ok(session_claim)
            }
        }

        /// Return a session to the session pool
        pub fn free_session(&self, session: Rc<Session>) {
            if session.is_idle() {
                // If the idle session was cancelled, remove the idle cancellable
                session.idle_free();
            }

            // Now that we have the session back its mailbox could have changed.
            // Update the mailbox reservation with the currently selected mailbox
            let mailbox = session.selection().map(|s| s.mailbox_name.clone());

            // Check if we have something else waiting on the session
            let mut queue = self.queue.borrow_mut();

            // We go through the queue and check if any task can now claim a session (this one)
            if let Some(item) = queue.get_mut(&mailbox).and_then(|q| q.pop()) {
                // A session with the same mailbox has been waiting. This gets priority.
                let claim = self.do_claim_session(session, mailbox);
                drop(queue);
                let _ = item.sender.try_send(claim);
                self.obj().notify_session_wait_count();
                return;
            }

            if !queue.is_empty() {
                for prio in Priority::ORDER {
                    for (mailbox, q) in queue.iter_mut() {
                        if q.peek().is_some_and(|p| p.priority == prio) {
                            if let Some(item) = q.pop() {
                                let claim = self.do_claim_session(session, mailbox.clone());
                                drop(queue);
                                let _ = item.sender.try_send(claim);
                                self.obj().notify_session_wait_count();
                                return;
                            }
                        }
                    }
                }
            }

            drop(queue);

            // We will notify anyone who wants that we now have a free session on the mailbox
            // This can be used to fetch messages in background, IDLE etc. The callee is
            // required to listen to the cancellable and act on its cancelled signal.
            if let Some(mailbox) = mailbox {
                // Only do this once per session, otherwise we might end up in an endless loop
                if !session.is_idle() {
                    let cancellable = gio::Cancellable::new();
                    session.set_idle_cancellable(cancellable.clone());
                    self.emit_session_idle(&mailbox, &cancellable);
                }
            }
        }

        pub(super) async fn shutdown(&self) {
            for session in self.sessions.take() {
                session.idle_abort();
                match glib::future_with_timeout(
                    std::time::Duration::from_secs(5),
                    session.shutdown(),
                )
                .await
                {
                    Ok(Ok(())) => {}
                    Ok(Err(err)) => {
                        log::error!("Error when shutting down: {err}");
                    }
                    Err(_timeout) => {
                        log::warn!(
                            "Session {:?} didn't stop in 5 seconds. Giving up.",
                            session.id()
                        );
                    }
                }
            }
        }

        pub(super) fn take_unsolicited_response_receiver(
            &self,
        ) -> Option<async_channel::Receiver<UnsolicitedResponse>> {
            self.unsolicited_response_receiver.take()
        }
    }
}

glib::wrapper! {
    pub(crate) struct SessionPool(ObjectSubclass<imp::SessionPool>);
}

impl SessionPool {
    pub async fn claim_session(
        &self,
        mailbox: Option<MailboxName>,
        priority: Priority,
    ) -> ClientResult<SessionClaim> {
        self.imp().claim_session(mailbox, priority).await
    }

    pub fn connect_session_idle<F>(&self, callback: F) -> SignalHandlerId
    where
        F: Fn(MailboxName, gio::Cancellable) + 'static,
    {
        self.connect_closure(
            "session-idle",
            false,
            closure_local!(
                |_: Self, mailbox: MailboxName, cancellable: gio::Cancellable| callback(
                    mailbox,
                    cancellable
                )
            ),
        )
    }

    pub async fn shutdown(&self) {
        self.imp().shutdown().await
    }

    pub fn take_unsolicited_response_receiver(
        &self,
    ) -> Option<async_channel::Receiver<UnsolicitedResponse>> {
        self.imp().take_unsolicited_response_receiver()
    }
}

impl std::default::Default for SessionPool {
    fn default() -> Self {
        glib::Object::new()
    }
}
