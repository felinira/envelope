// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use async_compat::CompatExt;
use imap_client::imap_types::core::Tag;

use crate::imap::IdleResponse;

use super::{ClientResult, authenticated::Authenticated};

#[must_use]
pub(crate) struct IdleHandle<'a> {
    pub(super) tag: Tag<'static>,
    pub(super) session: &'a mut Authenticated<imap_client::client::tokio::Client>,
    pub(super) done: bool,
}

impl<'a> IdleHandle<'a> {
    pub(crate) fn new(session: &'a mut Authenticated<imap_client::client::tokio::Client>) -> Self {
        let tag = session.client.enqueue_idle();

        IdleHandle {
            tag,
            session,
            done: false,
        }
    }

    pub(crate) async fn idle(&mut self) -> ClientResult<IdleResponse> {
        let tag = self.tag.clone();
        self.session.client.idle(tag).compat().await?;
        self.done = true;

        // TODO: Actually implement IDLE
        Ok(IdleResponse::Timeout)
    }

    pub(crate) async fn done(mut self) -> ClientResult<()> {
        if !self.done {
            self.session
                .client
                .idle_done(self.tag.clone())
                .compat()
                .await?;
            self.done = true;
        }

        Ok(())
    }
}

impl Drop for IdleHandle<'_> {
    fn drop(&mut self) {
        if !self.done {
            smol::block_on(async {
                log::warn!("Idle handle dropped without manually stopping. Shutting down session");
                if let Err(err) = self
                    .session
                    .client
                    .idle_done(self.tag.clone())
                    .compat()
                    .await
                {
                    log::error!("{err:?}");
                    let _ = self.session.shutdown().await;
                }
            });
        }
    }
}
