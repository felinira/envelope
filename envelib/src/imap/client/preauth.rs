// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use async_compat::CompatExt;
use imap_client::tasks::tasks::TaskError;

use crate::{
    account::Secret,
    gettext::gettextf,
    imap::{
        Capabilities, Standard, UnsolicitedResponse,
        error::{ClientError, ClientResult},
    },
};

use super::authenticated::Authenticated;

pub(super) struct Preauth<C> {
    pub client: C,
    pub account: crate::account::Account,
    pub service: crate::account::Service,
    pub capabilities: Option<crate::imap::Capabilities>,
}

impl Preauth<imap_client::client::tokio::Client> {
    pub async fn new(account: crate::account::Account) -> ClientResult<Self> {
        let imap = account.imap()?.clone();
        log::debug!("Connecting to {}:{}", imap.server, imap.port);

        let client = imap_client::client::tokio::Client::native_tls(
            &imap.server,
            imap.port,
            imap.connection_type.is_starttls(),
        )
        .compat()
        .await?;

        let capabilities = Some(client.state.capabilities().as_ref().iter().collect());

        Ok(Self {
            client,
            account,
            service: imap,
            capabilities,
        })
    }

    pub async fn authenticate(
        mut self,
        unsolicited_sender: Option<async_channel::Sender<UnsolicitedResponse>>,
    ) -> ClientResult<Authenticated<imap_client::client::tokio::Client>> {
        // TODO: OAUTH

        match &self.service.secret {
            Secret::Basic(basic) => {
                self.client
                    .authenticate_plain(basic.user.clone(), basic.password.clone())
                    .compat()
                    .await
            }
            Secret::None => {
                // Missing credentials
                return Err(ClientError::Authenticate {
                    secret: None,
                    server_mechanisms: self
                        .capabilities
                        .as_ref()
                        .map(|c| c.supported_authentication_methods())
                        .unwrap_or_default(),
                    message: gettextf(
                        "IMAP credentials missing for “{}” on server “{}”",
                        &[
                            &self.account.identity().address,
                            &format!("{}:{}", self.service.server, self.service.port),
                        ],
                    ),
                });
            }
        }
        .map_err(|err| match err {
            imap_client::client::tokio::ClientError::ResolveTask(
                TaskError::UnexpectedNoResponse(body),
            ) => {
                log::debug!("IMAP authenticate response: {}", body.text);
                let message = gettextf(
                    "IMAP authentication failed for “{}” on server “{}”",
                    &[
                        &self.account.identity().address,
                        &format!("{}:{}", self.service.server, self.service.port),
                    ],
                );

                ClientError::Authenticate {
                    secret: Some(self.service.secret.clone()),
                    server_mechanisms: self
                        .capabilities
                        .as_ref()
                        .map(|c| c.supported_authentication_methods())
                        .unwrap_or_default(),
                    message,
                }
            }
            err => err.into(),
        })?;

        // TODO: Successful auth, save last used authentication type if it differs
        /*if used_auth_type.is_some_and(|auth| Some(auth) != self.service.auth_method) {
            PersistentConfig::global_update(|config| {
                if let Some(account) = config.account_for_id_mut(&self.account.id) {
                    if let Ok(imap) = account.imap_mut() {
                        imap.auth_method = used_auth_type;
                    }
                }
            });
        }*/

        // Read capabilities
        let capabilities: Capabilities = self.client.state.capabilities().as_ref().iter().collect();
        log::trace!("{:?}", capabilities);

        log::debug!(
            "Established encrypted IMAP connection to {}:{}",
            self.service.server,
            self.service.port
        );

        if !capabilities.is_imap4_standard(Standard::Imap4rev1) {
            log::warn!("Unknown IMAP standard version detected. Here be dragons.");
        }

        Ok(Authenticated {
            client: self.client,
            capabilities,
            account_id: self.account.id,
            mailbox: Default::default(),
            unsolicited_sender,
        })
    }
}
