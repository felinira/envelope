// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::{
    collections::{BTreeMap, HashMap},
    num::NonZero,
    sync::Arc,
};

use async_compat::CompatExt;
use imap_client::imap_types::{
    IntoStatic,
    core::Vec1,
    fetch::{MessageDataItem, MessageDataItemName, Section},
    flag::FlagNameAttribute,
};

use crate::{
    account,
    imap::{
        BodyStructureExt, Capabilities, ImapSearchQuery, MailboxInfo, MailboxName,
        MailboxSelection, MailboxStatus, MessageData, SelectKind, SelectionType, Uid, UidRange,
        UnsolicitedResponse,
        error::{ClientError, ClientResult},
    },
    types::{InternetMessage, MailboxKind, MessageFlag, MessageFlags, MessageFlagsChange},
    utils::StrExt,
};

use super::IdleHandle;

pub struct Authenticated<S> {
    pub(super) client: S,
    pub(super) capabilities: Capabilities,
    pub(super) account_id: account::AccountId,
    pub(super) mailbox: Option<Arc<MailboxSelection>>,
    pub(super) unsolicited_sender: Option<async_channel::Sender<UnsolicitedResponse>>,
}

impl<S> std::fmt::Debug for Authenticated<S> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Authenticated")
            .field("client", &"?")
            .field("capabilities", &self.capabilities)
            .field("account_id", &self.account_id)
            .field("mailbox", &self.mailbox)
            .field("unsolicited_sender", &self.unsolicited_sender)
            .finish()
    }
}

pub(crate) type Client = Authenticated<imap_client::client::tokio::Client>;

impl Client {
    pub(crate) async fn from_account(
        account: account::Account,
        unsolicited_sender: Option<async_channel::Sender<UnsolicitedResponse>>,
    ) -> ClientResult<Self> {
        let preauth =
            super::preauth::Preauth::<imap_client::client::tokio::Client>::new(account.clone())
                .await?;
        let mut authenticated = preauth.authenticate(unsolicited_sender).await?;
        authenticated.enable_capabilities().await?;
        Ok(authenticated)
    }
}

impl<S> Authenticated<S> {
    #[expect(dead_code)]
    pub(super) fn mailbox(&self) -> ClientResult<&MailboxSelection> {
        self.mailbox
            .as_deref()
            .ok_or(ClientError::NoMailboxSelected)
    }

    pub(super) fn _send_unsolicited_response(&self, response: UnsolicitedResponse) {
        if let Some(sender) = &self.unsolicited_sender {
            if let Err(err) = sender.try_send(response) {
                log::error!("Send error when processing unsolicited fetches: {:?}", err);
            }
        }
    }
}

impl Client {
    pub fn try_get_unsolicited_response(&mut self) -> Option<()> {
        None // TODO
    }

    fn process_unsolicited(&mut self) {
        while let Some(_imap_response) = self.try_get_unsolicited_response() {
            /*if let Some(response) = UnsolicitedResponse::try_from_response(
                self.mailbox().map(|m| m.mailbox_name.clone()).ok(),
                imap_response,
            ) {
                self.send_unsolicited_response(response);
            }*/
        }
    }

    fn process_flags<'a>(
        &self,
        result: impl IntoIterator<Item = (NonZero<u32>, Vec1<MessageDataItem<'a>>)>,
    ) -> BTreeMap<Uid, MessageFlags> {
        let mut map = BTreeMap::new();

        for (uid, data) in result {
            let mut flags = MessageFlags::default();

            for item in data {
                match item {
                    MessageDataItem::Uid(_) => {}
                    MessageDataItem::Flags(vec) => {
                        flags.extend(vec.into_iter().map(MessageFlag::from));
                    }
                    other => {
                        // TODO unsolicited data
                        log::debug!("Received unsolicited data: {:?}", other);
                    }
                }
            }

            map.insert(uid.into(), flags);
        }

        map
    }

    fn process_fetch<'a>(
        &self,
        result: impl IntoIterator<Item = (NonZero<u32>, Vec1<MessageDataItem<'a>>)>,
    ) -> ClientResult<BTreeMap<Uid, MessageData>> {
        let mut map = BTreeMap::new();

        for (uid, data_items) in result {
            let mut message_data = MessageData {
                uid: uid.into(),
                thread_id: None,
                internal_date: None,
                flags: None,
                attachment_count: None,
                bodystructure: None,
                message: None,
            };

            for item in data_items {
                match item {
                    MessageDataItem::Body(body) => {
                        message_data.bodystructure = Some(body.into_static());
                    }
                    MessageDataItem::BodyExt { section, data, .. } => {
                        // TODO match sections properly and maybe differentiate
                        if message_data.message.is_none() {
                            if let Some(msg) = data.into_option() {
                                message_data.message = Some(InternetMessage::new(
                                    mail_parser::MessageParser::default()
                                        .parse(&*msg)
                                        .ok_or(ClientError::BodyParseError)?
                                        .into_owned(),
                                    section.is_none(),
                                ));
                            }
                        }
                    }
                    MessageDataItem::BodyStructure(bodystructure) => {
                        // TODO calculate attachment count
                        message_data.bodystructure = Some(bodystructure.into_static());
                    }
                    MessageDataItem::Envelope(_) => {}
                    MessageDataItem::Flags(vec) => {
                        let mut flags = message_data.flags.unwrap_or_default();
                        flags.extend(vec.into_iter().map(MessageFlag::from));
                        message_data.flags = Some(flags);
                    }
                    MessageDataItem::InternalDate(date_time) => {
                        message_data.internal_date =
                            Some(crate::utils::chrono_to_time(date_time.as_ref()));
                    }
                    MessageDataItem::Rfc822Header(nstring) => {
                        if message_data.message.is_none() {
                            if let Some(msg) = nstring.into_option() {
                                message_data.message = Some(InternetMessage::new(
                                    mail_parser::MessageParser::default()
                                        .parse(&*msg)
                                        .ok_or(ClientError::BodyParseError)?
                                        .into_owned(),
                                    false,
                                ));
                            }
                        }
                    }
                    MessageDataItem::Rfc822Text(nstring) => {
                        if message_data.message.is_none() {
                            if let Some(msg) = nstring.into_option() {
                                message_data.message = Some(InternetMessage::new(
                                    mail_parser::MessageParser::default()
                                        .parse(&*msg)
                                        .ok_or(ClientError::BodyParseError)?
                                        .into_owned(),
                                    false,
                                ));
                            }
                        }
                    }
                    MessageDataItem::Rfc822(nstring) => {
                        if let Some(msg) = nstring.into_option() {
                            message_data.message = Some(InternetMessage::new(
                                mail_parser::MessageParser::default()
                                    .parse(&*msg)
                                    .ok_or(ClientError::BodyParseError)?
                                    .into_owned(),
                                false,
                            ));
                        }
                    }
                    MessageDataItem::Rfc822Size(_) => {}
                    MessageDataItem::Uid(_) => {}
                    MessageDataItem::Binary { .. } => {}
                    MessageDataItem::BinarySize { .. } => {}
                }
            }

            map.insert(uid.into(), message_data);
        }

        Ok(map)
    }

    async fn enable_capabilities(&mut self) -> ClientResult<()> {
        let enable = self.capabilities.generate_enable();
        if !enable.is_empty() {
            self.client.enable(enable).compat().await?;
        }

        Ok(())
    }

    /// Return the current [`MailboxSelection`].
    pub fn selection(&self) -> Option<Arc<MailboxSelection>> {
        self.mailbox.clone()
    }

    /// Send a `LIST` command to find out all mailboxes in the account
    ///
    /// Return a [`MailboxInfo`] for each mailbox.
    pub async fn list(&mut self, parent: &str) -> ClientResult<Vec<MailboxInfo>> {
        self.process_unsolicited();

        let mut mailboxes = Vec::new();

        // TODO: Don't use wildcard
        let list = self.client.list(parent, "*").compat().await?;

        for (mailbox, hir, flags) in list {
            let mut kind = MailboxKind::Regular;
            let mut no_inferiors = false;
            let mut selectable = true;
            let mut marked = None;

            for flag in flags {
                match &flag {
                    FlagNameAttribute::Noinferiors => no_inferiors = true,
                    FlagNameAttribute::Noselect => selectable = false,
                    FlagNameAttribute::Marked => marked = Some(true),
                    FlagNameAttribute::Unmarked => marked = Some(false),
                    FlagNameAttribute::Extension(_ext) => {
                        // TODO: Get proper RFC 6154 types in here
                        match &*flag.to_string().to_ascii_lowercase() {
                            "\\all" => kind = MailboxKind::All,
                            "\\archive" => kind = MailboxKind::Archive,
                            "\\drafts" => kind = MailboxKind::Drafts,
                            "\\junk" => kind = MailboxKind::Junk,
                            "\\sent" => kind = MailboxKind::Sent,
                            "\\trash" => kind = MailboxKind::Trash,
                            _ => {}
                        }
                    }
                }
            }

            let name: MailboxName = mailbox.try_into()?;
            if name.is_inbox() {
                kind = MailboxKind::Inbox;
            }

            // Prevent duplicates and empty mailboxes from the server
            if name.as_ref() != parent && !name.as_ref().is_empty() {
                mailboxes.push(MailboxInfo {
                    name,
                    delimiter: hir.map(|hir| hir.inner().to_string()),
                    kind,
                    selectable,
                    no_inferiors,
                    marked,
                });
            }
        }

        Ok(mailboxes)
    }

    #[allow(dead_code)]
    async fn uid_search_count(&mut self, query: ImapSearchQuery) -> ClientResult<u32> {
        // TODO: Use the count method if compliant with RFC 9051 / RFC 4731 ESEARCH extension, it is likely much faster
        self.process_unsolicited();
        let criteria = query
            .0
            .into_iter()
            .map(|k| k.try_into())
            .collect::<Result<Vec<_>, _>>()?;
        Ok(self.client.uid_search(criteria).compat().await?.len() as u32)
    }

    /// `SELECT`, `EXAMINE` or `UNSELECT` a specific mailbox.
    pub async fn change_selection(
        &mut self,
        select: Option<&SelectKind>,
    ) -> ClientResult<Option<Arc<MailboxSelection>>> {
        let Some(select) = select else {
            // TODO proper unselect
            self.client
                .examine("INVALID-MAILBOX-142a57f3-19a1-4ff5-b885-473723c8ed2e")
                .compat()
                .await?;

            self.mailbox = None;
            return Ok(None);
        };

        let mut id = select.mailbox_name.clone();
        if id.as_ref().is_empty() {
            id = MailboxName::from("INBOX");
        }

        // The SELECT command automatically deselects any
        // currently selected mailbox before attempting the new selection.
        // Consequently, if a mailbox is selected and a SELECT command that
        // fails is attempted, no mailbox is selected.
        self.mailbox = None;

        let select_data = match select.selection_type {
            SelectionType::Select => {
                log::trace!("SELECT {id}");
                self.client.select(id.clone()).compat().await?
            }
            SelectionType::Examine => {
                log::trace!("EXAMINE {id}");
                self.client.examine(id.clone()).compat().await?
            }
        };

        let selection = MailboxSelection::from_select(id, select.selection_type, select_data);

        self.mailbox = Some(Arc::new(selection));
        self.process_unsolicited();
        Ok(self.mailbox.clone())
    }

    /// Store [`MessageFlags`] changes via [`MessageFlagsChange`].
    pub async fn store_flags(
        &mut self,
        uids: &dyn UidRange,
        flags: MessageFlags,
        change: MessageFlagsChange,
    ) -> ClientResult<BTreeMap<Uid, MessageFlags>> {
        // Because we can receive unsolicited fetches in-between flush the unsolicited responses first
        self.process_unsolicited();

        let Some(seq) = uids.to_seq() else {
            return Ok(Default::default());
        };

        let result = self
            .client
            .uid_store(seq, change.into(), flags.into_iter().map(Into::into))
            .compat()
            .await?;

        let map = self.process_flags(result);
        self.process_unsolicited();

        Ok(map)
    }

    /// Fetch [`MessageFlags`] in [`messages`](MessageUidRange).
    pub async fn fetch_flags(
        &mut self,
        uids: &dyn UidRange,
    ) -> ClientResult<BTreeMap<Uid, MessageFlags>> {
        // Because we can receive unsolicited fetches in-between flush the unsolicited responses first
        self.process_unsolicited();

        let Some(seq) = uids.to_seq() else {
            return Ok(Default::default());
        };

        let result = self
            .client
            .uid_fetch(seq, vec![MessageDataItemName::Flags].into())
            .compat()
            .await?;

        let map = self.process_flags(result);
        self.process_unsolicited();

        Ok(map)
    }

    /// Move `messages`[MessageUidRange] to [`mailbox`](MailboxName).
    pub async fn move_to(
        &mut self,
        ids: &dyn UidRange,
        destination: &MailboxName,
    ) -> ClientResult<()> {
        let Some(seq) = ids.to_seq() else {
            return Ok(());
        };

        self.client
            .uid_move_or_fallback(seq, destination.encode_utf7())
            .compat()
            .await?;

        self.process_unsolicited();

        Ok(())
    }

    /// Copy [`messages`](MessageUidRange) to [`destination`](MailboxName).
    pub async fn copy_to(
        &mut self,
        ids: &dyn UidRange,
        destination: &MailboxName,
    ) -> ClientResult<()> {
        let Some(seq) = ids.to_seq() else {
            return Ok(());
        };

        self.client
            .uid_copy(seq, destination.encode_utf7())
            .compat()
            .await?;

        self.process_unsolicited();

        Ok(())
    }

    async fn expunge(&mut self, uids: &dyn UidRange) -> ClientResult<()> {
        let Some(_seq) = uids.to_seq() else {
            return Ok(());
        };

        // TODO: More explicit expunge
        // let _msgids: Vec<_> = if self.capabilities.has(Capability::UidPlus) {
        //     log::debug!("UID EXPUNGE");
        //     self.client.expunge(seq).compat().await?
        // } else {
        //     log::debug!("EXPUNGE");
        //     self.client.expunge().await?
        // };

        log::debug!("EXPUNGE");
        self.client.expunge().compat().await?;

        self.process_unsolicited();

        Ok(())
    }

    /// `FETCH` all message headers in the specified [`MessageUidRange`].
    pub async fn fetch_headers(
        &mut self,
        uid_range: &dyn UidRange,
    ) -> ClientResult<BTreeMap<Uid, MessageData>> {
        log::trace!("list_messages: {:?}", uid_range);

        if uid_range.is_empty() {
            return Ok(Default::default());
        }

        // Because we can receive unsolicited fetches in-between flush the unsolicited responses first
        self.process_unsolicited();

        let Some(seq) = uid_range.to_seq() else {
            return Ok(Default::default());
        };

        // (UID FLAGS INTERNALDATE BODYSTRUCTURE BODY.PEEK[HEADER.FIELDS (From To Cc Bcc Subject Date Message-ID References Content-Type In-Reply-To)])
        let query = vec![
            MessageDataItemName::Uid,
            MessageDataItemName::Flags,
            MessageDataItemName::InternalDate,
            MessageDataItemName::BodyStructure,
            MessageDataItemName::BodyExt {
                section: Some(Section::HeaderFields(
                    None,
                    // apart from the References header and the Content-Type this is similar to an ENVELOPE query
                    Vec1::try_from(vec![
                        "From".try_into().unwrap(),
                        "Sender".try_into().unwrap(),
                        "To".try_into().unwrap(),
                        "Cc".try_into().unwrap(),
                        "Bcc".try_into().unwrap(),
                        "Subject".try_into().unwrap(),
                        "Date".try_into().unwrap(),
                        "Message-ID".try_into().unwrap(),
                        "References".try_into().unwrap(),
                        "Content-Type".try_into().unwrap(),
                        "In-Reply-To".try_into().unwrap(),
                    ])
                    .unwrap(),
                )),
                partial: None,
                peek: true,
            },
        ];

        let result = self.client.uid_fetch(seq, query.into()).compat().await?;
        self.process_unsolicited();

        let messages = self.process_fetch(result)?;
        Ok(messages)
    }

    /// Send a `STATUS` command to the mailbox. Does not change selection.
    pub async fn mailbox_status(
        &mut self,
        mailbox_name: &MailboxName,
    ) -> ClientResult<MailboxStatus> {
        let result =
            if let Some(selection) = self.selection().filter(|s| s.mailbox_name == *mailbox_name) {
                // We have this mailbox selected. We send a NOOP to get any new unsolicited responses.
                self.client.noop().compat().await?;

                // Noop will not give us an unread count, but we will find out after via unsolicited responses
                MailboxStatus::from(&*selection)
            } else {
                // We send a status command
                // TODO: STATUS is not yet implemented
                return Err(ClientError::Unsupported("unimplemented".to_string()));
            };

        self.process_unsolicited();
        Ok(result)
    }

    /// Fetches the first 500 characters of the body text for all [`messages`](MessageUidRange).
    pub async fn fetch_preview_text(
        &mut self,
        messages: &dyn UidRange,
    ) -> ClientResult<BTreeMap<Uid, String>> {
        // Because we can receive unsolicited fetches in-between flush the unsolicited responses first
        self.process_unsolicited();

        let Some(seq) = messages.to_seq() else {
            return Ok(Default::default());
        };

        let structure_fetch = self
            .client
            .uid_fetch(seq, vec![MessageDataItemName::BodyStructure].into())
            .compat()
            .await?;

        let data = self.process_fetch(structure_fetch)?;

        log::debug!("Preview fetch");
        let mut groups = HashMap::<Section, Vec<Uid>>::new();

        for (uid, data) in data {
            if let Some(structure) = data.bodystructure {
                if let Some(text_location) = structure.preview_section() {
                    groups.entry(text_location).or_default().push(uid);
                }
            } else {
                // TODO unsolicited data?
            }
        }

        let mut message_contents = BTreeMap::new();

        for (fetch_section, ids) in groups {
            log::debug!(
                "Fetching preview text for {} messages ({fetch_section:?})",
                ids.len()
            );

            let fetch_res = self
                .client
                .uid_fetch(
                    ids.to_seq().unwrap(),
                    vec![
                        MessageDataItemName::BodyStructure,
                        MessageDataItemName::BodyExt {
                            section: Some(fetch_section.clone()),
                            partial: Some((0, NonZero::new(200).unwrap())),
                            peek: true,
                        },
                    ]
                    .into(),
                )
                .compat()
                .await?;

            for (uid, fetch) in fetch_res {
                for section in fetch {
                    match section {
                        MessageDataItem::BodyExt { section, data, .. }
                            if section.as_ref() == Some(&fetch_section) =>
                        {
                            // We found the section
                            if let Some(text) = data.into_option().as_deref().and_then(
                                mail_parser::decoders::quoted_printable::quoted_printable_decode,
                            )                    .map(|bytes| String::from_utf8_lossy(bytes.as_slice()).into_owned()) {
                                log::trace!("found body: {:?}", text);
                                message_contents.insert(
                                    uid.into(),
                                    mail_parser::decoders::html::html_to_text(&text.ensure_no_nul())
                                        .split_whitespace()
                                        .collect::<Vec<_>>()
                                        .join(" "));
                            } else {
                                log::trace!("nothing found :( for path {:?}", &fetch_section);
                            }
                        }
                        _ => {}
                    }
                }
            }
        }

        self.process_unsolicited();
        Ok(message_contents)
    }

    /// Fetch the body part of the [`messages`](MessageUidRange).
    pub async fn fetch_body(
        &mut self,
        messages: &dyn UidRange,
    ) -> ClientResult<BTreeMap<Uid, InternetMessage>> {
        // Because we can receive unsolicited fetches in-between flush the unsolicited responses first
        self.process_unsolicited();

        let Some(seq) = messages.to_seq() else {
            return Ok(Default::default());
        };

        log::debug!("Fetching body for messages: {seq:?}");

        let data = self
            .client
            .uid_fetch(
                seq,
                vec![
                    MessageDataItemName::Flags,
                    MessageDataItemName::Rfc822Size,
                    MessageDataItemName::BodyExt {
                        section: None,
                        partial: None,
                        peek: true,
                    },
                ]
                .into(),
            )
            .compat()
            .await?;

        let message_data = self.process_fetch(data)?;
        let message_bodies = message_data
            .into_iter()
            .filter_map(|(uid, data)| data.message.map(|m| (uid, m)))
            .collect();

        self.process_unsolicited();
        Ok(message_bodies)
    }

    /// Permanantly delete (`messages`)[`MessageUidRange`].
    ///
    /// Flags the messages with `\Deleted` and then issues a `UID EXPUNGE` if supported.
    pub async fn delete_messages(&mut self, messages: &dyn UidRange) -> ClientResult<()> {
        self.store_flags(messages, MessageFlags::deleted(), MessageFlagsChange::Add)
            .await?;
        self.expunge(messages).await?;

        self.process_unsolicited();
        Ok(())
    }

    pub async fn shutdown(&mut self) -> ClientResult<()> {
        let task = imap_client::tasks::tasks::logout::LogoutTask::new();
        self.client.resolve(task).compat().await??;
        Ok(())
    }

    pub fn start_idle(&mut self) -> ClientResult<IdleHandle<'_>> {
        self.process_unsolicited();
        Ok(IdleHandle::new(&mut *self))
    }
}
