// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod capability;
mod date;
mod mailbox;
mod message;
mod response;
mod search;

pub(crate) use capability::*;
pub(crate) use date::*;
use imap_client::imap_types;
use imap_client::imap_types::body::BodyStructure;
use imap_client::imap_types::core::Vec1;
use imap_client::imap_types::fetch::{Part, Section};
pub(crate) use mailbox::*;
pub(crate) use message::*;
pub(crate) use response::*;
pub(crate) use search::*;

use std::collections::VecDeque;
use std::num::{NonZero, NonZeroU32};

use crate::types::*;

impl From<MailboxSelection> for SelectKind {
    fn from(value: MailboxSelection) -> Self {
        SelectKind {
            mailbox_name: value.mailbox_name,
            selection_type: value.selection_type,
        }
    }
}

pub trait BodyStructureExt {
    fn preview_section(&self) -> Option<imap_types::fetch::Section<'static>>;
}

impl BodyStructureExt for imap_types::body::BodyStructure<'_> {
    /// Returns a path to the MIME-IMB body part that is most likely to contain a useful preview text
    fn preview_section(&self) -> Option<Section<'static>> {
        #[derive(Debug, Copy, Clone, PartialEq, Eq, PartialOrd, Ord)]
        enum Subtype {
            Other,
            Xhtml,
            Html,
            Plain,
        }

        /// Build the section path leading to the best suited segment in this message
        ///
        /// When `is_text_body` is set to true it indicates that the path must be suffixed
        /// by a TEXT path specifier. In other terms, it means that the embedded message is
        /// not a MIME message, but rather a plain text one.
        ///
        /// If none is found this function returns Subtype::Other
        #[must_use]
        fn build_path(
            body: &BodyStructure<'_>,
            path: &mut VecDeque<NonZero<u32>>,
            is_text_body: &mut bool,
        ) -> Subtype {
            match body {
                BodyStructure::Single { body, .. } => match &body.specific {
                    imap_types::body::SpecificFields::Basic { r#type, subtype } => {
                        // Most likely an attachment, but could be XHTML
                        if r#type.as_ref() == b"application" && subtype.as_ref() == b"xhtml+xml" {
                            Subtype::Xhtml
                        } else {
                            Subtype::Other
                        }
                    }
                    imap_types::body::SpecificFields::Message { body_structure, .. } => {
                        // Nested MIME-IMB message
                        let ty = build_path(body_structure, &mut *path, &mut *is_text_body);
                        if ty > Subtype::Other && path.is_empty() {
                            // We don't have any MIME-IMB children, so this is a plain text message without any MIME-IMB content
                            *is_text_body = true;
                            ty
                        } else {
                            ty
                        }
                    }
                    imap_types::body::SpecificFields::Text { subtype, .. } => {
                        let sub = subtype.as_ref();
                        if sub == b"plain" {
                            Subtype::Plain
                        } else if sub == b"html" {
                            Subtype::Html
                        } else {
                            Subtype::Other
                        }
                    }
                },
                BodyStructure::Multi {
                    bodies, subtype, ..
                } => {
                    let sub = subtype.as_ref();
                    if sub == b"alternative" {
                        // Choose the best suited content-type we can find
                        let (i, ty) = bodies.as_ref().iter().enumerate().fold(
                            (0, Subtype::Other),
                            |(acc_idx, acc_ty), (i, body)| {
                                let new = build_path(body, &mut *path, &mut *is_text_body);
                                // Choose the best subtype
                                if new > acc_ty {
                                    (i, new)
                                } else {
                                    (acc_idx, acc_ty)
                                }
                            },
                        );

                        if ty != Subtype::Other {
                            // We have found our favorite multipart/alternative segment
                            // Indexing starts at 1
                            path.push_front(
                                NonZeroU32::MIN.checked_add(i.try_into().unwrap()).unwrap(),
                            );
                        }

                        ty
                    } else if sub == b"mixed" || sub == b"digest" {
                        // Choose the first content type that fits out criteria
                        for (i, body) in bodies.as_ref().iter().enumerate() {
                            let new = build_path(body, path, &mut *is_text_body);

                            if new != Subtype::Other {
                                path.push_front(
                                    NonZeroU32::MIN.checked_add(i.try_into().unwrap()).unwrap(),
                                );
                                return new;
                            }
                        }

                        Subtype::Other
                    } else {
                        Subtype::Other
                    }
                }
            }
        }

        let mut path = VecDeque::new();
        let mut is_text_body = false;
        let ty = build_path(self, &mut path, &mut is_text_body);

        if ty != Subtype::Other {
            if let Ok(vec1) = Vec1::try_from(Vec::from(path)) {
                // This is a MIME-IMB message with at least one path segment
                let part = Part(vec1);
                if is_text_body {
                    // This contains a multipart/rfc822 message embedded within that is not a MIME-IMB message
                    Some(Section::Text(Some(part)))
                } else {
                    Some(Section::Part(part))
                }
            } else {
                // This is not a MIME-IMB message
                Some(Section::Text(None))
            }
        } else {
            log::debug!("No preview section found");
            None
        }
    }
}

impl MailboxSelection {
    pub fn from_select(
        mailbox_name: MailboxName,
        selection_type: SelectionType,
        value: imap_client::tasks::tasks::select::SelectDataUnvalidated,
    ) -> Self {
        Self {
            selection_type,
            mailbox_name,
            uidvalidity: UidValidity::from(value.uid_validity),
            uidnext: Uid::from_option_or(value.uid_next, Uid::MAX),
            message_count: value.exists.unwrap_or_default(),
            recent_count: value.recent.unwrap_or_default(),
        }
    }
}
