// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use crate::imap_types;
use enumset::{EnumSet, EnumSetType};
use imap_types::ToStatic;
use imap_types::extensions::enable::CapabilityEnable;
use imap_types::response::Capability;

use crate::account::AuthMethod;

#[derive(Debug, EnumSetType, Hash)]
pub(crate) enum Standard {
    /// IMAP4rev1 protocol standard
    ///
    /// [RFC 3501](https://datatracker.ietf.org/doc/html/rfc3501)
    Imap4rev1,
    /// IMAP4rev2 protocol standard
    ///
    /// [RFC 9051](https://datatracker.ietf.org/doc/html/rfc9051)
    Imap4rev2,
}

#[derive(Debug, Default, Hash)]
pub(crate) struct Capabilities {
    pub standard: Option<Standard>,
    pub auth: Vec<String>,
    pub capability: Vec<imap_types::response::Capability<'static>>,
    pub enable: Vec<imap_types::extensions::enable::CapabilityEnable<'static>>,
    pub quota: Option<String>,
    pub rights: Option<String>,
    pub imap_sieve: Option<String>,
    pub unknown: Vec<String>,
}

impl<'a> FromIterator<&'a imap_types::response::Capability<'a>> for Capabilities {
    fn from_iter<T: IntoIterator<Item = &'a imap_types::response::Capability<'a>>>(
        iter: T,
    ) -> Self {
        let mut caps = Capabilities::default();
        for cap in iter {
            match cap {
                Capability::Imap4Rev1 => caps.standard = Some(Standard::Imap4rev1),
                cap => caps.capability.push(cap.to_static()),
            }
        }

        caps
    }
}

impl Capabilities {
    pub fn generate_enable(&self) -> Vec<CapabilityEnable> {
        // TODO enable utf8
        Vec::new()
    }

    pub fn supported_authentication_methods(&self) -> EnumSet<AuthMethod> {
        self.auth
            .iter()
            .filter_map(|m| m.parse::<AuthMethod>().ok())
            .collect()
    }

    #[expect(dead_code)]
    pub fn has(&self, capability: Capability) -> bool {
        self.capability.contains(&capability)
    }

    pub fn is_imap4_standard(&self, standard: Standard) -> bool {
        self.standard == Some(standard)
    }
}
