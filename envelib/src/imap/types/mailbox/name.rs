// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use base64::Engine;

static B64_UTF7: base64::engine::GeneralPurpose = base64::engine::GeneralPurpose::new(
    &base64::alphabet::IMAP_MUTF7,
    base64::engine::general_purpose::NO_PAD,
);

#[derive(
    Clone, Default, sqlx::Type, Debug, Hash, PartialEq, Eq, PartialOrd, Ord, glib::ValueDelegate,
)]
#[value_delegate(nullable)]
#[sqlx(transparent)]
pub(crate) struct MailboxName(String);

impl AsRef<str> for MailboxName {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl From<&str> for MailboxName {
    fn from(value: &str) -> Self {
        Self(value.to_string())
    }
}

impl From<String> for MailboxName {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl std::fmt::Display for MailboxName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Display::fmt(&self.0, f)
    }
}

#[expect(dead_code)]
impl MailboxName {
    pub fn new(id: String) -> MailboxName {
        Self(id)
    }

    pub fn inbox() -> Self {
        Self("INBOX".to_string())
    }

    pub fn is_inbox(&self) -> bool {
        self.0.eq_ignore_ascii_case("INBOX")
    }

    pub fn encode_utf7(&self) -> Vec<u8> {
        encode_utf7(&self.0)
    }
}

/// Encode imap-utf7
fn encode_utf7(value: &str) -> Vec<u8> {
    // The final buffer
    let mut buf = Vec::new();
    let mut iter = value.chars().peekable();

    let mut temp_u16 = [0; 2];
    let mut temp_utf16_bytes: Vec<u8> = Vec::new();

    while iter.peek().is_some() {
        // characters to encode verbatim
        while let Some(c) = iter.next_if(|c| matches!(c, ' '..='~')) {
            if c == '&' {
                buf.push(b'&');
                buf.push(b'-');
            } else {
                // 0x20 .. 0x7e
                let byte = c as u8;
                buf.push(byte);
            }
        }

        // characters to encode in imap-mutf7-base64
        while let Some(c) = iter.next_if(|c| !matches!(c, ' '..='~')) {
            // Will return slice of 1-2 u16
            let b = c.encode_utf16(&mut temp_u16);

            // First u16 always exists
            temp_utf16_bytes.extend_from_slice(&b[0].to_be_bytes());

            // Second might be filled as well
            if let Some(lsb) = b.get(1) {
                temp_utf16_bytes.extend_from_slice(&lsb.to_be_bytes());
            }
        }

        if !temp_utf16_bytes.is_empty() {
            // The base64 encoded data is indicated by an ampersand (0x26)
            buf.push(b'&');

            // Extend our data buffer by the required size
            let current_size = buf.len();
            buf.resize(
                current_size
                    + base64::encoded_len(temp_utf16_bytes.len(), false)
                        .expect("string too long to encode"),
                0,
            );

            let encoded_len = B64_UTF7
                .encode_slice(&temp_utf16_bytes, &mut buf[current_size..])
                .expect("buffer is not long enough");

            debug_assert_eq!(current_size + encoded_len, buf.len());
            temp_utf16_bytes.clear();

            // The end of the encoded data is indicated by a hyphen (0x2D)
            buf.push(b'-');
        }
    }

    buf
}

/// Decode imap-utf7
fn decode_utf7(data: &[u8]) -> Result<String, Utf7DecodeError> {
    let mut buf = String::new();
    let mut iter = data.iter().copied().enumerate().peekable();

    let mut temp_utf16_bytes = Vec::new();

    while let Some((idx, c)) = iter.next() {
        if c == b'&' {
            // modified BASE64
            let start = idx + 1; // skip the &
            let mut end = start;

            while iter.next_if(|(_, c)| *c != b'-').is_some() {
                end += 1;
            }

            if !matches!(iter.next(), Some((_, b'-'))) {
                return Err(Utf7DecodeError::UnexpectedEnd);
            }

            if end == start {
                buf.push('&');
            } else {
                B64_UTF7
                    .decode_vec(&data[start..end], &mut temp_utf16_bytes)
                    .map_err(|_| Utf7DecodeError::InvalidBase64)?;

                if temp_utf16_bytes.len() % 2 != 0 {
                    return Err(Utf7DecodeError::InvalidUnicode);
                }

                let utf16 = char::decode_utf16(
                    temp_utf16_bytes
                        .chunks_exact(2)
                        .map(|slice| u16::from_be_bytes([slice[0], slice[1]])),
                );

                for c in utf16 {
                    buf.push(c.map_err(|_| Utf7DecodeError::InvalidUnicode)?);
                }

                temp_utf16_bytes.clear();
            }
        } else if matches!(c, 0x20..=0x25 | 0x27..=0x7e) {
            // In modified UTF-7, printable US-ASCII characters, except for "&", represent themselves; that is, characters with octet values 0x20-0x25 and 0x27-0x7e
            buf.push(c.into());
        } else {
            return Err(Utf7DecodeError::InvalidByte(c));
        }
    }

    Ok(buf)
}

#[derive(Debug, Clone, thiserror::Error, PartialEq, Eq)]
pub enum Utf7DecodeError {
    #[error("Unexepected end of string")]
    UnexpectedEnd,
    #[error("Invalid byte: {0}")]
    InvalidByte(u8),
    #[error("Invalid unicode")]
    InvalidUnicode,
    #[error("Invalid base64")]
    InvalidBase64,
}

impl TryFrom<&[u8]> for MailboxName {
    type Error = Utf7DecodeError;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Ok(Self(decode_utf7(value)?))
    }
}

impl TryFrom<crate::imap_types::mailbox::Mailbox<'_>> for MailboxName {
    type Error = Utf7DecodeError;

    fn try_from(value: crate::imap_types::mailbox::Mailbox<'_>) -> Result<Self, Self::Error> {
        match value {
            imap_client::imap_types::mailbox::Mailbox::Inbox => Ok(Self("INBOX".to_string())),
            imap_client::imap_types::mailbox::Mailbox::Other(mailbox_other) => {
                Ok(MailboxName(decode_utf7(mailbox_other.as_ref())?))
            }
        }
    }
}

impl TryFrom<MailboxName> for crate::imap_types::mailbox::Mailbox<'static> {
    type Error = imap_client::imap_types::error::ValidationError;

    fn try_from(value: MailboxName) -> Result<Self, Self::Error> {
        let slice = encode_utf7(&value.0);
        crate::imap_types::mailbox::Mailbox::try_from(slice)
    }
}

#[cfg(test)]
mod test {
    use crate::imap::{
        Utf7DecodeError,
        types::mailbox::name::{decode_utf7, encode_utf7},
    };

    #[test]
    fn test_utf7_decode() {
        assert_eq!(decode_utf7(b"test 123").unwrap(), "test 123");
        assert_eq!(decode_utf7(b"INBOX").unwrap(), "INBOX");
        assert_eq!(decode_utf7(b"&BdAF6QXkBdQ-").unwrap(), "אשפה");
        // Example from RFC 3501
        assert_eq!(
            decode_utf7(b"~peter/mail/&U,BTFw-/&ZeVnLIqe-").unwrap(),
            "~peter/mail/台北/日本語"
        );
        // No shift to US-ASCII before !
        assert_eq!(
            decode_utf7(b"&Jjo!").unwrap_err(),
            Utf7DecodeError::UnexpectedEnd
        );
        // Superfluous shift, but we are permissive
        assert_eq!(decode_utf7(b"&U,BTFw-&ZeVnLIqe-").unwrap(), "台北日本語");
        // Correct
        assert_eq!(decode_utf7(b"&U,BTF2XlZyyKng-").unwrap(), "台北日本語");
        assert_eq!(
            encode_utf7(&decode_utf7(b"&U,BTFw-&ZeVnLIqe-").unwrap()),
            b"&U,BTF2XlZyyKng-"
        );
    }

    #[test]
    fn test_utf7_encode() {
        assert_eq!(encode_utf7("test 123"), b"test 123");
        assert_eq!(encode_utf7("INBOX"), b"INBOX");
        assert_eq!(encode_utf7("אשפה"), b"&BdAF6QXkBdQ-");
        // Example from RFC 3501
        assert_eq!(
            encode_utf7("~peter/mail/台北/日本語"),
            b"~peter/mail/&U,BTFw-/&ZeVnLIqe-",
        );
        assert_eq!(encode_utf7("台北日本語"), b"&U,BTF2XlZyyKng-");
    }
}
