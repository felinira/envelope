// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::num::NonZero;

#[derive(
    Debug,
    sqlx::Type,
    Default,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    glib::ValueDelegate,
)]
#[sqlx(transparent)]
pub(crate) struct UidValidity(u32);

impl AsRef<u32> for UidValidity {
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

impl From<u32> for UidValidity {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl std::fmt::Display for UidValidity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl UidValidity {
    pub const NULL: Self = Self(0);

    pub fn new(uid_validity: u32) -> Self {
        Self(uid_validity)
    }
}

impl From<Option<NonZero<u32>>> for UidValidity {
    fn from(value: Option<NonZero<u32>>) -> Self {
        if let Some(non_zero) = value {
            Self(non_zero.into())
        } else {
            Self(0)
        }
    }
}
