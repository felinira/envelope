// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use crate::imap::error::ClientError;

/// IMAP date
pub(crate) struct Date(time::Date);

static DATE_FORMAT: &[time::format_description::FormatItem] = time::macros::format_description!(
    "[day padding:none]-[month repr:short case_sensitive:true]-[year padding:zero]"
);

impl std::fmt::Display for Date {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0
                .format(DATE_FORMAT)
                .expect("date format must be correct")
        )
    }
}

impl From<time::Date> for Date {
    fn from(value: time::Date) -> Self {
        Self(value)
    }
}

impl From<&time::Date> for Date {
    fn from(value: &time::Date) -> Self {
        Self(value.to_owned())
    }
}

/// IMAP time
pub(crate) struct Time(time::Time);

static TIME_FORMAT: &[time::format_description::FormatItem] = time::macros::format_description!(
    "[hour padding:zero]:[minute padding:zero]:[second padding:zero]"
);

impl std::fmt::Display for Time {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0
                .format(TIME_FORMAT)
                .expect("date format must be correct")
        )
    }
}

impl From<time::Time> for Time {
    fn from(value: time::Time) -> Self {
        Self(value)
    }
}

impl From<&time::Time> for Time {
    fn from(value: &time::Time) -> Self {
        Self(value.to_owned())
    }
}

/// IMAP date-time
pub(crate) struct DateTime(time::OffsetDateTime);

static DATE_TIME_FORMAT: &[time::format_description::FormatItem] = time::macros::format_description!(
    "\"[day padding:none]-[month repr:short case_sensitive:true]-[year padding:zero] [hour padding:zero]:[minute padding:zero]:[second padding:zero] [offset_hour padding:zero sign:mandatory][offset_minute padding:zero]\""
);

impl std::fmt::Display for DateTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            self.0
                .format(DATE_TIME_FORMAT)
                .expect("date format must be correct")
        )
    }
}

impl From<time::OffsetDateTime> for DateTime {
    fn from(value: time::OffsetDateTime) -> Self {
        Self(value)
    }
}

impl From<&time::OffsetDateTime> for DateTime {
    fn from(value: &time::OffsetDateTime) -> Self {
        Self(value.to_owned())
    }
}

impl TryFrom<&[u8]> for DateTime {
    type Error = time::error::Parse;

    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let string = String::from_utf8_lossy(value);
        log::debug!("date str {}", string);
        Ok(Self(time::OffsetDateTime::parse(
            &string,
            DATE_TIME_FORMAT,
        )?))
    }
}

impl TryFrom<&mail_parser::DateTime> for DateTime {
    type Error = ClientError;

    fn try_from(value: &mail_parser::DateTime) -> Result<Self, Self::Error> {
        Ok(Self(time::OffsetDateTime::from_unix_timestamp(
            value.to_timestamp(),
        )?))
    }
}

impl DateTime {
    pub fn into_inner(self) -> time::OffsetDateTime {
        self.0
    }
}

#[cfg(test)]
mod test {
    use super::{Date, DateTime, Time};

    #[test]
    fn test_date() {
        assert_eq!(
            format!(
                "{}",
                Date(time::Date::from_calendar_date(1996, time::Month::July, 17).unwrap())
            ),
            "17-Jul-1996"
        );

        assert_eq!(
            format!(
                "{}",
                Date(time::Date::from_calendar_date(980, time::Month::January, 7).unwrap())
            ),
            "7-Jan-0980"
        );
    }

    #[test]
    fn test_time() {
        assert_eq!(
            format!("{}", Time(time::Time::from_hms(8, 4, 59).unwrap())),
            "08:04:59"
        );

        assert_eq!(
            format!("{}", Time(time::Time::from_hms(20, 10, 0).unwrap())),
            "20:10:00"
        );
    }

    #[test]
    fn test_date_time() {
        assert_eq!(
            format!(
                "{}",
                DateTime(
                    time::Date::from_calendar_date(1996, time::Month::July, 17)
                        .unwrap()
                        .with_hms(2, 44, 25)
                        .unwrap()
                        .assume_offset(time::UtcOffset::from_hms(-7, 0, 0).unwrap())
                )
            ),
            "\"17-Jul-1996 02:44:25 -0700\""
        );

        assert_eq!(
            format!(
                "{}",
                DateTime(
                    time::Date::from_calendar_date(980, time::Month::January, 7)
                        .unwrap()
                        .with_hms(10, 3, 7)
                        .unwrap()
                        .assume_offset(time::UtcOffset::from_hms(10, 30, 0).unwrap())
                )
            ),
            "\"7-Jan-0980 10:03:07 +1030\""
        );
    }
}
