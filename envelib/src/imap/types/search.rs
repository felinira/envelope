// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#![allow(dead_code)]

use imap_client::imap_types::search::SearchKey;

use crate::{imap::error::ClientError, utils::date_to_chrono};

use super::UidRange;

#[derive(Debug)]
pub(crate) enum ImapSearchKey {
    All,
    Answered,
    Bcc(String),
    Before(time::Date),
    Body(String),
    Cc(String),
    Deleted,
    Draft,
    Flagged,
    From(String),
    Header(String, String),
    Keyword(String),
    Larger(u64),
    New,
    Not(Box<ImapSearchKey>),
    Old,
    On(time::Date),
    Or(Box<ImapSearchKey>, Box<ImapSearchKey>),
    Recent,
    Seen,
    SentBefore(time::Date),
    SentOn(time::Date),
    SentSince(time::Date),
    Since(time::Date),
    Smaller(u64),
    Subject(String),
    Text(String),
    To(String),
    Uid(Box<dyn UidRange>),
    Unanswered,
    Undeleted,
    Undraft,
    Unflagged,
    Unkeyword(String),
    Unseen,
}

impl std::ops::BitOr for ImapSearchKey {
    type Output = ImapSearchKey;

    fn bitor(self, rhs: Self) -> Self::Output {
        Self::Or(Box::new(self), Box::new(rhs))
    }
}

impl std::ops::Not for ImapSearchKey {
    type Output = ImapSearchKey;

    fn not(self) -> Self::Output {
        Self::Not(Box::new(self))
    }
}

impl std::ops::BitAnd for ImapSearchKey {
    type Output = ImapSearchQuery;

    fn bitand(self, rhs: Self) -> Self::Output {
        ImapSearchQuery::from_iter([self, rhs])
    }
}

#[derive(Debug)]
pub(crate) struct ImapSearchQuery(pub(crate) Vec<ImapSearchKey>);

impl ImapSearchQuery {
    pub fn all() -> Self {
        Self(Vec::new())
    }
}

impl FromIterator<ImapSearchKey> for ImapSearchQuery {
    fn from_iter<T: IntoIterator<Item = ImapSearchKey>>(iter: T) -> Self {
        Self(iter.into_iter().collect())
    }
}

impl std::ops::BitAnd for ImapSearchQuery {
    type Output = ImapSearchQuery;

    fn bitand(self, rhs: Self) -> Self::Output {
        ImapSearchQuery::from_iter(self.0.into_iter().chain(rhs.0))
    }
}

impl TryFrom<ImapSearchKey> for SearchKey<'static> {
    type Error = crate::imap::error::ClientError;

    fn try_from(value: ImapSearchKey) -> Result<Self, Self::Error> {
        Ok(match value {
            ImapSearchKey::All => SearchKey::All,
            ImapSearchKey::Answered => SearchKey::Answered,
            ImapSearchKey::Bcc(s) => SearchKey::Bcc(s.try_into()?),
            ImapSearchKey::Before(date) => {
                SearchKey::Before(crate::utils::date_to_chrono(&date).try_into()?)
            }
            ImapSearchKey::Body(s) => SearchKey::Body(s.try_into()?),
            ImapSearchKey::Cc(s) => SearchKey::Cc(s.try_into()?),
            ImapSearchKey::Deleted => SearchKey::Deleted,
            ImapSearchKey::Draft => SearchKey::Draft,
            ImapSearchKey::Flagged => SearchKey::Flagged,
            ImapSearchKey::From(s) => SearchKey::From(s.try_into()?),
            ImapSearchKey::Header(key, value) => {
                SearchKey::Header(key.try_into()?, value.try_into()?)
            }
            ImapSearchKey::Keyword(s) => SearchKey::Keyword(s.try_into()?),
            ImapSearchKey::Larger(u) => SearchKey::Larger(u as u32),
            ImapSearchKey::New => SearchKey::New,
            ImapSearchKey::Not(key) => SearchKey::Not(Box::new((*key).try_into()?)),
            ImapSearchKey::Old => SearchKey::Old,
            ImapSearchKey::On(date) => SearchKey::On(date_to_chrono(&date).try_into()?),
            ImapSearchKey::Or(key1, key2) => {
                SearchKey::Or(Box::new((*key1).try_into()?), Box::new((*key2).try_into()?))
            }
            ImapSearchKey::Recent => SearchKey::Recent,
            ImapSearchKey::Seen => SearchKey::Seen,
            ImapSearchKey::SentBefore(date) => {
                SearchKey::SentBefore(date_to_chrono(&date).try_into()?)
            }
            ImapSearchKey::SentOn(date) => SearchKey::SentOn(date_to_chrono(&date).try_into()?),
            ImapSearchKey::SentSince(date) => {
                SearchKey::SentSince(date_to_chrono(&date).try_into()?)
            }
            ImapSearchKey::Since(date) => SearchKey::Since(date_to_chrono(&date).try_into()?),
            ImapSearchKey::Smaller(size) => SearchKey::Smaller(size as u32),
            ImapSearchKey::Subject(s) => SearchKey::Subject(s.try_into()?),
            ImapSearchKey::Text(s) => SearchKey::Text(s.try_into()?),
            ImapSearchKey::To(s) => SearchKey::To(s.try_into()?),
            ImapSearchKey::Uid(set) => SearchKey::Uid(
                set.to_seq()
                    .ok_or_else(|| ClientError::MissingField("uid".to_string()))?,
            ),
            ImapSearchKey::Unanswered => SearchKey::Unanswered,
            ImapSearchKey::Undeleted => SearchKey::Undeleted,
            ImapSearchKey::Undraft => SearchKey::Undraft,
            ImapSearchKey::Unflagged => SearchKey::Unflagged,
            ImapSearchKey::Unkeyword(s) => SearchKey::Unkeyword(s.try_into()?),
            ImapSearchKey::Unseen => SearchKey::Unseen,
        })
    }
}
