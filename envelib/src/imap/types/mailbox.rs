// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod name;
mod uidvalidity;

#[allow(unused_imports)]
pub(crate) use name::{MailboxName, Utf7DecodeError};
pub(crate) use uidvalidity::*;

use super::MailboxId;
use crate::model::{self, MessageExtProps};
use std::collections::BTreeMap;

pub(crate) trait CollectMailboxMap {
    type Type: Sized;

    fn collect_mailbox_map(self) -> BTreeMap<MailboxId, Vec<model::Message>>;
}

impl<T: AsRef<crate::model::Message>, U: Iterator<Item = T>> CollectMailboxMap for U {
    type Type = crate::model::Message;

    fn collect_mailbox_map(self) -> BTreeMap<MailboxId, Vec<model::Message>> {
        let mut map: BTreeMap<MailboxId, Vec<model::Message>> = BTreeMap::new();

        for message in self {
            let message = message.as_ref().to_owned();
            let mailbox = message.mailbox_id();

            map.entry(mailbox).or_default().push(message);
        }

        map
    }
}
