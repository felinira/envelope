// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

#[derive(
    Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, glib::ValueDelegate,
)]
pub(crate) struct SeqNum(u32);

impl AsRef<u32> for SeqNum {
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

impl From<u32> for SeqNum {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl std::fmt::Display for SeqNum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[allow(dead_code)]
impl SeqNum {
    pub const NULL: SeqNum = SeqNum(0);
    pub const MAX: SeqNum = SeqNum(u32::MAX);

    pub fn new(id: u32) -> Self {
        Self(id)
    }

    pub fn next(&self) -> Self {
        SeqNum(self.0 + 1)
    }
}

impl std::ops::Add for SeqNum {
    type Output = SeqNum;

    fn add(self, rhs: Self) -> Self::Output {
        SeqNum(self.0 + rhs.0)
    }
}

impl std::ops::Sub for SeqNum {
    type Output = SeqNum;

    fn sub(self, rhs: Self) -> Self::Output {
        SeqNum(self.0 - rhs.0)
    }
}
