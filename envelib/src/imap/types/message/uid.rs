// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::{
    borrow::{Borrow, Cow},
    collections::BTreeMap,
    num::{NonZero, NonZeroU32},
};

use glib::object::{Cast, CastNone};
use imap_client::imap_types::{
    core::Vec1,
    sequence::{SeqOrUid, Sequence, SequenceSet},
};

use crate::{
    error::ErrorKindExt,
    imap::MailboxName,
    model::{self, ImapMailbox, ImapMessage, MailboxExtProps, MessageExtProps},
    types::MailboxId,
};

#[derive(
    Debug,
    Default,
    sqlx::Type,
    Clone,
    Copy,
    PartialEq,
    Eq,
    PartialOrd,
    Ord,
    Hash,
    glib::ValueDelegate,
)]
#[sqlx(transparent)]
pub(crate) struct Uid(u32);

impl AsRef<u32> for Uid {
    fn as_ref(&self) -> &u32 {
        &self.0
    }
}

impl From<u32> for Uid {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

impl std::fmt::Display for Uid {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

impl Uid {
    pub const NULL: Uid = Uid(0);
    pub const FIRST: Uid = Uid(1);
    pub const MAX: Uid = Uid(u32::MAX);

    pub fn new(id: u32) -> Self {
        Self(id)
    }

    pub fn from_option_or(value: Option<NonZero<u32>>, or: Uid) -> Self {
        if let Some(non_zero) = value {
            Self(non_zero.into())
        } else {
            or
        }
    }

    pub fn next(&self) -> Self {
        Uid(self.0.saturating_add(1))
    }

    pub fn prev(&self) -> Self {
        Uid(self.0.saturating_sub(1))
    }
}

impl std::ops::Add for Uid {
    type Output = Uid;

    fn add(self, rhs: Self) -> Self::Output {
        Uid(self.0.saturating_add(rhs.0))
    }
}

impl std::ops::Sub for Uid {
    type Output = Uid;

    fn sub(self, rhs: Self) -> Self::Output {
        Uid(self.0.saturating_sub(rhs.0))
    }
}

impl From<Uid> for Option<SeqOrUid> {
    fn from(value: Uid) -> Self {
        if value == Uid::MAX {
            Some(SeqOrUid::Asterisk)
        } else {
            NonZero::new(value.0).map(SeqOrUid::Value)
        }
    }
}

impl TryFrom<Uid> for SeqOrUid {
    type Error = ();

    fn try_from(value: Uid) -> Result<Self, Self::Error> {
        if value == Uid::MAX {
            Ok(SeqOrUid::Asterisk)
        } else {
            NonZero::new(value.0).map(SeqOrUid::Value).ok_or(())
        }
    }
}

impl TryFrom<&Uid> for SeqOrUid {
    type Error = ();

    fn try_from(value: &Uid) -> Result<Self, Self::Error> {
        Self::try_from(*value)
    }
}

impl From<NonZero<u32>> for Uid {
    fn from(value: NonZero<u32>) -> Self {
        Uid(value.into())
    }
}

#[derive(Debug)]
pub(crate) enum UidValues<'a> {
    Range {
        start_bound: std::ops::Bound<Uid>,
        end_bound: std::ops::Bound<Uid>,
    },
    Values(Cow<'a, [Uid]>),
}

impl<'a, T: std::ops::RangeBounds<Uid>> From<&'a T> for UidValues<'a> {
    fn from(value: &'a T) -> Self {
        Self::Range {
            start_bound: value.start_bound().cloned(),
            end_bound: value.end_bound().cloned(),
        }
    }
}

impl UidValues<'_> {
    pub fn first(&self) -> Uid {
        match self {
            Self::Range { start_bound, .. } => match start_bound {
                std::ops::Bound::Included(x) => *x,
                std::ops::Bound::Excluded(x) => x.next(),
                std::ops::Bound::Unbounded => Uid::FIRST,
            },
            Self::Values(s) => s.iter().min().copied().unwrap_or(Uid::NULL),
        }
    }

    pub fn last(&self) -> Uid {
        match self {
            Self::Range { end_bound, .. } => match end_bound {
                std::ops::Bound::Included(x) => *x,
                std::ops::Bound::Excluded(x) => x.prev(),
                std::ops::Bound::Unbounded => Uid::MAX,
            },
            Self::Values(s) => s.iter().max().copied().unwrap_or(Uid::NULL),
        }
    }
}

impl<'a> IntoIterator for UidValues<'a> {
    type Item = Uid;

    type IntoIter = UidRangeIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        match self {
            Self::Range { .. } => UidRangeIter::Range(self.first()..=self.last()),
            Self::Values(values) => UidRangeIter::Values(values, 0),
        }
    }
}

pub(crate) trait UidRange: std::fmt::Debug + Send + Sync {
    fn values(&self) -> UidValues;

    /// length (counting from 1, as uids start with 1)
    fn len(&self) -> usize;
    fn take_n(&mut self, count: usize) -> Box<dyn UidRange>;
    fn is_contiguous(&self) -> bool;
    fn first(&self) -> Uid {
        self.values().first()
    }
    fn last(&self) -> Uid {
        self.values().last()
    }
    fn is_empty(&self) -> bool {
        self.len() == 0
    }
    fn to_owned(&self) -> Box<dyn UidRange>;
    #[expect(dead_code)]
    fn contains(&self, uid: &Uid) -> bool;
    fn chunks(&self, chunk_size: usize) -> UidRangeChunksIter<'_> {
        UidRangeChunksIter {
            chunk_size,
            range: self.to_owned(),
        }
    }
    fn to_seq(&self) -> Option<SequenceSet>;
}

impl UidRange for std::ops::RangeInclusive<Uid> {
    fn values(&self) -> UidValues {
        self.into()
    }

    fn is_contiguous(&self) -> bool {
        true
    }

    fn len(&self) -> usize {
        (self.end().0 + 1 - self.start().0) as usize
    }

    fn take_n(&mut self, count: usize) -> Box<dyn UidRange> {
        let count = count.min(self.len()) as u32;
        let old_end = self.end().0;
        *self = Uid(self.start().0)..=Uid(old_end - count);
        Box::new(Uid(old_end - count + 1)..=Uid(old_end))
    }

    fn to_owned(&self) -> Box<dyn UidRange> {
        Box::new(Uid(self.start().0)..=Uid(self.end().0))
    }

    fn contains(&self, uid: &Uid) -> bool {
        self.start() <= uid && uid <= self.end()
    }

    fn to_seq(&self) -> Option<SequenceSet> {
        let start = self
            .start()
            .try_into()
            .unwrap_or(SeqOrUid::Value(NonZero::<u32>::MIN));
        let end = self.end().try_into().unwrap_or(SeqOrUid::Asterisk);

        Some(SequenceSet(Vec1::from(Sequence::Range(start, end))))
    }
}

impl UidRange for std::ops::Range<Uid> {
    fn values(&self) -> UidValues {
        self.into()
    }

    fn is_contiguous(&self) -> bool {
        true
    }

    fn len(&self) -> usize {
        (self.end.0 - self.start.0) as usize
    }

    fn take_n(&mut self, count: usize) -> Box<dyn UidRange> {
        let count = count.min(self.len()) as u32;
        let old_end = self.end.0;
        *self = self.start..Uid(old_end - count);
        Box::new(Uid(old_end - count)..Uid(old_end))
    }

    fn to_owned(&self) -> Box<dyn UidRange> {
        Box::new(self.start..self.end)
    }

    fn contains(&self, uid: &Uid) -> bool {
        self.start <= *uid && *uid < self.end
    }

    fn to_seq(&self) -> Option<SequenceSet> {
        let start = self
            .start
            .try_into()
            .unwrap_or(SeqOrUid::Value(NonZero::<u32>::MIN));
        let end = self.end.prev().try_into().unwrap_or(SeqOrUid::Asterisk);

        Some(SequenceSet(Vec1::from(Sequence::Range(start, end))))
    }
}

impl UidRange for &[Uid] {
    fn values(&self) -> UidValues {
        UidValues::Values(Cow::Borrowed(self))
    }

    fn is_contiguous(&self) -> bool {
        false
    }

    fn len(&self) -> usize {
        self[..].len()
    }

    fn take_n(&mut self, count: usize) -> Box<dyn UidRange> {
        let new;
        (*self, new) = self.split_at(self.len().saturating_sub(count));
        Box::new(new.to_vec())
    }

    fn to_owned(&self) -> Box<dyn UidRange> {
        Box::new(Vec::from_iter(self.iter().cloned()))
    }

    fn contains(&self, uid: &Uid) -> bool {
        self[..].contains(uid)
    }

    fn to_seq(&self) -> Option<SequenceSet> {
        if self.is_empty() {
            return None;
        }

        fn to_seq_set(current_set: &[NonZeroU32]) -> Sequence {
            if current_set.len() == 1 {
                (*current_set.first().unwrap()).into()
            } else {
                Sequence::Range(
                    (*current_set.first().unwrap()).into(),
                    (*current_set.last().unwrap()).into(),
                )
            }
        }

        let mut ids = self
            .iter()
            .filter_map(|u| NonZeroU32::new(*u.as_ref()))
            .collect::<Vec<_>>();
        ids.sort();

        let mut query: Vec<Sequence> = Vec::new();
        let mut current_set: Vec<NonZeroU32> = Vec::new();

        for id in ids {
            if current_set.is_empty() {
                current_set.push(id);
            } else {
                let last = *current_set.last().unwrap();
                if last.saturating_add(1) == id {
                    current_set.push(id);
                } else {
                    query.push(to_seq_set(&current_set));
                    current_set.clear();
                    current_set.push(id);
                }
            }
        }

        if !current_set.is_empty() {
            query.push(to_seq_set(&current_set));
        }

        Vec1::try_from(query).ok().map(SequenceSet)
    }
}

impl UidRange for Vec<Uid> {
    fn values(&self) -> UidValues {
        UidValues::Values(Cow::Borrowed(self))
    }

    fn is_contiguous(&self) -> bool {
        false
    }

    fn len(&self) -> usize {
        self.len()
    }

    fn take_n(&mut self, count: usize) -> Box<dyn UidRange> {
        Box::new(self.split_off(self.len().saturating_sub(count)))
    }

    fn to_owned(&self) -> Box<dyn UidRange> {
        Box::new(self.clone())
    }

    fn contains(&self, uid: &Uid) -> bool {
        self[..].contains(uid)
    }

    fn to_seq(&self) -> Option<SequenceSet> {
        self.as_slice().to_seq()
    }
}

impl<'a> IntoIterator for &'a dyn UidRange {
    type Item = Uid;
    type IntoIter = UidRangeIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        self.values().into_iter()
    }
}

pub(crate) enum UidRangeIter<'a> {
    Range(std::ops::RangeInclusive<Uid>),
    Values(Cow<'a, [Uid]>, usize),
}

impl Iterator for UidRangeIter<'_> {
    type Item = Uid;

    fn next(&mut self) -> Option<Self::Item> {
        match self {
            UidRangeIter::Range(range) => {
                if range.start() <= range.end() {
                    let n = *range.start();
                    *range = n.next()..=*range.end();
                    Some(n)
                } else {
                    None
                }
            }
            UidRangeIter::Values(values, pos) => {
                if let Some(n) = values.get(*pos) {
                    *pos += 1;
                    Some(*n)
                } else {
                    None
                }
            }
        }
    }
}

pub(crate) struct UidRangeChunksIter<'a> {
    chunk_size: usize,
    range: Box<dyn UidRange + 'a>,
}

impl Iterator for UidRangeChunksIter<'_> {
    type Item = Box<dyn UidRange>;

    fn next(&mut self) -> Option<Self::Item> {
        Some(self.range.take_n(self.chunk_size)).filter(|range| !range.is_empty())
    }
}

#[derive(thiserror::Error, Debug)]
#[error("Message with invalid mailbox encountered")]
pub struct TryCollectUidRangeError {
    pub(crate) selected: MailboxName,
    pub(crate) found: Option<MailboxId>,
}

impl From<TryCollectUidRangeError> for crate::Error {
    fn from(value: TryCollectUidRangeError) -> Self {
        crate::ErrorKind::Connection.with_message(format!(
            "Internal Inconsistency: Wrong mailbox selected ({:?})",
            value.selected
        ))
    }
}

pub trait ToUidRange {
    fn to_uid_range(&self) -> Vec<Uid>;
}

impl ToUidRange for BTreeMap<Uid, model::ImapMessage> {
    fn to_uid_range(&self) -> Vec<Uid> {
        self.keys().cloned().collect::<Vec<_>>()
    }
}

pub(crate) trait TryCollectUidRange {
    type Type: Sized;

    fn try_collect_uid_range(
        self,
        mailbox: impl Borrow<MailboxName>,
    ) -> Result<Vec<Uid>, TryCollectUidRangeError>;
    fn try_collect_uid_map(
        self,
        mailbox: impl Borrow<MailboxName>,
    ) -> Result<BTreeMap<Uid, Self::Type>, TryCollectUidRangeError>;
}

impl<T: AsRef<crate::model::Message>, U: Iterator<Item = T>> TryCollectUidRange for U {
    type Type = crate::model::ImapMessage;

    fn try_collect_uid_range(
        self,
        mailbox: impl Borrow<MailboxName>,
    ) -> Result<Vec<Uid>, TryCollectUidRangeError> {
        let mailbox = mailbox.borrow();

        self.map(|msg| {
            let msg = msg.as_ref().downcast_ref::<ImapMessage>().ok_or_else(|| {
                TryCollectUidRangeError {
                    selected: mailbox.to_owned(),
                    found: msg.as_ref().mailbox().map(|m| m.id()),
                }
            })?;

            if msg
                .mailbox()
                .and_downcast_ref::<ImapMailbox>()
                .is_some_and(|mb| mb.imap_name() == *mailbox)
            {
                Ok(msg.imap_uid())
            } else {
                Err(TryCollectUidRangeError {
                    selected: mailbox.to_owned(),
                    found: msg.mailbox().map(|m| m.id()),
                })
            }
        })
        .collect()
    }

    fn try_collect_uid_map(
        self,
        mailbox: impl Borrow<MailboxName>,
    ) -> Result<BTreeMap<Uid, Self::Type>, TryCollectUidRangeError> {
        let mailbox = mailbox.borrow();

        self.map(|msg| {
            let msg = msg.as_ref().downcast_ref::<ImapMessage>().ok_or_else(|| {
                TryCollectUidRangeError {
                    selected: mailbox.to_owned(),
                    found: msg.as_ref().mailbox().map(|m| m.id()),
                }
            })?;

            if msg
                .mailbox()
                .and_downcast_ref::<ImapMailbox>()
                .is_some_and(|mb| mb.imap_name() == *mailbox)
            {
                Ok((msg.imap_uid(), msg.clone()))
            } else {
                Err(TryCollectUidRangeError {
                    selected: mailbox.to_owned(),
                    found: msg.mailbox().map(|m| m.id()),
                })
            }
        })
        .collect()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn range_inclusive() {
        let mut range = Uid::new(5)..=Uid::new(10);
        assert_eq!(range.len(), 6);
        assert_eq!(range.values().first(), Uid::new(5));
        assert_eq!(range.values().last(), Uid::new(10));

        let split = range.take_n(2);
        assert_eq!(range.len(), 4);
        assert_eq!(range.values().first(), Uid::new(5));
        assert_eq!(range.values().last(), Uid::new(8));

        assert_eq!(split.len(), 2);
        assert_eq!(split.values().first(), Uid::new(9));
        assert_eq!(split.values().last(), Uid::new(10));

        let split2 = range.take_n(4);
        assert!(range.is_empty());
        assert_eq!(range.len(), 0);
        assert_eq!(split2.len(), 4);
        assert_eq!(split2.values().first(), Uid::new(5));
        assert_eq!(split2.values().last(), Uid::new(8));

        let split3 = range.take_n(1);
        assert!(range.is_empty());
        assert_eq!(split3.len(), 0);
        assert!(split3.is_empty());
    }

    #[test]
    fn range_vec() {
        let mut range = vec![Uid::new(1), Uid::new(2), Uid::new(3), Uid::new(9)];
        assert_eq!(range.len(), 4);
        assert_eq!(range.values().first(), Uid::new(1));
        assert_eq!(range.values().last(), Uid::new(9));

        let split = range.take_n(2);
        assert_eq!(range.len(), 2);
        assert_eq!(range.values().first(), Uid::new(1));
        assert_eq!(range.values().last(), Uid::new(2));

        assert_eq!(split.len(), 2);
        assert_eq!(split.values().first(), Uid::new(3));
        assert_eq!(split.values().last(), Uid::new(9));

        let split2 = range.take_n(2);
        assert!(range.is_empty());
        assert_eq!(split2.len(), 2);
        assert_eq!(split2.values().first(), Uid::new(1));
        assert_eq!(split2.values().last(), Uid::new(2));
    }

    #[test]
    fn range_slice() {
        let array = [Uid::new(1), Uid::new(2), Uid::new(3), Uid::new(9)];
        let mut range = array.as_slice();
        assert_eq!(range.len(), 4);
        assert_eq!(range.values().first(), Uid::new(1));
        assert_eq!(range.values().last(), Uid::new(9));

        let split = range.take_n(2);
        assert_eq!(range.len(), 2);
        assert_eq!(range.values().first(), Uid::new(1));
        assert_eq!(range.values().last(), Uid::new(2));

        assert_eq!(split.len(), 2);
        assert_eq!(split.values().first(), Uid::new(3));
        assert_eq!(split.values().last(), Uid::new(9));

        let split2 = range.take_n(2);
        assert!(range.is_empty());
        assert_eq!(split2.len(), 2);
        assert_eq!(split2.values().first(), Uid::new(1));
        assert_eq!(split2.values().last(), Uid::new(2));
    }

    #[test]
    fn range_query() {
        let mut range = Uid::new(5)..Uid::new(10);
        assert_eq!(range.len(), 5);
        assert_eq!(range.values().first(), Uid::new(5));
        assert_eq!(range.values().last(), Uid::new(9));

        let split = range.take_n(2);
        assert_eq!(range.len(), 3);
        assert_eq!(range.values().first(), Uid::new(5));
        assert_eq!(range.values().last(), Uid::new(7));

        assert_eq!(split.len(), 2);
        assert_eq!(split.values().first(), Uid::new(8));
        assert_eq!(split.values().last(), Uid::new(9));

        let split2 = range.take_n(3);
        assert!(range.is_empty());
        assert_eq!(range.len(), 0);
        assert_eq!(split2.len(), 3);
        assert_eq!(split2.values().first(), Uid::new(5));
        assert_eq!(split2.values().last(), Uid::new(7));

        let split3 = range.take_n(1);
        assert!(range.is_empty());
        assert_eq!(split3.len(), 0);
        assert!(split3.is_empty());
    }

    #[test]
    fn to_seq() {
        let range = Uid::new(5)..Uid::new(10);
        let seq = range.to_seq().unwrap();
        assert_eq!(
            seq.0.as_ref(),
            &[Sequence::Range(
                5.try_into().unwrap(),
                9.try_into().unwrap()
            )]
        );

        let range = Uid::new(0)..=Uid::new(100);
        let seq = range.to_seq().unwrap();
        assert_eq!(
            seq.0.as_ref(),
            &[Sequence::Range(
                1.try_into().unwrap(),
                100.try_into().unwrap()
            )]
        );

        let range = vec![
            Uid::FIRST,
            Uid::new(3),
            Uid::new(5),
            Uid::new(6),
            Uid::new(10),
            Uid::new(2),
            Uid::new(8),
        ];

        let seq = range.to_seq().unwrap();
        assert_eq!(
            seq.0.as_ref(),
            &[
                Sequence::Range(1.try_into().unwrap(), 3.try_into().unwrap()),
                Sequence::Range(5.try_into().unwrap(), 6.try_into().unwrap()),
                Sequence::Single(8.try_into().unwrap()),
                Sequence::Single(10.try_into().unwrap())
            ]
        );

        let range = Uid::new(5)..=Uid::MAX;
        let seq = range.to_seq().unwrap();
        assert_eq!(
            seq.0.as_ref(),
            &[Sequence::Range(5.try_into().unwrap(), SeqOrUid::Asterisk)]
        );

        let range = Uid::NULL..=Uid::MAX;
        let seq = range.to_seq().unwrap();
        assert_eq!(
            seq.0.as_ref(),
            &[Sequence::Range(1.try_into().unwrap(), SeqOrUid::Asterisk)]
        );
    }
}
