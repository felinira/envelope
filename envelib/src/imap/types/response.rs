// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use imap_client::imap_types::body::BodyStructure;

use super::*;
use crate::types::*;

#[expect(dead_code)]
#[derive(Debug, Clone)]
pub(crate) enum FetchData {
    Flags(MessageFlags),

    /// Will cause a full re-sync of the message
    Unknown,
}

#[allow(dead_code)]
#[derive(Debug, Clone)]
pub(crate) enum MailboxAttribute {
    HighestModSeq(u64),
    UidValidity(UidValidity),
    MailboxFlagsChanged,
    Messages(u32),
    Recent(u32),
    Exists(u32),
    Unseen(u32),

    UidNext(Uid),
    Fetch {
        seqnum: SeqNum,
        uid: Option<Uid>,
        modseq: Option<u64>,
        fetch: FetchData,
    },
    Expunge {
        seqnum: SeqNum,
        uid: Option<Uid>,
    },
    Other,
}

#[derive(Debug, Clone)]
pub(crate) struct UnsolicitedResponse {
    pub mailbox_name: MailboxName,
    pub attributes: Vec<MailboxAttribute>,
    pub time: std::time::Instant,
}

#[expect(dead_code)]
#[derive(Debug, Clone, sqlx::FromRow)]
pub(crate) struct MailboxInfo {
    pub name: MailboxName,
    // TODO: Use char
    pub delimiter: Option<String>,
    pub kind: MailboxKind,
    #[sqlx(default)]
    pub selectable: bool,
    #[sqlx(default)]
    pub no_inferiors: bool,
    #[sqlx(default)]
    pub marked: Option<bool>,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub(crate) enum SelectionType {
    Select,
    Examine,
}

#[derive(Clone, Debug)]
pub(crate) struct SelectKind {
    pub mailbox_name: MailboxName,
    pub selection_type: SelectionType,
}

impl SelectKind {
    pub fn select(mailbox_name: MailboxName) -> Self {
        Self {
            mailbox_name,
            selection_type: SelectionType::Select,
        }
    }

    #[allow(dead_code)]
    pub fn examine(mailbox_name: MailboxName) -> Self {
        Self {
            mailbox_name,
            selection_type: SelectionType::Examine,
        }
    }
}

#[derive(Clone, Debug)]
pub(crate) struct MailboxSelection {
    /// The kind of selection
    pub selection_type: SelectionType,

    /// The mailbox name
    pub mailbox_name: MailboxName,

    /// The current uid validity for this mailbox
    pub uidvalidity: UidValidity,

    /// The next uid that's to be expected in this mailbox
    pub uidnext: Uid,

    /// The amount of messages
    pub message_count: u32,

    /// The amount of recent messages
    pub recent_count: u32,
}

#[derive(Debug, Clone)]
pub(crate) struct MailboxStatus {
    /// The current uid validity for this mailbox
    pub uidvalidity: UidValidity,

    /// The next uid that's to be expected in this mailbox
    pub uidnext: Uid,

    /// The amount of messages
    pub message_count: u32,

    /// Recent messages count
    pub recent_count: u32,

    /// Unread messages count
    pub unseen_count: Option<u32>,
}

impl From<&MailboxSelection> for MailboxStatus {
    fn from(value: &MailboxSelection) -> Self {
        Self {
            uidvalidity: value.uidvalidity,
            uidnext: value.uidnext,
            message_count: value.message_count,
            recent_count: value.recent_count,
            unseen_count: None,
        }
    }
}

#[expect(dead_code)]
#[derive(Debug)]
pub(crate) enum IdleResponse {
    ManualInterrupt,
    Timeout,

    Response(UnsolicitedResponse),
}

#[derive(Clone, Debug)]
pub(crate) struct MessageData {
    pub uid: Uid,
    pub thread_id: Option<ThreadId>,
    pub internal_date: Option<time::OffsetDateTime>,
    pub flags: Option<MessageFlags>,
    pub attachment_count: Option<usize>,
    pub bodystructure: Option<BodyStructure<'static>>,
    pub message: Option<InternetMessage>,
}

impl PartialEq for MessageData {
    fn eq(&self, other: &Self) -> bool {
        self.uid == other.uid
    }
}

impl Eq for MessageData {
    fn assert_receiver_is_total_eq(&self) {}
}

impl Ord for MessageData {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.internal_date.cmp(&other.internal_date)
    }
}

impl PartialOrd for MessageData {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        Some(self.cmp(other))
    }
}

impl From<&MessageData> for MessageUpdate {
    fn from(value: &MessageData) -> Self {
        if let Some(flags) = value.flags.as_ref() {
            Self {
                new_messages: Vec::new(),
                message_count: 1,
                unseen_count: if flags.is_unseen() { 1 } else { 0 },
                recent_count: if flags.is_recent() { 1 } else { 0 },
            }
        } else {
            Self {
                new_messages: Vec::new(),
                message_count: 1,
                unseen_count: 0,
                recent_count: 0,
            }
        }
    }
}
