// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::str::FromStr;

use enumset::{EnumSet, EnumSetType, enum_set};
use serde::{Deserialize, Serialize};
use zeroize::Zeroizing;

/// An authentication method 'class'
///
/// Used to determine the SASL authentication methods that should be tried to authenticate to a server.
///
/// See [IANA SASL Mechanisms](https://www.iana.org/assignments/sasl-mechanisms/sasl-mechanisms.xhtml)
///
/// Derived from either:
/// - Parsing an autoconfig.xml file
/// - Manual user input
/// - Successful past connections
#[derive(Debug, Serialize, Deserialize, EnumSetType)]
#[enumset(repr = "u32")]
#[serde(rename_all = "snake_case")]
pub enum AuthMethod {
    /// SASL PLAIN and LOGIN, IMAP LOGIN fallback
    PasswordCleartext,
    /// SASL CRAM-MD5, SCRAM-*
    PasswordEncrypted,
    /// SASL OAUTHBEARER or XOAUTH2
    OAuth2,
}

impl AuthMethod {
    pub const PASSWORD: EnumSet<AuthMethod> =
        enum_set!(Self::PasswordCleartext | Self::PasswordEncrypted);
}

#[derive(Debug, PartialEq, glib::ValueDelegate)]
#[value_delegate(from = u32)]
pub struct AuthMethods(EnumSet<AuthMethod>);

impl From<u32> for AuthMethods {
    fn from(value: u32) -> Self {
        Self(EnumSet::from_repr(value))
    }
}

impl From<EnumSet<AuthMethod>> for AuthMethods {
    fn from(value: EnumSet<AuthMethod>) -> Self {
        Self(value)
    }
}

impl From<AuthMethods> for u32 {
    fn from(value: AuthMethods) -> Self {
        value.0.as_repr()
    }
}

impl From<&AuthMethods> for u32 {
    fn from(value: &AuthMethods) -> Self {
        value.0.as_repr()
    }
}

impl From<AuthMethods> for EnumSet<AuthMethod> {
    fn from(value: AuthMethods) -> Self {
        value.0
    }
}

#[derive(thiserror::Error, Debug)]
pub struct UnsupportedAuthenticationMethod(String);

impl std::fmt::Display for UnsupportedAuthenticationMethod {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "UnsupportedAuthenticationMethod({})", self.0)
    }
}

impl FromStr for AuthMethod {
    type Err = UnsupportedAuthenticationMethod;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "LOGIN" | "PLAIN" => Ok(AuthMethod::PasswordCleartext),
            "CRAM-MD5" => Ok(AuthMethod::PasswordEncrypted),
            "SCRAM-SHA-1" | "SCRAM-SHA-1-PLUS" | "SCRAM-SHA-256" | "SCRAM-SHA-256-PLUS" => {
                Ok(AuthMethod::PasswordEncrypted)
            }
            "XOAUTH2" | "OAUTHBEARER" => Ok(AuthMethod::OAuth2),
            unsupported => Err(UnsupportedAuthenticationMethod(unsupported.to_string())),
        }
    }
}

#[derive(Clone, Default, Debug, Serialize, Deserialize, PartialEq, Eq, glib::Boxed)]
#[boxed_type(name = "EnvelopeAccountSecret", nullable)]
#[serde(rename_all = "snake_case")]
#[non_exhaustive]
pub enum Secret {
    Basic(Basic),
    #[default]
    None,
}

impl Secret {
    pub fn new_basic(user: String, password: Zeroizing<String>) -> Self {
        Self::Basic(Basic { user, password })
    }
}

#[derive(Debug, thiserror::Error)]
#[non_exhaustive]
#[error("Unsupported credentials for this service")]
pub struct UnsupportedCredentialsError();

impl TryFrom<Secret> for lettre::transport::smtp::authentication::Credentials {
    type Error = UnsupportedCredentialsError;

    fn try_from(value: Secret) -> Result<Self, Self::Error> {
        let (user, pass) = match value {
            Secret::Basic(creds) => (creds.user, creds.password),
            Secret::None => return Err(UnsupportedCredentialsError()),
        };
        Ok(lettre::transport::smtp::authentication::Credentials::new(
            user,
            pass.to_string(),
        ))
    }
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub struct Basic {
    pub user: String,
    pub password: Zeroizing<String>,
}

impl std::fmt::Debug for Basic {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Basic")
            .field("user", &self.user)
            .field("password", &"***")
            .finish()
    }
}

#[derive(Clone, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub struct OAuth2 {
    pub user: String,
    pub token: Zeroizing<String>,
}

impl std::fmt::Debug for OAuth2 {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("OAuth2")
            .field("user", &self.user)
            .field("token", &"***")
            .finish()
    }
}
