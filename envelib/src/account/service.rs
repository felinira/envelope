// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::fmt::Display;

use serde::{Deserialize, Serialize};

use super::credentials::AuthMethod;

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(transparent)]
pub struct ServiceId(uuid::Uuid);

impl ServiceId {
    pub fn new() -> Self {
        Self(uuid::Uuid::new_v4())
    }
}

impl Default for ServiceId {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for ServiceId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[serde(rename_all = "snake_case")]
pub enum ServiceType {
    Imap,
    Smtp,
}

impl std::fmt::Display for ServiceType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                ServiceType::Imap => "IMAP",
                ServiceType::Smtp => "SMTP",
            }
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub struct Service {
    pub service_id: ServiceId,
    pub server: String,
    pub port: u16,

    pub connection_type: super::TlsStrategy,
    #[serde(default)]
    pub auth_method: Option<AuthMethod>,
    #[serde(skip)]
    pub secret: super::credentials::Secret,
}

impl Service {
    pub fn new_basic(
        service_type: ServiceType,
        server: url::Host,
        tls_strategy: super::TlsStrategy,
        secret: super::credentials::Secret,
    ) -> Self {
        let port = match (service_type, &tls_strategy) {
            (ServiceType::Imap, super::TlsStrategy::StartTls) => 143,
            (ServiceType::Imap, super::TlsStrategy::Tls) => 993,
            (ServiceType::Smtp, super::TlsStrategy::StartTls) => 587,
            (ServiceType::Smtp, super::TlsStrategy::Tls) => 465,
        };

        Self {
            service_id: Default::default(),
            server: server.to_string(),
            port,
            connection_type: tls_strategy,
            auth_method: None,
            secret,
        }
    }

    /// Check whether the account is a development test server. Disabled in release builds.
    #[cfg(not(feature = "devel"))]
    pub fn is_test_server(&self) -> bool {
        false
    }

    /// Check whether the account is a development test server. Enabled in debug builds.
    #[cfg(feature = "devel")]
    pub fn is_test_server(&self) -> bool {
        // TODO this just uses an incomplete heuristic, maybe we should store this in the config instead?
        self.server == "localhost"
            && crate::devel::TEST_SERVER_PORTS
                .iter()
                .any(|p| *p == self.port)
    }
}
