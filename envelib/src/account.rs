// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod credentials;
mod service;

use crate::gettext::*;
use std::fmt::Display;
use std::{collections::HashMap, str::FromStr};

use serde::{Deserialize, Serialize};

pub use credentials::{AuthMethod, Secret};
pub use service::{Service, ServiceId, ServiceType};

use thiserror::Error as ThisError;

#[derive(Debug, ThisError)]
pub enum AccountConfigError {
    #[error("Read error: {0}")]
    Serde(#[from] serde_json::Error),
    #[error("Service not found: {0}")]
    ServiceNotFound(String),
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq)]
#[serde(rename_all = "snake_case")]
pub enum TlsStrategy {
    StartTls,
    Tls,
}

impl TlsStrategy {
    pub fn is_starttls(&self) -> bool {
        *self == TlsStrategy::StartTls
    }

    pub fn is_tls(&self) -> bool {
        *self == TlsStrategy::Tls
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub struct EmailAddress {
    local_part: String,
    domain: String,
}

impl EmailAddress {
    pub fn local_part(&self) -> &str {
        &self.local_part
    }
}

impl std::fmt::Display for EmailAddress {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}@{}", self.local_part, self.domain)
    }
}

impl FromStr for EmailAddress {
    type Err = EmailValidationError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let Some((local_part, domain)) = s.split_once('@') else {
            return Err(EmailValidationError::MissingAt);
        };

        for c in local_part.chars() {
            if !c.is_ascii_graphic() {
                return Err(EmailValidationError::InvalidLocalPart(gettextf(
                    "The character “{0}” is not allowed in an email address local part",
                    &[&c],
                )));
            }
        }

        let domain = url::Host::parse(domain)
            .map_err(|err| EmailValidationError::InvalidDomain(err.to_string()))?;

        Ok(Self {
            local_part: local_part.to_string(),
            domain: domain.to_string(),
        })
    }
}

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum EmailValidationError {
    MissingAt,
    InvalidLocalPart(String),
    InvalidDomain(String),
}

impl std::fmt::Display for EmailValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                EmailValidationError::MissingAt => gettext("Missing “@” character."),
                EmailValidationError::InvalidLocalPart(msg) => msg.to_string(),
                EmailValidationError::InvalidDomain(msg) => msg.to_string(),
            }
        )
    }
}

impl std::error::Error for EmailValidationError {}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub struct EmailName(String);

impl EmailName {
    pub fn into_string(self) -> String {
        self.0
    }
}

impl std::ops::Deref for EmailName {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl std::fmt::Display for EmailName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl From<String> for EmailName {
    fn from(value: String) -> Self {
        Self(value)
    }
}

impl FromStr for EmailName {
    type Err = EmailNameValidationError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if !s.is_empty() {
            Ok(Self(s.to_string()))
        } else {
            Err(EmailNameValidationError::Empty)
        }
    }
}

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum EmailNameValidationError {
    Empty,
}

impl std::fmt::Display for EmailNameValidationError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", gettext("Name must not be empty"))
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash)]
#[serde(rename_all = "snake_case")]
pub struct Alias {
    pub address: EmailAddress,
    pub name: Option<EmailName>,
}

impl Alias {
    pub fn new(address: EmailAddress, name: Option<EmailName>) -> Self {
        Self { address, name }
    }
}

impl std::fmt::Display for Alias {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(name) = &self.name {
            write!(f, "{} <{}>", name, self.address)
        } else {
            write!(f, "{}", self.address)
        }
    }
}

impl From<Alias> for crate::types::Address {
    fn from(value: Alias) -> Self {
        Self::new(
            value.name.map(|n| n.into_string()),
            value.address.to_string(),
        )
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, Hash, glib::Boxed)]
#[boxed_type(name = "EnvelopeAccountId", nullable)]
#[serde(transparent)]
pub struct AccountId(uuid::Uuid);

impl AccountId {
    pub fn new() -> Self {
        Self(uuid::Uuid::new_v4())
    }
}

impl Default for AccountId {
    fn default() -> Self {
        Self::new()
    }
}

impl Display for AccountId {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

#[derive(Clone, Debug, Serialize, Deserialize, PartialEq, Eq, glib::Boxed)]
#[boxed_type(name = "EnvelopeAccountConfig", nullable)]
#[serde(rename_all = "snake_case")]
pub struct Account {
    pub services: HashMap<ServiceType, Service>,
    pub identity: Alias,
    pub aliases: Vec<Alias>,
    pub id: crate::account::AccountId,
}

impl Account {
    pub fn new(
        identity: Alias,
        services: impl IntoIterator<Item = (ServiceType, Service)>,
    ) -> Self {
        Self {
            services: services.into_iter().collect(),
            identity,
            aliases: vec![],
            id: AccountId::new(),
        }
    }

    pub fn own_addrs(&self) -> Vec<Alias> {
        let mut aliases: Vec<_> = self.aliases.clone();
        aliases.push(self.identity.clone());
        aliases
    }

    pub fn identity(&self) -> &Alias {
        &self.identity
    }

    pub fn aliases(&self) -> impl Iterator<Item = &Alias> {
        std::iter::once(&self.identity).chain(&self.aliases)
    }

    pub fn service(&self, service_id: &ServiceId) -> Option<(&ServiceType, &Service)> {
        self.services
            .iter()
            .find(|(_, s)| s.service_id == *service_id)
    }

    pub fn service_mut(&mut self, service_id: &ServiceId) -> Option<(&ServiceType, &mut Service)> {
        self.services
            .iter_mut()
            .find(|(_, s)| s.service_id == *service_id)
    }

    pub fn service_of(&self, service_kind: ServiceType) -> Option<&Service> {
        self.services.get(&service_kind)
    }

    pub fn service_of_mut(&mut self, service_kind: ServiceType) -> Option<&mut Service> {
        self.services.get_mut(&service_kind)
    }

    pub fn imap(&self) -> Result<&Service, AccountConfigError> {
        self.services
            .get(&ServiceType::Imap)
            .ok_or_else(|| AccountConfigError::ServiceNotFound("IMAP".to_string()))
    }

    pub fn imap_mut(&mut self) -> Result<&mut Service, AccountConfigError> {
        self.services
            .get_mut(&ServiceType::Imap)
            .ok_or_else(|| AccountConfigError::ServiceNotFound("IMAP".to_string()))
    }

    pub fn smtp(&self) -> Result<&Service, AccountConfigError> {
        self.services
            .get(&ServiceType::Smtp)
            .ok_or_else(|| AccountConfigError::ServiceNotFound("SMTP".to_string()))
    }

    pub fn new_test_server() -> Self {
        let mut services = HashMap::new();
        services.insert(
            ServiceType::Imap,
            Service {
                service_id: Default::default(),
                server: "localhost".to_string(),
                port: 3993,
                connection_type: TlsStrategy::Tls,
                auth_method: Some(AuthMethod::PasswordEncrypted),
                secret: Secret::new_basic(String::new(), zeroize::Zeroizing::new(String::new())),
            },
        );

        let identity = Alias::new(
            "testaccount@envelope.drey.app".parse().unwrap(),
            Some("Test User".parse().unwrap()),
        );

        let aliases = vec![];
        let id = AccountId::new();

        Self {
            services,
            identity,
            aliases,
            id,
        }
    }

    pub fn is_test_server(&self) -> bool {
        self.services.values().all(|s| s.is_test_server())
    }
}
