// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

mod attachment;
mod datetime;
mod index_store;
mod mailbox;
mod message;
mod task;
mod thread;

pub use attachment::Attachment;
pub(crate) use datetime::{ToGlibDate, ToOffsetDateTime};
pub use index_store::IndexStoreExt;
pub(crate) use mailbox::ImapMailbox;
pub use mailbox::{Mailbox, MailboxExt, MailboxExtProps, MailboxStore, MailboxTarget};
pub(crate) use message::ImapMessage;
pub use message::{
    Message, MessageDetailLevel, MessageExt, MessageExtProps, MessagePart, MessageStore,
};
pub use task::{Task, TaskStatus};
pub use thread::{Thread, ThreadStore};

use glib::types::StaticTypeExt as _;

pub(crate) fn ensure_types() {
    Attachment::ensure_type();

    Mailbox::ensure_type();
    ImapMailbox::ensure_type();
    MailboxStore::ensure_type();

    Message::ensure_type();
    MessageDetailLevel::ensure_type();
    ImapMessage::ensure_type();
    MessageStore::ensure_type();

    Task::ensure_type();
    TaskStatus::ensure_type();

    Thread::ensure_type();
    ThreadStore::ensure_type();
}
