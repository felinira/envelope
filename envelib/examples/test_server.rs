// SPDX-FileCopyrightText: The Envelope Developers
//
// SPDX-License-Identifier: MPL-2.0

use std::process::exit;

use envelib::devel::test_server;

#[tokio::main]
async fn main() {
    let mut args = std::env::args();
    if let Some(first) = args.nth(1) {
        if first == "--stop" {
            test_server::stop().await.unwrap().unwrap();
            println!("Stopped");
            exit(0);
        }
    }

    tokio::select! {
        res = test_server::start(true, true) => res,
        _ = tokio::signal::ctrl_c() => {
            test_server::stop().await.unwrap().unwrap();
            exit(0);
        }
    }
    .unwrap()
    .unwrap();

    let main_task = test_server::send_emails();
    let abort_handle = main_task.abort_handle();

    let stdout_task = test_server::attach_stdout();
    let stdout_abort_handle = stdout_task.abort_handle();

    let abort_task = tokio::spawn(async move {
        tokio::signal::ctrl_c().await.unwrap();

        abort_handle.abort();
        stdout_abort_handle.abort();

        test_server::stop().await.unwrap().unwrap();
        println!("Stopped");
        exit(0);
    });

    let _ = main_task.await;
    abort_task.await.unwrap();
}
